-----------------------------------------------------------
           file: VERSION_OF_WINDOW.txt
-----------------------------------------------------------

             recorded informations on

V E R S I O N S   O F   W I N D O W   F O R   C E R V I C A D

-----------------------------------------------------------
VERSION_OF_WINDOW is a defined macro that have a numeric value.
Its numeric value can be compared for conditional compiling.
Referenced version are listed below from the current to 
  the first.

-----------------------------------------------------------
              VERSION_OF_WINDOW==2
-----------------------------------------------------------
 * The get_value return the internal value.
 * new resource type: TEXT_TABLE.
 * The items of WIN_LIST are managed as TEXT_TABLE.
 * Attribut XmNdeleteResponse added in the WIN_SHELL class.

-----------------------------------------------------------
              VERSION_OF_WINDOW==1
-----------------------------------------------------------
 * first version.
 * no form window
 * the get_value return a copy

-----------------------------------------------------------
