//-------------------------------------------------------------------
// WIN_LIST.hxx
// ---------------
// implementation of Motif class "XmList" 
// J.E.Bollo & Maryl�ne, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __win_list_hxx
#define __win_list_hxx
//-------------------------------------------------------------------
#include "macros.h"
#include "WIN_PRIMITIVE.hxx"
//-------------------------------------------------------------------
class WIN_LIST : public WIN_PRIMITIVE
{
	DECLARE_RESOURCE_CLIENT(WIN_LIST)
		// use the resource mechanism

	DECLARE_WINDOW(WIN_LIST)
		// use window class mechanism

public:

	int item_count;
		// number of items in the list

	int list_size_policy;
		// The method for resizing the widget when an item exceeds
		// the width of the work area

	char ** items;
		// items of the list
/*
	int selected_item_count;
		// number selected items in the list

	char ** selected_items;
		// text of selected items in the list
*/
	int selection_policy;
		// determines the effect of a selection action

	int top_item_position;
		// the position of the first item that will be visible in the list

	int visible_item_count;
		// number of items to display in the work area

	bool scroll_horizontal;
		// adds an horizontal scrollbar if True

	bool scroll_vertical;
		// adds a vertical scrollbar if True

	bool  scroll_left_side;
		// if True, the vertical scrollbar is placed to the left

	bool  scroll_top_side;
		// if True, the horizontal scrollbar is placed above

	int prefered_width;
		// the width of the widthest item of the list; useful for the 
		// attribute list_size_policy

protected:

	~WIN_LIST();
		// destruction

public:

	WIN_LIST();
		// construction

	virtual LRESULT callback(UINT uMsg,WPARAM wParam,LPARAM lParam);
		// the "this" WINDOW callback

	virtual LONG get_windows_style() const;
		// retrieve the windows style used to create the widget 

	virtual bool create_control();
		// function called to create the embedded control
		// return true if creation process should continue, false otherwise

	bool is_multiple_sel() const;
		// returns true when the selection policy stands for multiple selection.

	int * get_selected_pos() const;
		// returns the list of selected items positions

	void select_pos(int position, int notify);
		// selects the item which position is "position"
		// if position is 0, selects the last item
		// triggers off the associated call-back if notify = 1

	void delete_pos(int position);
		// deletes the item which position is "position"
		// if 0, deletes the last item

	int pos_of_item(const char* txt) const;
		// returns the position of the item "txt"
		// or 0 if txt is not an item of the list

	char* item_of_pos(int position) const;
		// returns the text of the item which position is "position"
		// if position=0, returns the last item

	int add_item_pos(int position, const char *txt);
		// adds the item txt in the list at the position "position"
		// if position = 0, adds it at the end of the list
		// return 0 when ok and 1 otherwise

	int append_item(const char *txt)
		// adds the item txt in the list at the end
		// return 0 when ok and 1 otherwise
		{ return add_item_pos(0,txt); }

	int replace_item_pos(int position, const char *txt);
		// replaces the item of position "position" by the item "txt"

	int get_selected_item_count() const;
		// returns the number of selected items

	char* get_ith_selected_item(int position) const;
		// returns the text of the selected item at the position "position" 
		//(in the list of selected items)

	void delete_all_items();
		// deletes all items in a list

	virtual char * get_text() const
		// gets all the text
		{ return get_ith_selected_item(1);}	

	void set_items(char const * const * items);
		// set all items of of list

	char** get_items() const;
		// get all items in the list

	void set_selection_policy(int value);
		// set the selection policy of a list
		// (single, multiple or extended)

	void set_visible_item_count (int value);
		// adjust the height of the list to
		// show the given number of items

	int get_top_item_position () const;
		// returns the index (based 1) of the item 
		//which is at the top of the list

	void set_top_item_position (int top_index);
		// puts the item which index (based 1) is "top_index" 
		// at the top of the list


	void set_size_policy (int value)
		// Determine if the List must resize or no
		// when  number and length of items changes
		{list_size_policy = value;}

	void get_prefered_size(SIZE &result);
		// computes the prefered size of this (from rows & columns) in result
		// a zero in returned cx or cy stands for no prefered size

	virtual XtGeometryResult query_geometry (XtWidgetGeometry *request, 
										 XtWidgetGeometry *prefered);
		// The method query_geometry called when the parent  
		// wants to know the widget's prefered size

	int width_of_item(const char* txt_item);
		// returns the width of an item


	// --------------- Attributs management methods -------------------

	virtual int internal_get_values
		// function used internally to get attribut values 
		// should not be called directly (call get_values)
		// arglist is the array of the values to get
		// count is the count of values to be put into arglist
		// got is an array that flag if an attribut have been put
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs put
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		) const;

	virtual int internal_set_values
		// function used internally to set attribut values 
		// should not be called directly (call set_values)
		// arglist is the array of the values to be set,
		// count is the count of values in arglist
		// got is an array that flag if an attribut have been set
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs set
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		);

};
//-------------------------------------------------------------------
#endif // VERSION_OF_WINDOW != 1
//-------------------------------------------------------------------
#endif // __win_list_hxx
