//---------------------------------------------------------
// SIMPLE_WIN_READER.hxx
// -----------------
// Just a simple reader
// J.E.Bollo, M3G (c) 1998
//---------------------------------------------------------
#ifndef __simple_win_reader_hxx
#define __simple_win_reader_hxx
//---------------------------------------------------------
#include "macros.h"
#include "SIMPLE_READER.hxx"
//---------------------------------------------------------
class SIMPLE_WIN_READER : public SIMPLE_READER
	// implement a simple reader that use the WINDOWS files
{
private:

	HANDLE	hfile;
		// handle to file

public:

	SIMPLE_WIN_READER();
		// construction

	~SIMPLE_WIN_READER();
		// destruction

	virtual BOOL open_file(LPCSTR nom_fichier);
			// open the file nom_fichier
			// return TRUE if ok, FALSE otherwise

	virtual int read(void *where,int len);
		// read len byte at memory place where
		// return number of bytes read, 0 at EOF, -1 in case of error

	virtual void close();
		// close the opened file

	virtual BOOL is_opened();
		// checks if opened, returns TRUE when opened
};
//---------------------------------------------------------
#endif
