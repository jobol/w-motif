//-------------------------------------------------------------------
// WIDGET_TREE.hxx
// -------------------
// this class performs operations upon widgets trees
// G. MUSSET, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __widget_tree_hxx
#define __widget_tree_hxx
//-------------------------------------------------------------------
#include "macros.h"
//-------------------------------------------------------------------
class WIN_COMPOSITE;
//-------------------------------------------------------------------

class WIDGET_TREE
{
private:

	int array_size;
		// size of window_array

	WIN_COMPOSITE ** wdg_array;
		// widgets array

	int idx;
		// current index in array

	int free_cells;
		// number of free cells in array

	int alloc_increment;
		// number of cells in array allocated simultaneously

	void add_composite_children(WIN_COMPOSITE * wdg);
		// adds Composite children of wdg in array

public:

	WIDGET_TREE();
		// constructor

	~WIDGET_TREE();
		// destructor

	WIN_COMPOSITE ** get_composite_list(WIN_COMPOSITE * wdg);
		// returns the list of Composite widget in the tree
		// under wdg ordered by depth

	int get_composite_number() const
		// returns the number of composite children in wdg_array;
		// must be called after get_composite_list
		{ return array_size; }

};
//-------------------------------------------------------------------
#endif
