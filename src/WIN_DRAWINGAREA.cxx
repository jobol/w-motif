//-------------------------------------------------------------------
// WIN_DRAWINGAREA.cxx
// -------------------
// implements a class which defines the Motif class "XmDrawingArea"
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#include "WIN_DRAWINGAREA.hxx"
#include "win_naming.h"
#include "WINDOWS_MANAGER.hxx"
#include "WINDOWS_COMMONS.h"
//-------------------------------------------------------------------
const int border_width  = 0;
const int border_height = 0;
//-------------------------------------------------------------------
BEGIN_RES_DESC(WIN_DRAWINGAREA,WIN_BULLETIN_BOARD,NAME_OF_WIN_DRAWINGAREA_CLASS)
END_RES_DESC
//-------------------------------------------------------------------
IMPLEMENT_WINDOW(WIN_DRAWINGAREA)
//-------------------------------------------------------------------
ATOM WIN_DRAWINGAREA::register_window_class() const
{
	return WINDOWS_MANAGER::register_window_class(
					(const char*)get_class_name(),CS_VREDRAW|CS_HREDRAW,
					LoadCursor(NULL,IDC_ARROW));
}
//-------------------------------------------------------------------
WIN_DRAWINGAREA::WIN_DRAWINGAREA()
	: WIN_BULLETIN_BOARD()
{
}
//-------------------------------------------------------------------
WIN_DRAWINGAREA::~WIN_DRAWINGAREA()
{
}

//-------------------------------------------------------------------

