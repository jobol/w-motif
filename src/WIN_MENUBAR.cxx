//---------------------------------------------------------
// WIN_MENUBAR.cxx
// ---------------
// implements a class that defines the menu bars
// J.E.Bollo, M3G (c) 1998
//---------------------------------------------------------
#include "WIN_MENUBAR.hxx"
#include "WIN_SHELL.hxx"
#include "win_naming.h"
//---------------------------------------------------------
BEGIN_RES_DESC(WIN_MENUBAR,WIN_MENU,NAME_OF_WIN_MENUBAR_CLASS)
END_RES_DESC
//---------------------------------------------------------
WIN_MENUBAR::WIN_MENUBAR()
 : WIN_MENU()
{
}
//---------------------------------------------------------
WIN_MENUBAR::~WIN_MENUBAR()
{
}
//---------------------------------------------------------
void WIN_MENUBAR::redraw() const
{
	if(menu_handle!=NULL)
	{
		register WIN_CORE * parent_windows = get_parent_win();
		if(parent_windows!=NULL)
			DrawMenuBar(parent_windows->hwnd());
	}
}
//---------------------------------------------------------
void WIN_MENUBAR::attach_menu(const WIN_CORE * to) const
	// attach this menu to the window
{
	assert(to!=NULL,("invalid parameter"));
	if(to->hwnd()!=NULL && menu_handle!=NULL)
		SetMenu(to->hwnd(),menu_handle);
}
//---------------------------------------------------------
void WIN_MENUBAR::set_name_and_parent(const char *name,WIN_COMPOSITE *parent)
	// store a widget name and its parent
{
	set_name(name);
	WIN_SHELL * shell = parent->get_parent_shell();
	parent_win = shell;
	if(shell!=NULL)
		shell->set_menu_bar(this);
}
//---------------------------------------------------------
HMENU WIN_MENUBAR::create_menu() const
	// return a new menu handle for this
{
	return CreateMenu();
}
//---------------------------------------------------------
void WIN_MENUBAR::map()
	// set this mapped
{
	if(!mapped)
	{
		attach_menu(get_parent_win()) ;
		redraw();
	}
	mapped = true;
}
//---------------------------------------------------------
void WIN_MENUBAR::unmap()
	// set this mapped
{
	if(mapped)
	{
		SetMenu(get_parent_win()->hwnd(),NULL);
		redraw();
	}
	mapped = false;
}
//---------------------------------------------------------
