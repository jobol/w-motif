//---------------------------------------------------------
// NAMES_SPACE.cxx
// ------------------
// object is space of names but could be a space of strings
// in fact it implements a hash table of strings
// J.E.Bollo and G. Musset, M3G (c) 1998
//---------------------------------------------------------
#include "NAMES_SPACE.hxx"
//---------------------------------------------------------

int NAMES_SPACE::key_of(const char * string, int length) const
{
	return length==0
		? 0
		: (int)string[0] + 128*(int)string[length/2] + 1024*(int)string[length-1];
}

NAMES_SPACE::~NAMES_SPACE()
{
	int i;
	NAME_ENTRY *entry;

	for (i=0; i<table_size; i++) {
		while (table[i] != NULL) {
			entry = table[i];
			table[i] = entry->next_name_entry;
			free (entry);
		}
	}
	if (table_size != 0) free(table);
}

void NAMES_SPACE::set_table_size(int size)
{
	int i;

	assert(size>0,("table size is not > 0"));
	assert(table_size==0,("can't resize NAMES_SPACE"));
	table = (NAME_ENTRY **)malloc (size * sizeof(NAME_ENTRY*));
	assert(table!=NULL,("memory depletion"));
	for (i=0; i<size; i++) table[i] = NULL;
	table_size = size;
}

NAME NAMES_SPACE::add(const char *string)
{
	int index, len;
	NAME_ENTRY *entry;

	assert(table_size!=0,("can't use NAMES_SPACE without set_table_size"));
	assert(string!=NULL,("null string pointer"));
	len = strlen(string);
	NAME name = find(string);
	if (name.is_valid()) {
		// the name already exists : increment ref_count
		//----------------------------------------------
		entry_of_name(name)->count_of_refs++;;
		return name;
	} else {
		// the name does not exist : create a new entry
		//---------------------------------------------
		entry = (NAME_ENTRY *)malloc(sizeof(NAME_ENTRY) + len);
		assert(entry!=NULL,("memory depletion"));
		index = key_of(string, len) % table_size;
		entry->length_of_string = len;
		entry->count_of_refs = 1;
		memcpy(entry->the_string, string, len+1);
		entry->next_name_entry = table[index];
		table[index] = entry;
		return NAME(entry);
	}
}

void NAMES_SPACE::remove(NAME name)
{
	assert(table_size!=0,("can't use NAMES_SPACE without set_table_size"));
	assert(name.is_valid(),("invalid name"));
	NAME_ENTRY * entry = entry_of_name(name);
	char * string = entry->the_string;
	int       len = entry->length_of_string;
	int     index = key_of(string, len) % table_size;
	NAME_ENTRY **ptr = &table[index];
	while (*ptr != NULL) { 
		if (*ptr == entry) {	// entry found
			if (--entry->count_of_refs <= 0) {
				*ptr = entry->next_name_entry;
				free(entry);
			}
			return;
		}
		ptr = &((*ptr)->next_name_entry);
	}
	error(("impossible case uncountered, check name validity (%s)",
		name.get_string()));
}

void NAMES_SPACE::remove(const char *string)
{
	assert(table_size!=0,("can't use NAMES_SPACE without set_table_size"));
	assert(string!=NULL,("null string pointer"));
	int len   = strlen(string);
	int index = key_of(string, len) % table_size;
	NAME_ENTRY **ptr = &table[index];
	while (*ptr != NULL) { 
		NAME_ENTRY *entry = *ptr;
		if (entry->length_of_string == len)
			if (!strcmp(string, entry->the_string)) {
				if (--entry->count_of_refs <= 0) {
					*ptr = entry->next_name_entry;
					free(entry);
				}
				return;
			}
		ptr = &((*ptr)->next_name_entry);
	}
	error(("impossible case uncountered, check name validity"));
}

NAME NAMES_SPACE::find(const char *string) const
{
	int index, len;
	NAME_ENTRY *entry;

	assert(table_size!=0,("can't use NAMES_SPACE without set_table_size"));
	assert(string!=NULL,("null pointer"));
	len = strlen(string);
	index = key_of(string, len) % table_size;
	entry = table[index];
	while (entry != NULL) {
		if (entry->length_of_string == len)
			if (!strcmp(string, entry->the_string)) return NAME(entry);
		entry = entry->next_name_entry;
	}
	return NAME(NULL);
}


