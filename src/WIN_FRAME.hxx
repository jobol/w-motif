//-------------------------------------------------------------------
// WIN_FRAME.hxx
// ---------------
// implements a class which defines the Motif class "XmFrame"
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __win_frame_hxx
#define __win_frame_hxx
//-------------------------------------------------------------------
#include "macros.h"
#include "WIN_COMPOSITE.hxx"
//-------------------------------------------------------------------
class WIN_FRAME : public WIN_COMPOSITE
{
	DECLARE_RESOURCE_CLIENT(WIN_FRAME)
		// use the resource mechanism

	DECLARE_WINDOW(WIN_FRAME)
		// use window class mechanism

public:

	int margin_width;
		// spacing between one side of the label text
		// and the nearest margin

	int margin_height;
		// spacing between one side of the label text
		// and the nearest margin

	int shadow_type;
		// the shadow type for this, should be
		// XmSHADOW_IN, XmSHADOW_OUT, XmSHADOW_ETCHED_IN, XmSHADOW_ETCHED_OUT

protected:

	~WIN_FRAME();
		// destruction

public:

	WIN_FRAME();
		// construction

	virtual void do_paint(HDC hdc,const RECT *rect) const;
		// paint in response to a WM_PAINT

	virtual bool create_window();
		// create the window associated to the object

	virtual LONG get_windows_style() const;
		// retrieve the windows style used to create the widget 

	virtual void change_managed();
		// The method change_manager called once during the realize phase and then
		// every time that one of its children becomes managed or unmanaged
		
	virtual XtGeometryResult geometry_manager(WIN_CORE *child,
								  XtWidgetGeometry *request,
								  XtWidgetGeometry *geometry_return); 
		// The method geometry_manager called every time  
		// one of its children requests a new size
		
	virtual XtGeometryResult query_geometry (XtWidgetGeometry *request, 
											 XtWidgetGeometry *prefered);
		// The method query_geometry called when the parent  
		// wants to know the widget's prefered size

	virtual void resize();
		// The method resize called every time a widget changes size

	// --------------- Attributs management methods -------------------

	virtual int internal_get_values
		// function used internally to get attribut values 
		// should not be called directly (call get_values)
		// arglist is the array of the values to get
		// count is the count of values to be put into arglist
		// got is an array that flag if an attribut have been put
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs put
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		) const;

	virtual int internal_set_values
		// function used internally to set attribut values 
		// should not be called directly (call set_values)
		// arglist is the array of the values to be set,
		// count is the count of values in arglist
		// got is an array that flag if an attribut have been set
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs set
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		);
};
//-------------------------------------------------------------------
#endif
