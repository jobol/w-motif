//---------------------------------------------------------
// SIMPLE_READER.hxx
// -----------------
// the simple reader abstraction
// J.E.Bollo, M3G (c) 1998
//---------------------------------------------------------
#ifndef __simple_reader_hxx
#define __simple_reader_hxx
//---------------------------------------------------------
#include "macros.h"
//---------------------------------------------------------
class SIMPLE_READER
	// the simple reader abstraction
{
private:

	DWORD	last_error;
		// the last error

protected:

	void set_error(DWORD error_code)
		// set the error code, only inheriters
	{
		last_error = error_code;
	}

public:

	SIMPLE_READER() : last_error(0) {}
		// construction

	DWORD get_last_error()
		// return the last error code
	{
		return last_error;
	}

public:
	// the pure virtual interface functions

	virtual BOOL open_file(LPCSTR nom_fichier) = 0;
		// open the file nom_fichier
		// return TRUE if ok, FALSE otherwise

	virtual BOOL is_opened() = 0;
		// checks if opened, return TRUE if opened

	virtual int read(void *where,int len) = 0;
		// read len byte at memory place where
		// return number of bytes read, 0 at EOF, -1 in case of error

	virtual void close() = 0;
		// close the opened file
};
//---------------------------------------------------------
#endif
