//-------------------------------------------------------------------
// WIDGET_TREE.cxx
// -------------------
// this class performs operations upon widgets trees
// G. MUSSET, M3G (c) 1998
//-------------------------------------------------------------------

#include "WIDGET_TREE.hxx"
#include "WIN_COMPOSITE.hxx"

//-------------------------------------------------------------------
WIDGET_TREE::WIDGET_TREE()
	: array_size(0)
	, wdg_array(NULL)
	, idx(0)
	, free_cells(0)
	, alloc_increment(10)
{}

//-------------------------------------------------------------------
WIDGET_TREE::~WIDGET_TREE()
{
	if (wdg_array != NULL) free(wdg_array);
}

//-------------------------------------------------------------------

void WIDGET_TREE::add_composite_children(WIN_COMPOSITE * wdg)
	// adds Composite children of wdg in array
{
	const RESOURCE_CLASS * composite_class = WIN_COMPOSITE::the_resource_class();
	WIN_CORE ** children = wdg->get_children();
	int num_children = wdg->get_num_children();

	for (int i=0; i<num_children; i ++)
	{
		WIN_CORE *child = children[i];
		if (child->is_kind_of(composite_class))
		{
			// increase allocated space for array if full
			//-------------------------------------------
			if (free_cells <= 0)
			{
				wdg_array = (WIN_COMPOSITE **)realloc(wdg_array, 
						(array_size+alloc_increment)*sizeof(WIN_COMPOSITE *));
				assert (wdg_array!=NULL, ("memory depletion"));
				free_cells = alloc_increment;
			}

			// adds child in array
			//--------------------
			wdg_array[array_size++] = (WIN_COMPOSITE *)child;
			free_cells--;
		}
	}
}

//-------------------------------------------------------------------

WIN_COMPOSITE ** WIDGET_TREE::get_composite_list(WIN_COMPOSITE * wdg)
	// returns the list of Composite widget in the tree
	// under wdg ordered by depth
{
	if (wdg_array != NULL) free(wdg_array);
	wdg_array = (WIN_COMPOSITE **)malloc(alloc_increment*sizeof(WIN_COMPOSITE *));
	assert (wdg_array!=NULL, ("memory depletion"));
	array_size = 1;
	free_cells = alloc_increment - array_size;
	wdg_array[0] = wdg;
	idx = 0;
	while (idx < array_size)
	{
		add_composite_children(wdg_array[idx]);
		idx++;
	}
	return wdg_array;
}
//-------------------------------------------------------------------

