//-------------------------------------------------------------------
// RESOURCE_NAME.cxx
// -------------------
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#include "RESOURCE_NAME.hxx"
//-------------------------------------------------------------------
void RESOURCE_NAME::clear_state()
{
	if(current_state == DYNAMIC_SET)
	{
		free(class_names);
		free(object_names);
	}
	current_state = CLEAR;
	growing_mode = UNSET;
	level = 0;
}
//-------------------------------------------------------------------
void RESOURCE_NAME::grow_arrays()
	// make arrays growing of one level
	// dont change level
{
	int size;
	switch(current_state)
	{
	case UNSET:
		object_names = names_buffer[0];
		class_names  = names_buffer[1];
		current_state = STATIC_SET;
		break;

	case STATIC_SET:
		if(level<default_allocated_length) break;
		size = sizeof(NAME*)*default_allocated_length;
		object_names = (NAME*)malloc(2*size);
		class_names = (NAME*)malloc(2*size);
		assert(object_names!=NULL,("memory depletion"));
		assert(class_names!=NULL,("memory depletion"));
		memcpy(object_names,names_buffer[0],size);
		memcpy(class_names,names_buffer[1],size);
		current_state = DYNAMIC_SET;
		break;

	case DYNAMIC_SET:
		size = sizeof(NAME*)*(level+1);
		object_names = (NAME*)realloc(object_names,size);
		class_names = (NAME*)realloc(class_names,size);
		assert(object_names!=NULL,("memory depletion"));
		assert(class_names!=NULL,("memory depletion"));
		break;

	default:
		error(("should not be"));
	}
}
//-------------------------------------------------------------------
