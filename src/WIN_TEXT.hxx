//-------------------------------------------------------------------
// WIN_TEXT.hxx
// ---------------
// implementation of Motif class "Text" 
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __win_text_hxx
#define __win_text_hxx
//-------------------------------------------------------------------
#include "macros.h"
#include "WIN_PRIMITIVE.hxx"
//-------------------------------------------------------------------
class WIN_TEXT : public WIN_PRIMITIVE
{
	DECLARE_RESOURCE_CLIENT(WIN_TEXT)
		// use the resource mechanism

	DECLARE_WINDOW(WIN_TEXT)
		// use window class mechanism

public:

	char * value;
		// the text to display

	bool auto_show_cursor_position;
		// if True, the visible portion of the text will
		// always contain the insert cursor

	bool editable;
		// if True, the user is allowed to edit the text

	int edit_mode;
		// if XmSINGLE_LINE_EDIT, single-line edit mode;
		// if XmSINGLE_MULTI_EDIT, multiple-line edit mode

	int top_character;
		// location of text to display at the top of the window

	int columns;
		// number of character spaces that should fit horizontally

	int rows;
		// number of character spaces that should fit vertically

	bool scroll_horizontal;
		// adds an horizontal scrollbar if True

	bool scroll_vertical;
		// adds a vertical scrollbar if True

	bool  scroll_left_side;
		// if True, the vertical scrollbar is placed to the left

	bool  scroll_top_side;
		// if True, the horizontal scrollbar is placed above

protected:

	~WIN_TEXT();
		// destruction

public:

	WIN_TEXT();
		// construction

	virtual LRESULT callback(UINT uMsg,WPARAM wParam,LPARAM lParam);
		// the "this" WINDOW callback

	virtual LONG get_windows_style() const;
		// retrieve the windows style used to create the widget 

	virtual bool create_control();
		// function called to create the embedded control
		// return true if creation process should continue, false otherwise

	void add_text(const char* txt);
		// adds txt at the end or at the position of the cursor or 
		// at the end of the selection

	void append_text(const char* txt);
		// adds txt at the end of the text

	virtual void set_text(const char* txt);
		// replaces all the text by "txt"

	void copy_text() const { SendMessage(hwndCtl,WM_COPY,0,0); }
		// copies the current selection to the clipboard

	void cut_text() const{ SendMessage(hwndCtl,WM_CUT,0,0); }
		// deletes the current selection, if any, in the edit control and 
		// copies the deleted text to the clipboard

	void paste_text() const { SendMessage(hwndCtl,WM_PASTE,0,0); }
		// copies the current content of the clipboard to the edit control at the current caret position

	virtual char* get_text() const;
		// gets all the text

	char* get_selected_text() const;
		// gets the selected text

	int get_top_character() const;
		// returns the position of the first visible character of text
		//as the number of characters from the beginning of the text
		//where the first position character is 0

	void set_top_character(int position);

	void set_editable(bool state);
		// sets the state of attribute editable

	void set_edit_mode(int val);
		// sets attribute edit_mode

	int get_insertion_position() const;
		// returns the the position of insertion cursor

	void set_insertion_position(int pos)
		// sets the the position of insertion cursor
		{ SendMessage(hwndCtl, EM_SETSEL, pos, pos);}

	void set_rows(int val);
		// sets the rows number of the text widget, and calculates the new width and heigth

	void set_columns(int val);
		// sets the columns number of the text widget, and calculates the new width and heigth

	void get_prefered_size(SIZE &result);
		// computes the prefered size of this (from rows & columns) in result
		// a zero in returned cx or cy stands for no prefered size

	XtGeometryResult query_geometry (XtWidgetGeometry *request, 
										 XtWidgetGeometry *prefered);
		// The method query_geometry called when the parent  
		// wants to know the widget's prefered size


	// --------------- Attributs management methods -------------------

	virtual int internal_get_values
		// function used internally to get attribut values 
		// should not be called directly (call get_values)
		// arglist is the array of the values to get
		// count is the count of values to be put into arglist
		// got is an array that flag if an attribut have been put
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs put
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		) const;

	virtual int internal_set_values
		// function used internally to set attribut values 
		// should not be called directly (call set_values)
		// arglist is the array of the values to be set,
		// count is the count of values in arglist
		// got is an array that flag if an attribut have been set
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs set
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		);
};
//-------------------------------------------------------------------
#endif // VERSION_OF_WINDOW != 1
//-------------------------------------------------------------------
#endif // __win_text_hxx
