//-------------------------------------------------------------------
// WIN_TOGGLEBUTTON.hxx
// ---------------
// a class used to test the WINDOW system
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __win_togglebutton_hxx
#define __win_togglebutton_hxx
//-------------------------------------------------------------------
#include "macros.h"
#include "WIN_LABEL.hxx"
//-------------------------------------------------------------------
class WIN_TOGGLEBUTTON : public WIN_LABEL
{
	DECLARE_RESOURCE_CLIENT(WIN_TOGGLEBUTTON)
		// use the resource mechanism

	DECLARE_WINDOW(WIN_TOGGLEBUTTON)
		// use window class mechanism

public:

	bool indicator_on;
		// indicator is visible if TRUE. looks like a PushButton if FALSE

//	char * select_insensitive_pixmap;
		// Pixmap used for an insensitive ToggleButton when selected

//	char * select_pixmap;
		// Pixmap used for a sensitive ToggleButton when selected

	bool set;
		// state of the ToggleButton

	int indicator_type;
		// shape of indicator (depending of radio or normal behavior)

protected:

	~WIN_TOGGLEBUTTON();
		// destruction

public:

	WIN_TOGGLEBUTTON();
		// construction

	virtual LRESULT callback(UINT uMsg,WPARAM wParam,LPARAM lParam);
		// the "this" WINDOW callback

	virtual LONG get_windows_style() const;
		// retrieve the windows style used to create the widget 

	virtual bool create_control();
		// function called to create the embedded control
		// return true if creation process should continue, false otherwise

	virtual void arm();
		// virtual function called when button is pressed down

	virtual void disarm();
		// virtual function called when button is no longer pressed down

	virtual void value_changed();
		// virtual function called when value change

	void set_indicator_on(bool state);
		// set state of the attribute indicator_on

	virtual void set_state(bool state);
		// set state of the Toggle (checked or unchecked)

	virtual void change_state();
		// change state of the Toggle (checked or unchecked)

	bool get_state() const;
		// get state of the Toggle (checked or unchecked)

	virtual XtGeometryResult query_geometry (XtWidgetGeometry *request, 
										 XtWidgetGeometry *prefered);
		// The method query_geometry called when the parent  
		// wants to know the widget's prefered size

	// --------------- Attributs management methods -------------------

	virtual int internal_get_values
		// function used internally to get attribut values 
		// should not be called directly (call get_values)
		// arglist is the array of the values to get
		// count is the count of values to be put into arglist
		// got is an array that flag if an attribut have been put
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs put
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		) const;

	virtual int internal_set_values
		// function used internally to set attribut values 
		// should not be called directly (call set_values)
		// arglist is the array of the values to be set,
		// count is the count of values in arglist
		// got is an array that flag if an attribut have been set
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs set
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		);

};
//-------------------------------------------------------------------
#endif
