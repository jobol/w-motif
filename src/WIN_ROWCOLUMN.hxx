//-------------------------------------------------------------------
// WIN_ROWCOLUMN.hxx
// ---------------
// implements the "XmRowColumn" class
// J.E.Bollo & G. Musset, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __win_rowcolumn_hxx
#define __win_rowcolumn_hxx
//-------------------------------------------------------------------
#include "macros.h"
#include "win_naming.h"
#include "WIN_COMPOSITE.hxx"
#include "WIN_TOGGLEBUTTON.hxx"
//-------------------------------------------------------------------
class WIN_ROWCOLUMN : public WIN_COMPOSITE
{
	DECLARE_RESOURCE_CLIENT(WIN_ROWCOLUMN)
		// use the resource mechanism

	DECLARE_WINDOW(WIN_ROWCOLUMN)
		// use window class mechanism

private:

	WIN_CORE * requesting_child;
		// the child who makes a geometry_request to the RowColumn

	XtWidgetGeometry * child_geometry_request;
		// The geometry request parameters of the requesting child

public:

	bool adjust_last;
		// if true the las row (or column) is expanded
		// so as to be flush with the edge

	bool adjust_margin;
		// if true text in each row (or column) will align 
		// with other text in its row (or column)

	int entry_alignment;
		// if is_aligned is true, tells the children how to align

	bool is_aligned;
		// if true, enable alignment specified in entry_alignment


	int margin_width;
		// margin width

	int margin_height;
		// margin height

	int num_columns;
		// number of columns in a vertical RowColumn
		// or number of rows in an horizontal RowColumn

	int orientation;
		// The direction for laying out the children of the RowColumn

	int packing;
		// The method of spacing the items

	bool radio_always_one;
		// if true, one of the RadioButtons must be always selected

	bool radio_behavior;
		// if true, children ToggleButtons become RadioButtons

	int spacing;
		// horizontal and vertical spacing between children

protected:

	~WIN_ROWCOLUMN();
		// destruction

public:

	WIN_ROWCOLUMN();
		// construction

	virtual LRESULT callback(UINT uMsg,WPARAM wParam,LPARAM lParam);
		// the "this" WINDOW callback

	virtual LONG get_windows_style() const;
		// retrieve the windows style used to create the widget 

	virtual bool create_window();
		// create the window associated to the object

	void set_adjust_last(bool value)
		// sets attribute adjust_last
		{ 
			adjust_last = value;
			if (hwnd() != NULL)
				geometry_manager(NULL, NULL, NULL);
		}

	void set_adjust_margin(bool value)
		// sets attribute adjust_margin
		{ adjust_margin = value; }

	void set_entry_alignment(int value)
		// sets attribute entry_alignment
		{ entry_alignment = value; }

	void set_margin_width(int value)
		// sets attribute margin_width
		{ 
			margin_width = value;
			if (hwnd() != NULL)
				geometry_manager(NULL, NULL, NULL);
		}

	void set_margin_height(int value)
		// sets attribute margin_height
		{ 
			margin_height = value;
			if (hwnd() != NULL)
				geometry_manager(NULL, NULL, NULL);
		}

	void set_orientation(int value)
		// sets attribute orientation
		{ 
			orientation = value;
			if (hwnd() != NULL)
				geometry_manager(NULL, NULL, NULL);
		}

	void set_spacing(int value)
		// sets attribute spacing
		{ 
			spacing = value;
			if (hwnd() != NULL)
				geometry_manager(NULL, NULL, NULL);
		}

	void set_packing(int value)
		// sets attribute spacing
		{ 
			packing = value;
			if (hwnd() != NULL)
				geometry_manager(NULL, NULL, NULL);
		}


	virtual void insert_child (WIN_CORE * child);
		// insert a child in the children array


	virtual void change_managed();
		// The method change_manager called once during the realize phase and then
		// every time that one of its children becomes managed or unmanaged
		
	virtual XtGeometryResult geometry_manager(WIN_CORE *child,
								  XtWidgetGeometry *request,
								  XtWidgetGeometry *geometry_return); 
		// The method geometry_manager called every time  
		// one of its children requests a new size
		
	bool do_layout
	(
		bool			  act,
		WIN_CORE		 *the_child,
		XtWidgetGeometry *request,
		XtWidgetGeometry *result
	);
		// do the bulletin board layout
		// return true if geometry need to change
		// if act is true the geometry is changed automatically (by geometry_request)
		// the geometry is returned in result if not NULL
		// it take into account the_child request if both are not NULL

		
	virtual XtGeometryResult query_geometry (XtWidgetGeometry *request, 
											 XtWidgetGeometry *prefered);
		// The method query_geometry called when the parent  
		// wants to know the widget's prefered size

	void resize();
		// The method resize called every time a widget changes size

	virtual int internal_get_values
		// function used internally to get attribut values 
		// should not be called directly (call get_values)
		// arglist is the array of the values to get
		// count is the count of values to be put into arglist
		// got is an array that flag if an attribut have been put
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs put
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		) const;

	virtual int internal_set_values
		// function used internally to set attribut values 
		// should not be called directly (call set_values)
		// arglist is the array of the values to be set,
		// count is the count of values in arglist
		// got is an array that flag if an attribut have been set
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs set
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		);

private:

	bool get_children_geometry
	(
		int 			 first,
		int 			 last,
		WIN_CORE		 *the_child,
		XtWidgetGeometry *request,
		XtWidgetGeometry *result
	);
		// This function returns characteristics about children geometry

	bool get_children_geometry
	(
		WIN_CORE		 *the_child,
		XtWidgetGeometry *request,
		XtWidgetGeometry *result
	)
		// This function returns characteristics about children geometry
		{ return get_children_geometry(0, num_children-1, 
											the_child, request, result); }

	int readjust_children_size(int first,int last);
		// readjust the width (or height if horizontal orientation) of children
		// whose index in children list is comprised between first and last
		// return the width (height) applied to these children

	bool manage_radio_buttons(WIN_TOGGLEBUTTON *calling_button);
		// set state of radio buttons when one of them is changing state
};
//-------------------------------------------------------------------
#endif
