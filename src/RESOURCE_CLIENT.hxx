//---------------------------------------------------------
// RESOURCE_CLIENT.hxx
// -------------------
// polymorphic resource client class
// J.E.Bollo, M3G (c) 1998
//---------------------------------------------------------
#ifndef __resource_client_hxx
#define __resource_client_hxx
//---------------------------------------------------------
#include "macros.h"
#include "RESOURCE_CLASS.hxx"
#include "RESOURCE_SCANNER.hxx"
//---------------------------------------------------------
class RESOURCE_DESCRIPTOR;
//---------------------------------------------------------
class RESOURCE_CLIENT
	// this is the base class for classes that implements
	// the resource mechanismes
{
public:

	virtual const RESOURCE_CLASS * get_resource_class() const = 0;
		// retrieve the resource class descriptor of this

	virtual void set_resource_values(const RESOURCE_SCANNER & scanner);
		// put values of scanner, the currently scanned resources, to this
		// default use the RESOURCE_DESCRIPTOR described by the RESOURCE_CLASS

	const RESOURCE_DESCRIPTOR * get_resource_descriptor() const
		// retrieve the resource class descriptor of this
		{ return get_resource_class()->get_resource_descriptor(); }

	NAME get_class_name() const
		// retrieve the resource class descriptor of this
		{ return get_resource_class()->get_class_name(); }

	bool is_kind_of(NAME name) const
		// is name the name of this or of an ancestor
		{ return get_resource_class()->is_kind_of(name); }

	bool is_kind_of(const char *name) const
		// is name the name of this or of an ancestor
		{ return get_resource_class()->is_kind_of(name); }

	bool is_kind_of(const RESOURCE_CLASS *the_class) const
		// true when the_class is this or one of the ancestors
		{ return get_resource_class()->is_kind_of(the_class); }
};
//---------------------------------------------------------
#define DECLARE_RESOURCE_CLIENT(class) \
public:\
	static RESOURCE_CLASS       __the__class;\
	static RESOURCE_DESCRIPTOR  __the__descriptor[];\
	static const RESOURCE_CLASS * the_resource_class()\
		{ return &__the__class; }\
	static void * create_new_instance()\
		{ return new class; }\
	virtual const RESOURCE_CLASS * get_resource_class() const\
		{ return the_resource_class(); }
//---------------------------------------------------------
#define DECLARE_VIRTUAL_RESOURCE_CLIENT(class) \
public:\
	static RESOURCE_CLASS       __the__class;\
	static RESOURCE_DESCRIPTOR  __the__descriptor[];\
	static const RESOURCE_CLASS * the_resource_class()\
		{ return &__the__class; }\
	static void * create_new_instance()\
		{ return NULL; }\
	virtual const RESOURCE_CLASS * get_resource_class() const\
		{ return the_resource_class(); }
//---------------------------------------------------------
#define BEGIN_ROOT_RES_DESC(class,name) \
	RESOURCE_CLASS class::__the__class(\
		name,NULL,class::__the__descriptor,\
		class::create_new_instance);\
	RESOURCE_DESCRIPTOR class::__the__descriptor[] = {
//---------------------------------------------------------
#define BEGIN_RES_DESC(class,parent,name) \
	RESOURCE_CLASS class::__the__class(\
		name,&parent::__the__class,\
		class::__the__descriptor,\
		class::create_new_instance);\
	RESOURCE_DESCRIPTOR class::__the__descriptor[] = {
//---------------------------------------------------------
#define END_RES_DESC RES_DESC_ATR_END };
//---------------------------------------------------------
#endif

