//-------------------------------------------------------------------
// RESOURCE_SCANNER.hxx
// -------------------
// 
// J.E.Bollo and G.Musset, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __resource_scanner_hxx
#define __resource_scanner_hxx
//-------------------------------------------------------------------
#include "macros.h"
#include "RESOURCE_DATABASE.hxx"
#include "RESOURCE_NAME.hxx"
//-------------------------------------------------------------------

class RESOURCE_CIRCULAR_BUFFER
	// circular resource buffer only used by RESOURCE_SCANNER
{
private:

	enum {
		normal_buffer_size = 1000,
			// initial buffer size before dynamic allocation

		increment_size = 1000,
			// size of increment for dynamic allocation
	};

	int buffer_size;
		// current buffer size (before or after dynamic allocation)

	const RESOURCE_TREE *normal_buffer[normal_buffer_size];
		// static buffer used before dynamic allocation

	const RESOURCE_TREE **buffer;	
		// current buffer (static or dynamic)

	int write_index;
	 	// write index

	int limit_index;
		// limit between the read and write buffer

	int read_index;
		// read index

private:
	void test_overflow();
		// increase buffer size if the buffer is full

	void increment(int &index) { index = (index + 1) % buffer_size; } 
		// increment an index in the circuler buffer

public:
	RESOURCE_CIRCULAR_BUFFER();
		// construction

	~RESOURCE_CIRCULAR_BUFFER();
		// destruction

	void change_buffer();
		// read buffer is freed and write buffer becomes read buffer

	void add_in_buffer(const RESOURCE_TREE *resource);
		// add an item in write buffer

	const RESOURCE_TREE *sequential_read();
		// read sequentially an item in read buffer

	const RESOURCE_TREE *indexed_read(int index) const;
		// read an indexed item in read buffer

	int get_count() const
		// returns the number of items in read buffer
	{
		return (limit_index + buffer_size - read_index) % buffer_size; 
	}
};

//-------------------------------------------------------------------

class RESOURCE_SCANNER
{
private:

	const RESOURCE_DATABASE * database;
		// scanned database

	RESOURCE_CIRCULAR_BUFFER resource_list;
		// read/write double circular buffer used to scan

	void get_resources (const RESOURCE_TREE *parent, const RESOURCE_NAME &resource_name, int i);
		// build the resource list matching the given name
		// at the given level

public:

	RESOURCE_SCANNER(const RESOURCE_DATABASE &db) : database(&db) {}
		// construction of a new resource scanner

	int scan(const RESOURCE_NAME &resource_name);
		// build the resource list matching the given name
		// and return the number of items of this list

	const char *get_value(NAME res_name) const;
		// give the value of the given resource in the
		// list previously builded by function scan
};
//-------------------------------------------------------------------
#endif

