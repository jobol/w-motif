//---------------------------------------------------------
// RESOURCE_CLASS.hxx
// ------------------
// wrapper for resource classes
// J.E.Bollo, M3G (c) 1998
//---------------------------------------------------------
#ifndef __resource_class_hxx
#define __resource_class_hxx
//---------------------------------------------------------
#include "macros.h"
#include "NAMES_SPACE.hxx"
//---------------------------------------------------------
class RESOURCE_CLIENT;
class RESOURCE_DESCRIPTOR;
class RESOURCE_SCANNER;
//---------------------------------------------------------
class RESOURCE_CLASS
	// object recording class functionalities for resource system
{
private:

	const char * class_name_string;
		// the class name as a string

	NAME class_name;
		// the class name as a name

	const RESOURCE_CLASS * ancestor;
		// the parent class

	const RESOURCE_DESCRIPTOR * resource_descriptor_array;
		// array describing the resource of the instances of the class

	void * (*creator_of_instances)();
		// pointer to instance generator function

private:

	static RESOURCE_CLASS * first_resource_class;
		// head of the class chaining

	RESOURCE_CLASS * next_resource_class;
		// link to next within the class chaining

	void chain_me();
		// add this to the class chaining

	void unchain_me();
		// remove this to the class chaining

public:

	RESOURCE_CLASS(
		const char *name,
		const RESOURCE_CLASS *ancestor,
		const RESOURCE_DESCRIPTOR *descriptor,
		void * (*creator_of_instances)()
		);
		// builds a new window class object

	~RESOURCE_CLASS();
		// destroys the window class object

	const RESOURCE_CLASS * get_class_ancestor() const 
		// return the class ancestor
		{ return ancestor; }

	void * create_instance() const;
		// ask for a new instance of RESOURCE_CLIENT 

	NAME get_class_name() const
		// return the name of the class
		{ return class_name; }

	const RESOURCE_DESCRIPTOR * get_resource_descriptor() const
		// retrieving the resouces descriptions (and array null terminated)
		{ return resource_descriptor_array; }

	bool is_kind_of(NAME name) const;
		// is name the name of this or of an ancestor

	bool is_kind_of(const char *name) const;
		// is name the name of this or of an ancestor

	bool is_kind_of(const RESOURCE_CLASS *the_class) const;
		// true when the_class is this or one of the ancestors

	void do_names();
		// declare the names for this

	static void do_all_names();
		// declare all the names

	static const RESOURCE_CLASS * get_class(NAME name);
		// retrieve a RESOURCE_CLASS using the chain
		// return NULL when not found

	static const RESOURCE_CLASS * get_class(const char *name);
		// retrieve a RESOURCE_CLASS using the chain
		// return NULL when not found

	virtual void set_resource_values(
		const RESOURCE_SCANNER *scanner,RESOURCE_CLIENT *client) const;
		// put values of scanner, the currently scanned resources, to this
		// default use the RESOURCE_DESCRIPTOR described by the RESOURCE_CLASS

	static bool is_valid_resource_class(const RESOURCE_CLASS *ptr);
		// checks that ptr is a valid RESOURCE_CLASS pointer
		// return true if valid and false otherwise
};
//---------------------------------------------------------
#endif

