//-------------------------------------------------------------------
// WIN_MESSAGE_BOX.cxx
// ---------------
// implements the "XmMessageBox" class
// J.E.Bollo & G. Musset, M3G (c) 1998
//
// This class is implemented only for supporting attributes
// XmNokLabelString, XmNcancelLabelString and XmNhelpLabelString
// but does not create a real window
//-------------------------------------------------------------------
#include "WIN_MESSAGE_BOX.hxx"
#include "WINDOWS_MANAGER.hxx"
#include "win_naming.h"
#include "windows_commons.h"
//-------------------------------------------------------------------
BEGIN_RES_DESC(WIN_MESSAGE_BOX,WIN_BULLETIN_BOARD,NAME_OF_WIN_MESSAGE_BOX_CLASS)
	RES_DESC_ATR(XmNokLabelString,		TEXT,		WIN_MESSAGE_BOX, ok_label_string),
	RES_DESC_ATR(XmNcancelLabelString,	TEXT,		WIN_MESSAGE_BOX, cancel_label_string),
	RES_DESC_ATR(XmNhelpLabelString,	TEXT,		WIN_MESSAGE_BOX, help_label_string),
END_RES_DESC
//-------------------------------------------------------------------
IMPLEMENT_WINDOW(WIN_MESSAGE_BOX)
//-------------------------------------------------------------------
ATOM WIN_MESSAGE_BOX::register_window_class() const
{
	return WINDOWS_MANAGER::register_window_class(
					(const char*)get_class_name(),0,
					LoadCursor(NULL,IDC_ARROW));
}
//-------------------------------------------------------------------
WIN_MESSAGE_BOX::WIN_MESSAGE_BOX()
	: WIN_BULLETIN_BOARD()
{
	ok_label_string = strdup("Ok");
	cancel_label_string = strdup("Cancel");
	help_label_string = strdup("Help");
}
//-------------------------------------------------------------------
WIN_MESSAGE_BOX::~WIN_MESSAGE_BOX()
{
	if (ok_label_string != NULL) free(ok_label_string);
	if (cancel_label_string != NULL) free(cancel_label_string);
	if (help_label_string != NULL) free(help_label_string);
}
//-------------------------------------------------------------------
LONG WIN_MESSAGE_BOX::get_windows_style() const
	// retrieve the windows style used to create the widget 
{
	return WS_CHILD|WS_CLIPSIBLINGS|WS_VISIBLE|WS_CLIPCHILDREN;
}
//-------------------------------------------------------------------
bool WIN_MESSAGE_BOX::create_window()
{
	return WIN_CORE::create_window(get_windows_style());
}
//-------------------------------------------------------------------

int WIN_MESSAGE_BOX::internal_get_values
	// function used internally to get attribut values 
	// should not be called directly (call get_values)
	// arglist is the array of the values to get
	// count is the count of values to be put into arglist
	// got is an array that flag if an attribut have been put
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs put
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) const
{
	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.
#if VERSION_OF_WINDOW >= 2

		if(equality(attribut,XmNokLabelString))
			arglist[index].value.pvalue->psztext = ok_label_string;
		else
		if(equality(attribut,XmNcancelLabelString))
			arglist[index].value.pvalue->psztext = cancel_label_string;
		else
		if(equality(attribut,XmNhelpLabelString))
			arglist[index].value.pvalue->psztext = help_label_string;

#else

		if(equality(attribut,XmNokLabelString))
			arglist[index].value.pvalue->psztext = strdup(ok_label_string==NULL ? "" : ok_label_string);
		else
		if(equality(attribut,XmNcancelLabelString))
			arglist[index].value.pvalue->psztext = strdup(cancel_label_string==NULL ? "" : cancel_label_string);
		else
		if(equality(attribut,XmNhelpLabelString))
			arglist[index].value.pvalue->psztext = strdup(help_label_string==NULL ? "" : help_label_string);

#endif

		else
			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
	}

	// return total count of found attributs (including ancestors)
	return found_count + WIN_BULLETIN_BOARD::internal_get_values(arglist,count,got,equality);
}

//-------------------------------------------------------------------


int WIN_MESSAGE_BOX::internal_set_values
	// function used internally to set attribut values 
	// should not be called directly (call set_values)
	// arglist is the array of the values to be set,
	// count is the count of values in arglist
	// got is an array that flag if an attribut have been set
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs set
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) 
{
/*
	// empty geometry request
	bool redraw_needed = false;
	XtWidgetGeometry request;
	request.request_mode = 0;
*/
	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.
		if(equality(attribut,XmNokLabelString))
			safe_assign_copy_of_text (&ok_label_string, arglist[index].value.psztext);
		else
		if(equality(attribut,XmNcancelLabelString))
			safe_assign_copy_of_text (&cancel_label_string, arglist[index].value.psztext);
		else
		if(equality(attribut,XmNhelpLabelString))
			safe_assign_copy_of_text (&help_label_string, arglist[index].value.psztext);
		else
			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
	}

/*
	// make the geometry request if necessary
	if(request.request_mode!=0)
		geometry_request(&request);
	if(redraw_needed)
		redraw();
*/
	// return total count of found attributs (including ancestors)
	return found_count + WIN_BULLETIN_BOARD::internal_set_values(arglist,count,got,equality);
}

