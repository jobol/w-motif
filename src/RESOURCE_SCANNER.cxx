//---------------------------------------------------------
// RESOURCE_SCANNER.cxx
// ------------------
// 
// J.E.Bollo and G. Musset, M3G (c) 1998
//---------------------------------------------------------

#include "RESOURCE_NAMES_SPACE.hxx"
#include "RESOURCE_SCANNER.hxx"

//***************************************************************************
//
//	Mananagement of the circular buffer for storing resource list
//
//***************************************************************************

//--------------------------------------
// Initialisation of the circular buffer
//--------------------------------------
RESOURCE_CIRCULAR_BUFFER::RESOURCE_CIRCULAR_BUFFER()
  : read_index(0) 
  , limit_index(0) 
  , write_index(0)
  , buffer_size(normal_buffer_size)
  , buffer(normal_buffer)
{
}

//--------------------------------------
// Destruction of the circular buffer
//--------------------------------------
RESOURCE_CIRCULAR_BUFFER::~RESOURCE_CIRCULAR_BUFFER()
{
}

//-------------------------------------
//change_buffer:  read buffer is freed
// and write buffer becomes read buffer
//-------------------------------------
void RESOURCE_CIRCULAR_BUFFER::change_buffer()
{
	read_index = limit_index;	
	limit_index = write_index;
}

//----------------------------
// add an item in write buffer
//----------------------------

void RESOURCE_CIRCULAR_BUFFER::add_in_buffer(const RESOURCE_TREE *resource)
{
	// Do not write if the resource is already present in the buffer
	for (int i = limit_index; i != write_index; increment(i))
		if (resource == buffer[i]) return;

	buffer[write_index] = resource;
	increment(write_index);
	test_overflow();
}

//-----------------------------------------
// read sequentially an item in read buffer
//-----------------------------------------

const RESOURCE_TREE * RESOURCE_CIRCULAR_BUFFER::sequential_read()
{
	if (read_index != limit_index) {
		const RESOURCE_TREE *resource = buffer[read_index];
		increment(read_index);
		return resource;
	}
	return NULL;	// no more item to read
}

//------------------------------------
// read an indexed item in read buffer
//------------------------------------

const RESOURCE_TREE * RESOURCE_CIRCULAR_BUFFER::indexed_read(int index) const
{
	assert (index >=0, ("bad index in indexed_read"));
	assert (index < get_count(), ("bad index in indexed_read"));
	return buffer[(index + read_index) % buffer_size];
}
		
//-------------------------------------------
// increase buffer size if the buffer is full
// (the write index catch the read index up)
//-------------------------------------------

void RESOURCE_CIRCULAR_BUFFER::test_overflow()
{
	// In the current release, overflow is not allowed
	assert (read_index!=write_index, ("circular buffer full"));
}

//***************************************************************************
//***************************************************************************

//------------------------------------------------
// build the resource list matching the given name
// and return the number of items of this list
// At the end of function the write buffer is empty
//   we asser that between two calls to scan it stays empty
//------------------------------------------------

int RESOURCE_SCANNER::scan(const RESOURCE_NAME &resource_name)
{
	// get the number of items in the window names list
	//-------------------------------------------------

	int nb_names = resource_name.get_level();

	// Search in resource database the children of the root resource
	// whose name match the first name of the window names list and
	// store them in the result resource list
	//--------------------------------------------------------------

	const RESOURCE_TREE *parent_resource = database->get_root();
	get_resources(parent_resource, resource_name, 0);
	resource_list.change_buffer();

	// For each name of the window names list, search in
	// resource database the children of the resources found
	// in the previous step and whose name match the current name
	// of the window names list.
	// Store these resources in the result resource list
	//-----------------------------------------------------------

	for (int i=1; i<nb_names; i++) {
		while ((parent_resource = resource_list.sequential_read()) != NULL) {
			get_resources(parent_resource, resource_name, i);
		}
		resource_list.change_buffer();
	}

	// check for wild bind ends

	while ((parent_resource = resource_list.sequential_read()) != NULL) {
		resource_list.add_in_buffer(parent_resource);
		const RESOURCE_TREE *resource = parent_resource->get_wild_bind_daughter();
		if(resource!=NULL) resource_list.add_in_buffer(resource);
	}
	resource_list.change_buffer();

	return resource_list.get_count();
}

//------------------------------------------------
// build the resource list matching the given name
// at the given level
//------------------------------------------------

void RESOURCE_SCANNER::get_resources
(
	const RESOURCE_TREE *parent,
	const RESOURCE_NAME &resource_name,
	int i
)
{

	// first, get the ressource whose window name match the given name
	//----------------------------------------------------------------

	NAME name = resource_name.object_name(i);
	const RESOURCE_TREE *resource = parent->get_named_daughter(name);
	if (resource != NULL) resource_list.add_in_buffer(resource);

	// Then, get the ressource whose class name match the given name
	//--------------------------------------------------------------

	name = resource_name.class_name(i);
	resource = parent->get_named_daughter(name);
	if (resource != NULL) resource_list.add_in_buffer(resource);

	// Then, get the any name resource
	//--------------------------------

	resource = parent->get_any_name_daughter();
	if (resource != NULL) resource_list.add_in_buffer(resource);

	if (parent->is_wild_bind()) {

		// If the parent is a wild bind resource, do not try to
		// get the wild bind child because we know that it cannot
		// exist but put the parent again in the result list
		//-------------------------------------------------------

		resource_list.add_in_buffer(parent);

	} else {
		
		// The parent is not a wild bind resource:
		// If there exists a wild bind resource reiterate the operation
		// for the children of this resource. The wild bind resource
		// is placed after its children in the result list
		//-------------------------------------------------------------

		resource = parent->get_wild_bind_daughter();
		if (resource != NULL) {
			get_resources (resource, resource_name, i);
			resource_list.add_in_buffer(resource);
		};
	};
}

//--------------------------------------------
// give the value of the given resource in the
// list previously builded by function scan
//--------------------------------------------

const char * RESOURCE_SCANNER::get_value(NAME res_name) const
{
	// get the number of items in the list builded by function scan
	//-------------------------------------------------------------
	int nb_res = resource_list.get_count();
	
	// scan sequentially this list and return the value of the first
	// encountered resource whose name match the given name
	//--------------------------------------------------------------
 
	for (int i=0; i<nb_res; i++) {
		const RESOURCE_TREE *resource = resource_list.indexed_read(i);
		resource = resource->get_named_daughter(res_name);
		if (resource != NULL) {
			const char *value = resource->get_value();
			if (value != NULL ) return value;
		}
	}
  
	return NULL;
}
