//-------------------------------------------------------------------
// WIN_COMPOSITE.hxx
// ---------------
// implements a class used to define the Xt class "Composite"
// J.E.Bollo & G. Musset, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __win_composite_hxx
#define __win_composite_hxx
//-------------------------------------------------------------------
#include "macros.h"
#include "WIN_CORE.hxx"
//-------------------------------------------------------------------
class WIN_COMPOSITE : public WIN_CORE
{
	DECLARE_VIRTUAL_RESOURCE_CLIENT(WIN_COMPOSITE)
		// use the resource mechanisme

public:

	WIN_CORE ** children;
		// list of children of the composite window

	int num_children;
		// number of children of the composite window

protected:

	~WIN_COMPOSITE();
		// destruction

public:

	WIN_COMPOSITE();
		// construction

	virtual void destroy();
		// public destruction

	int get_num_children() const;
		// returns the number of children of the Composite window

	WIN_CORE ** get_children() const;
		// returns the list of children of the Composite window

	WIN_CORE * get_child_at(int index) const ;
		// return the i-th child

	bool containts_child(const WIN_CORE *child) const;
		// return true if the child is in the list of children

	int get_index_of_child(const WIN_CORE *child) const;
		// return the index of child in the list of children

	virtual void insert_child (WIN_CORE * child);
		// insert a child in the children array

	virtual void delete_child (WIN_CORE * child);
		// delete a child in the children array
		// and delete its attached constraint if any

	virtual void realize();
		// realise this and sub-widgets

	virtual int constraint_get_values
		// function used internally to get attribut values for constraints
		// should not be called directly (call get_values)
		// that method is called for the parent of the child for wich
		//   the original get_values is requested
		// arglist is the array of the values to get
		// count is the count of values to be put into arglist
		// got is an array that flag if an attribut have been put
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs put
		(
			const WIN_CORE *child,
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		) const;

	virtual int constraint_set_values
		// function used internally to set attribut values for constraints
		// should not be called directly (call set_values)
		// that method is called for the parent of the child for wich
		//   the original get_values is requested
		// arglist is the array of the values to be set,
		// count is the count of values in arglist
		// got is an array that flag if an attribut have been set
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs set
		(
			WIN_CORE *child,
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		);

	// --------------- Geometry management methods -------------------

	virtual void change_managed() = 0;
		// The method change_manager called once during the realize phase and then
		// every time that one of its children becomes managed or unmanaged
		
	virtual XtGeometryResult geometry_manager(
									WIN_CORE *child,
									XtWidgetGeometry * request,
									XtWidgetGeometry * geometry_return) = 0;
		// The method geometry_manager called every time  
		// one of its children requests a new size

	// --------------- Object management methods -------------------

	virtual void delete_this(CB_EMITER emiter) { delete (WIN_COMPOSITE *)emiter; }
	// delete this

};
//-------------------------------------------------------------------
//                           INLINES
//-------------------------------------------------------------------
inline int WIN_COMPOSITE::get_num_children() const 
	// returns the number of children of the Composite window
{
	return num_children;
}
//-------------------------------------------------------------------
inline WIN_CORE ** WIN_COMPOSITE::get_children() const
	// returns the list of children of the Composite window
{
	return children;
}
//-------------------------------------------------------------------
inline WIN_CORE * WIN_COMPOSITE::get_child_at(int index) const 
	// return the i-th child
{
	assert(index>=0 && index<num_children,("bad index"));
	return children[index];
}
//-------------------------------------------------------------------
inline bool WIN_COMPOSITE::containts_child(const WIN_CORE *child) const 
	// return true if the child is in the list of children
{
	int index = num_children;
	do { index--; } while(index>=0 && children[index]!=child);
	return index>=0;
}
//-------------------------------------------------------------------
inline int WIN_COMPOSITE::get_index_of_child(const WIN_CORE *child) const 
	// return the index of child in the list of children
{
	assert(containts_child(child),("bad child: not in the children list"));
	int index = num_children;
	do { index--; } while(index>=0 && children[index]!=child);
	return index;
}
//-------------------------------------------------------------------
#endif

