//-------------------------------------------------------------------
// WIN_LABEL.cxx
// ---------------
// implements the Motif class "XmLabel"
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#include "WIN_LABEL.hxx"
#include "WINDOWS_MANAGER.hxx"
#include "win_naming.h"
#include "WINDOWS_COMMONS.h"
//-------------------------------------------------------------------
BEGIN_RES_DESC(WIN_LABEL,WIN_PRIMITIVE,NAME_OF_WIN_LABEL_CLASS)
	RES_DESC_ATR(XmNlabelString,	TEXT,		WIN_LABEL, label_string),
	RES_DESC_ATR(XmNaccelerator,	TEXT,		WIN_LABEL, accelerator),
	RES_DESC_ATR(XmNmnemonic,		TEXT,		WIN_LABEL, mnemonic),
	RES_DESC_ATR(XmNlabelPixmap,	TEXT,		WIN_LABEL, label_pixmap),
	RES_DESC_ATR(XmNlabelInsensitivePixmap,	TEXT,  WIN_LABEL, label_insensitive_pixmap),
	RES_DESC_ATR(XmNlabelType,		ENUM,		WIN_LABEL, label_type),
	RES_DESC_ATR(XmNmarginWidth,	INTEGER,	WIN_LABEL, margin_width),
	RES_DESC_ATR(XmNmarginHeight,	INTEGER,	WIN_LABEL, margin_height),
	RES_DESC_ATR(XmNmarginTop,		INTEGER,	WIN_LABEL, margin_top),
	RES_DESC_ATR(XmNmarginBottom,	INTEGER,	WIN_LABEL, margin_bottom),
	RES_DESC_ATR(XmNmarginLeft,		INTEGER,	WIN_LABEL, margin_left),
	RES_DESC_ATR(XmNmarginRight,	INTEGER,	WIN_LABEL, margin_right),
END_RES_DESC
//-------------------------------------------------------------------
IMPLEMENT_WINDOW(WIN_LABEL)
//-------------------------------------------------------------------
ATOM WIN_LABEL::register_window_class() const
{
	WNDCLASSEX wce;
	WINDOWS_MANAGER::get_window_class_info("STATIC",&wce);
	return WINDOWS_MANAGER::register_window_class(
					(const char*)get_class_name(),wce.style);
}
//-------------------------------------------------------------------
WIN_LABEL::WIN_LABEL()
	: WIN_PRIMITIVE()
	, accelerator(NULL)
	, label_type(XmSTRING)
	, mnemonic(NULL)
	, label_pixmap(NULL)
	, label_insensitive_pixmap(NULL)
	, margin_width(0)
	, margin_height(0)
	, margin_top(0)
	, margin_bottom(0)
	, margin_left(0)
	, margin_right(0)
{
	label_string = strdup("");
	traversal_on = false;
}
//-------------------------------------------------------------------
WIN_LABEL::~WIN_LABEL()
{
	if(label_string!=NULL) free(label_string);
	if(accelerator!=NULL) free(accelerator);
	if(mnemonic!=NULL) free(mnemonic);
	if(label_pixmap!=NULL) free(label_pixmap);
	if(label_insensitive_pixmap!=NULL) free(label_insensitive_pixmap);
}

//-------------------------------------------------------------------
LONG WIN_LABEL::get_windows_style() const
	// retrieve the windows style used to create the widget 
{
	return WS_CHILD | WS_VISIBLE | SS_CENTER;
}
//-------------------------------------------------------------------

bool WIN_LABEL::create_control()
{
	hwndCtl = CreateWindow(
				"STATIC",
				label_string,
				WS_CHILD | WS_VISIBLE | SS_CENTER,
				0, 0,width,height,
				this->hwnd(),
				(HMENU)1,
				WINDOWS_MANAGER::get_application_instance(),
				NULL);
	return hwndCtl!=NULL;
}

//-------------------------------------------------------------------

LRESULT WIN_LABEL::callback(UINT uMsg,WPARAM wParam,LPARAM lParam)
{
	switch(uMsg)
	{
	case WM_CTLCOLORSTATIC:
		{
			HDC hdc = (HDC)wParam;
			SetBkMode(hdc, OPAQUE);
			SetBkColor(hdc, color_of_brush(background));
			SetTextColor(hdc, foreground);
			return (LRESULT)background;
		}

	case WM_SIZE:
		if(get_resource_class()==WIN_LABEL::the_resource_class())
			redraw();
		break;
	}

	return WIN_PRIMITIVE::callback(uMsg,wParam,lParam);
}

//-------------------------------------------------------------------

void WIN_LABEL::set_text(const char* txt)
	// replaces all the text of the label by "txt"
{
#if VERSION_OF_WINDOW >= 2

	safe_assign_copy_of_text (&label_string, txt);
	if (hwndCtl!=NULL)
	{
		char * text_crlf = add_carriage_return(label_string);
		SendMessage(hwndCtl,WM_SETTEXT,0,(LPARAM)text_crlf);
		free(text_crlf);
	}
	update_geometry_request (CWWidth|CWHeight);

#else

	if (hwndCtl!=NULL)
	{
		char * text_crlf = add_carriage_return(txt==NULL ? "" : txt);
		SendMessage(hwndCtl,WM_SETTEXT,0,(LPARAM)text_crlf);
		free(text_crlf);
	}
	else
	{
		if(label_string!=NULL) free(label_string);
		label_string = strdup(txt);
	}
	update_geometry_request(CWWidth|CWHeight);

#endif
}

//-------------------------------------------------------------------

char* WIN_LABEL::get_text() const
	// gets all the text
{
#if VERSION_OF_WINDOW >= 2

	return label_string;

#else

	if (hwndCtl==NULL)
		return strdup(label_string==NULL ? "" : label_string);

	// return WIN_PRIMITIVE::get_text();
	int length = SendMessage(hwndCtl,WM_GETTEXTLENGTH,0,0);
	char* text = new char[length+1];
	if(text==NULL)
		return NULL;

	SendMessage(hwndCtl,WM_GETTEXT,length+1,(LPARAM)text);
	char * text_lf = remove_carriage_return(text);
	free(text);
	return text_lf;

#endif
}

//-------------------------------------------------------------------

XtGeometryResult WIN_LABEL::query_geometry (XtWidgetGeometry *request, 
										 XtWidgetGeometry *prefered)
	// The method query_geometry called when the parent  
	// wants to know the widget's prefered size
{
	SIZE size;
	XtWidgetGeometry pref;

	window_multiline_text_extend(hwndCtl,label_string,font_list,&size);
	pref.width = size.cx;
	pref.height = size.cy;
	pref.request_mode = CWWidth|CWHeight;

	return complete_query_geometry(request,prefered,&pref);
}

//-------------------------------------------------------------------

int WIN_LABEL::internal_get_values
	// function used internally to get attribut values 
	// should not be called directly (call get_values)
	// arglist is the array of the values to get
	// count is the count of values to be put into arglist
	// got is an array that flag if an attribut have been put
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs put
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) const
{
	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.

		if(equality(attribut,XmNlabelString))
			arglist[index].value.pvalue->psztext = get_text();
		else
		if(equality(attribut,XmNlabelType))
			arglist[index].value.pvalue->integer = label_type;
		else
		if(equality(attribut,XmNmarginWidth))
			arglist[index].value.pvalue->integer = margin_width;
		else
		if(equality(attribut,XmNmarginHeight))
			arglist[index].value.pvalue->integer = margin_height;
		else
		if(equality(attribut,XmNmarginTop))
			arglist[index].value.pvalue->integer = margin_top;
		else
		if(equality(attribut,XmNmarginBottom))
			arglist[index].value.pvalue->integer = margin_bottom;
		else
		if(equality(attribut,XmNmarginLeft))
			arglist[index].value.pvalue->integer = margin_left;
		else
		if(equality(attribut,XmNmarginRight))
			arglist[index].value.pvalue->integer = margin_right;
		else

#if VERSION_OF_WINDOW >= 2

		if(equality(attribut,XmNaccelerator))
			arglist[index].value.pvalue->psztext = accelerator;
		else
		if(equality(attribut,XmNmnemonic))
			arglist[index].value.pvalue->psztext = mnemonic;
		else
		if(equality(attribut,XmNlabelPixmap))
			arglist[index].value.pvalue->psztext = label_pixmap;
		else
		if(equality(attribut,XmNlabelInsensitivePixmap))
			arglist[index].value.pvalue->psztext = label_insensitive_pixmap;

#else

		if(equality(attribut,XmNaccelerator))
			arglist[index].value.pvalue->psztext = strdup(accelerator==NULL ? "" : accelerator);
		else
		if(equality(attribut,XmNmnemonic))
			arglist[index].value.pvalue->psztext = strdup(mnemonic==NULL ? "" : mnemonic);
		else
		if(equality(attribut,XmNlabelPixmap))
			arglist[index].value.pvalue->psztext = strdup(label_pixmap==NULL ? "" : label_pixmap);
		else
		if(equality(attribut,XmNlabelInsensitivePixmap))
			arglist[index].value.pvalue->psztext = strdup(label_insensitive_pixmap==NULL ? "" : label_insensitive_pixmap);

#endif

		else
			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
	}

	// return total count of found attributs (including ancestors)
	return found_count + WIN_PRIMITIVE::internal_get_values(arglist,count,got,equality);
}


//-------------------------------------------------------------------

int WIN_LABEL::internal_set_values
	// function used internally to set attribut values 
	// should not be called directly (call set_values)
	// arglist is the array of the values to be set,
	// count is the count of values in arglist
	// got is an array that flag if an attribut have been set
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs set
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) 
{
	// empty geometry request
	bool redraw_needed = false;
/*
	XtWidgetGeometry request;
	request.request_mode = 0;
*/

	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.

		if(equality(attribut,XmNlabelString))
			set_text(arglist[index].value.psztext);
		else
		if(equality(attribut,XmNaccelerator))
		{
			if(accelerator!=NULL) 
				free(accelerator);
			if (arglist[index].value.psztext != NULL)
				accelerator = strdup(arglist[index].value.psztext);
			else
				accelerator = NULL;
		}
		else
		if(equality(attribut,XmNmnemonic))
		{
			if(mnemonic!=NULL) 
				free(mnemonic);
			mnemonic = strdup(arglist[index].value.psztext == NULL ? "" : arglist[index].value.psztext);
		}
		else
		if(equality(attribut,XmNlabelPixmap))
		{
			if(label_pixmap!=NULL) 
				free(label_pixmap);
			label_pixmap = strdup(arglist[index].value.psztext == NULL ? "" : arglist[index].value.psztext);
			redraw_needed = true;
		}
		else
		if(equality(attribut,XmNlabelInsensitivePixmap))
		{
			if(label_insensitive_pixmap!=NULL) 
				free(label_insensitive_pixmap);
			label_insensitive_pixmap = 
				strdup(arglist[index].value.psztext == NULL ? "" : arglist[index].value.psztext);
			redraw_needed = true;
		}
		else
		if(equality(attribut,XmNlabelType))
		{
			label_type = arglist[index].value.integer;
			redraw_needed = true;
		}
		else
		if(equality(attribut,XmNmarginWidth))
		{
			margin_width = arglist[index].value.integer;
			redraw_needed = true;
		}
		else
		if(equality(attribut,XmNmarginHeight))
		{
			margin_height = arglist[index].value.integer;
			redraw_needed = true;
		}
		else
		if(equality(attribut,XmNmarginTop))
		{
			margin_top = arglist[index].value.integer ;
			redraw_needed = true;
		}
		else
		if(equality(attribut,XmNmarginBottom))
		{
			margin_bottom = arglist[index].value.integer ;
			redraw_needed = true;
		}
		else
		if(equality(attribut,XmNmarginLeft))
		{
			margin_left = arglist[index].value.integer ;
			redraw_needed = true;
		}
		else
		if(equality(attribut,XmNmarginRight))
		{
			margin_right = arglist[index].value.integer;
			redraw_needed = true;
		}

		else
			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
	}

/*
	// make the geometry request if necessary
	if(request.request_mode!=0)
		geometry_request(&request);
*/
	if(redraw_needed)
		redraw();

	// return total count of found attributs (including ancestors)
	return found_count  + WIN_PRIMITIVE::internal_set_values(arglist,count,got,equality);
}

