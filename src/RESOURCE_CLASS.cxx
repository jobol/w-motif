//---------------------------------------------------------
// RESOURCE_CLASS.cxx
// ------------------
// wrapper for resource classes
// J.E.Bollo, M3G (c) 1998
//---------------------------------------------------------
#include "RESOURCE_CLASS.hxx"
#include "RESOURCE_NAMES_SPACE.hxx"
#include "RESOURCE_SCANNER.hxx"
#include "RESOURCE_DESCRIPTOR.hxx"
//---------------------------------------------------------

RESOURCE_CLASS * RESOURCE_CLASS::first_resource_class = NULL;
	// head of the RESOURCE_CLASSes chain

//---------------------------------------------------------

void RESOURCE_CLASS::chain_me()
	// add this to the class chaining
{
	next_resource_class = first_resource_class;
	first_resource_class = this;
}

//---------------------------------------------------------

void RESOURCE_CLASS::unchain_me()
	// remove this to the class chaining
{
	assert(first_resource_class!=NULL,("can't append!"));
	if(first_resource_class==this)
		first_resource_class = next_resource_class;
	else
	{
		RESOURCE_CLASS * iter = first_resource_class;
		while(iter->next_resource_class!=this)
		{
			iter = iter->next_resource_class;
			assert(iter!=NULL,("can not append!"));
		}
		iter->next_resource_class = next_resource_class;
	}
}

//---------------------------------------------------------

RESOURCE_CLASS::RESOURCE_CLASS
	// builds a new window class object
(
	const char *the_name,
	const RESOURCE_CLASS *the_ancestor,
	const RESOURCE_DESCRIPTOR *the_descriptor,
	void * (*the_creator_of_instances)()
)
	: class_name()
	, class_name_string(the_name)
	, ancestor(the_ancestor)
	, resource_descriptor_array(the_descriptor)
	, creator_of_instances(the_creator_of_instances)
{
	chain_me();
}

//---------------------------------------------------------

RESOURCE_CLASS::~RESOURCE_CLASS()
	// destroys the window class object
{
	unchain_me();
}

//---------------------------------------------------------

void * RESOURCE_CLASS::create_instance() const
	// ask for a new instance of RESOURCE_CLIENT 
{
	return creator_of_instances!=NULL ? creator_of_instances() : NULL;
}

//---------------------------------------------------------

const RESOURCE_CLASS * RESOURCE_CLASS::get_class(NAME name)
	// retrieve a RESOURCE_CLASS using the chain
{
	RESOURCE_CLASS * result = first_resource_class;
	while(result!=NULL && name!=result->get_class_name())
		result = result->next_resource_class;
	return result;
}

//---------------------------------------------------------

const RESOURCE_CLASS * RESOURCE_CLASS::get_class(const char *name)
	// retrieve a RESOURCE_CLASS using the chain
{
	const RESOURCE_CLASS * result = NULL;
	if(name!=NULL)
	{
		NAME the_name = resource_names_space.find(name);
		if(the_name.is_valid())
			result = get_class(the_name);
	}
	return result;
}

//---------------------------------------------------------

bool RESOURCE_CLASS::is_kind_of(NAME name) const
	// is name the name of this or of an ancestor
{
	const RESOURCE_CLASS * result = this;
	while(result!=NULL && name!=result->get_class_name())
		result = result->ancestor;
	return result!=NULL;
}

//---------------------------------------------------------

bool RESOURCE_CLASS::is_kind_of(const char *name) const
	// is name the name of this or of an ancestor
{
	bool result = false;
	if(name!=NULL)
	{
		NAME the_name = resource_names_space.find(name);
		if(the_name.is_valid())
			result = is_kind_of(the_name);
	}
	return result;
}

//---------------------------------------------------------

bool RESOURCE_CLASS::is_kind_of(const RESOURCE_CLASS *the_class) const
	// true when the_class is this or one of the ancestors
{
	const RESOURCE_CLASS * result = this;
	while(result!=NULL && result!=the_class)
		result = result->ancestor;
	return result!=NULL;
}

//---------------------------------------------------------

void RESOURCE_CLASS::set_resource_values
(
	const RESOURCE_SCANNER *scanner,
	RESOURCE_CLIENT *client
) const
		// put values of scanner, the currently scanned resources, to this
		// default use the RESOURCE_DESCRIPTOR described by the RESOURCE_CLASS
{
	if(ancestor!=NULL)
		ancestor->set_resource_values(scanner,client);
	const RESOURCE_DESCRIPTOR * res_desc = resource_descriptor_array;
	if(res_desc!=NULL)
	{
		while(!res_desc->is_empty())
		{
			const char * value = scanner->get_value(res_desc->get_name());
			if(value!=NULL)
				res_desc->set_value(client,value);
			res_desc++;
		}
	}
}

//---------------------------------------------------------

void RESOURCE_CLASS::do_names()
	// declare the names
{
	class_name = resource_names_space.add(class_name_string);
	RESOURCE_DESCRIPTOR * res_desc_ptr = 
		const_cast<RESOURCE_DESCRIPTOR *>(resource_descriptor_array);
	while(!res_desc_ptr->is_empty())
	{
		res_desc_ptr->do_name();
		res_desc_ptr++;
	}
}

//---------------------------------------------------------

void RESOURCE_CLASS::do_all_names()
	// declare all the names
{
	RESOURCE_CLASS * iter = first_resource_class;
	while(iter!=NULL)
	{
		iter->do_names();
		iter = iter->next_resource_class;
	}
}

//---------------------------------------------------------

bool RESOURCE_CLASS::is_valid_resource_class(const RESOURCE_CLASS *ptr)
	// checks that ptr is a valid RESOURCE_CLASS pointer
	// return true if valid and false otherwise
{
	RESOURCE_CLASS * iter = first_resource_class;
	while(iter!=NULL && iter!=ptr)
		iter = iter->next_resource_class;
	return iter!=NULL;
}

//---------------------------------------------------------
