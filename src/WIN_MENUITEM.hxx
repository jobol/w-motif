//-------------------------------------------------------------------
// WIN_MENUITEM.hxx
// ---------------
// Class which implements the items of menus
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __win_menunitem_hxx
#define __win_menunitem_hxx
//-------------------------------------------------------------------
#include "macros.h"
#include "WIN_TOGGLEBUTTON.hxx"
//-------------------------------------------------------------------
class WIN_MENU;
//-------------------------------------------------------------------
class WIN_MENUITEM : public WIN_TOGGLEBUTTON
{
	DECLARE_RESOURCE_CLIENT(WIN_MENUITEM)
		// use the resource mechanism

public:

	int item_id;
		// item ID of this

	bool mapped;
		// is this mapped?

protected:

	~WIN_MENUITEM();
		// destruction

public:

	WIN_MENUITEM();
		// construction

	virtual bool create_window()
		// function called to create window
		// don't create window for menu items
		{ return true; }

	virtual void set_text(const char* txt);
		// replaces all the text by "txt"

	virtual char* get_text() const;
		// gets all the text

	virtual LRESULT callback(UINT uMsg,WPARAM wParam,LPARAM lParam);
		// the "this" WINDOW callback

	void fill_menu_info_struct(MENUITEMINFO &menu_info) const;
		// fill the menu_info accordling to this

	virtual void redraw() const;
		// redraw the item

	void activate();
		// primitive activate callback

	virtual void set_name_and_parent(const char *name,WIN_COMPOSITE *parent);
		// store a widget name and its parent

	virtual void set_geometry(int x,int y,int width,int heigth) {}
		// apply unconditionnaly the requested geometry

	WIN_MENU * get_parent_menu() const
		// returns the parent option menu
		{ return (WIN_MENU*)get_parent_win(); }

	virtual void set_sensitive(bool value);
		// set attribute sensitive

	virtual void set_state(bool state);
		// set state of the Toggle (checked or unchecked)

	virtual void change_state();
		// change state of the Toggle (checked or unchecked)

	virtual void manage();
		// set this managed

	virtual void unmanage();
		// set this not managed

	virtual void map();
		// set this mapped

	virtual void unmap();
		// set this mapped

	virtual void realize();
		// realise this

	virtual bool is_realized() const;
		// is this realized?

	virtual bool is_mapped() const
		// is this managed?
		{ return mapped; }
};
//-------------------------------------------------------------------
#endif
