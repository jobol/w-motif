//-------------------------------------------------------------------
// READ_RESOURCE_X.hxx
// -------------------
// object that read the X resources files for a RESOURCE_DATABASE
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __read_resource_x_hxx
#define __read_resource_x_hxx
//-------------------------------------------------------------------
#include "macros.h"
#include "SIMPLE_READER.hxx"
#include "SIMPLE_ERROR.hxx"
#include "RESOURCE_DATABASE.hxx"
#include "RESOURCE_READER.hxx"
//-------------------------------------------------------------------
class READ_RESOURCE_X : public RESOURCE_READER
{
public:
	READ_RESOURCE_X() : RESOURCE_READER() {}
		// public constructor

	BOOL read_resource(SIMPLE_READER &simple_reader,
				RESOURCE_DATABASE &resource_database,SIMPLE_ERROR &error);
		// read the resource and return TRUE if OK, FALSE otherwise

	virtual BOOL read_resource(const char *file_name,
				RESOURCE_DATABASE &resource_database,SIMPLE_ERROR &error);
		// read the resource and return TRUE if OK, FALSE otherwise
};
//-------------------------------------------------------------------
#endif

