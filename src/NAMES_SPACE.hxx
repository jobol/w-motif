//---------------------------------------------------------
// NAMES_SPACE.hxx
// ------------------
// object is space of names but could be a space of strings
// in fact it implements a hash table of strings
// J.E.Bollo, M3G (c) 1998
//---------------------------------------------------------
#ifndef __names_space_hxx
#define __names_space_hxx
//---------------------------------------------------------
#include "macros.h"
//---------------------------------------------------------
struct NAME_ENTRY
{
private:
	// very private

	NAME_ENTRY * next_name_entry;
					// link to the next

	int          count_of_refs;
					// counts the number of references to that entry

	int          length_of_string;
					// length of the string that follows

	char         the_string[1];
					// that stucture is of variable length
					// so the_string has at least one char: the ending nul
					// but bytes are allocated to store the string here
					// caution: when debugging you only see the first char

	friend class NAME;
	friend class NAMES_SPACE;
};
//---------------------------------------------------------
class NAME
{
private:

	const NAME_ENTRY *name;
		// the name

	NAME(const NAME_ENTRY *name_entry) : name(name_entry) {}
		// build a new name

	friend class NAMES_SPACE;

public:

	NAME() : name(NULL) {}
		// default constructor

	NAME(const NAME &other) : name(other.name) {}
		// copy constructor

	NAME &operator =(const NAME &other) { name=other.name; return *this; }
		// copy operator

	int get_length() const 
		// retrieve the length of the string
	{
		assert(is_valid(),("can't get_length on null NAME"));
		return name->length_of_string;
	}

	const char * get_string() const
		// retrieving the string
	{
		assert(is_valid(),("can't get_string on null NAME"));
		return name->the_string;
	}

	bool operator ==(const NAME &other) const { return name==other.name; }
		// equality

	bool operator !=(const NAME &other) const { return name!=other.name; }
		// inequality

	bool is_null() const { return name==NULL; }
		// is it a null name?

	bool is_valid() const { return name!=NULL; }
		// is it a valid name?

	operator const char * () const { return get_string(); }
		// automatical conversion to string
};
//---------------------------------------------------------
class NAMES_SPACE
{
private:
	// private members

	NAME_ENTRY ** table;
		// the hash table

	int table_size;
		// size of the table

	int key_of(const char * string, int length) const;
		// compute the key code of the string

private:
	// private functions

	NAME_ENTRY * entry_of_name(NAME name) const
		// the entry for the name
	{
		return (NAME_ENTRY *)(name.name);
	}

public:

	NAMES_SPACE() : table(NULL), table_size(0) {}
		// creation

	NAMES_SPACE(int size) : table(NULL), table_size(0) { set_table_size(size); }
		// creation with size

	~NAMES_SPACE();
		// destruction

	void set_table_size(int size);
		// set the size of the table

	NAME add(const char *string);
		// add a a name of value string to the space

	void remove(NAME name);
		// remove a name from the space

	void remove(const char *string);
		// remove the name of value string from the space

	NAME find(const char *string) const;
		// find a name by it's string value
};
//---------------------------------------------------------
#endif
