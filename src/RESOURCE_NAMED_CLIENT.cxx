//---------------------------------------------------------
// RESOURCE_NAMED_CLIENT.cxx
// -------------------
// polymorphic resource descriptor class
// J.E.Bollo, M3G (c) 1998
//---------------------------------------------------------
#include "RESOURCE_NAMED_CLIENT.hxx"
//---------------------------------------------------------

void RESOURCE_NAMED_CLIENT::make_resource_name(RESOURCE_NAME &resource_name)
{
	resource_name.growing_from_object_to_root();
	RESOURCE_NAMED_CLIENT * current = this;
	while(current!=NULL)
	{
		NAME object_name = current->get_name();
		NAME class_name = current->get_class_name();
		resource_name.grow(object_name,class_name);
		//!!! attention: ce qui suit ne se compile pas (plante du compilo msdev vc 5.??)
		//!!! resource_name.grow(current->get_name(),current->get_class_name());
		current = current->get_parent_resource();
	}
}

//---------------------------------------------------------

void RESOURCE_NAMED_CLIENT::scan_resource_values(const RESOURCE_DATABASE & database)
	// put values from the resources database to this
	// default builds a RESOURCE_NAME
	//   and then builds a RESOURCE_SCANNER
	//   and then calls set_resource_values
{
	RESOURCE_NAME resource_name;
	RESOURCE_SCANNER resource_scanner(database);
	make_resource_name(resource_name);
	resource_scanner.scan(resource_name);
	set_resource_values(resource_scanner);
}

//---------------------------------------------------------

