//-------------------------------------------------------------------
// WIN_PUSHBUTTON.hxx
// ---------------
// a class used to test the WINDOW system
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __win_pushbutton_hxx
#define __win_pushbutton_hxx
//-------------------------------------------------------------------
#include "macros.h"
#include "WIN_LABEL.hxx"
//-------------------------------------------------------------------
class WIN_PUSHBUTTON : public WIN_LABEL
{
	DECLARE_RESOURCE_CLIENT(WIN_PUSHBUTTON)
		// use the resource mechanism

	DECLARE_WINDOW(WIN_PUSHBUTTON)
		// use window class mechanism

protected:

	~WIN_PUSHBUTTON();
		// destruction

public:

	WIN_PUSHBUTTON();
		// construction

	virtual LRESULT callback(UINT uMsg,WPARAM wParam,LPARAM lParam);
		// the "this" WINDOW callback

	virtual LONG get_windows_style() const;
		// retrieve the windows style used to create the widget 

	virtual bool create_control();
		// function called to create the embedded control
		// return true if creation process should continue, false otherwise

	XtGeometryResult query_geometry (XtWidgetGeometry *request, 
										 XtWidgetGeometry *prefered);
		// The method query_geometry called when the parent  
		// wants to know the widget's prefered size

	virtual void activate();
		// primitive activate callback
};
//-------------------------------------------------------------------
#endif
