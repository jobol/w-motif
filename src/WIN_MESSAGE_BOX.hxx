//-------------------------------------------------------------------
// WIN_MESSAGE_BOX.hxx
// ---------------
// implements the "XmMessageBox" class
// J.E.Bollo & G. Musset, M3G (c) 1998
//
// This class is implemented only for supporting attributes
// XmNokLabelString, XmNcancelLabelString and XmNhelpLabelString
// but does not create a real window
//-------------------------------------------------------------------
#ifndef __win_message_box_hxx
#define __win_message_box_hxx
//-------------------------------------------------------------------
#include "macros.h"
#include "WIN_BULLETIN_BOARD.hxx"
//-------------------------------------------------------------------
class WIN_MESSAGE_BOX : public WIN_BULLETIN_BOARD
{
	DECLARE_RESOURCE_CLIENT(WIN_MESSAGE_BOX)
		// use the resource mechanism

	DECLARE_WINDOW(WIN_MESSAGE_BOX)
		// use window class mechanism

public:

	char * ok_label_string;
		// text of "Ok" button

	char * cancel_label_string;
		// text of "Cancel" button

	char * help_label_string;
		// text of "Help" button

protected:

	~WIN_MESSAGE_BOX();
		// destruction

public:

	WIN_MESSAGE_BOX();
		// construction

	virtual bool create_window();
		// create the window associated to the object

	virtual LONG get_windows_style() const;
		// retrieve the windows style used to create the widget 


	// --------------- Attributs management methods -------------------

	int internal_get_values
		// function used internally to get attribut values 
		// should not be called directly (call get_values)
		// arglist is the array of the values to get
		// count is the count of values to be put into arglist
		// got is an array that flag if an attribut have been put
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs put
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		) const;

	int internal_set_values
		// function used internally to set attribut values 
		// should not be called directly (call set_values)
		// arglist is the array of the values to be set,
		// count is the count of values in arglist
		// got is an array that flag if an attribut have been set
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs set
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		);
};
//-------------------------------------------------------------------
#endif
