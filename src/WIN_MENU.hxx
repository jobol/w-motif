//-------------------------------------------------------------------
// WIN_MENU.hxx
// ---------------
// define a class which defines the menus
// J.E.Bollo & G. Musset, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __win_menu_hxx
#define __win_menu_hxx
//-------------------------------------------------------------------
#include "macros.h"
#include "WIN_ROWCOLUMN.hxx"
#include "WIN_MENUITEM.hxx"
//-------------------------------------------------------------------
class WIN_SUBMENU;
//-------------------------------------------------------------------
class WIN_MENU : public WIN_ROWCOLUMN
{
	DECLARE_VIRTUAL_RESOURCE_CLIENT(WIN_MENU)
		// use the resource mechanism

public:

	HMENU menu_handle;
		// WINDOWS menu handle of this

	bool mapped;
	// is this mapped?

protected:

	~WIN_MENU();
		// destruction

public:

	WIN_MENU();
		// construction

	LONG get_windows_style() const;	
		// returns the styles for the embedded control

	virtual void redraw() const;
		// redraw this

	virtual LRESULT callback(UINT uMsg,WPARAM wParam,LPARAM lParam);
		// the "this" WINDOW callback

	virtual bool create_window();
		// create a window

	virtual void resize();
		// The method resize called every time a widget changes size

	virtual void insert_child (WIN_CORE * child);
		// insert a child in the children array

	virtual void delete_child (WIN_CORE * child);
		// delete a child in the children array

	virtual void set_name_and_parent(const char *name,WIN_COMPOSITE *parent) = 0;
		// store a widget name and its parent

	// --------------------- management functions ------------------------

	virtual void map();
		// set this mapped

	virtual void unmap();
		// set this mapped

	virtual bool is_realized() const
		// is this realized?
		{ return menu_handle!=NULL; }

	virtual bool is_mapped() const
		// is this managed?
		{ return mapped; }

	// --------------- Geometry management methods -------------------

	virtual void change_managed() {} 
		// The method change_manager called once during the realize phase and then
		// every time that one of its children becomes managed or unmanaged
		
	virtual XtGeometryResult geometry_manager(
									WIN_CORE *child,
									XtWidgetGeometry * request,
									XtWidgetGeometry * geometry_return)
		// The method geometry_manager called every time  
		// one of its children requests a new size
		{ return XtGeometryYes; }

	// ------------------ option item management --------------------

	virtual HMENU create_menu() const = 0;
		// return a new menu handle for this

	bool is_item_visible(const WIN_CORE * item) const;
		// is the given child item visible

	void show_menu_item(const WIN_CORE * item);
		// show the given menu item

	void hide_menu_item(const WIN_CORE * item);
		// hide the given menu item

	int get_index_of_item(const WIN_CORE * item) const;
		// get the combox index of the item

	WIN_CORE * get_item_of_index(int index) const;
		// get the item for an index in the combox

	void update_item(const WIN_CORE * item);
		// change the item of this
	
	void update_map_item(WIN_MENUITEM * item,bool map);
		// change the mapping for item of this
	
	void update_manage_item(WIN_MENUITEM * item,bool manage);
		// change the management for item of this
	
	void update_sensitive_item(WIN_MENUITEM * item,bool sensitive);
		// change the sensitivity for item of this

	void update_map_submenu(WIN_SUBMENU * submenu,bool map);
		// change the mapping for submenu of this
	
	void update_manage_submenu(WIN_SUBMENU * submenu,bool manage);
		// change the management for submenu of this
	
	void update_sensitive_submenu(WIN_SUBMENU * submenu,bool sensitive);
		// change the sensitivity for submenu of this
};
//-------------------------------------------------------------------
#endif
