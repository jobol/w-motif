//-------------------------------------------------------------------
// WIN_LIST.cxx
// ---------------
// implements the Motif class "XmList"
// horizontal scrolls are not implemented
// J.E.Bollo & Maryl�ne, M3G (c) 1998
//-------------------------------------------------------------------
#include "WIN_LIST.hxx"
#include "WINDOWS_MANAGER.hxx"
#include "win_naming.h"
#include "windows_commons.h"
//-------------------------------------------------------------------
BEGIN_RES_DESC(WIN_LIST,WIN_PRIMITIVE,NAME_OF_WIN_LIST_CLASS)
	RES_DESC_ATR(XmNitemCount,			INTEGER,	WIN_LIST, item_count),
	RES_DESC_ATR(XmNitems,				TEXT_TABLE,	WIN_LIST, items),
	RES_DESC_ATR(XmNlistSizePolicy,		ENUM,		WIN_LIST, list_size_policy),
//	RES_DESC_ATR(XmNselectedItemCount,	INTEGER,	WIN_LIST, selected_item_count),
//	RES_DESC_ATR(XmNselectedItems,		TEXT_TABLE,	WIN_LIST, selected_items),
	RES_DESC_ATR(XmNselectionPolicy,	ENUM,		WIN_LIST, selection_policy),
	RES_DESC_ATR(XmNscrollVertical,		BOOLEA,		WIN_LIST, scroll_vertical),
	RES_DESC_ATR(XmNtopItemPosition,	INTEGER,	WIN_LIST, top_item_position),
	RES_DESC_ATR(XmNvisibleItemCount,	INTEGER,	WIN_LIST, visible_item_count),
END_RES_DESC
//-------------------------------------------------------------------
IMPLEMENT_WINDOW(WIN_LIST)
//-------------------------------------------------------------------
ATOM WIN_LIST::register_window_class() const
{
	WNDCLASSEX wce;
	WINDOWS_MANAGER::get_window_class_info("LISTBOX",&wce);
	return WINDOWS_MANAGER::register_window_class(
					(const char*)get_class_name(),wce.style);
}
//-------------------------------------------------------------------
WIN_LIST::WIN_LIST()
  : WIN_PRIMITIVE()
  , item_count(0)
  , items(0)
  , list_size_policy(XmVARIABLE)
  , selection_policy(XmBROWSE_SELECT)
  , top_item_position(1)
  , visible_item_count(0)
  , scroll_horizontal(FALSE)
  , scroll_vertical(FALSE)
  , scroll_left_side(FALSE)
  , scroll_top_side(FALSE)
  , prefered_width (0)
{
	traversal_type = XmTAB_GROUP;

	items = (char**)malloc(sizeof(char*));
	assert(items!=NULL,("memory depletion"));
	items[0] = NULL;
}
//-------------------------------------------------------------------
WIN_LIST::~WIN_LIST()
{
	free_text_table (items);
}
//-------------------------------------------------------------------
LONG WIN_LIST::get_windows_style() const
	// retrieve the windows style used to create the widget 
{
	DWORD style = WS_CHILD | WS_VISIBLE | LBS_NOINTEGRALHEIGHT | LBS_NOTIFY;

	// set from selection policy
	//--------------------------
	if (selection_policy==XmMULTIPLE_SELECT)
		style |= LBS_MULTIPLESEL;
	else if (selection_policy==XmEXTENDED_SELECT)
		style |= LBS_EXTENDEDSEL;

	if (scroll_horizontal) style |= WS_HSCROLL;
	if (scroll_vertical) style |= WS_VSCROLL;

	return style;
}
//-------------------------------------------------------------------

bool WIN_LIST::create_control()
{
	// create the LISTBOX window
	hwndCtl = CreateWindowEx(
#ifdef WITH_BORDERS
					WS_EX_CLIENTEDGE,
#else
					0,
#endif
					"LISTBOX",
					"",
					get_windows_style(),
					0, 0,width,height,
					this->hwnd(),
					(HMENU)1,
					WINDOWS_MANAGER::get_application_instance(),
					NULL);
	if (hwndCtl==NULL) 
		return false;

	// simple fill
	int i = 0;
	while (i < item_count)
		SendMessage(hwndCtl, LB_ADDSTRING, 0, (LPARAM)(items[i++])); 
	SendMessage(hwndCtl, LB_SETTOPINDEX, top_item_position-1, 0);

	return true;
}

//-------------------------------------------------------------------
LRESULT WIN_LIST::callback(UINT uMsg,WPARAM wParam,LPARAM lParam)
{
	switch(uMsg)
	{
	case WM_CTLCOLORLISTBOX:
		{
			HDC hdc = (HDC)wParam;
			SetBkMode(hdc, OPAQUE);
			SetTextColor(hdc, foreground);
			SetBkColor(hdc, color_of_brush(background));
			return (LRESULT)background;
		}

	case WM_COMMAND:
		switch(HIWORD(wParam))
		{
		case LBN_SELCHANGE:
			switch(selection_policy)
			{
			default:
			case XmBROWSE_SELECT:   activate_callbacks(XmNbrowseSelectionCallback); break;
			case XmEXTENDED_SELECT: activate_callbacks(XmNextentedSelectionCallback); break;
			case XmMULTIPLE_SELECT: activate_callbacks(XmNmultipleSelectionCallback); break;
			case XmSINGLE_SELECT:   activate_callbacks(XmNsingleSelectionCallback); break;
			}
			break;
		case LBN_DBLCLK:
			activate_callbacks(XmNdefaultActionCallback);
			break;
		}
		return 0;
	}
						
	return WIN_PRIMITIVE::callback(uMsg,wParam,lParam);
}

//-------------------------------------------------------------------

bool WIN_LIST::is_multiple_sel() const
	// returns true when the selection policy stands for multiple selection.
{
	return selection_policy == XmMULTIPLE_SELECT || selection_policy == XmEXTENDED_SELECT;
}

//-------------------------------------------------------------------

int * WIN_LIST::get_selected_pos() const
	// returns an array of integers which contains : 
	// in the first item : the number of selected items in the list
	// after : the positions of selected items
	// WARNING : returns a buffer that the caller must free
{
	int * buf;
	if(!is_realized())
	{
		buf = (int*)malloc(sizeof(int));
		*buf = 0;
		return buf;
	}

	int nb_items = (int)SendMessage(hwndCtl, LB_GETSELCOUNT, 0, 0);
	if (nb_items == LB_ERR)
	{
		// case of single selection list
		//------------------------------
		buf = (int*)malloc(2 * sizeof(int));
		assert (buf != NULL, ("memory depletion"));
	
		buf[1] = (int)SendMessage(hwndCtl, LB_GETCURSEL, 0, 0);
		buf[0] = buf[1]!=LB_ERR;
			// buf[0] = 0 if no selection; buf[0]=1 if buf[1] == the item selected
	}
	else
	{
		// case of multiple selection list
		//--------------------------------
		buf = (int*)malloc(2 * (nb_items + 1)*sizeof(int));
		assert (buf != NULL, ("memory depletion"));

		buf[0] = SendMessage(hwndCtl, LB_GETSELITEMS, nb_items, (LPARAM)(buf+1)); 
	}

	// transforme les index
	nb_items = buf[0];
	while (nb_items > 0) 
		buf[nb_items--]++;

	return buf;
}

//-------------------------------------------------------------------

void WIN_LIST::select_pos(int position, int notify)
	// select in the list the item which position is "position"
	// if position is 0, selects the last item
{
	if(!is_realized())
		return;

	if(position==0)
		position = item_count;

	if (position<=0 || position>item_count)
		return;

	position--;
	if (is_multiple_sel())
		SendMessage(hwndCtl, LB_SETSEL, true, position);
	else
		SendMessage(hwndCtl, LB_SETCURSEL, position, 0);	

	if (notify)
		callback(WM_COMMAND,MAKELONG(1,LBN_SELCHANGE),(LPARAM)hwndCtl);
}

//-------------------------------------------------------------------

void WIN_LIST::delete_all_items()
	// deletes all items in a list
{
	// clears the window
	if(is_realized())
		SendMessage(hwndCtl, LB_RESETCONTENT, 0, 0);

	// clears the items
	while (item_count!=0)
	{
		item_count--;
		char * item = items[item_count];
		items[item_count] = NULL;
		assert(item!=NULL,("internal null item"));
		free (item);
	}
}

//-------------------------------------------------------------------

int WIN_LIST::pos_of_item(const char* txt) const
	// returns the position of the item "txt", 
	// or 0 if txt is not an item of the list
{
	int pos = item_count;
	while (pos>0 && strcmp(txt,items[pos-1])!=0)
		pos--;
	return pos;
}

//-------------------------------------------------------------------

char* WIN_LIST::item_of_pos (int position) const
	// returns the text of the item which position is "position"
	// if position=0, returns the last item
	// WARNING : returns a buffer that the caller must free
{
	if (position == 0)
		position = item_count;

	if (position<=0 || position>item_count)
		return NULL;

	return copy_of_text(items[position-1]);
}


//-------------------------------------------------------------------

int WIN_LIST::add_item_pos (int position, const char *txt)
	// adds the item txt in the list at the position "position"
	// when position is 0 the txt is added at the end of the list
	// return 0 when ok and 1 otherwise
{
	// control
	if(txt==NULL || position<0 || position>item_count)
		return 1;

	// grows the items table
	char ** tt = (char**) realloc (items, (2+item_count)*sizeof(char*));
	if (tt == NULL)
		return 1;
	items = tt;

	// copy the text
	char * txt2 = copy_of_text(txt);
	if (txt2 == NULL)
		return 1;

	// insert the item
	item_count++;
	if (position == 0)
		position = item_count;
	int i = item_count;
	while (i>=position)
	{
		items[i] = items[i-1];
		i--;
	}
	items[i] = txt2;

	// if realized update the window
	if(is_realized())
	{
		if(SendMessage(hwndCtl,LB_INSERTSTRING,position-1,(LPARAM)txt)==LB_ERR)
			return 1;
		
		int item_width = width_of_item(txt);
		if (item_width > prefered_width)
			prefered_width = item_width;
		
		if (list_size_policy == XmVARIABLE
		|| list_size_policy == XmRESIZE_IF_POSSIBLE)
		{
			if (prefered_width > width)
				update_geometry_request(CWWidth);
		}
	}
	return 0;
}
//-------------------------------------------------------------------

void WIN_LIST::delete_pos(int position) 
	// deletes the item which position is "position"
	// if 0, deletes the last item
{
	// 0 stands for the last
	if(position==0)
		position = item_count;

	// control of validity
	if (position<=0 || position>item_count)
		return;

	// get the item, compute its size 
	char * deleted_item = items[--position];
	assert(deleted_item!=NULL,("internal error"));
	int deleted_width = width_of_item(deleted_item);

	// remove from table
	item_count--;
	int i = position;
	while (i < item_count)
	{
		items[i] = items[i+1];
		i++;
	}
	free(deleted_item);

	// update window if needed
	if(is_realized())
	{
		SendMessage(hwndCtl, LB_DELETESTRING, position - 1, 0);
		if (deleted_width == prefered_width)
			// We have deleted the widthest item of the list
			// What is the next widthest item of the list ?
		{
			int max = 0, ii;
			
			for (ii = 1; ii <= item_count ; ii++)
			{
				char* current_item = item_of_pos(ii);
				int item_width = width_of_item(current_item);
				if (item_width > max)
					max = item_width;
				delete current_item;
			}
			prefered_width = max;

			if (list_size_policy == XmRESIZE_IF_POSSIBLE)
				update_geometry_request(CWWidth);
		}
	}	
}

//-------------------------------------------------------------------

int WIN_LIST::replace_item_pos(int position, const char *txt)
	// replaces the item of position "position" by the item "txt"
{
	if (position<0 || position>item_count)
		return 1;

	if (position==item_count)
		position = 0;

	delete_pos(position);
	return add_item_pos(position,txt);
}

//-------------------------------------------------------------------

int WIN_LIST::get_selected_item_count() const
	// returns the number of selected items
{
	if(!is_realized())
		return 0;

	LRESULT resu = SendMessage(hwndCtl, LB_GETSELCOUNT, 0, 0);
	if (resu == LB_ERR)
	{
		// case of single selection list
		//------------------------------
		return  SendMessage(hwndCtl, LB_GETCURSEL, 0, 0) == LB_ERR ? 0 : 1;
	}
	// case of multiple selection list
	//--------------------------------
	return (int)resu;
}

//-------------------------------------------------------------------

char* WIN_LIST::get_ith_selected_item(int position) const
	// returns the text of the selected item at the position "position" 
	//(in the list of selected items)
	// WARNING : returns a buffer that the caller must free
{
	char* txt = NULL;

	if(is_realized() && position>=1 && position<=item_count)
	{
		int* sel_pos = get_selected_pos();
		if (sel_pos != NULL)
		{
			// sel_pos[0] contains the number of selected items
			if (position <= sel_pos[0] && position > 0)
				txt = item_of_pos(sel_pos[position]);

			free(sel_pos);
		}
	}
	return txt;
}

//-------------------------------------------------------------------

void WIN_LIST::set_items(char const * const * txt)
	// set all items in the list
{
	assign_copy_of_text_table (&items, txt);
	item_count = text_table_count (items);

	if(is_realized())
	{
		SendMessage(hwndCtl, LB_RESETCONTENT, 0, 0); 
		int i = 0;
		while (i < item_count)
			SendMessage(hwndCtl, LB_ADDSTRING, 0, (LPARAM)(items[i++])); 
	}
}

//-------------------------------------------------------------------

char** WIN_LIST::get_items() const
	// get all items in the list
{
	return items;
}

//-------------------------------------------------------------------

void WIN_LIST::set_selection_policy(int value)
	// set the selection policy of a list
	// (single, multiple or extended)
{
	// ! WARNING not dynamic
	if(!is_realized())
		selection_policy = value;
}

//-------------------------------------------------------------------

void WIN_LIST::set_visible_item_count (int value)
	// adjust the height of the list to
	// show the given number of items
{
	visible_item_count = value;
	update_geometry_request(CWHeight);
}

//-------------------------------------------------------------------

int WIN_LIST::get_top_item_position () const
	// returns the index (based 1) of the item which is at the top of the list
{
	if(is_realized())
		((volatile WIN_LIST*)this)->top_item_position = 1 + SendMessage(hwndCtl,LB_GETTOPINDEX,0, 0);
	return top_item_position;
}

//-------------------------------------------------------------------

void WIN_LIST::set_top_item_position (int top_index)
	// puts the item which index (based 1) is "top_index" at the top of the list
{
	if (top_index<=0 || top_index>item_count)
		return;

	top_item_position = top_index;
	
	if(is_realized())
		SendMessage(hwndCtl,LB_SETTOPINDEX, top_item_position-1, 0);
}

//-------------------------------------------------------------------

void WIN_LIST::get_prefered_size(SIZE &result)
	// computes the prefered size of this (from rows & columns) in result
{
	window_multiline_text_extend (hwndCtl, "M", font_list, &result);

	if(visible_item_count!=0)
		result.cy *= visible_item_count;
#ifdef WITH_BORDERS
	result.cy += 2*GetSystemMetrics(SM_CYEDGE);
#endif
//	if (scroll_horizontal == true) 
//		result.cy += GetSystemMetrics(SM_CYHSCROLL);

	result.cx = prefered_width;

#ifdef WITH_BORDERS
	result.cx += 2*GetSystemMetrics(SM_CXEDGE);
#endif
	if (scroll_vertical == true) 
		result.cx += GetSystemMetrics(SM_CXHSCROLL);
}

//-------------------------------------------------------------------

XtGeometryResult WIN_LIST::query_geometry (XtWidgetGeometry *request, 
										 XtWidgetGeometry *prefered)
	// The method query_geometry called when the parent  
	// wants to know the widget's prefered size
{
	SIZE size;
	XtWidgetGeometry pref;
	pref.request_mode = 0;

	get_prefered_size(size);

	pref.request_mode = CWWidth|CWHeight;
	pref.width = size.cx;
	pref.height = size.cy;

	return complete_query_geometry(request,prefered,&pref);
}

//-------------------------------------------------------------------

int WIN_LIST::width_of_item(const char* txt_item)
	// returns the width of an item
{
	SIZE size;
	window_multiline_text_extend (hwndCtl, txt_item, font_list, &size);
	return size.cx;
}


//-------------------------------------------------------------------

int WIN_LIST::internal_get_values
	// function used internally to get attribut values 
	// should not be called directly (call get_values)
	// arglist is the array of the values to get
	// count is the count of values to be put into arglist
	// got is an array that flag if an attribut have been put
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs put
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) const
{
	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.

		if(equality(attribut,XmNitemCount))
			arglist[index].value.pvalue->integer = item_count;
		else
		if(equality(attribut,XmNlistSizePolicy))
			arglist[index].value.pvalue->integer = list_size_policy;
		else
		if(equality(attribut,XmNitems))
			arglist[index].value.pvalue->texttable = get_items(); 
		else
		if(equality(attribut,XmNselectionPolicy))
			arglist[index].value.pvalue->integer = selection_policy;
		else
		if(equality(attribut,XmNtopItemPosition))
			arglist[index].value.pvalue->integer = get_top_item_position();
		else
		if(equality(attribut,XmNvisibleItemCount))
			arglist[index].value.pvalue->integer = visible_item_count;
		else
		if(equality(attribut,XmNscrollHorizontal))
			arglist[index].value.pvalue->boolean = scroll_horizontal;
		else
		if(equality(attribut,XmNscrollVertical))
			arglist[index].value.pvalue->boolean = scroll_vertical;
		else
		if(equality(attribut,XmNscrollLeftSide))
			arglist[index].value.pvalue->boolean = scroll_left_side;
		else
		if(equality(attribut,XmNscrollTopSide))
			arglist[index].value.pvalue->boolean = scroll_top_side;
		else
			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
	}

	// return total count of found attributs (including ancestors)
	return found_count + WIN_PRIMITIVE::internal_get_values(arglist,count,got,equality) ;
}


//-------------------------------------------------------------------

int WIN_LIST::internal_set_values
	// function used internally to set attribut values 
	// should not be called directly (call set_values)
	// arglist is the array of the values to be set,
	// count is the count of values in arglist
	// got is an array that flag if an attribut have been set
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs set
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) 
{
	// empty geometry request
	bool redraw_needed = false;

	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.

		if(equality(attribut,XmNitemCount))
			item_count = arglist[index].value.integer;
		else
		if(equality(attribut,XmNlistSizePolicy))
			set_size_policy(arglist[index].value.integer);
		else
		if(equality(attribut,XmNitems))
			set_items(arglist[index].value.texttable);
		else
		if(equality(attribut,XmNselectionPolicy))
			set_selection_policy(arglist[index].value.integer);
		else
		if(equality(attribut,XmNtopItemPosition))
			set_top_item_position(arglist[index].value.integer);
		else
		if(equality(attribut,XmNvisibleItemCount))
			set_visible_item_count(arglist[index].value.integer);
		else
		if(equality(attribut,XmNscrollHorizontal))
		{
			scroll_horizontal = arglist[index].value.boolean;
			redraw_needed = true;
		}
		else
		if(equality(attribut,XmNscrollVertical))
		{
			scroll_vertical = arglist[index].value.boolean;
			redraw_needed = true;
		}
		else
		if(equality(attribut,XmNscrollLeftSide))
		{
			scroll_left_side = arglist[index].value.boolean;
			redraw_needed = true;
		}
		else
		if(equality(attribut,XmNscrollTopSide))
		{
			scroll_top_side = arglist[index].value.boolean;
			redraw_needed = true;
		}
		else
			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
	}

	if(redraw_needed)
		redraw();

	// return total count of found attributs (including ancestors)
	return found_count + WIN_PRIMITIVE::internal_set_values(arglist,count,got,equality) ;
}

//-------------------------------------------------------------------
#endif // VERSION_OF_WINDOW != 1
