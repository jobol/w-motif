//-------------------------------------------------------------------
// WIN_SEPARATOR.cxx
// -----------------
// implements a class which defines the Motif class "XmSeparator"
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#include "WIN_SEPARATOR.hxx"
#include "WINDOWS_MANAGER.hxx"
#include "win_naming.h"
#include "WINDOWS_COMMONS.h"
//-------------------------------------------------------------------
const int default_border_thickness = 0;
//-------------------------------------------------------------------
BEGIN_RES_DESC(WIN_SEPARATOR,WIN_CORE,NAME_OF_WIN_SEPARATOR_CLASS)
	RES_DESC_ATR(XmNmargin,			INTEGER,	WIN_SEPARATOR, margin),
	RES_DESC_ATR(XmNorientation,	ENUM,		WIN_SEPARATOR, orientation),
	RES_DESC_ATR(XmNseparatorType,	ENUM,		WIN_SEPARATOR, separator_type),
END_RES_DESC
//-------------------------------------------------------------------
IMPLEMENT_WINDOW(WIN_SEPARATOR)
//-------------------------------------------------------------------

WIN_SEPARATOR::WIN_SEPARATOR()
 : WIN_CORE()
 , margin(0)
 , orientation(XmHORIZONTAL)
 , separator_type(XmSHADOW_ETCHED_IN)
{
	traversal_on = false;
}

//-------------------------------------------------------------------
WIN_SEPARATOR::~WIN_SEPARATOR()
{
}

//-------------------------------------------------------------------
ATOM WIN_SEPARATOR::register_window_class() const
{
	return WINDOWS_MANAGER::register_window_class(
					(const char*)get_class_name(),CS_VREDRAW|CS_HREDRAW,
					LoadCursor(NULL,IDC_ARROW));
}

//-------------------------------------------------------------------

LONG WIN_SEPARATOR::get_windows_style() const
	// retrieve the windows style used to create the widget 
{
	return WS_CHILD|WS_VISIBLE;
}

//---------------------------------------------------------

bool WIN_SEPARATOR::create_window()
{
	return WIN_CORE::create_window(get_windows_style());
}

//-------------------------------------------------------------------

int WIN_SEPARATOR::internal_get_values
	// function used internally to get attribut values 
	// should not be called directly (call get_values)
	// arglist is the array of the values to get
	// count is the count of values to be put into arglist
	// got is an array that flag if an attribut have been put
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs put
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) const
{
	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.
		if(equality(attribut,XmNmargin))
			arglist[index].value.pvalue->integer = margin;
		else
		if(equality(attribut,XmNorientation))
			arglist[index].value.pvalue->integer = orientation;
		else
		if(equality(attribut,XmNseparatorType))
			arglist[index].value.pvalue->integer = separator_type;
		else
			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
	}

	// return total count of found attributs (including ancestors)
	return found_count + WIN_CORE::internal_get_values(arglist,count,got,equality);
}

//-------------------------------------------------------------------

int WIN_SEPARATOR::internal_set_values
	// function used internally to set attribut values 
	// should not be called directly (call set_values)
	// arglist is the array of the values to be set,
	// count is the count of values in arglist
	// got is an array that flag if an attribut have been set
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs set
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) 
{
	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.
		if(equality(attribut,XmNmargin))
			margin = arglist[index].value.integer;
		else
		if(equality(attribut,XmNorientation))
			orientation = arglist[index].value.integer;
		else
		if(equality(attribut,XmNseparatorType))
			separator_type = arglist[index].value.integer;
		else
			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
	}

	// redraw if necessary
	if(found_count!=0)
		redraw();

	// return total count of found attributs (including ancestors)
	return found_count  + WIN_CORE::internal_set_values(arglist,count,got,equality);
}

//-------------------------------------------------------------------

XtGeometryResult WIN_SEPARATOR::query_geometry (XtWidgetGeometry *request, 
											 XtWidgetGeometry *prefered)
	// The method query_geometry called when the parent  
	// wants to know the widget's prefered size
{
	// default thickness
	int lrg = 2*default_border_thickness;

	// thickness from separator type
	switch(separator_type)
	{
	case XmNO_LINE:
		break;
	case XmSINGLE_LINE:
	case XmSINGLE_DASHED_LINE:
		lrg += 1;
		break;
	case XmDOUBLE_LINE:
	case XmDOUBLE_DASHED_LINE:
		lrg += 3;
		break;
	case XmSHADOW_ETCHED_OUT:
	case XmSHADOW_ETCHED_IN:
	default:
		lrg += 2;
		break;
	}

	// build internal prefered size
	XtWidgetGeometry pref;
	switch(orientation)
	{
	case XmVERTICAL:
		pref.request_mode = CWWidth;
		pref.width = lrg;
		break;

	case XmHORIZONTAL:
	default:
		pref.request_mode = CWHeight;
		pref.height = lrg;
		break;
	}

	return complete_query_geometry(request,prefered,&pref);
}

//-------------------------------------------------------------------

void WIN_SEPARATOR::do_paint(HDC hdc,const RECT *rect) const
	// paint in response to a WM_PAINT
{
	// erase background
	WIN_CORE::do_paint(hdc,rect);

	// return if nothing to draw
	if(separator_type==XmNO_LINE)
		return;

	// computes the pens

	int pen_style;
	
	switch(separator_type)
	{
	case XmSINGLE_LINE:
	case XmDOUBLE_LINE:
	case XmSHADOW_ETCHED_OUT:
	case XmSHADOW_ETCHED_IN:
	default:
		pen_style = PS_SOLID;
		break;
	case XmSINGLE_DASHED_LINE:
	case XmDOUBLE_DASHED_LINE:
		pen_style = PS_DASH;
		break;
	}

	LOGBRUSH logbr;
	GetObject(background,sizeof(logbr),&logbr);

	HPEN lighted = CreatePen(pen_style,0,enlight_color(logbr.lbColor,224));
	HPEN darked = CreatePen(pen_style,0,enlight_color(logbr.lbColor,96));

	// draw

	int pos;
	POINT oldpos;
	HGDIOBJ oldpen = SelectObject(hdc,darked);
	COLORREF oldbk = SetBkColor(hdc,logbr.lbColor);

	switch(orientation)
	{
	case XmVERTICAL:
		pos = width/2;
		switch(separator_type)
		{
		case XmSINGLE_LINE:
		case XmSINGLE_DASHED_LINE:
			MoveToEx(hdc,pos,margin,&oldpos);
			LineTo(hdc,pos,height-margin);
			break;

		case XmDOUBLE_LINE:
		case XmDOUBLE_DASHED_LINE:
			MoveToEx(hdc,pos-1,margin,&oldpos);
			LineTo(hdc,pos-1,height-margin);
			MoveToEx(hdc,pos+1,margin,NULL);
			LineTo(hdc,pos+1,height-margin);
			break;

		case XmSHADOW_ETCHED_OUT:
			MoveToEx(hdc,pos,margin,&oldpos);
			LineTo(hdc,pos,height-margin);
			SelectObject(hdc,lighted);
			MoveToEx(hdc,pos-1,margin,NULL);
			LineTo(hdc,pos-1,height-margin);
			break;

		case XmSHADOW_ETCHED_IN:
		default:
			MoveToEx(hdc,pos-1,margin,&oldpos);
			LineTo(hdc,pos-1,height-margin);
			SelectObject(hdc,lighted);
			MoveToEx(hdc,pos,margin,NULL);
			LineTo(hdc,pos,height-margin);
			break;
		}
		break;

	case XmHORIZONTAL:
	default:
		pos = height/2;
		switch(separator_type)
		{
		case XmSINGLE_LINE:
		case XmSINGLE_DASHED_LINE:
			MoveToEx(hdc,margin,pos,&oldpos);
			LineTo(hdc,width-margin,pos);
			break;

		case XmDOUBLE_LINE:
		case XmDOUBLE_DASHED_LINE:
			MoveToEx(hdc,margin,pos-1,&oldpos);
			LineTo(hdc,width-margin,pos-1);
			MoveToEx(hdc,margin,pos+1,NULL);
			LineTo(hdc,width-margin,pos+1);
			break;

		case XmSHADOW_ETCHED_OUT:
			MoveToEx(hdc,margin,pos,&oldpos);
			LineTo(hdc,width-margin,pos);
			SelectObject(hdc,lighted);
			MoveToEx(hdc,margin,pos-1,NULL);
			LineTo(hdc,width-margin,pos-1);
			break;

		case XmSHADOW_ETCHED_IN:
		default:
			MoveToEx(hdc,margin,pos-1,&oldpos);
			LineTo(hdc,width-margin,pos-1);
			SelectObject(hdc,lighted);
			MoveToEx(hdc,margin,pos,NULL);
			LineTo(hdc,width-margin,pos);
			break;
		}
		break;
	}

	// limpiar
	MoveToEx(hdc,oldpos.x,oldpos.y,NULL);
	SelectObject(hdc,oldpen);
	SetBkColor(hdc,oldbk);
	DeleteObject(lighted);
	DeleteObject(darked);
}

//-------------------------------------------------------------------
