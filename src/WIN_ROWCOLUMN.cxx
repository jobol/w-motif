//-------------------------------------------------------------------
// WIN_ROWCOLUMN.cxx
// -----------------
// implements the "XmRowColumn" class
// J.E.Bollo & G. Musset, M3G (c) 1998
//-------------------------------------------------------------------
#include "WIN_ROWCOLUMN.hxx"
#include "WINDOWS_MANAGER.hxx"
//-------------------------------------------------------------------
BEGIN_RES_DESC(WIN_ROWCOLUMN,WIN_COMPOSITE,NAME_OF_WIN_ROWCOLUMN_CLASS)
	RES_DESC_ATR(XmNadjustLast,		BOOLEA,		WIN_ROWCOLUMN, adjust_last),
	RES_DESC_ATR(XmNadjustMargin,	BOOLEA,		WIN_ROWCOLUMN, adjust_margin),
	RES_DESC_ATR(XmNentryAlignment,	ENUM,		WIN_ROWCOLUMN, entry_alignment),
	RES_DESC_ATR(XmNisAligned,		BOOLEA,		WIN_ROWCOLUMN, is_aligned),
	RES_DESC_ATR(XmNmarginWidth,	INTEGER,	WIN_ROWCOLUMN, margin_width),
	RES_DESC_ATR(XmNmarginHeight,	INTEGER,	WIN_ROWCOLUMN, margin_height),
	RES_DESC_ATR(XmNnumColumns,		INTEGER,	WIN_ROWCOLUMN, num_columns),
	RES_DESC_ATR(XmNorientation,	ENUM,		WIN_ROWCOLUMN, orientation),
	RES_DESC_ATR(XmNpacking,		ENUM,		WIN_ROWCOLUMN, packing),
	RES_DESC_ATR(XmNradioAlwaysOne,	BOOLEA,		WIN_ROWCOLUMN, radio_always_one),
	RES_DESC_ATR(XmNradioBehavior,	BOOLEA,		WIN_ROWCOLUMN, radio_behavior),
	RES_DESC_ATR(XmNspacing,		INTEGER,	WIN_ROWCOLUMN, spacing),
END_RES_DESC
//-------------------------------------------------------------------
IMPLEMENT_WINDOW(WIN_ROWCOLUMN)
//-------------------------------------------------------------------
ATOM WIN_ROWCOLUMN::register_window_class() const
{
	return WINDOWS_MANAGER::register_window_class(
					(const char*)get_class_name(),0,
					LoadCursor(NULL,IDC_ARROW));
}
//-------------------------------------------------------------------
WIN_ROWCOLUMN::WIN_ROWCOLUMN()
	: WIN_COMPOSITE()
	, adjust_last(true)
	, adjust_margin(true)
	, entry_alignment(XmALIGNMENT_BEGINNING)
	, is_aligned(true)
	, margin_width(3)
	, margin_height(3)
	, num_columns(1)
	, orientation(XmVERTICAL)
	, packing(XmPACK_TIGHT)
	, radio_always_one(true)
	, radio_behavior(false)
	, spacing(3)
	, requesting_child(NULL)
	, child_geometry_request(NULL)
{
}
//-------------------------------------------------------------------
WIN_ROWCOLUMN::~WIN_ROWCOLUMN()
{
}
//-------------------------------------------------------------------
LRESULT WIN_ROWCOLUMN::callback(UINT uMsg,WPARAM wParam,LPARAM lParam)
{
	switch(uMsg)
	{
	case WM_TOGGLE_STATE_CHANGE:
		if (radio_behavior)
		{
			radio_behavior = false;	// prevents recurssivity
			bool ok = manage_radio_buttons((WIN_TOGGLEBUTTON *)wParam);
			radio_behavior = true;
			return ok ? 0 : 1;
		}
		return 0;
	}

	return WIN_COMPOSITE::callback(uMsg,wParam,lParam);
}

//-------------------------------------------------------------------
LONG WIN_ROWCOLUMN::get_windows_style() const
	// retrieve the windows style used to create the widget 
{
	return WS_CHILD|WS_CLIPSIBLINGS|WS_VISIBLE|WS_CLIPCHILDREN;
}
//-------------------------------------------------------------------
bool WIN_ROWCOLUMN::create_window()
{
	return WIN_CORE::create_window(get_windows_style());
}
//-------------------------------------------------------------------
void WIN_ROWCOLUMN::insert_child (WIN_CORE * child)
	// method for child insertion specific to the RowColumn
{
	// call standard method for child insertion
	WIN_COMPOSITE::insert_child (child);

	// ToggleButtons become RadioButtons if the RowColumn has radioBehavior 
	if (radio_behavior 
	&&  child->is_kind_of(WIN_TOGGLEBUTTON::the_resource_class()))
	{
		((WIN_TOGGLEBUTTON *)child)->indicator_type = XmONE_OF_MANY;
	}
}

//-------------------------------------------------------------------
void WIN_ROWCOLUMN::change_managed()
	// The method change_manager called once during the realize phase and then
	// every time that one of its children becomes managed or unmanaged
{
	do_layout(true,NULL,NULL,NULL);
}
//-------------------------------------------------------------------
XtGeometryResult WIN_ROWCOLUMN::geometry_manager(
	WIN_CORE			*child,
	XtWidgetGeometry	*request,
	XtWidgetGeometry	*geometry_return
) 
	// The method geometry_manager called every time  
	// one of its children requests a new size or position
{
	assert(requesting_child==NULL,("internal error"));

	XtWidgetGeometry resulting_geometry;
	if (request != NULL && geometry_return != NULL)
		*geometry_return = *request;

	// compute new size
	if(do_layout(false,child,geometry_return,&resulting_geometry))
	{

		requesting_child = child;
		child_geometry_request = request;
		// if geometry change then query to do it in fact
		if(geometry_request(&resulting_geometry)==XtGeometryNo)
		{
			requesting_child = NULL;
			child_geometry_request = NULL;
			// no if growing
			if((resulting_geometry.request_mode & CWWidth)!=0
				&& resulting_geometry.width>width && child != NULL)
				return XtGeometryNo;
			if((resulting_geometry.request_mode & CWHeight)!=0
 				&& resulting_geometry.height>height && child != NULL)
				return XtGeometryNo;
			do_layout(true, child, geometry_return, &resulting_geometry);
		}
	}
	requesting_child = NULL;
	child_geometry_request = NULL;
	
	return XtGeometryDone;
}



//-------------------------------------------------------------------

bool WIN_ROWCOLUMN::get_children_geometry
(
	int first,
	int last,
	WIN_CORE		 *the_child,
	XtWidgetGeometry *request,
	XtWidgetGeometry *result
)
	// This function returns characteristics about children geometry
	// values returned in result structure are :
	//	x = maximum width of children
	//	y = maximum height of children
	//	width = sum of width of all children
	//	height = sum of height of all children
{

	if (result == NULL) return false;

	int max_width = 0;
	int max_height = 0;
	int total_width = 0;
	int total_height = 0;

	// loop on all children
	for (int i=first; i<=last; i++)
	{
		WIN_CORE *  child = children[i];
		int width = 0;
		int height = 0;
		XtWidgetGeometry query, wanted;
		query.request_mode = CWWidth | CWHeight;
		query.width = width;
		query.height = height;
		XtGeometryResult response = child->query_geometry(&query,&wanted);
		if(response!=XtGeometryNo)
		{
			if((wanted.request_mode & CWWidth)!=0)
				width = wanted.width;
			if((wanted.request_mode & CWHeight)!=0)
				height = wanted.height;
		}

		if(the_child!=NULL && request!=NULL && the_child==child)
		{
			if((request->request_mode & CWWidth)!=0)
				width = request->width;
			if((request->request_mode & CWHeight)!=0)
				height = request->height;
		}
		max_width = max(max_width, width);
		max_height = max(max_height, height);
		total_width += width;
		total_height += height;
	}
	result->x = max_width;
	result->y = max_height;
	result->width = total_width;
	result->height = total_height;

	return true;
}

//-------------------------------------------------------------------

int WIN_ROWCOLUMN::readjust_children_size(int first, int last)
	// readjust the width (or height if horizontal orientation) of children
	// whose index in children list is comprised between first and last
	// return the width (height) applied to these children
{
	int new_size = 0;

	XtWidgetGeometry children_geometry;
	get_children_geometry(first, last, NULL, NULL, &children_geometry);	

	for (int i=first; i<=last; i++)
	{
		int x = children[i]->x;
		int y = children[i]->y;
		int width = children[i]->width;
		int height = children[i]->height;
		if (orientation == XmVERTICAL)
			width = children_geometry.x;
		else
			height = children_geometry.y;
		children[i]->set_geometry(x, y, width, height);
	}
	if (orientation == XmVERTICAL) return children_geometry.x;
	return children_geometry.y;
}

//-------------------------------------------------------------------

bool WIN_ROWCOLUMN::do_layout
(
	bool			  act,
	WIN_CORE		 *the_child,
	XtWidgetGeometry *request,
	XtWidgetGeometry *result
)
	// do the RowColumn layout
	// return true if geometry need to change
	// if act is true the geometry is changed automatically (by geometry_request)
	// the geometry is returned in result if not NULL
	// it take into account the_child request if both are not NULL
{

	int num_cols = 0;
	int num_rows = 0;

	// make a virtual result if needed
	XtWidgetGeometry internal_result_if_needed;
	if(result==NULL)
		result = &internal_result_if_needed;

	if (the_child == NULL && requesting_child != NULL)
	{
		the_child = requesting_child;
		request = child_geometry_request;
	}

	if (!act)
	{	
		// do a virtual layout and compute the new size
		// of the RowColumn induced by this layout
		//---------------------------------------------

		int new_width = 0;
		int new_height = 0;

		XtWidgetGeometry children_geometry;
		get_children_geometry(the_child, request, &children_geometry);

		if (packing == XmPACK_TIGHT)
		{
			if (orientation == XmVERTICAL)
			{
				new_width = children_geometry.x + 2*margin_width;
				new_height = children_geometry.height + max(num_children-1, 0)*spacing + 2*margin_height;
			}
			else
			{
				new_width = children_geometry.width + max(num_children-1, 0)*spacing  + 2*margin_width;
				new_height = children_geometry.y + 2*margin_height;
			}	
		}
		else if (packing == XmPACK_COLUMN)
		{
			if(num_columns<=0)
				num_columns = 1;
			if (orientation == XmVERTICAL)
			{
				num_cols = num_columns;
				num_rows = (num_children + num_cols - 1)/num_cols;
			}
			else
			{
				num_rows = num_columns;
				num_cols = (num_children + num_rows - 1)/num_rows;
			}
			new_width = children_geometry.x*num_cols 
							+ max(num_cols-1, 0)*spacing + 2*margin_width;
			new_height = children_geometry.y*num_rows 
							+ max(num_rows-1, 0)*spacing + 2*margin_height;
		}

		// check if change and build result
		result->request_mode = 0;
//		if(new_width!=width)
//		{
			result->request_mode = CWWidth;
			result->width = new_width;
//		}
//		if(new_height!=height)
//		{
			result->request_mode |= CWHeight;
			result->height = new_height;
//		}

		// no change
		if(result->request_mode==0)
			return false;
		return true;
	}

	// act == true : do an actual layout to children 
	//----------------------------------------------

	int new_width = 0;
	int new_height = 0;

	XtWidgetGeometry children_geometry;
	get_children_geometry(the_child, request, &children_geometry);
	
	if (packing == XmPACK_TIGHT)
	{
		if (orientation == XmVERTICAL)
		{
			if (adjust_last)
				children_geometry.x = this->width - 2*margin_width;
		}
		else
		{
			if (adjust_last)
				children_geometry.y = this->height - 2*margin_height;
		}
	} 
	else if (packing == XmPACK_COLUMN)
	{
		if(num_columns<=0)
			num_columns = 1;
		if (orientation == XmVERTICAL)
		{
			num_cols = num_columns;
			num_rows = (num_children + num_cols - 1)/num_cols;
		}
		else
		{
			num_rows = num_columns;
			num_cols = (num_children + num_rows - 1)/num_rows;
		}
	}
		
	
	int x = margin_width;
	int y = margin_height;
	int idx_row = 0;
	int idx_col = 0;
	int first_in_current_column = 0;
	int last_in_current_column;
			
	WINDOWS_MANAGER::freeze_changes();
	
	// loop on all children
	for (int i=0; i<num_children; i++)
	{
		last_in_current_column = i;

		WIN_CORE * child = children[i];

		int width = child->width;
		int height = child->height;
/*
		int width = 0;
		int height = 0;
*/

		XtWidgetGeometry query, wanted;
		query.request_mode = CWWidth | CWHeight;
		query.width = width;
		query.height = height;
		XtGeometryResult response = child->query_geometry(&query,&wanted);
		if(response!=XtGeometryNo)
		{
			if((wanted.request_mode & CWWidth)!=0)
				width = wanted.width;
			if((wanted.request_mode & CWHeight)!=0)
				height = wanted.height;
		}
		if(the_child!=NULL && request!=NULL && the_child==child)
		{
			if((request->request_mode & CWWidth)!=0)
				width = request->width;
			if((request->request_mode & CWHeight)!=0)
				height = request->height;
		}
		if (packing == XmPACK_TIGHT) 
		{
			if (orientation == XmVERTICAL)
			{
				// verify that the child is not outside the RowColumn
				// if yes, resize currrent column and place the child 
				// on another column
				//--------------------------------------------------

				if (this->height!=0 && y+height>this->height && 
					last_in_current_column!=first_in_current_column)
				{
					int new_size = readjust_children_size(
										first_in_current_column, 
										last_in_current_column-1);
					y = margin_height;
					x += new_size + margin_width;
					first_in_current_column = i;
					get_children_geometry(
						last_in_current_column, 
						num_children-1, 
						the_child, 
						request, 
						&children_geometry);
					if (adjust_last)
						children_geometry.x = this->width - margin_width - x;
				}

				if (children_geometry.x > 0)
					width = children_geometry.x;

				child->set_geometry(x, y, width, height);
				y += height + spacing;
				
			}
			else
			{
				// verify that the child is not outside the RowColumn
				// if yes, resize currrent row and place the child 
				// on another row
				//--------------------------------------------------

				if (this->width!=0 && x+width>this->width && 
					last_in_current_column!=first_in_current_column)
				{
					int new_size = readjust_children_size(
										first_in_current_column, 
										last_in_current_column-1);
					x = margin_width;
					y += new_size + margin_height;
					first_in_current_column = i;
					get_children_geometry(
						last_in_current_column, 
						num_children-1, 
						the_child, 
						request, 
						&children_geometry);
					if (adjust_last)
						children_geometry.y = this->height - margin_height - y;
				}

				if (children_geometry.y > 0)
					height = children_geometry.y;

				child->set_geometry(x, y, width, height);
				x += width + spacing;
			}
		} 
		else if (packing == XmPACK_COLUMN)
		{
			if (orientation == XmVERTICAL)
			{
				if (idx_row >= num_rows)
				{
					idx_row = 0;
					idx_col++;
					x += children_geometry.x + spacing;
					y = margin_height;
				}
				if (idx_row == 0 && idx_col == num_cols-1 && adjust_last) 
					children_geometry.x = this->width - x - margin_width;

				if (children_geometry.x > 0) 
					width = children_geometry.x;
				else
					width = child->width;

				child->set_geometry(x, y, width, children_geometry.y);
				
				y += children_geometry.y + spacing;
				idx_row++;
			}
			else
			{
				if (idx_col >= num_cols)
				{
					idx_col = 0;
					idx_row++;
					y += children_geometry.y + spacing;
					x = margin_width;
				}
				if (idx_col == 0 && idx_row == num_rows-1 && adjust_last) 
					children_geometry.y = this->height - y - margin_height;

				if (children_geometry.y > 0) 
					height = children_geometry.y;
				else
					height = child->height;

				child->set_geometry(x, y, children_geometry.x, height);
				
				x += children_geometry.x + spacing;
				idx_col++;
			}
		} 
		else
		{
			// cas XmPACK_NONE non traite actuellement
		}
	}

	WINDOWS_MANAGER::unfreeze_changes();
	return true;

}


//-------------------------------------------------------------------

XtGeometryResult WIN_ROWCOLUMN::query_geometry
(
	XtWidgetGeometry *request, 
	XtWidgetGeometry *prefered
)
		// The method query_geometry called when the parent  
		// wants to know the widget's prefered size
{
	XtWidgetGeometry pref;
	do_layout(false,NULL,NULL,&pref);

	return complete_query_geometry(request,prefered,&pref);
}

//-------------------------------------------------------------------

void WIN_ROWCOLUMN::resize()
	// The method resize called every time a widget changes size
{
	do_layout(true, NULL, NULL, NULL);
}

//-------------------------------------------------------------------

bool WIN_ROWCOLUMN::manage_radio_buttons(WIN_TOGGLEBUTTON *calling_button)
	// set state of radio buttons when one of them is changing state
{
	// if button was checked change is enable when radio_always_one is false
	if (calling_button->get_state()) 
		return !radio_always_one;

	// otherwise button was unchecked, so uncheck all other child buttons
	const RESOURCE_CLASS * toggle_class = WIN_TOGGLEBUTTON::the_resource_class();
	int i = 0;
	while(i<num_children)
	{
		if(children[i]!=calling_button)
		{
			if(children[i]->is_kind_of(toggle_class))
			{
				WIN_TOGGLEBUTTON * tog = (WIN_TOGGLEBUTTON *)children[i];
				if(tog->get_state())
					tog->change_state();
			}
		}
		i++;
	}

	// enable the button to change
	return true;
}

//-------------------------------------------------------------------

int WIN_ROWCOLUMN::internal_get_values
	// function used internally to get attribut values 
	// should not be called directly (call get_values)
	// arglist is the array of the values to get
	// count is the count of values to be put into arglist
	// got is an array that flag if an attribut have been put
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs put
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) const
{
	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.

		if(equality(attribut,XmNadjustLast))
			arglist[index].value.pvalue->boolean = adjust_last;
		else
		if(equality(attribut,XmNadjustMargin))
			arglist[index].value.pvalue->boolean = adjust_margin;
		else
		if(equality(attribut,XmNisAligned))
			arglist[index].value.pvalue->boolean = is_aligned;
		else
		if(equality(attribut,XmNradioAlwaysOne))
			arglist[index].value.pvalue->boolean = radio_always_one;
		else
		if(equality(attribut,XmNradioBehavior))
			arglist[index].value.pvalue->boolean = radio_behavior;
		else
		if(equality(attribut,XmNmarginWidth))
			arglist[index].value.pvalue->integer = margin_width;
		else
		if(equality(attribut,XmNmarginHeight))
			arglist[index].value.pvalue->integer = margin_height;
		else
		if(equality(attribut,XmNnumColumns))
			arglist[index].value.pvalue->integer = num_columns;
		else
		if(equality(attribut,XmNspacing))
			arglist[index].value.pvalue->integer = spacing;
		else
		if(equality(attribut,XmNentryAlignment))
			arglist[index].value.pvalue->integer = entry_alignment;
		else
		if(equality(attribut,XmNorientation))
			arglist[index].value.pvalue->integer = orientation;
		else
		if(equality(attribut,XmNpacking))
			arglist[index].value.pvalue->integer = packing;
		else
			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
	}

	// return total count of found attributs (including ancestors)
	return found_count + WIN_COMPOSITE::internal_get_values(arglist,count,got,equality);
}

//-------------------------------------------------------------------

int WIN_ROWCOLUMN::internal_set_values
	// function used internally to set attribut values 
	// should not be called directly (call set_values)
	// arglist is the array of the values to be set,
	// count is the count of values in arglist
	// got is an array that flag if an attribut have been set
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs set
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) 
{
	// empty geometry request
	bool relayout_needed = false;

	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		bool relayout = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.




		if(equality(attribut,XmNadjustLast))
			adjust_last = arglist[index].value.boolean;
		else
		if(equality(attribut,XmNadjustMargin))
			adjust_margin = arglist[index].value.boolean;
		else
		if(equality(attribut,XmNisAligned))
			is_aligned = arglist[index].value.boolean;
		else
		if(equality(attribut,XmNradioAlwaysOne))
		{
			relayout = false;
			radio_always_one = arglist[index].value.boolean;
		}
		else
		if(equality(attribut,XmNradioBehavior))
			radio_behavior = arglist[index].value.boolean;
		else
		if(equality(attribut,XmNmarginWidth))
			margin_width = arglist[index].value.integer;
		else
		if(equality(attribut,XmNmarginHeight))
			margin_height = arglist[index].value.integer;
		else
		if(equality(attribut,XmNnumColumns))
			num_columns = arglist[index].value.integer;
		else
		if(equality(attribut,XmNspacing))
			spacing = arglist[index].value.integer;
		else
		if(equality(attribut,XmNentryAlignment))
			entry_alignment = arglist[index].value.integer;
		else
		if(equality(attribut,XmNorientation))
			orientation = arglist[index].value.integer;
		else
		if(equality(attribut,XmNpacking))
			packing = arglist[index].value.integer;
		else
		{
			relayout = false;
			found = false;
		}

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
		if(relayout)
			relayout_needed = true;
	}

	if(relayout_needed)
		update_geometry_request(CWHeight|CWWidth);

	// return total count of found attributs (including ancestors)
	return found_count + WIN_COMPOSITE::internal_set_values(arglist,count,got,equality);
}

//-------------------------------------------------------------------


