//-------------------------------------------------------------------
// WIN_BULLETIN_BOARD.hxx
// ---------------
// implements the "XmBulletinBoard" class
// J.E.Bollo & G. Musset, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __win_bulletin_board_hxx
#define __win_bulletin_board_hxx
//-------------------------------------------------------------------
#include "macros.h"
#include "WIN_COMPOSITE.hxx"
//-------------------------------------------------------------------
class WIN_BULLETIN_BOARD : public WIN_COMPOSITE
{
	DECLARE_RESOURCE_CLIENT(WIN_BULLETIN_BOARD)
		// use the resource mechanism

	DECLARE_WINDOW(WIN_BULLETIN_BOARD)
		// use window class mechanism

public:

	int margin_width;
		// margin width

	int margin_height;
		// margin height

	HFONT button_font_list;
		// font applicable to buttons contained in the BulletinBoard

	HFONT label_font_list;
		// font applicable to labels contained in the BulletinBoard

	HFONT text_font_list;
		// font applicable to texts contained in the BulletinBoard

protected:

	~WIN_BULLETIN_BOARD();
		// destruction

public:

	WIN_BULLETIN_BOARD();
		// construction

	virtual bool create_window();
		// create the window associated to the object

	virtual LONG get_windows_style() const;
		// retrieve the windows style used to create the widget 

	void set_margin_width(int value)
		// sets attribute margin_width
		{ margin_width = value; }

	void set_margin_height(int value)
		// sets attribute margin_height
		{ margin_height = value; }

	virtual void change_managed();
		// The method change_manager called once during the realize phase and then
		// every time that one of its children becomes managed or unmanaged
		
	virtual XtGeometryResult geometry_manager(WIN_CORE *child,
								  XtWidgetGeometry *request,
								  XtWidgetGeometry *geometry_return); 
		// The method geometry_manager called every time  
		// one of its children requests a new size
		
	bool do_layout
	(
		bool			  act,
		WINDOW			 *the_child,
		XtWidgetGeometry *request,
		XtWidgetGeometry *result
	);
		// do the bulletin board layout
		// return true if geometry need to change
		// if act is true the geometry is changed automatically (by geometry_request)
		// the geometry is returned in result if not NULL
		// it take into account the_child request if both are not NULL

		
	virtual XtGeometryResult query_geometry (XtWidgetGeometry *request, 
											 XtWidgetGeometry *prefered);
		// The method query_geometry called when the parent  
		// wants to know the widget's prefered size


	// --------------- Attributs management methods -------------------

	virtual int internal_get_values
		// function used internally to get attribut values 
		// should not be called directly (call get_values)
		// arglist is the array of the values to get
		// count is the count of values to be put into arglist
		// got is an array that flag if an attribut have been put
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs put
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		) const;

	virtual int internal_set_values
		// function used internally to set attribut values 
		// should not be called directly (call set_values)
		// arglist is the array of the values to be set,
		// count is the count of values in arglist
		// got is an array that flag if an attribut have been set
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs set
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		);
};
//-------------------------------------------------------------------
#endif
