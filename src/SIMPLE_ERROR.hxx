//---------------------------------------------------------
// SIMPLE_ERROR.hxx
// -----------------
// Just a simple error reporting object
// J.E.Bollo, M3G (c) 1998
//---------------------------------------------------------
#ifndef __simple_error_hxx
#define __simple_error_hxx
//---------------------------------------------------------
#include "macros.h"
//---------------------------------------------------------
class SIMPLE_ERROR
	// simple error reporting system
{
private:

	int nr_of_errors;
		// count of errors

	int nr_of_warning;
		// count of warnings

	char * errors_text;
		// text of the reported errors

	int length_of_text;
		// the real length of errors_text

	char * current_context;
		// name of the current context

public:

	SIMPLE_ERROR();
		// construction

	~SIMPLE_ERROR();
		// destruction

	void add_error(int line,int column,const char *label,...);
		// record the error label at line and column

	void add_warning(int line,int column,const char *label,...);
		// record the warning label at line and column

	void add_error(const char *label,...);
		// record the error label at line and column

	void add_warning(const char *label,...);
		// record the warning label at line and column

	void reset();
		// reset all

	int get_error_count() const;
		// get the number of errors

	int get_warning_count() const;
		// get the number of warning

	const char * get_details() const;
		// get the text of all errors and warnings

	void set_file_name(const char * file_name);
		// set the context to file_name

	void set_context(const char * context);
		// set the context

	const char * get_context() const;
		// retrieves the current context

	void print(const char *text,...);
		// print a text in the details

	void vprint(const char *text,va_list valis);
		// print a text in the details
};
//---------------------------------------------------------
#endif
