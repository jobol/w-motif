//---------------------------------------------------------
// RESOURCE_NAMES_SPACE.hxx
// ------------------------
// define the NAMES_SPACE for resources
// J.E.Bollo, M3G (c) 1998
//---------------------------------------------------------
#ifndef __resource_names_space_hxx
#define __resource_names_space_hxx
//---------------------------------------------------------
#include "macros.h"
#include "NAMES_SPACE.hxx"
//---------------------------------------------------------
extern NAMES_SPACE resource_names_space;
//---------------------------------------------------------
#endif
