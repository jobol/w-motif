//-------------------------------------------------------------------
// WIN_PUSHBUTTON.cxx
// ---------------
// implements the Motif class "XmLabel"
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#include "WIN_PUSHBUTTON.hxx"
#include "win_naming.h"
#include "WINDOWS_MANAGER.hxx"
#include "WINDOWS_COMMONS.h"
//-------------------------------------------------------------------
BEGIN_RES_DESC(WIN_PUSHBUTTON,WIN_LABEL,NAME_OF_WIN_PUSHBUTTON_CLASS)
END_RES_DESC
//-------------------------------------------------------------------
IMPLEMENT_WINDOW(WIN_PUSHBUTTON)
//-------------------------------------------------------------------
ATOM WIN_PUSHBUTTON::register_window_class() const
{
	WNDCLASSEX wce;
	WINDOWS_MANAGER::get_window_class_info("BUTTON",&wce);
	return WINDOWS_MANAGER::register_window_class(
					(const char*)get_class_name(),wce.style);
}
//-------------------------------------------------------------------
WIN_PUSHBUTTON::WIN_PUSHBUTTON()
  : WIN_LABEL()
{
	traversal_on = true;
}
//-------------------------------------------------------------------
WIN_PUSHBUTTON::~WIN_PUSHBUTTON()
{
}
//-------------------------------------------------------------------
LONG WIN_PUSHBUTTON::get_windows_style() const
	// retrieve the windows style used to create the widget 
{
	return WS_CHILD | WS_VISIBLE | BS_TEXT | BS_PUSHBUTTON | BS_MULTILINE;
}
//-------------------------------------------------------------------

bool WIN_PUSHBUTTON::create_control()
{
	hwndCtl = CreateWindow(
				"BUTTON",
				label_string,
				get_windows_style(),
				0, 0,width,height,
				this->hwnd(),
				(HMENU)1,
				WINDOWS_MANAGER::get_application_instance(),
				NULL);
	return hwndCtl!=NULL;
}

//-------------------------------------------------------------------

void WIN_PUSHBUTTON::activate()
	// primitive activate callback
{
	activate_callbacks(XmNactivateCallback);
}

//-------------------------------------------------------------------

LRESULT WIN_PUSHBUTTON::callback(UINT uMsg,WPARAM wParam,LPARAM lParam)
{
	switch(uMsg)
	{
	case WM_CTLCOLORBTN:
		{
			HDC hdc = (HDC)wParam;
			SetBkMode(hdc, TRANSPARENT);
			SetTextColor(hdc, foreground);
			return (LRESULT)background;
		}

	case WM_COMMAND:
		{
			activate();
			return 0;
		}

	}
						
	return WIN_LABEL::callback(uMsg,wParam,lParam);
}
//-------------------------------------------------------------------

XtGeometryResult WIN_PUSHBUTTON::query_geometry (XtWidgetGeometry *request, 
										 XtWidgetGeometry *prefered)
	// The method query_geometry called when the parent  
	// wants to know the widget's prefered size
{
	SIZE size;
	XtWidgetGeometry pref;

	window_multiline_text_extend(hwndCtl,label_string,font_list,&size);
	pref.width = size.cx + 2*GetSystemMetrics(SM_CXEDGE) + 8;
	pref.height = size.cy + 2*GetSystemMetrics(SM_CYEDGE) + 4;
	pref.request_mode = CWWidth|CWHeight;

	return complete_query_geometry(request,prefered,&pref);
}

//-------------------------------------------------------------------
