//-------------------------------------------------------------------
// WIN_OPTIONMENU.hxx
// ---------------
// implements a class which defines the Motif widget "XmOptionMenu"
// J.E.Bollo & G. Musset, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __win_optionmenu_hxx
#define __win_optionmenu_hxx
//-------------------------------------------------------------------
#include "macros.h"
#include "WIN_COMPOSITE.hxx"
#include "WIN_OPTIONITEM.hxx"
//-------------------------------------------------------------------
class WIN_OPTIONMENU : public WIN_COMPOSITE
{
	DECLARE_RESOURCE_CLIENT(WIN_OPTIONMENU)
		// use the resource mechanism

	DECLARE_WINDOW(WIN_OPTIONMENU)
		// use the window mechanism
		
public:

	HFONT font_list;
		// the font of the text to display
	
	HWND hwndCtl;
		// handle HWND of the child STATIC control

protected:

	~WIN_OPTIONMENU();
		// destruction

public:

	WIN_OPTIONMENU();
		// construction

	void set_font_list (HFONT fontlist);

	virtual void set_font_list (const char *font_list);
		// set the font_list attribute

	void set_sensitive(bool value);
		// set attribute sensitive

	LONG get_windows_style() const;	
		// returns the styles for the embedded control

	virtual void redraw() const;
		// redraw the window

	virtual LRESULT callback(UINT uMsg,WPARAM wParam,LPARAM lParam);
		// the "this" WINDOW callback

	virtual bool create_control();
		// function called to create the embedded control
		// return true if creation process should continue, false otherwise

	virtual bool create_window();
		// create a window

	virtual void resize();
		// The method resize called every time a widget changes size

	virtual void insert_child (WIN_CORE * child);
		// insert a child in the children array

	virtual void delete_child (WIN_CORE * child);
		// delete a child in the children array

	// --------------- Attributs management methods -------------------

	virtual int internal_get_values
		// function used internally to get attribut values 
		// should not be called directly (call get_values)
		// arglist is the array of the values to get
		// count is the count of values to be put into arglist
		// got is an array that flag if an attribut have been put
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs put
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		) const;

	virtual int internal_set_values
		// function used internally to set attribut values 
		// should not be called directly (call set_values)
		// arglist is the array of the values to be set,
		// count is the count of values in arglist
		// got is an array that flag if an attribut have been set
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs set
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		);

	// --------------- Geometry management methods -------------------

	virtual void change_managed() {} 
		// The method change_manager called once during the realize phase and then
		// every time that one of its children becomes managed or unmanaged
		
	virtual XtGeometryResult geometry_manager(
									WIN_CORE *child,
									XtWidgetGeometry * request,
									XtWidgetGeometry * geometry_return)
		// The method geometry_manager called every time  
		// one of its children requests a new size
		{ return XtGeometryYes; }

	XtGeometryResult query_geometry (XtWidgetGeometry *request, 
										 XtWidgetGeometry *prefered);
		// The method query_geometry called when the parent  
		// wants to know the widget's prefered size

	// ------------------ option item management --------------------

	int get_index_of_item(const WIN_OPTIONITEM * item) const;
		// get the combox index of the item

	WIN_OPTIONITEM * get_item_of_index(int index) const;
		// get the item for an index in the combox

	void update_string_item(const WIN_OPTIONITEM * item);
		// change the string for item of this
	
	void update_map_item(WIN_OPTIONITEM * item,bool map);
		// change the mapping for item of this
	
	void update_manage_item(WIN_OPTIONITEM * item,bool manage);
		// change the management for item of this
	
	void update_sensitive_item(WIN_OPTIONITEM * item,bool sensitive);
		// change the sensitivity for item of this

};
//-------------------------------------------------------------------
#endif