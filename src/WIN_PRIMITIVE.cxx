//-------------------------------------------------------------------
// WIN_PRIMITIVE.cxx
// ---------------
// implements a class which defines the Motif class "XmPrimitive"
// J.E.Bollo & G. Musset, M3G (c) 1998
//-------------------------------------------------------------------
#include "WIN_PRIMITIVE.hxx"
#include "WINDOWS_MANAGER.hxx"
#include "win_naming.h"
#include "WINDOWS_COMMONS.h"
//-------------------------------------------------------------------
BEGIN_RES_DESC(WIN_PRIMITIVE,WIN_CORE,NAME_OF_WIN_PRIMITIVE_CLASS)
	RES_DESC_ATR(XmNfontList,	FONT,	WIN_PRIMITIVE, font_list),
END_RES_DESC
//-------------------------------------------------------------------

WIN_PRIMITIVE::WIN_PRIMITIVE()
 : WIN_CORE()
 , font_list(NULL)
 , hwndCtl(NULL)
{
}

//-------------------------------------------------------------------
WIN_PRIMITIVE::~WIN_PRIMITIVE()
{
	if(font_list!=NULL)
		DeleteObject(font_list);
}

//---------------------------------------------------------

bool WIN_PRIMITIVE::create_window()
{
	if(!WIN_CORE::create_window(WS_CHILD|WS_CLIPCHILDREN|WS_VISIBLE))
		return false;

	if(!create_control())
		return false;

	if(hwndCtl!=NULL)
		SendMessage(hwndCtl,WM_SETFONT, (WPARAM)font_list, TRUE);

	return true;
}

//---------------------------------------------------------

LRESULT WIN_PRIMITIVE::callback(UINT uMsg,WPARAM wParam,LPARAM lParam)
{
	switch(uMsg)
	{
	case WM_SETFONT:
		if(hwndCtl!=NULL)
			SendMessage(hwndCtl,uMsg,wParam,lParam);
		return 0;
	}

	return WIN_CORE::callback(uMsg,wParam,lParam);
}

//-------------------------------------------------------------------

void WIN_PRIMITIVE::set_font_list (HFONT fontlist)
	// set the font_list attribute
{
	assign_copy_of_font (&font_list, fontlist);
	if (hwnd() != NULL)
		SendMessage(hwnd(),WM_SETFONT, (WPARAM)font_list, TRUE);
	update_geometry_request(CWWidth|CWHeight);
}

//-------------------------------------------------------------------

void WIN_PRIMITIVE::set_font_list (const char *fontlist)
	// set the font_list attribute
{
	HFONT font = text_to_font(fontlist);
	set_font_list (font);
	if (font != NULL)
		DeleteObject(font);
}

//---------------------------------------------------------

void WIN_PRIMITIVE::redraw() const
{
	if (hwndCtl != NULL)
		WINDOWS_MANAGER::redraw_window(hwndCtl);
}

//-------------------------------------------------------------------

void WIN_PRIMITIVE::set_focus() const
	// set the focus to the window
{
	if(hwndCtl != NULL)
		SetFocus(hwndCtl);
}

//-------------------------------------------------------------------

void WIN_PRIMITIVE::set_sensitive(bool value)
	// set attribute sensitive
{
	if(sensitive != value)
	{
		WIN_CORE::set_sensitive(value);
		if (hwndCtl != NULL)
			EnableWindow(hwndCtl, sensitive);
	}
}

//-------------------------------------------------------------------

void WIN_PRIMITIVE::resize()
	//set the size to the embedded control hwndCtl
{
	if(hwndCtl != NULL)
		WINDOWS_MANAGER::move_window(hwndCtl, 0, 0, width, height);
}

//-------------------------------------------------------------------

int WIN_PRIMITIVE::internal_get_values
	// function used internally to get attribut values 
	// should not be called directly (call get_values)
	// arglist is the array of the values to get
	// count is the count of values to be put into arglist
	// got is an array that flag if an attribut have been put
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs put
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) const
{
	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.
#if VERSION_OF_WINDOW >= 2

		if(equality(attribut,XmNfontList))
			arglist[index].value.pvalue->hfont = font_list;

#else

		if(equality(attribut,XmNfontList))
		{
			if(font_list==NULL)
				arglist[index].value.pvalue->hfont = NULL;
			else
			{
				LOGFONT logfont;
				GetObject(font_list,sizeof logfont,&logfont);
				arglist[index].value.pvalue->hfont = 
							CreateFontIndirect(&logfont);
			}
		}

#endif 

		else
			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
	}

	// return total count of found attributs (including ancestors)
	return found_count + WIN_CORE::internal_get_values(arglist,count,got,equality);
}

//-------------------------------------------------------------------

int WIN_PRIMITIVE::internal_set_values
	// function used internally to set attribut values 
	// should not be called directly (call set_values)
	// arglist is the array of the values to be set,
	// count is the count of values in arglist
	// got is an array that flag if an attribut have been set
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs set
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) 
{
/*
	// empty geometry request
	bool redraw_needed = false;
	XtWidgetGeometry request;
	request.request_mode = 0;
*/
	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.
		if(equality(attribut,XmNfontList))
		{
			set_font_list(arglist[index].value.hfont);
		}
		else
			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
	}

/*
	// make the geometry request if necessary
	if(request.request_mode!=0)
		geometry_request(&request);
	if(redraw_needed)
		redraw();
*/
	// return total count of found attributs (including ancestors)
	return found_count  + WIN_CORE::internal_set_values(arglist,count,got,equality);
}

//---------------------------------------------------------
bool WIN_PRIMITIVE::treat_the_message(MSG *pmsg)
{
	// no key processing is needed
	if(pmsg->hwnd!=hwndCtl || pmsg->message!=WM_KEYDOWN)
		return false;

	// get the key DLGC code
	LRESULT code = 0;
	switch(pmsg->wParam)
	{
	case VK_TAB:
		code = DLGC_WANTTAB;
		break;
//	case VK_RETURN: ???? default push button management!
	case VK_LEFT:
	case VK_UP:
	case VK_RIGHT:
	case VK_DOWN:
		code = DLGC_WANTARROWS;
		break;
	default:
		// that key is not managed
		return false;
	}

	// test if the control takes control over key windows-management
	code |= DLGC_WANTALLKEYS;
	if((SendMessage(hwndCtl,WM_GETDLGCODE,0,0) & code)!=0)
		return false; // yes it takes the control

	// calls the 
	int action;
	bool shift = GetKeyState(VK_SHIFT)<0;
	switch(pmsg->wParam)
	{
	case VK_TAB:
		action = shift ? XmTRAVERSE_PREV_TAB_GROUP : XmTRAVERSE_NEXT_TAB_GROUP;
		break;
	case VK_LEFT:
		action = XmTRAVERSE_LEFT;
		break;
	case VK_UP:
		action = XmTRAVERSE_UP;
		break;
	case VK_RIGHT:
		action = XmTRAVERSE_RIGHT;
		break;
	case VK_DOWN:
		action = XmTRAVERSE_DOWN;
		break;
	}
	process_traversal(action);
	return true;
}
//---------------------------------------------------------
