//-------------------------------------------------------------------
// WIN_FORM.hxx
// ------------
// implements the "XmForm" class
// J.E.Bollo & G.Grison, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __win_form_hxx
#define __win_form_hxx
//-------------------------------------------------------------------
#include "macros.h"
#include "win_naming.h"
#include "WIN_BULLETIN_BOARD.hxx"
//-------------------------------------------------------------------
class SIDE;
//-------------------------------------------------------------------
class WIN_FORM : public WIN_BULLETIN_BOARD
	// the class that implements the XmForm motif class
{
	DECLARE_RESOURCE_CLIENT(WIN_FORM)
		// use the resource mechanism

	DECLARE_WINDOW(WIN_FORM)
		// use window class mechanism

private:

	WIN_CORE * requesting_child;
		// the child who makes a geometry_request to the RowColumn

	XtWidgetGeometry * child_geometry_request;
		// The geometry request parameters of the requesting child

public:

	int fraction_base;
		// number of division, used with position, default is 100

	int horizontal_spacing;
		// the offset for right and left attachment
		// in pixels, default is 0

	int vertical_spacing;
		// the offset for top and bottom attachment
		// in pixels, default is 0

protected:

	~WIN_FORM();
		// destruction

public:

	WIN_FORM();
		// construction

	virtual bool create_window();
		// create the window associated to the object

	virtual void insert_child (WIN_CORE * child);
		// insert a child in the children array

	virtual void change_managed();
		// The method change_manager called once during the realize phase and then
		// every time that one of its children becomes managed or unmanaged
		
	virtual XtGeometryResult geometry_manager(WIN_CORE *child,
								  XtWidgetGeometry *request,
								  XtWidgetGeometry *geometry_return); 
		// The method geometry_manager called every time  
		// one of its children requests a new size
		
	bool do_layout
	(
		bool			  act,
		WIN_CORE		 *the_child,
		XtWidgetGeometry *request,
		XtWidgetGeometry *result
	);
		// do the form layout
		// return true if geometry need to change
		// if act is true the geometry is changed automatically (by geometry_request)
		// the geometry is returned in result if not NULL
		// it take into account the_child request if both are not NULL
		
	virtual XtGeometryResult query_geometry (XtWidgetGeometry *request, 
											 XtWidgetGeometry *prefered);
		// The method query_geometry called when the parent  
		// wants to know the widget's prefered size

	virtual void resize();
		// The method resize called every time a widget changes size


	virtual int internal_get_values
		// function used internally to get attribut values 
		// should not be called directly (call get_values)
		// arglist is the array of the values to get
		// count is the count of values to be put into arglist
		// got is an array that flag if an attribut have been put
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs put
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		) const;

	virtual int internal_set_values
		// function used internally to set attribut values 
		// should not be called directly (call set_values)
		// arglist is the array of the values to be set,
		// count is the count of values in arglist
		// got is an array that flag if an attribut have been set
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs set
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		);

	virtual int constraint_get_values
		// function used internally to get attribut values for constraints
		// should not be called directly (call get_values)
		// that method is called for the parent of the child for wich
		//   the original get_values is requested
		// arglist is the array of the values to get
		// count is the count of values to be put into arglist
		// got is an array that flag if an attribut have been put
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs put
		(
			const WIN_CORE *child,
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		) const;

	virtual int constraint_set_values
		// function used internally to set attribut values for constraints
		// should not be called directly (call set_values)
		// that method is called for the parent of the child for wich
		//   the original get_values is requested
		// arglist is the array of the values to be set,
		// count is the count of values in arglist
		// got is an array that flag if an attribut have been set
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs set
		(
			WIN_CORE *child,
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		);

private:

	int find_and_fixe_child_side (WIN_CORE * child, SIDE side) const;
		// computes the coordinate of side of the child and returns it
		// the computed coordinate is set into the coordinate field
		//  of the constraint
		// this functions may change the state value of the constraints
		//  of the child

	int find_and_fixe_child_coordinate (WIN_CORE * child, SIDE side) const;
		// computes the coordinate of side of the child and returns it
		// the computed coordinate is set into the coordinate field
		//  of the constraint
		// this functions may change the state value of the constraints
		//  of the child
};
//-------------------------------------------------------------------
#endif
