//-------------------------------------------------------------------
// RESOURCE_DATABASE.hxx
// -------------------
// entry objects of the resource database object
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __resource_database_hxx
#define __resource_database_hxx
//-------------------------------------------------------------------
#include "macros.h"
#include "RESOURCE_TREE.hxx"
//-------------------------------------------------------------------
class RESOURCE_DATABASE
{
private:

	RESOURCE_TREE root;
		// root of the database

public:

	RESOURCE_DATABASE() : root() {}
		// construction of a new database

	RESOURCE_TREE * get_root()  { return &root; }
		// return the root of the database

	const RESOURCE_TREE * get_root()  const { return &root; }
		// return the root of the database
};
//-------------------------------------------------------------------
#endif

