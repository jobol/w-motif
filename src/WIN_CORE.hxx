//---------------------------------------------------------
// WIN_CORE.hxx
// ------------
// the WIN_CORE class (corresponds to the Xt class "Core")
// J.E.Bollo & G. Musset, M3G (c) 1998
//---------------------------------------------------------
#ifndef __win_core_hxx
#define __win_core_hxx
//---------------------------------------------------------
#include "WINDOW.hxx"
#include "RESOURCE_NAMED_CLIENT.hxx"
#include "RESOURCE_DESCRIPTOR.hxx"
#include "CALLBACK_SERVER.hxx"
#include "CONSTRAINT.hxx"
//---------------------------------------------------------
class WIN_COMPOSITE;
class WIN_SHELL;
class WIN_MENUPOPUP;
//---------------------------------------------------------
class WIN_CORE
  : public WINDOW
  , public RESOURCE_NAMED_CLIENT
  , public CALLBACK_SERVER
{
	DECLARE_VIRTUAL_RESOURCE_CLIENT(WIN_CORE)
		// use the resource mechanisme

public:

	int x;
		// x coordinate of the upper left corner of the window

	int y;
		// y coordinate of the upper left corner of the window

	int width;
		// window width in pixels

	int height;
		// window height in pixels
		
	HBRUSH background;
		// the background color

	COLORREF foreground;
		// the foreground

	bool sensitive;
		// tells whether a widget is sensitive to input

	CONSTRAINT * constraints;
		// constraints from the parent

	WIN_COMPOSITE * parent_win;
		// my parent

	bool managed;
		// is this managed

	WIN_MENUPOPUP * popup_menu;
		// the attached popup menu, if any

	bool traversal_on;
		// true when this is traversable with keyboard

	int traversal_type;
		// is it tab group or not? valid values are
		// XmNONE, XmTAB_GROUP, XmSTICKY_TAB_GROUP, XmEXCLUSIVE_TAB_GROUP
		// but only XmNONE and XmTAB_GROUP are treated

	bool destroy_pending;
		// flag used to lock destruction to be done one time only

protected:

	~WIN_CORE();
		// destruction

public:

	WIN_CORE();
		// construction

	virtual void destroy();
		// public destruction

	virtual LRESULT callback(UINT uMsg,WPARAM wParam,LPARAM lParam);
		// the "this" WINDOW callback

	void set_background (COLORREF color);
		// set the background with color

	virtual void set_sensitive(bool value);
		// set attribute sensitive

	void set_background (const char* color);
		// set the background with color

	virtual void set_foreground (COLORREF color);
		// set the foreground with color

	virtual void set_foreground (const char *color);
		// set the foreground with color

	void set_popup_menu(WIN_MENUPOPUP * menu)
		// set a new popup menu to this
		{ popup_menu = menu; }

	bool is_managed() const
		// is this managed?
		{ return managed; }

	virtual bool is_realized() const
		// is this realized?
		{ return is_attached(); }

	virtual bool is_mapped() const
		// is this managed?
		{ return is_realized() && IsWindowVisible(hwnd())!=0; }

	virtual void manage();
		// set this managed

	virtual void unmanage();
		// set this not managed

	virtual void map();
		// set this mapped

	virtual void unmap();
		// set this mapped

	virtual void realize();
		// realise this

	virtual void do_paint(HDC hdc,const RECT *rect) const;
		// virtual than can be override to paint
		// there is no erase background so you have to
		// erase the background inside that function
		//   the default WIN_CORE do_paint erase the background

	virtual void paint() const;
		// reaction to the WM_PAINT, calls do_paint
		// should better not be override

	virtual void set_name_and_parent(const char *name,WIN_COMPOSITE *parent);
		// store a widget name and its parent

	WIN_COMPOSITE * get_parent_win() const
		// return the parent widget
		{ return parent_win; }

	WIN_SHELL * get_parent_shell() const;
		// return the parent widget

	bool process_traversal(int traversal_action);
		// perform the traversal action

	virtual RESOURCE_NAMED_CLIENT * get_parent_resource() const;
		// get the parent

	virtual void set_resource_values(const RESOURCE_SCANNER & scanner);
		// put values of scanner, the currently scanned resources
		// use default and check if constraints should be checked

	void scan_resources();
		// scan the resource database

	virtual LONG get_windows_style() const = 0;
		// retrieve the windows style used to create the widget 
		// (intended especially for cervicad-mask-sub-system)

	bool create_window
		// creation of a window for the system
			(
				LONG         style,
				DWORD        dwExStyle = 0,
				LPCTSTR      lpszWindowText = NULL
			);

	virtual bool create_window() = 0;
		// 	from WINDOW class

	virtual ATOM get_window_class_atom() const = 0;
		// retrieve the window class atom

	void activate_callbacks(CB_REASON reason)
		// activate the callback on this
		{ CALLBACK_SERVER::activate_callbacks(this,reason); }

	// --------------- Geometry management methods -------------------

	virtual void resize() {}
		// The method resize is called every time a widget changes size
		
	virtual XtGeometryResult query_geometry (XtWidgetGeometry *request, 
											 XtWidgetGeometry *prefered)
		// The method query_geometry called when the parent  
		// wants to know the widget's prefered size
		{ *prefered = *request; return XtGeometryYes; }

	virtual void set_geometry(int x,int y,int width,int heigth);
		// apply unconditionnaly the requested geometry

	void set_geometry(XtWidgetGeometry *request);
		// apply unconditionnaly the requested geometry

	virtual XtGeometryResult geometry_request (XtWidgetGeometry *request);
		// request to change the geometry of this according to request
		// returns only XtGeometryYes (request accepted) or XtGeometryNo (denied)

	XtGeometryResult complete_query_geometry(
		XtWidgetGeometry *original_request, 
		XtWidgetGeometry *original_prefered, 
		XtWidgetGeometry *prefered);
		// fills original_prefered accordling to the original_request
		// and the this prefered geometry and then return what should
		// be returned by the calling query_geometry method

	virtual void update_geometry_request(int mask);
		// request new geometry determined upon the CWX, CWY, CWHeight, CWWidth
		// values of mask and the returned value and geometry of query_geometry

	// --------------- Attributs management methods -------------------

	virtual void set_values(ARGLIST arglist,int count);
		// set count values from arglist to this

	virtual void get_values(ARGLIST arglist,int count) const;
		// get count values from this to arglist

	virtual int internal_get_values
		// function used internally to get attribut values 
		// should not be called directly (call get_values)
		// arglist is the array of the values to get
		// count is the count of values to be put into arglist
		// got is an array that flag if an attribut have been put
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs put
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		) const;

	virtual int internal_set_values
		// function used internally to set attribut values 
		// should not be called directly (call set_values)
		// arglist is the array of the values to be set,
		// count is the count of values in arglist
		// got is an array that flag if an attribut have been set
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs set
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		);

	virtual void delete_this(CB_EMITER emiter) { delete (WIN_CORE *)emiter; }
		// delete this

};
//---------------------------------------------------------
#endif
