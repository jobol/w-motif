//-------------------------------------------------------------------
// WIN_SUBMENU.cxx
// ------------
// implements a class which defines the submenus
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#include "WIN_SUBMENU.hxx"
#include "WINDOWS_MANAGER.hxx"
#include "win_naming.h"
//-------------------------------------------------------------------
BEGIN_RES_DESC(WIN_SUBMENU,WIN_MENUPOPUP,NAME_OF_WIN_SUBMENU_CLASS)
END_RES_DESC
//-------------------------------------------------------------------
WIN_SUBMENU::WIN_SUBMENU()
 : WIN_MENUPOPUP()
{
	menu_item = new WIN_MENUITEM;
}

//-------------------------------------------------------------------

WIN_SUBMENU::~WIN_SUBMENU()
{
	if(menu_item!=NULL)
		menu_item->destroy();
}

//-------------------------------------------------------------------

void WIN_SUBMENU::set_name_and_parent(const char *name,WIN_COMPOSITE *parent)
	// store a widget name and its parent
{
	if(parent!=NULL && parent->is_kind_of(WIN_MENU::the_resource_class()))
		WIN_CORE::set_name_and_parent(name,parent);
}

//-------------------------------------------------------------------

int WIN_SUBMENU::internal_get_values
	// function used internally to get attribut values 
	// should not be called directly (call get_values)
	// arglist is the array of the values to get
	// count is the count of values to be put into arglist
	// got is an array that flag if an attribut have been put
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs put
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) const
{
	return menu_item==NULL ? 0 
		: menu_item->internal_get_values(arglist,count,got,equality);
}

//-------------------------------------------------------------------

int WIN_SUBMENU::internal_set_values
	// function used internally to set attribut values 
	// should not be called directly (call set_values)
	// arglist is the array of the values to be set,
	// count is the count of values in arglist
	// got is an array that flag if an attribut have been set
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs set
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) 
{
	if(menu_item==NULL)
		return 0;

	int nr = menu_item->internal_set_values(arglist,count,got,equality);
	if(nr!=0)
		redraw();
	return nr;
}

//-------------------------------------------------------------------

void WIN_SUBMENU::fill_menu_info_struct(MENUITEMINFO &menu_info) const
	// fill the menu_info accordling to this
{
	if(menu_item!=NULL)
		menu_item->fill_menu_info_struct(menu_info);
	else
		menu_info.fMask = 0;

	menu_info.fMask |= MIIM_SUBMENU;
	menu_info.hSubMenu = menu_handle;
}

//-------------------------------------------------------------------

void WIN_SUBMENU::set_resource_values(const RESOURCE_SCANNER & scanner)
	// put values of scanner, the currently scanned resources
	// use default and check for constraints
{
	if(menu_item!=NULL)
		menu_item->set_resource_values(scanner);
}

//-------------------------------------------------------------------

void WIN_SUBMENU::set_text(const char* txt)
	// replaces all the text by "txt"
{
	if(menu_item!=NULL)
		menu_item->set_text(txt);
	if(get_parent_menu()!=NULL)
		get_parent_menu()->update_item(this);
}

//-------------------------------------------------------------------

char* WIN_SUBMENU::get_text() const
	// gets all the text
{
	if(menu_item!=NULL)
		return menu_item->get_text();
	return strdup("");
}

//-------------------------------------------------------------------

void WIN_SUBMENU::realize()
	// realise this and sub-widgets
{
	WIN_MENU::realize();
	map();
}

//-------------------------------------------------------------------

void WIN_SUBMENU::set_sensitive(bool value)
	// set attribute sensitive
{
	if(get_parent_menu()!=NULL)
		get_parent_menu()->update_sensitive_submenu(this,value);
}

//-------------------------------------------------------------------

void WIN_SUBMENU::map()
	// set this mapped
{
	if(get_parent_menu()!=NULL)
		get_parent_menu()->update_map_submenu(this,true);
}

//-------------------------------------------------------------------

void WIN_SUBMENU::unmap()
	// set this mapped
{
	if(get_parent_menu()!=NULL)
		get_parent_menu()->update_map_submenu(this,false);
}

//-------------------------------------------------------------------

void WIN_SUBMENU::manage()
	// set this mapped
{
	if(get_parent_menu()!=NULL)
		get_parent_menu()->update_manage_submenu(this,true);
}

//-------------------------------------------------------------------

void WIN_SUBMENU::unmanage()
	// set this mapped
{
	if(get_parent_menu()!=NULL)
		get_parent_menu()->update_manage_submenu(this,false);
}

//-------------------------------------------------------------------
