//---------------------------------------------------------
// WINDOW.cxx
// ----------
// window for Windows
// J.E.Bollo, M3G (c) 1998
//---------------------------------------------------------
#include "macros.h"
#include "WINDOW.hxx"
#include "WINDOWS_MANAGER.hxx"
//---------------------------------------------------------
// Attachment is defined as the assignement of the
// GWL_USERDATA value of a HWND window to the this value.
//---------------------------------------------------------
void WINDOW::attach(HWND hwnd)
{
	assert(the_hwnd==NULL,("can't attach attached window"));
	assert(hwnd!=NULL,("use detach to cancel attach"));
	long old = GetWindowLong(hwnd,GWL_USERDATA);
	assert(old==0,("previous user data no null (%d)",old));
	SetWindowLong(hwnd,GWL_USERDATA,(long)this);
	the_hwnd = hwnd;
}
//---------------------------------------------------------
void WINDOW::detach()
{
	assert(the_hwnd!=NULL,("can't detach unattached window"));
	SetWindowLong(the_hwnd,GWL_USERDATA,0);
	the_hwnd = NULL;
}
//---------------------------------------------------------
WINDOW *WINDOW::instance_of(HWND hwnd)
{
	return (WINDOW*)GetWindowLong(hwnd,GWL_USERDATA);
}
//---------------------------------------------------------
LRESULT WINDOW::callback(UINT uMsg,WPARAM wParam,LPARAM lParam)
{
	return default_callback(uMsg,wParam,lParam);
}
//---------------------------------------------------------
LRESULT WINDOW::default_callback(UINT uMsg,WPARAM wParam,LPARAM lParam)
{
	return DefWindowProc(hwnd(),uMsg,wParam,lParam);
}
//---------------------------------------------------------
void WINDOW::destroy_window()
	// destruction of the window
{
	if (is_attached())
	{
		DestroyWindow(the_hwnd);
		the_hwnd = NULL;
	}
}
//---------------------------------------------------------
void WINDOW::set_text(const char* txt)
	// replaces all the text by "txt"
{
	if(hwnd()!=NULL)
		SetWindowText(hwnd(),txt);
}
//---------------------------------------------------------
char* WINDOW::get_text() const
	// gets all the text
{
	if(hwnd()==NULL)
		return NULL;
	int len = GetWindowTextLength(hwnd()) + 1;
	char * txt = (char *)malloc(len);
	GetWindowText(hwnd(),txt,len);
	return txt;
}
//---------------------------------------------------------
void WINDOW::redraw() const
	// redraw the window
{
	if(is_attached())
		WINDOWS_MANAGER::redraw_window(hwnd());
}
//---------------------------------------------------------
void WINDOW::set_focus() const
	// set the focus to the window
{
	if(is_attached())
		SetFocus(hwnd());
}
//---------------------------------------------------------
bool WINDOW::treat_the_message(MSG *msg)
	// returns true if window pre-treats the message and false otherwise
	// when it returns true the caller should not call neither
	// the TranslateMeassage nor the DispatchMessage calls
{
	return false;
}
//---------------------------------------------------------
