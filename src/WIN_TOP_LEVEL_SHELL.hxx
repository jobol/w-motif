//-------------------------------------------------------------------
// WIN_TOP_LEVEL_SHELL.hxx
// ---------------
// implements a class which defines the Xt class "ToplevelShell"
// J.E.Bollo & G. Musset, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __win_top_level_shell_hxx
#define __win_top_level_shell_hxx
//-------------------------------------------------------------------
#include "macros.h"
#include "WIN_SHELL.hxx"
//-------------------------------------------------------------------
class WIN_TOP_LEVEL_SHELL : public WIN_SHELL
{
	DECLARE_RESOURCE_CLIENT(WIN_TOP_LEVEL_SHELL)
		// use the resource mechanism

	DECLARE_WINDOW(WIN_TOP_LEVEL_SHELL)
		// use window class mechanism

public:

	bool iconic;
		// flag indicating if shell must be iconified at creation

	char * icon_name;
		// name of file describing the icon

protected:

	~WIN_TOP_LEVEL_SHELL();
		// destruction

public:

	WIN_TOP_LEVEL_SHELL();
		// construction

	virtual LRESULT callback(UINT uMsg,WPARAM wParam,LPARAM lParam);
		// the "this" WINDOW callback

	virtual LONG get_windows_style() const;
		// retrieve the windows style used to create the widget 

	virtual bool create_window();
		// create the window associated to the object

	void set_iconic(bool value);
		// set attribute iconic


	// --------------- Attributs management methods -------------------

	virtual int internal_get_values
		// function used internally to get attribut values 
		// should not be called directly (call get_values)
		// arglist is the array of the values to get
		// count is the count of values to be put into arglist
		// got is an array that flag if an attribut have been put
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs put
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		) const;

	virtual int internal_set_values
		// function used internally to set attribut values 
		// should not be called directly (call set_values)
		// arglist is the array of the values to be set,
		// count is the count of values in arglist
		// got is an array that flag if an attribut have been set
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs set
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		);
};
//-------------------------------------------------------------------
#endif