//-------------------------------------------------------------------
// WIN_SHELL.hxx
// ---------------
// implements a class which groups the Xt classes "Shell", "WMShell",
// and "VendorShell" 
// J.E.Bollo & G. Musset, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __win_shell_hxx
#define __win_shell_hxx
//-------------------------------------------------------------------
#include "macros.h"
#include "WIN_COMPOSITE.hxx"
//-------------------------------------------------------------------
class WIN_MENUBAR;
//-------------------------------------------------------------------
class WIN_SHELL : public WIN_COMPOSITE
{
	DECLARE_RESOURCE_CLIENT(WIN_SHELL)
		// use the resource mechanism

	DECLARE_WINDOW(WIN_SHELL)
		// use window class mechanism

public:

	bool allow_shell_resize;
		// flag indicating if shell resizing is allowed

	char * geometry;
		// window geometry (position and size)

	int max_width;
		// maximum window width

	int max_height;
		// maximum window height

	int min_width;
		// minimum window width

	int min_height;
		// minimum window width

	char * title;
		//  window title

	bool transient;
		// flag indicating if it is a transient shell

	HFONT button_font_list;
		// font list for buttons included in the current shell

	HFONT label_font_list;
		// font list for labels included in the current shell

	HFONT text_font_list;
		// font list for texts included in the current shell

	int mwm_decorations;
		// window managers decorations applied to the window

	int mwm_functions;
		// functions list included in the system menu

	WIN_MENUBAR * menubar;
		// the current menu bar

	int delete_response;
		// what to do in case of close, 
		//  one of: XmDESTROY, XmUNMAP, XmDO_NOTHING

protected:

	~WIN_SHELL();
		// destruction

public:

	WIN_SHELL();
		// construction

	LRESULT callback(UINT uMsg,WPARAM wParam,LPARAM lParam);
		// the WIN_SHELL callback for Windows

	virtual void set_text(const char *txt);
		// sets the title of the shell widget

	void set_menu_bar(WIN_MENUBAR * menu);
		// set the menu

	void set_mwm_decorations(int value)
		// sets the decorations of the shell widget
	{
		mwm_decorations = value;
	}

	virtual void do_paint(HDC hdc,const RECT *rect) const;
		// paint in response to a WM_PAINT

	virtual LONG get_windows_style() const;
		// retrieve the windows style used to create the widget 

	virtual bool create_window();
		// create a new window, pure virtual

	bool shell_process_traversal(WIN_CORE * child,int traversal_action);
		// process children traversal from child to the one that
		// correspond to traversal_action. traversal_action can be:
		// XmTRAVERSE_CURRENT, XmTRAVERSE_PREV_TAB_GROUP, 
		// XmTRAVERSE_NEXT_TAB_GROUP, XmTRAVERSE_LEFT, XmTRAVERSE_UP,
		// XmTRAVERSE_RIGHT, XmTRAVERSE_DOWN
		// returns true if the focus has been set

	virtual void realize();
		// realise a shell widget and sub-widgets

	virtual void set_geometry(int x,int y,int width,int heigth);
		// apply unconditionnaly the requested geometry

	virtual void change_managed();
		// The method change_manager for a shell widget
		// called once during the realize phase and then
		// every time that its child becomes managed or unmanaged

	virtual XtGeometryResult geometry_manager(
		WIN_CORE			*child,
		XtWidgetGeometry	*request,
		XtWidgetGeometry	*geometry_return
		); 
		// The method geometry_manager called every time  
		// one of its children requests a new size

	void do_layout(int call_type);
		// manages the size of the child of the shell 

	virtual void resize();
		// The method resize is called every time a widget changes size

	virtual XtGeometryResult geometry_request (XtWidgetGeometry *request);
		// request to change the geometry of this according to request
		// returns only XtGeometryYes (request accepted) or XtGeometryNo (denied)

	virtual void set_resource_values(const RESOURCE_SCANNER & scanner);
		// put values of scanner, the currently scanned resources
		// use default and check if constraints should be checked

	void geometry_changed();
		// called when the geometry attribut has changed

	// --------------- Attributs management methods -------------------

	virtual int internal_get_values
		// function used internally to get attribut values 
		// should not be called directly (call get_values)
		// arglist is the array of the values to get
		// count is the count of values to be put into arglist
		// got is an array that flag if an attribut have been put
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs put
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		) const;

	virtual int internal_set_values
		// function used internally to set attribut values 
		// should not be called directly (call set_values)
		// arglist is the array of the values to be set,
		// count is the count of values in arglist
		// got is an array that flag if an attribut have been set
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs set
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		);
};
//-------------------------------------------------------------------
#endif
