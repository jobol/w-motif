//---------------------------------------------------------
// CONSTRAINT.hxx
// --------------
// the constraints base class for WIN_CORE inheriters
// J.E.Bollo, M3G (c) 1998
//---------------------------------------------------------
#ifndef __constraint_hxx
#define __constraint_hxx
//---------------------------------------------------------
#include "RESOURCE_CLIENT.hxx"
//---------------------------------------------------------
class CONSTRAINT
	: public RESOURCE_CLIENT
{
public:
	CONSTRAINT() : RESOURCE_CLIENT() {}
		// constructor

	~CONSTRAINT() {}
		// destructor
};
//---------------------------------------------------------
#endif
