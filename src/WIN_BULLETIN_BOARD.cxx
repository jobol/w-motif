//-------------------------------------------------------------------
// WIN_BULLETIN_BOARD.cxx
// ---------------
// implements the "XmBulletinBoard" class
// J.E.Bollo & G. Musset, M3G (c) 1998
//-------------------------------------------------------------------
#include "WIN_BULLETIN_BOARD.hxx"
#include "WINDOWS_MANAGER.hxx"
#include "win_naming.h"
#include "windows_commons.h"
//-------------------------------------------------------------------
BEGIN_RES_DESC(WIN_BULLETIN_BOARD,WIN_COMPOSITE,NAME_OF_WIN_BULLETIN_BOARD_CLASS)
	RES_DESC_ATR(XmNmarginWidth,	INTEGER,	WIN_BULLETIN_BOARD, margin_width),
	RES_DESC_ATR(XmNmarginHeight,	INTEGER,	WIN_BULLETIN_BOARD, margin_height),
	RES_DESC_ATR(XmNbuttonFontList, FONT,		WIN_BULLETIN_BOARD, button_font_list),
	RES_DESC_ATR(XmNlabelFontList,	FONT,		WIN_BULLETIN_BOARD, label_font_list),
	RES_DESC_ATR(XmNtextFontList,	FONT,		WIN_BULLETIN_BOARD,	text_font_list),
END_RES_DESC
//-------------------------------------------------------------------
IMPLEMENT_WINDOW(WIN_BULLETIN_BOARD)
//-------------------------------------------------------------------
ATOM WIN_BULLETIN_BOARD::register_window_class() const
{
	return WINDOWS_MANAGER::register_window_class(
					(const char*)get_class_name(),0,
					LoadCursor(NULL,IDC_ARROW));
}
//-------------------------------------------------------------------
WIN_BULLETIN_BOARD::WIN_BULLETIN_BOARD()
	: WIN_COMPOSITE()
	, margin_width(10)
	, margin_height(10)
	, button_font_list(NULL)
	, label_font_list(NULL)
	, text_font_list(NULL)
{
}
//-------------------------------------------------------------------
WIN_BULLETIN_BOARD::~WIN_BULLETIN_BOARD()
{
}
//-------------------------------------------------------------------
LONG WIN_BULLETIN_BOARD::get_windows_style() const
	// retrieve the windows style used to create the widget 
{
	return WS_CHILD|WS_CLIPSIBLINGS|WS_VISIBLE|WS_CLIPCHILDREN;
}
//-------------------------------------------------------------------
bool WIN_BULLETIN_BOARD::create_window()
{
	return WIN_CORE::create_window(get_windows_style());
}
//-------------------------------------------------------------------
void WIN_BULLETIN_BOARD::change_managed()
	// The method change_manager called once during the realize phase and then
	// every time that one of its children becomes managed or unmanaged
{
	do_layout(true,NULL,NULL,NULL);
}
//-------------------------------------------------------------------
XtGeometryResult WIN_BULLETIN_BOARD::geometry_manager(
	WIN_CORE			*child,
	XtWidgetGeometry	*request,
	XtWidgetGeometry	*geometry_return
) 
	// The method geometry_manager called every time  
	// one of its children requests a new size or position
{
	XtWidgetGeometry resulting_geometry;
	*geometry_return = *request;

	// seeks for border clipping constraints
	bool wrong_x_y = false;
	if((request->request_mode & CWX)!=0 && request->x<margin_width)
	{
		wrong_x_y = true;
		geometry_return->x = margin_width;
	}
	if((request->request_mode & CWY)!=0 && request->y<margin_height)
	{
		wrong_x_y = true;
		geometry_return->y = margin_height;
	}

	// compute new size
	if(do_layout(false,child,geometry_return,&resulting_geometry))
	{
		// if geometry change then query to do it in fact
		if(geometry_request(&resulting_geometry)==XtGeometryNo)
		{
			// no if growing
			if((resulting_geometry.request_mode & CWWidth)!=0
				&& resulting_geometry.width>width)
				return XtGeometryNo;
			if((resulting_geometry.request_mode & CWHeight)!=0
				&& resulting_geometry.height>height)
				return XtGeometryNo;
		}
	}
	// return yes or almost when margin problem encountered
	return wrong_x_y ? XtGeometryAlmost : XtGeometryYes;
}

//-------------------------------------------------------------------

bool WIN_BULLETIN_BOARD::do_layout
(
	bool			  act,
	WINDOW			 *the_child,
	XtWidgetGeometry *request,
	XtWidgetGeometry *result
)
	// do the bulletin board layout
	// return true if geometry need to change
	// if act is true the geometry is changed automatically (by geometry_request)
	// the geometry is returned in result if not NULL
	// it take into account the_child request if both are not NULL
{
	// make a virtual result if needed
	XtWidgetGeometry internal_result_if_needed;
	if(result==NULL)
		result = &internal_result_if_needed;
	
	// compute the new size
	RECT total;
//	total.left = 1000000;
//	total.top = 1000000;
	total.right = 0;
	total.bottom = 0;
	// loop on all children
	for (int i=0; i<num_children; i++)
	{
		WIN_CORE * core = children[i];
		int x = core->x;
		int y = core->y;
		int width = core->width;
		int height = core->height;
		if(the_child!=NULL && request!=NULL && the_child==core)
		{
			if((request->request_mode & CWX)!=0)
				x = request->x;
			if((request->request_mode & CWY)!=0)
				y = request->y;
			if((request->request_mode & CWWidth)!=0)
				width = request->width;
			if((request->request_mode & CWHeight)!=0)
				height = request->height;
		}

/* */
		if(width==0 || height==0)
		{
			XtWidgetGeometry query, wanted;
			query.request_mode = 0;
			if(width==0)
			{
				query.request_mode |= CWWidth;
				query.width = 10;
			}
			if(height==0)
			{
				query.request_mode |= CWHeight;
				query.height = 10;
			}
			XtGeometryResult response = core->query_geometry(&query,&wanted);
			if(response!=XtGeometryNo)
			{
				if((wanted.request_mode & CWWidth)!=0)
					width = wanted.width;
				if((wanted.request_mode & CWHeight)!=0)
					height = wanted.height;
				if(core!=the_child && act)
				{
					wanted.request_mode &= CWHeight|CWWidth;
					core->set_geometry(&wanted);
				}
			}
		}
/* */
//		total.left = min(total.left, x);
//		total.top = min(total.top, y);
		total.right = max(total.right, x+width);
		total.bottom = max(total.bottom, y+height);
	}
	int new_width = total.right + margin_width;
	int new_height = total.bottom + margin_height;

	// check if change and build result
	result->request_mode = CWHeight|CWWidth;
	result->width = new_width;
	result->height = new_height;

	// no change
	if(result->request_mode==0)
		return false;

	return true;
}


//-------------------------------------------------------------------

XtGeometryResult WIN_BULLETIN_BOARD::query_geometry
(
	XtWidgetGeometry *request, 
	XtWidgetGeometry *prefered
)
		// The method query_geometry called when the parent  
		// wants to know the widget's prefered size
{
	XtWidgetGeometry pref;
	do_layout(false,NULL,NULL,&pref);

	return complete_query_geometry(request,prefered,&pref);
}

//-------------------------------------------------------------------


int WIN_BULLETIN_BOARD::internal_get_values
	// function used internally to get attribut values 
	// should not be called directly (call get_values)
	// arglist is the array of the values to get
	// count is the count of values to be put into arglist
	// got is an array that flag if an attribut have been put
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs put
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) const
{
	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.

		if(equality(attribut,XmNmarginWidth))
			arglist[index].value.pvalue->integer = margin_width;
		else
		if(equality(attribut,XmNmarginHeight))
			arglist[index].value.pvalue->integer = margin_height;
		else

#if VERSION_OF_WINDOW >= 2

		if(equality(attribut,XmNbuttonFontList))
			arglist[index].value.pvalue->hfont = button_font_list;
		else	
		if(equality(attribut,XmNlabelFontList))
			arglist[index].value.pvalue->hfont = label_font_list;
		else
		if(equality(attribut,XmNtextFontList))
			arglist[index].value.pvalue->hfont = text_font_list;

#else

		if(equality(attribut,XmNbuttonFontList))
		{	
			if(button_font_list==NULL)
				arglist[index].value.pvalue->hfont = NULL;
			else
			{
				LOGFONT logfont;
				GetObject(button_font_list,sizeof logfont,&logfont);
				arglist[index].value.pvalue->hfont = 
							CreateFontIndirect(&logfont);
			}
		}
		else	
		if(equality(attribut,XmNlabelFontList))
		{	
			if(label_font_list==NULL)
				arglist[index].value.pvalue->hfont = NULL;
			else
			{
				LOGFONT logfont;
				GetObject(label_font_list,sizeof logfont,&logfont);
				arglist[index].value.pvalue->hfont = 
							CreateFontIndirect(&logfont);
			}
		}
		else
		if(equality(attribut,XmNtextFontList))
		{	
			if(text_font_list==NULL)
				arglist[index].value.pvalue->hfont = NULL;
			else
			{
				LOGFONT logfont;
				GetObject(text_font_list,sizeof logfont,&logfont);
				arglist[index].value.pvalue->hfont = 
							CreateFontIndirect(&logfont);
			}
		}

#endif

		else
			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
	}

	// return total count of found attributs (including ancestors)
	return found_count + WIN_COMPOSITE::internal_get_values(arglist,count,got,equality);
}


//-------------------------------------------------------------------

int WIN_BULLETIN_BOARD::internal_set_values
	// function used internally to set attribut values 
	// should not be called directly (call set_values)
	// arglist is the array of the values to be set,
	// count is the count of values in arglist
	// got is an array that flag if an attribut have been set
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs set
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) 
{
	// empty geometry request
	bool redraw_needed = false;
/*
	XtWidgetGeometry request;
	request.request_mode = 0;
*/

	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.
		if(equality(attribut,XmNmarginWidth))
			set_margin_width (arglist[index].value.integer);
		else
		if(equality(attribut,XmNmarginHeight))
			set_margin_height(arglist[index].value.integer);
		else
		if(equality(attribut,XmNbuttonFontList))
		{	
			assign_copy_of_font (&button_font_list, arglist[index].value.hfont);
			redraw_needed = true;
		}
		else	
		if(equality(attribut,XmNlabelFontList))
		{	
			assign_copy_of_font (&label_font_list, arglist[index].value.hfont);
			redraw_needed = true;
		}
		else
		if(equality(attribut,XmNtextFontList))
		{	
			assign_copy_of_font (&text_font_list, arglist[index].value.hfont);
			redraw_needed = true;
		}
		else
			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
	}

/*
	// make the geometry request if necessary
	if(request.request_mode!=0)
		geometry_request(&request);
*/
	if(redraw_needed)
		redraw();

	// return total count of found attributs (including ancestors)
	return found_count  + WIN_COMPOSITE::internal_set_values(arglist,count,got,equality);
}

