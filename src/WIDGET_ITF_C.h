//---------------------------------------------------------
// WIDGET_ITF_C.h
// --------------
// interface function with C
// J.E.Bollo, M3G (c) 1998
//---------------------------------------------------------
#ifndef __widget_itf_c_h
#define __widget_itf_c_h
//---------------------------------------------------------
#include "WIDGET_TYPES.h"
#include "win_naming.h"
//---------------------------------------------------------

#ifdef __cplusplus
	extern "C" {
#endif

//---------------------------------------------------------
// the types
//---------------------------------------------------------

typedef void * CWIDGET;
	// the C type for the widgets

typedef void * CWIDGETCLASS;
	// the C type for the widgets class

typedef void (*WIDGET_CALLBACK)(CWIDGET,void*,void*);
	// the type of the callback functions

//---------------------------------------------------------
// the classes
//---------------------------------------------------------

extern CWIDGETCLASS	CCORE_CLASS;
extern CWIDGETCLASS	CCOMPOSITE_CLASS;
extern CWIDGETCLASS	CSHELL_CLASS;
extern CWIDGETCLASS	CTOP_LEVEL_SHELL_CLASS;

extern CWIDGETCLASS	CBULLETIN_BOARD_CLASS;
extern CWIDGETCLASS	CDRAWINGAREA_CLASS;
extern CWIDGETCLASS	CFORM_CLASS;
extern CWIDGETCLASS	CFRAME_CLASS;
extern CWIDGETCLASS	CLABEL_CLASS;
extern CWIDGETCLASS	CLIST_CLASS;
extern CWIDGETCLASS	CMESSAGE_BOX_CLASS;
extern CWIDGETCLASS	CPRIMITIVE_CLASS;
extern CWIDGETCLASS	CPUSHBUTTON_CLASS;
extern CWIDGETCLASS	CROWCOLUMN_CLASS;
extern CWIDGETCLASS	CSCROLLEDWINDOW_CLASS;
extern CWIDGETCLASS	CSELECTION_BOX_CLASS;
extern CWIDGETCLASS	CSEPARATOR_CLASS;
extern CWIDGETCLASS	CTEXT_CLASS;
extern CWIDGETCLASS	CTOGGLEBUTTON_CLASS;

extern CWIDGETCLASS	COPTIONITEM_CLASS;
extern CWIDGETCLASS	COPTIONMENU_CLASS;
extern CWIDGETCLASS	CMENUITEM_CLASS;
extern CWIDGETCLASS	CMENUBAR_CLASS;
extern CWIDGETCLASS	CMENUPOPUP_CLASS;
extern CWIDGETCLASS	CSUBMENU_CLASS;


//---------------------------------------------------------
// the functions
//---------------------------------------------------------

void init_widget_system
	// initialise the widget system
	// if resource_filename is not NULL
	//   then it reads the resource file of name resource_filename 
	// set the application instance hInstance
	(
		const char *	resource_filename,
		HINSTANCE		hInstance
	);

CWIDGETCLASS widget_class_of_class_name(const char * class_name);
	// returns the class of widget for the given class_name
	// if invalid class_name returns NULL

CWIDGET create_widget
	// create a widget of class class_name and return it
	// return NULL in case of problems
	(
		const char *	name,
		CWIDGETCLASS	widget_class,
		CWIDGET			parent,
		ARGLIST			arg_list,
		int				count_of_arg
	);

CWIDGET create_widget_solo(CWIDGETCLASS widget_class);
	// create a widget of class class_name and return it
	// return NULL in case of problems

void set_widget_name_and_parent
	// set the name and the parent of the widget
	// parent can be NULL for shell widget
	// then scans the resource defined values
	(
		CWIDGET			widget,
		const char *	name,
		CWIDGET			parent
	);

void destroy_widget(CWIDGET widget);
	// destroy widget

void set_widget_values(CWIDGET widget,ARGLIST arglist,int count);
	// set the values on the widget
	// a number of count values are recorded in arglist

void get_widget_values(CWIDGET widget,ARGLIST arglist,int count);
	// set the values on the widget
	// a number of count values are recorded in arglist

void realize_widget(CWIDGET widget);
	// realize widget

int map_widget(CWIDGET widget);
	// map widget

void unmap_widget(CWIDGET widget);
	// unmap widget

void manage_widget(CWIDGET widget);
	// manage widget

void unmanage_widget(CWIDGET widget);
	// unmanage widget

void add_widget_callback
	// add a callback function to widget for the reason
	// callback function will be called with data
	(
		CWIDGET			widget,
		const char *	reason,
		WIDGET_CALLBACK	callback,
		void *			data
	);

void remove_widget_callback
	// remove a callback function that have been recorded
	// for the widget, the reason and the data
	(
		CWIDGET			widget,
		const char *	reason,
		WIDGET_CALLBACK	callback,
		void *			data
	);

LONG get_windows_style_of_widget(CWIDGET widget);
	// retrieve the windows style used to create the widget 
	// (intended especially for cervicad-mask-sub-system)
	
CWIDGETCLASS get_widget_class(CWIDGET widget);
	// retrieve the widget class of the widget
	// return NULL in case of problems

CWIDGET get_parent_widget(CWIDGET widget);
	// retrieve the parent widget of the widget
	// return NULL in case of problems

HWND get_widget_hwnd(CWIDGET widget);
	// returns the HWND of the widget 

//---------------------------------------------------------
#ifndef NO_X_MOTIF

#	define Widget						CWIDGET
#	define WidgetClass					CWIDGETCLASS

#	define shellWidgetClass				CSHELL_CLASS
#	define topLevelShellWidgetClass		CTOP_LEVEL_SHELL_CLASS
#	define xmLabelWidgetClass			CLABEL_CLASS
#	define xmPushButtonWidgetClass		CPUSHBUTTON_CLASS
#	define xmToggleButtonWidgetClass	CTOGGLEBUTTON_CLASS
#	define xmTextWidgetClass			CTEXT_CLASS
#	define xmListWidgetClass			CLIST_CLASS
#	define xmBulletinBoardWidgetClass	CBULLETIN_BOARD_CLASS
#	define xmMessageBoxWidgetClass		CMESSAGE_BOX_CLASS
#	define xmSelectionBoxWidgetClass	CSELECTION_BOX_CLASS
#	define xmRowColumnWidgetClass		CROWCOLUMN_CLASS
#	define xmFrameWidgetClass			CFRAME_CLASS
#	define xmFormWidgetClass			CFORM_CLASS
#	define xmSeparatorWidgetClass		CSEPARATOR_CLASS
#	define xmScrolledWindowWidgetClass	CSCROLLEDWINDOW_CLASS
#	define xmDrawingAreaWidgetClass				CDRAWINGAREA_CLASS

#	define xmOptionMenuWidgetClass		COPTIONMENU_CLASS
#	define xmOptionItemWidgetClass		COPTIONITEM_CLASS
#	define xmMenuItemWidgetClass		CMENUITEM_CLASS
#	define xmMenuBarWidgetClass			CMENUBAR_CLASS
#	define xmMenuPopupWidgetClass		CMENUPOPUP_CLASS
#	define xmSubMenuWidgetClass			CSUBMENU_CLASS

#	define coreWidgetClass				CCORE_CLASS
#	define compositeWidgetClass			CCOMPOSITE_CLASS
#	define xmPrimitiveWidgetClass		CPRIMITIVE_CLASS

#	define Arg							ARG
#	define ArgList						ARGLIST
#	define XtArgVal						RES_VAL
#	define XtSetArg						set_arg
#	define XtSetValues					set_widget_values
#	define XtGetValues					get_widget_values
#	define XtClass						get_widget_class
#	define XtParent						get_parent_widget
#	define XtDestroyWidget				destroy_widget
#	define XtMapWidget					map_widget
#	define XtUnmapWidget				unmap_widget
#	define XtRealize					realize_widget
#	define XtAddCallback				add_widget_callback

#endif
//---------------------------------------------------------
#ifdef __cplusplus
	}
#endif
//---------------------------------------------------------
#endif


