//-------------------------------------------------------------------
// RESOURCE_NAME.hxx
// -----------------
// a RESOURCE_NAME is a hierarchical name of a resource
// J.E.Bollo and G.Musset, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __window_name_hxx
#define __window_name_hxx
//-------------------------------------------------------------------
#include "macros.h"
#include "NAMES_SPACE.hxx"
//-------------------------------------------------------------------
class RESOURCE_NAME
{
private:

	enum { default_allocated_length = 32 };
		// length of the buffer of name "statically" allocated 

	enum STATES { CLEAR, STATIC_SET, DYNAMIC_SET };
		// possible states 

	enum MODES { UNSET, ROOT_TO_OBJECT, OBJECT_TO_ROOT };
		// possible states 

	STATES current_state;
		// current state of the objet

	MODES growing_mode;
		// current mode of growing

	NAME names_buffer[2][default_allocated_length];
		// the buffer of name "statically" allocated 
		
	int level;
		// level of the hierarchical object name

	NAME * class_names;
		// array of hierarchical class names

	NAME * object_names;
		// array of hierarchical object names

private:

	void clear_state();
		// inconditionnal clearing of the state

	void grow_arrays();
		// make arrays growing of one level
		// dont change level

	int index_of(int lvl) const
		// get the internal index for the given level
	{
		return growing_mode==ROOT_TO_OBJECT ? lvl : (level-lvl-1);
	}

public:

	RESOURCE_NAME()
		// empty construction, only state is revelant
		 : current_state(CLEAR)
		 , growing_mode(UNSET)
		 , level(0)
		{}

	~RESOURCE_NAME()
		// destruction is like clearing state
		{ clear_state(); }

	void growing_from_root_to_object()
		// set growing_mode from root to objet
		// and reset the resource name
	{
		growing_mode = ROOT_TO_OBJECT;
		level = 0;
	}

	void growing_from_object_to_root()
		// set growing_mode from root to objet
		// and reset the resource name
	{
		growing_mode = OBJECT_TO_ROOT;
		level = 0;
	}

	void grow(NAME object_name,NAME class_name)
		// grow this by one unit using object_name, class_name
		// and the current growing_mode
	{
		grow_arrays();
		object_names[level] = object_name;
		class_names[level] = class_name;
		level++;
	}

	void ungrow()
		// ungrow this by one unit
		// growing_mode should be ROOT_TO_OBJECT
	{
		assert(level!=0,("requests on empty names are prohibited"));
		assert(growing_mode==ROOT_TO_OBJECT,("invalid mode for ungrow"));
		level--;
	}

	int get_level() const
		// get current level
	{
		return level;
	}

	NAME class_name(int i) const
		// retrieve the i th class name (0 is toplevel)
	{
		assert(level!=0,("requests on empty names are prohibited"));
		//assert(current_state!=CLEAR,("uninitialized WINDOW_NAME"));
		//assert(growing_mode!=UNSET,("invalid growing mode"));
		assert(i>=0,("invalid parameter: negatif"));
		assert(i<level,("invalid parameter: greater than level"));
		return class_names[index_of(i)];
	}

	NAME object_name(int i) const
		// retrieve the i th window name (0 is toplevel)
	{
		assert(level!=0,("requests on empty names are prohibited"));
		// and so assert(current_state!=CLEAR,("uninitialized WINDOW_NAME"));
		// and so assert(growing_mode!=UNSET,("invalid growing mode"));
		assert(i>=0,("invalid parameter: negatif"));
		assert(i<level,("invalid parameter: greater than level"));
		return object_names[index_of(i)];
	}
};
//-------------------------------------------------------------------
#endif
