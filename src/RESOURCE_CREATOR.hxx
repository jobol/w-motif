//-------------------------------------------------------------------
// RESOURCE_CREATOR.hxx
// --------------------
// object used to create new entries in a RESOURCE_DATABASE
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __resource_creator_hxx
#define __resource_creator_hxx
//-------------------------------------------------------------------
#include "macros.h"
//-------------------------------------------------------------------
class RESOURCE_DATABASE;
class RESOURCE_TREE;
class NAMES_SPACE;
//-------------------------------------------------------------------
class RESOURCE_CREATOR
	// resource creator acts like a resource iterator 
	// dedicated to the creation of new filters
	// the implementation controls the state of creation
	// in a hard way
	// allowed sequences of operations must conform to the
	// following grammar ([]:option(0 or 1),<>:repeat(1 or more):
	//
	// life
	//	: creation [<operation>] [any_sequence] destruction
	//
	// operation
	//	: normal_sequence validate
	//  | any_sequence invalidate;
	//
	// normal_sequence
	//  : naming valuing;
	//
	// any_sequence
	//  : 
	//  | naming 
	//  | naming valuing;
	//
	// naming
	//  : start_naming [<name>];
	//
	// valuing
	//  : end_naming [<set_value>];
	//
	// name
	//  : [add_wild_bind] a_name;
	//
	// a_name
	//  : add_name
	//  | add_any_name;
{
private:

	enum STATES { CLEAR, NAMING, VALUING };
		// enumerate the states of the object
	
	STATES state;
		// current state

	RESOURCE_DATABASE * the_database;
		// the data base for wich that is an entry

	RESOURCE_TREE * current_resource;
		// curent position in the resource tree

public:

	RESOURCE_CREATOR(RESOURCE_DATABASE &resources);
		// construction of a new resource creator

	~RESOURCE_CREATOR();
		// destruction of the resource creator

	void start_naming();
		// start definition of names

	void add_wild_bind();
		// add a wild bind in the definition of names

	void add_any_name();
		// add the name "any name" in the definition of names

	void add_name(const char * name);
		// add the name in the definition of names

	void end_naming();
		// end definition of names

	void set_value(const char * value);
		// set the value associated to the name

	void invalidate();
		// invalidate the current entry

	void validate();
		// validate the current entry
};
//-------------------------------------------------------------------
#endif
