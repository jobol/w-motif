//---------------------------------------------------------
// RESOURCE_CLIENT.cxx
// -------------------
// polymorphic resource descriptor class
// J.E.Bollo, M3G (c) 1998
//---------------------------------------------------------
#include "RESOURCE_CLIENT.hxx"
//---------------------------------------------------------

void RESOURCE_CLIENT::set_resource_values(const RESOURCE_SCANNER & scanner)
	// put values of scanner, the currently scanned resources, to this
{
	const RESOURCE_CLASS * class_resource = get_resource_class();
	assert(class_resource!=NULL,("internal problem"));
	class_resource->set_resource_values(&scanner,this);
}		

//---------------------------------------------------------

