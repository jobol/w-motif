//-------------------------------------------------------------------
// WIN_FORM.cxx
// ---------------
// implements the "XmForm" class
// J.E.Bollo & G.Grison, M3G (c) 1998
//-------------------------------------------------------------------
#include "WIN_FORM.hxx"
#include "WINDOWS_MANAGER.hxx"
#include "CONSTRAINT.hxx"
#include "RESOURCE_NAMES_SPACE.hxx"
//-------------------------------------------------------------------
//
//   PART 1 : THE SIDES MANAGEMENT SYSTEM
//
//-------------------------------------------------------------------

class SIDE
	// The class side serves the WIN_FORM objects to manage 
	//  them children constrained geometry.
{
public:
	// Those constants define the four allowed sides

	static const int left_side;
		// the left side is  equal to 0: positive and vertical

	static const int top_side;
		// the top side is  equal to 1: positive and horizontal

	static const int right_side;
		// the right side is  equal to 2: negative and vertical

	static const int bottom_side;
		// the bottom side is  equal to 3: negative and horizontal

private:
	// private members

	int side;
		// The side represented by this

	enum 
		// sign of side definition
		{ 
			positive = 0, 
				// positive means that going in is adding a positive
				//  offset, left and top sides are positive.

			negative = 2, 
				// negative means that going in is adding a negative
				//  offset, right and bottom sides are negative.

			sign_mask = 2
				// for internal use: selects the sign bit
		};

	enum 
		// orientation of side definition
		{ 
			horizontal = 1, 
				// horizontal means that the side is represented as 
				//  horizontal, top and bottom are horizontal

			vertical = 0,
				// vertical means that the side is represented as 
				//  vertical, left and right are vertical

			orientation_mask = 1
				// for internal use: selects the orientation bit
		};

protected:
	// private methods

	bool is_positive() const { return (side & sign_mask) == positive; }
		// true when side is positive (left or top)

	bool is_negative() const { return (side & sign_mask) == negative; }
		// true when side is positive (right or bottom)

	bool is_vertical() const { return (side & orientation_mask) == vertical; }
		// true when side is vertical (left or right)

	bool is_horizontal() const { return (side & orientation_mask) == horizontal; }
		// true when side is horizontal (top or bottom)

public:
	// public methods

	SIDE(int side) : side(side) {}
		// construct the side from an integer

	operator int () const { return side; }
		// convert to integer (or index)

	SIDE opposite_side() const { return SIDE(side ^ sign_mask); }
		// get the opposite side of this

	int step_in(int x) const { return is_positive() ? x : -x; }
		// apply to x the sign correction in way such that 
		//  a positive x return an offset that go in the rectangle 
		//  bordered by the this.

	int step_out(int x) const { return is_positive() ? -x : x; }
		// apply to x the sign correction in way such that 
		//  a positive x return an offset that go out the rectangle 
		//  bordered by the this.

	int select_length(int width, int height) const
		// select the length of the side depending on its orientation
		{ return is_horizontal() ? height : width; }

	int select_side(int x, int y) const
		// select the side affix coordinate depending on its orientation
		{ return is_horizontal() ? y : x; }

	int select_edge(int x, int y) const
		// select the side edge coordinate depending on its orientation
		{ return is_horizontal() ? x : y; }

	int select(int left, int top, int right, int bottom) const
		// select the side coordinate 
		{ return is_horizontal() 
				? (is_positive() ? top : bottom)
				: (is_positive() ? left : right); }
};

//-------------------------------------------------------------------

const int SIDE::left_side = SIDE::positive | SIDE::vertical;
	// the left side is  equal to 0: positive and vertical

const int SIDE::top_side = SIDE::positive | SIDE::horizontal;
	// the top side is  equal to 1: positive and horizontal

const int SIDE::right_side = SIDE::negative | SIDE::vertical;
	// the right side is  equal to 2: negative and vertical

const int SIDE::bottom_side = SIDE::negative | SIDE::horizontal;
	// the bottom side is  equal to 3: negative and horizontal

//-------------------------------------------------------------------
//
//   PART 2 : THE CONSTRAINTS FOR THE WIN_FORM CHILDREN
//
//-------------------------------------------------------------------

class FORM_CONSTRAINT 
	: public CONSTRAINT
	// The FORM_CONSTRAINT class stores contraints and informations
	//  about the children of a WIN_FORM widget.
{
	friend class WIN_FORM;
		// only WIN_FORM use this class

	DECLARE_RESOURCE_CLIENT(FORM_CONSTRAINT)

public:

	virtual void set_resource_values(const RESOURCE_SCANNER & scanner)
		// put values of scanner, the currently scanned resources, to this
		// this methods overwrite default one because we have difficulties to 
		//  use C++ pointers to members that are arrays fields
	{
		// private and statical values
		enum { attachment, offset, position };
		static NAME names[4][3];
		static bool names_done = false;

		// locale
		int side;

		// declaration of names to the resource_names_space
		if (!names_done)
		{
			side = SIDE::left_side;
			names[side][attachment] = resource_names_space.add(XmNleftAttachment);
			names[side][offset]     = resource_names_space.add(XmNleftOffset);
			names[side][position]   = resource_names_space.add(XmNleftPosition);

			side = SIDE::top_side;
			names[side][attachment] = resource_names_space.add(XmNtopAttachment);
			names[side][offset]     = resource_names_space.add(XmNtopOffset);
			names[side][position]   = resource_names_space.add(XmNtopPosition);

			side = SIDE::right_side;
			names[side][attachment] = resource_names_space.add(XmNrightAttachment);
			names[side][offset]     = resource_names_space.add(XmNrightOffset);
			names[side][position]   = resource_names_space.add(XmNrightPosition);

			side = SIDE::bottom_side;
			names[side][attachment] = resource_names_space.add(XmNbottomAttachment);
			names[side][offset]     = resource_names_space.add(XmNbottomOffset);
			names[side][position]   = resource_names_space.add(XmNbottomPosition);

			names_done = true;
		}

		// scans for each side
		for (side = 0 ; side < 4 ; side ++)
		{
			const char * value;

			value = scanner.get_value(names[side][attachment]);
			if(value != NULL)
				sides[side].attachment = RESOURCE_DESCRIPTOR::text_to_enum(value);

			value = scanner.get_value(names[side][offset]);
			if(value != NULL)
				sides[side].offset = atoi(value);

			value = scanner.get_value(names[side][position]);
			if(value != NULL)
				sides[side].position = atoi(value);
		}
	}
		
private:

	struct SIDE_DATA
		// that structure records side's informations
	{
		int attachment;
			// kind of attachment
			// is one of the following values:
			//	XmATTACH_NONE, XmATTACH_FORM,
			//  XmATTACH_OPPOSITE_FORM, XmATTACH_WIDGET,
			//  XmATTACH_OPPOSITE_WIDGET, XmATTACH_POSITION, XmATTACH_SELF

		WIN_CORE * widget;
			// the attched widget if any

		int offset;
			// offset to base line

		int position;
			// position of base line

		enum { clear, running, set } state;
			// is the position (the coordinate) set? 
			//  being computed (running)? or clear?

		int coordinate;
			// current computed coordinate of the side

		SIDE_DATA() 
			// default initialisation
			: attachment(XmATTACH_NONE)
			, widget(NULL)
			, offset(0)
			, position(0)
			, state(clear)
			, coordinate(0)
			{}

		void set_clear()        { state = clear; }
			// set state to clear

		void set_running()      { state = running; }
			// set state to running (being computed)

		void set_set()          { state = set; }
			// set state to set

		bool is_clear() const   { return state == clear; }
			// true when state is clear

		bool is_running() const { return state == running; }
			// true when state is running

		bool is_set() const     { return state == set; }
			// true when state is set
	};

	SIDE_DATA sides[4];
		// four sides of constaints

	FORM_CONSTRAINT(int horizontal_offset=0, int vertical_offset=0)
		// construction is private (the only client WIN_FORM is a friend)
	{
		sides[SIDE::left_side].offset = horizontal_offset;
		sides[SIDE::top_side].offset = vertical_offset;
		sides[SIDE::right_side].offset = horizontal_offset;
		sides[SIDE::bottom_side].offset = vertical_offset;
	}

public:

	~FORM_CONSTRAINT() {}
		// destruction does nothing

	void clear_constraint(int x,int y,int width,int height)
		// clears the four sides states and reset coordinates to
		//  x, y, width, height or equivalent
	{
		sides[SIDE::left_side].set_clear();
		sides[SIDE::top_side].set_clear();
		sides[SIDE::right_side].set_clear();
		sides[SIDE::bottom_side].set_clear();

		sides[SIDE::left_side].coordinate = x;
		sides[SIDE::top_side].coordinate = y;
		sides[SIDE::right_side].coordinate = x + width - 1;
		sides[SIDE::bottom_side].coordinate = y + height - 1;
	}

	void set_widget_geometry(WIN_CORE *widget)
		// set the geometry of the given widget accordling to the
		//  current values of the four coordinates of the four sides.
	{
		int x = sides[SIDE::left_side].coordinate;
		int y = sides[SIDE::top_side].coordinate;
		int w = 1 + sides[SIDE::right_side].coordinate - x;
		int h = 1 + sides[SIDE::bottom_side].coordinate - y;
		widget->set_geometry(x,y,w,h);
	}

	int get_side_length_minus_one(SIDE side) const
	{
		int width = sides[SIDE::right_side].coordinate - sides[SIDE::left_side].coordinate;
		int height = sides[SIDE::bottom_side].coordinate - sides[SIDE::top_side].coordinate;
		return side.select_length(width,height);
	}
};

//-------------------------------------------------------------------

BEGIN_ROOT_RES_DESC(FORM_CONSTRAINT,NAME_OF_FORM_CONSTRAINT_CLASS)
	// empty. see public method 'set_resource_values'
END_RES_DESC

//-------------------------------------------------------------------
//
//   PART 3 : THE WIN_FORM METHODS
//
//-------------------------------------------------------------------
BEGIN_RES_DESC(WIN_FORM,WIN_BULLETIN_BOARD,NAME_OF_WIN_FORM_CLASS)
	RES_DESC_ATR(XmNfractionBase,		INTEGER,	WIN_FORM, fraction_base),
	RES_DESC_ATR(XmNhorizontalSpacing,	INTEGER,	WIN_FORM, horizontal_spacing),
	RES_DESC_ATR(XmNverticalSpacing,	INTEGER,	WIN_FORM, vertical_spacing),
END_RES_DESC
//-------------------------------------------------------------------
IMPLEMENT_WINDOW(WIN_FORM)
//-------------------------------------------------------------------
ATOM WIN_FORM::register_window_class() const
{
	return WINDOWS_MANAGER::register_window_class(
					(const char*)get_class_name(),0,
					LoadCursor(NULL,IDC_ARROW));
}
//-------------------------------------------------------------------
WIN_FORM::WIN_FORM()
	: WIN_BULLETIN_BOARD()
	, fraction_base(100)
	, horizontal_spacing(0)
	, vertical_spacing(0)
	, requesting_child(NULL)
	, child_geometry_request(NULL)
{}
//-------------------------------------------------------------------
WIN_FORM::~WIN_FORM()
{
}
//-------------------------------------------------------------------
bool WIN_FORM::create_window()
{
	return WIN_CORE::create_window(WS_CHILD|WS_CLIPSIBLINGS|WS_VISIBLE);
}
//-------------------------------------------------------------------
void WIN_FORM::insert_child (WIN_CORE * child)
	// method for child insertion specific to the Form
{
	assert(child!=NULL,("null child!"));
	WIN_COMPOSITE::insert_child (child);
	child->constraints = new FORM_CONSTRAINT(horizontal_spacing, vertical_spacing);
	assert(child->constraints!=NULL,("memory depletion"));
}
//-------------------------------------------------------------------
void WIN_FORM::change_managed()
	// The method change_manager called once during the realize phase and then
	// every time that one of its children becomes managed or unmanaged
{
	do_layout(true,NULL,NULL,NULL);
}
//-------------------------------------------------------------------
XtGeometryResult WIN_FORM::geometry_manager
(
	WIN_CORE			*child,
	XtWidgetGeometry	*request,
	XtWidgetGeometry	*geometry_return
) 
	// The method geometry_manager called every time  
	// one of its children requests a new size or position
{
	assert(requesting_child==NULL,("internal error"));

	if (request != NULL && geometry_return != NULL)
		*geometry_return = *request;

	// compute new size
	XtWidgetGeometry resulting_geometry;
	if(do_layout(true,child,geometry_return,&resulting_geometry))
	{
		requesting_child = child;
		child_geometry_request = request;

		XtGeometryResult resu = geometry_request(&resulting_geometry);

		requesting_child = NULL;
		child_geometry_request = NULL;

		// if geometry change then query to do it in fact
		if(resu==XtGeometryNo)
		{
			// no if growing
			if((resulting_geometry.request_mode & CWWidth)!=0
				&& resulting_geometry.width>width && child != NULL)
				return XtGeometryNo;
			if((resulting_geometry.request_mode & CWHeight)!=0
 				&& resulting_geometry.height>height && child != NULL)
				return XtGeometryNo;

			// yes if not growing
			do_layout(true, child, geometry_return, &resulting_geometry);
		}
	}
	
	return XtGeometryDone;
}
//-------------------------------------------------------------------
int WIN_FORM::find_and_fixe_child_coordinate (WIN_CORE * child, SIDE side) const
	// 
{
	assert (containts_child(child),("child not contained"));

	// usefuls shortnames
	FORM_CONSTRAINT * constraint = (FORM_CONSTRAINT *)child->constraints;
	FORM_CONSTRAINT::SIDE_DATA * side_data = &(constraint->sides[side]);

	// checks the state
	if(!side_data->is_clear())
	{
		if(!side_data->is_set())
			; // loop detected
		return side_data->coordinate;
	}

	// computing in progress
	side_data->set_running();

	// locals
	SIDE opposite_side = side.opposite_side();
	int ret, temp;

	// treatment of the attachment
	switch(side_data->attachment) 
	{
	case XmATTACH_NONE:
		temp = constraint->get_side_length_minus_one(side);
		ret = find_and_fixe_child_coordinate (child, opposite_side);
		ret += side.step_out(temp);
		break;

	case XmATTACH_FORM:
		ret = side.select(0,0,width-1,height-1);
		ret += side.step_in(side_data->offset);
		break;

	case XmATTACH_OPPOSITE_FORM:
		ret = opposite_side.select(0,0,width-1,height-1);
		ret += side.step_in(side_data->offset);
		break;

	case XmATTACH_WIDGET:
		if (containts_child(side_data->widget))
		{
			ret = find_and_fixe_child_side (side_data->widget,opposite_side);
			ret += side.step_in(side_data->offset);
		}
		else
			ret = side_data->coordinate;
		break;

	case XmATTACH_OPPOSITE_WIDGET:
		if (containts_child(side_data->widget))
		{
			ret = find_and_fixe_child_side (side_data->widget,side);
			ret += side.step_in(side_data->offset);
		}
		else
			ret = side_data->coordinate;
		break;

	case XmATTACH_POSITION:
		ret = side_data->position * side.select_length(width,height);
		if(fraction_base!=0)
			ret /= fraction_base;
		ret += side.step_in(side_data->offset);
		break;

	default:
		// error ... set as XmATTACH_SELF

	case XmATTACH_SELF:
		ret = side_data->coordinate;
		temp = side.select_length(width,height);
		if(temp!=0)
		{
			temp = (fraction_base!=0 ? ret*fraction_base : ret) / temp;
			side_data->attachment = XmATTACH_POSITION;
			side_data->position = temp;
		}
		break;
	}

	side_data->set_set();
	return side_data->coordinate = ret;
}
//-------------------------------------------------------------------
int WIN_FORM::find_and_fixe_child_side (WIN_CORE * child, SIDE side) const
	// 
{
	if(!containts_child(child))
		return 0;

	int coordinate = find_and_fixe_child_coordinate (child, side);

	FORM_CONSTRAINT * constraint = (FORM_CONSTRAINT *)child->constraints;
	FORM_CONSTRAINT::SIDE_DATA * side_data = &(constraint->sides[side]);

	return coordinate + side.step_out(side_data->offset);
}
//-------------------------------------------------------------------
bool WIN_FORM::do_layout
(
	bool			  act, // act == 1 => redimensionne les enfants
	WIN_CORE		 *the_child, // != 0 si sollicit� par 1 enfant
	XtWidgetGeometry *request, // dimension sollicit��e par l'enfant
	XtWidgetGeometry *result	// dim calcul�e pour la forme
)
	// do the RowColumn layout
	// return true if geometry need to change
	// if act is true the geometry is changed automatically (by geometry_request)
	// the geometry is returned in result if not NULL
	// it takes into account the_child request if both are not NULL
{
	// make a virtual result if needed
	XtWidgetGeometry internal_result_if_needed;
	if(result==NULL)
		result = &internal_result_if_needed;

	if (the_child == NULL && requesting_child != NULL)
	{
		the_child = requesting_child;
		request = child_geometry_request;
	}

	if (!act)
	{	
		// do a virtual layout and compute the new size
		// of the Form induced by this layout
		//---------------------------------------------
		//return true;
		// act == true: perform effective layout of children 
		//--------------------------------------------------
	}
	
	// reset children constraints states

	WIN_CORE * child;
	FORM_CONSTRAINT * constraint;
	int i;

	for (i = 0; i < num_children; i++) 
	{
		child = children[i];
		assert (child != NULL,("no null child allowed"));

		int left = child->x;
		int top = child->y;
		int width = child->width;
		int height = child->height;

		if (child==the_child) 
		{
			if((request->request_mode & CWX)!=0)
				left = request->width;
			if((request->request_mode & CWY)!=0)
				top = request->height;
			if((request->request_mode & CWWidth)!=0)
				width = request->width;
			if((request->request_mode & CWHeight)!=0)
				height = request->height;
		}
		else
		{
			XtWidgetGeometry query, wanted;
			query.request_mode = CWWidth | CWHeight;
			query.width = width;
			query.height = height;
			XtGeometryResult response = child->query_geometry(&query,&wanted);
			if(response!=XtGeometryNo)
			{
				if((wanted.request_mode & CWWidth)!=0)
					width = wanted.width;
				if((wanted.request_mode & CWHeight)!=0)
					height = wanted.height;
			}
		}

		constraint = (FORM_CONSTRAINT *)child->constraints;
		constraint->clear_constraint(left,top,width,height);
	}

	for (i = 0; i < num_children; i++)
	{
		child = children[i];
		find_and_fixe_child_side (child, SIDE::left_side);
		find_and_fixe_child_side (child, SIDE::top_side);
		find_and_fixe_child_side (child, SIDE::right_side);
		find_and_fixe_child_side (child, SIDE::bottom_side);
	}

	if(act)
	{
		WINDOWS_MANAGER::freeze_changes();

		for (i = 0; i < num_children; i++) 
		{
			child = children[i];
			constraint = (FORM_CONSTRAINT *)child->constraints;
			constraint->set_widget_geometry(child);
		}

		WINDOWS_MANAGER::unfreeze_changes();
	}

	return false;
}
//-------------------------------------------------------------------
XtGeometryResult WIN_FORM::query_geometry
(
	XtWidgetGeometry *request, 
	XtWidgetGeometry *prefered
)
	// The method query_geometry called when the parent  
	// wants to know the widget's prefered size
{
	XtWidgetGeometry pref;
	do_layout(false,NULL,NULL,&pref);

	return complete_query_geometry(request,prefered,&pref);
}
//-------------------------------------------------------------------
void WIN_FORM::resize()
	// The method resize called every time a widget changes size
{
	do_layout(true, NULL, NULL, NULL);
}
//-------------------------------------------------------------------
int WIN_FORM::constraint_set_values
(
	WIN_CORE *child,
	ARGLIST	  arglist,
	int		  narg,
	bool	 *got,
	bool    (*equality)(const char*,const char*)
)
{
	FORM_CONSTRAINT *constraint = (FORM_CONSTRAINT*)child->constraints;

	// loop on arglist
	int found_count = 0;
	int index = narg;
	while(index>0)
	{
		// skip when already done
		if (got[--index])
			continue;

		const char * attribut = arglist[index].name;
		bool found = true;

		if (equality(attribut,XmNleftAttachment))
			constraint->sides[SIDE::left_side].attachment = arglist[index].value.integer;
		else
		if (equality(attribut,XmNleftOffset))
			constraint->sides[SIDE::left_side].offset = arglist[index].value.integer;
		else
		if (equality(attribut,XmNleftPosition))
			constraint->sides[SIDE::left_side].position = arglist[index].value.integer;
		else
		if (equality(attribut,XmNleftWidget))
		{
			WIN_CORE * widget = (WIN_CORE*)(arglist[index].value.address);
			constraint->sides[SIDE::left_side].widget = containts_child(widget) ? widget : NULL;
		}
		else
		if (equality(attribut,XmNrightAttachment))
			constraint->sides[SIDE::right_side].attachment = arglist[index].value.integer;
		else
		if (equality(attribut,XmNrightOffset))
			constraint->sides[SIDE::right_side].offset = arglist[index].value.integer;
		else
		if (equality(attribut,XmNrightPosition))
			constraint->sides[SIDE::right_side].position = arglist[index].value.integer;
		else
		if (equality(attribut,XmNrightWidget))
		{
			WIN_CORE * widget = (WIN_CORE*)(arglist[index].value.address);
			constraint->sides[SIDE::right_side].widget = containts_child(widget) ? widget : NULL;
		}
		else
		if (equality(attribut,XmNtopAttachment))
			constraint->sides[SIDE::top_side].attachment = arglist[index].value.integer;
		else
		if (equality(attribut,XmNtopOffset))
			constraint->sides[SIDE::top_side].offset = arglist[index].value.integer;
		else
		if (equality(attribut,XmNtopPosition))
			constraint->sides[SIDE::top_side].position = arglist[index].value.integer;
		else
		if (equality(attribut,XmNtopWidget))
		{
			WIN_CORE * widget = (WIN_CORE*)(arglist[index].value.address);
			constraint->sides[SIDE::top_side].widget = containts_child(widget) ? widget : NULL;
		}
		else
		if (equality(attribut,XmNbottomAttachment))
			constraint->sides[SIDE::bottom_side].attachment = arglist[index].value.integer;
		else
		if (equality(attribut,XmNbottomOffset))
			constraint->sides[SIDE::bottom_side].offset = arglist[index].value.integer;
		else
		if (equality(attribut,XmNbottomPosition))
			constraint->sides[SIDE::bottom_side].position = arglist[index].value.integer;
		else
		if (equality(attribut,XmNbottomWidget))
		{
			WIN_CORE * widget = (WIN_CORE*)(arglist[index].value.address);
			constraint->sides[SIDE::bottom_side].widget = containts_child(widget) ? widget : NULL;
		}
		else
			found = false;

		if (found)
		{
			found_count++;
			got[index] = true;
		}
	}
	return found_count;
}
//-------------------------------------------------------------------
int WIN_FORM::constraint_get_values
(
	const WIN_CORE	*child ,
	ARGLIST			 arglist,
	int				 count,
	bool			*got,
	bool		   (*equality)(const char*,const char*)
) const
{
	FORM_CONSTRAINT *constraint = (FORM_CONSTRAINT*)child->constraints;

	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		// skip when already done
		if (got[--index])
			continue;

		const char * attribut = arglist[index].name;
		bool found = true;

		if (equality(attribut,XmNleftAttachment))
			arglist[index].value.integer = constraint->sides[SIDE::left_side].attachment;
		else
		if (equality(attribut,XmNleftOffset))
			arglist[index].value.integer = constraint->sides[SIDE::left_side].offset;
		else
		if (equality(attribut,XmNleftPosition))
			arglist[index].value.integer = constraint->sides[SIDE::left_side].position;
		else
		if (equality(attribut,XmNleftWidget))
			arglist[index].value.address = (void*)(constraint->sides[SIDE::left_side].widget);
		else
		if (equality(attribut,XmNrightAttachment))
			arglist[index].value.integer = constraint->sides[SIDE::right_side].attachment;
		else
		if (equality(attribut,XmNrightOffset))
			arglist[index].value.integer = constraint->sides[SIDE::right_side].offset;
		else
		if (equality(attribut,XmNrightPosition))
			arglist[index].value.integer = constraint->sides[SIDE::right_side].position;
		else
		if (equality(attribut,XmNrightWidget))
			arglist[index].value.address = (void*)(constraint->sides[SIDE::right_side].widget);
		else
		if (equality(attribut,XmNtopAttachment))
			arglist[index].value.integer = constraint->sides[SIDE::top_side].attachment;
		else
		if (equality(attribut,XmNtopOffset))
			arglist[index].value.integer = constraint->sides[SIDE::top_side].offset;
		else
		if (equality(attribut,XmNtopPosition))
			arglist[index].value.integer = constraint->sides[SIDE::top_side].position;
		else
		if (equality(attribut,XmNtopWidget))
			arglist[index].value.address = (void*)(constraint->sides[SIDE::top_side].widget);
		else
		if (equality(attribut,XmNbottomAttachment))
			arglist[index].value.integer = constraint->sides[SIDE::bottom_side].attachment;
		else
		if (equality(attribut,XmNbottomOffset))
			arglist[index].value.integer = constraint->sides[SIDE::bottom_side].offset;
		else
		if (equality(attribut,XmNbottomPosition))
			arglist[index].value.integer = constraint->sides[SIDE::bottom_side].position;
		else
		if (equality(attribut,XmNbottomWidget))
			arglist[index].value.address = (void*)(constraint->sides[SIDE::bottom_side].widget);
		else
			found = false;

		if (found)
		{
			found_count++;
			got[index] = true;
		}
	}
	return found_count;
}

//-------------------------------------------------------------------

int WIN_FORM::internal_get_values
	// function used internally to get attribut values 
	// should not be called directly (call get_values)
	// arglist is the array of the values to get
	// count is the count of values to be put into arglist
	// got is an array that flag if an attribut have been put
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs put
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) const
{
	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.

		if(equality(attribut,XmNfractionBase))
			arglist[index].value.pvalue->integer = fraction_base;
		else
		if(equality(attribut,XmNhorizontalSpacing))
			arglist[index].value.pvalue->integer = horizontal_spacing;
		else
		if(equality(attribut,XmNverticalSpacing))
			arglist[index].value.pvalue->integer = vertical_spacing;
		else
			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
	}

	// return total count of found attributs (including ancestors)
	return found_count + WIN_BULLETIN_BOARD::internal_get_values(arglist,count,got,equality);
}

//-------------------------------------------------------------------

int WIN_FORM::internal_set_values
	// function used internally to set attribut values 
	// should not be called directly (call set_values)
	// arglist is the array of the values to be set,
	// count is the count of values in arglist
	// got is an array that flag if an attribut have been set
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs set
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) 
{
	// empty geometry request
	bool redraw_needed = false;
/*
	XtWidgetGeometry request;
	request.request_mode = 0;
*/

	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.

		if(equality(attribut,XmNfractionBase))
			fraction_base = arglist[index].value.integer;
		else
		if(equality(attribut,XmNhorizontalSpacing))
			horizontal_spacing = arglist[index].value.integer;
		else
		if(equality(attribut,XmNverticalSpacing))
			vertical_spacing = arglist[index].value.integer;
		else
			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
			redraw_needed = true;
		}
	}

/*
	// make the geometry request if necessary
	if(request.request_mode!=0)
		geometry_request(&request);
*/
	if(redraw_needed)
		redraw();

	// return total count of found attributs (including ancestors)
	return found_count  + WIN_BULLETIN_BOARD::internal_set_values(arglist,count,got,equality);
}

//-------------------------------------------------------------------

