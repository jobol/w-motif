//-------------------------------------------------------------------
// READ_RESOURCE_X.cxx
// -------------------
// object that read the X resources files for a RESOURCE_DATABASE
// J.E.BOLLO, M3G (c) 1998
//-------------------------------------------------------------------
#include "READ_RESOURCE_X.hxx"
#include "RESOURCE_DATABASE.hxx"
#include "RESOURCE_CREATOR.hxx"
#include "SIMPLE_WIN_READER.hxx"
//-------------------------------------------------------------------
BOOL READ_RESOURCE_X::read_resource
(
	const char         *file_name,         // filename
	RESOURCE_DATABASE  &resource_database, // RESOURCE_DATABASE
	SIMPLE_ERROR       &error
)
	// that function treats the resource name as a file name,
	// open that file throught a simple reader and then
	// calls the function that operate on simple reader 
{
	SIMPLE_WIN_READER reader;

	error.print("Opening file %s\n",file_name);
	BOOL result = reader.open_file(file_name);

	if(result==FALSE)
	{
		set_last_error(reader.get_last_error());
		error.add_error("can't open file %s",file_name);
	}
	else
	{
		error.set_file_name(file_name);
		result = read_resource(reader,resource_database,error);
		reader.close();
	}

	return result;
}
//-------------------------------------------------------------------
// Here we define the several constants of reading process
//-------------------------------------------------------------------
#define INPUT_LEN	4096
//-------------------------------------------------------------------
enum STATES {
	WAIT,
	AFTER_BIND,
	WAIT_BIND,
	IN_NAME,
	WAIT_END_NAMES,
	WAIT_VALUE,
	READ_VALUE,
	ESCAPED_VALUE,
	OCTAL_VALUE1,
	OCTAL_VALUE2,
	COMMENT
};
//-------------------------------------------------------------------
#define END_OF_LINE	'\n'
#define WILD_BIND	'*'
#define ANY_NAME	'?'
#define END_OF_NAMES	':'
#define STRICT_BIND	'.'
#define START_COMMENT	'!'
#define END_VALUE	END_OF_LINE
#define END_COMMENT	END_OF_LINE
#define DIRECTIVE	'#'
#define SPACE		' '
#define HTAB		'\t'
#define ESCAPE		'\\'
#define CRETURN     '\r'
//-------------------------------------------------------------------
#include <ctype.h>
#undef isspace
#define isspace(x)	((x)==SPACE || (x)==HTAB)
#define iseol(x)	((x)==END_OF_LINE)
//#define isupper(x)	((x)>='A' && (x)<='Z')
//#define islower(x)	((x)>='a' && (x)<='z')
//#define isalpha(x)	(isupper(x) || islower(x))
//#define isdigit(x)	((x)>='0' && (x)<='9')
//#define isalnum(x)	(isalpha(x) || isdigit(x))
#define isoctal(x)	((x)>='0' && (x)<='7')
#define isname(x)	(isalnum(x) || (x)=='-' || (x)=='_')
//-------------------------------------------------------------------
BOOL READ_RESOURCE_X::read_resource
(
	SIMPLE_READER      &reader,
	RESOURCE_DATABASE  &resource_db,
	SIMPLE_ERROR       &error
)
{
	char   input[INPUT_LEN];        // input buffer
	int    input_len;               // current input length

	int    end_encountered = 0;     // is the end of file encountered
	int    line = 1;                // current line number
	int    column = 1;              // current column number
	char   car;                     // scanned character
	STATES state = WAIT;            // current state of processing
	int    read_result;             // read status
	RESOURCE_CREATOR res_creator(resource_db);
	                                // creator in the resource database

	// main reading loop

	while(end_encountered == 0)
	{
		// read the next character

		read_result = reader.read(&car,sizeof(char));

		// checks the read status

		if(read_result == -1) // read error
		{
			set_last_error(reader.get_last_error());
			res_creator.invalidate();
			error.add_error(line,column,"fatal read error %d",get_last_error());
			return FALSE;
		}

		if(read_result == 0) // end of file
		{
			// at end of file we introduce a new line

			car = END_OF_LINE;
			end_encountered = 1;
		}

		if(car==CRETURN) continue;

		// process automaton

		switch(state)
		{
		case WAIT:

			// we wait for a new resource line

			if(car==START_COMMENT)
			{
				state = COMMENT;
			}
			else if(car==WILD_BIND || car==ANY_NAME || isname(car))
			{
				res_creator.start_naming();
				if(car==WILD_BIND)
				{
					res_creator.add_wild_bind();
					state = AFTER_BIND;
				}
				else if(car==ANY_NAME)
				{
					res_creator.add_any_name();
					state = WAIT_BIND;
				}
				else if(isname(car))
				{
					input[0] = car;
					input_len = 1;
					state = IN_NAME;
				}
			}
			else if(car==DIRECTIVE)
			{
				error.add_warning(line,column,"directives are skipped");
				state = COMMENT;
			}
			else if(!isspace(car) && !iseol(car))
			{
				error.add_error(line,column,"invalid character, line skipped");
				res_creator.invalidate();
				state = COMMENT;
			}
			break;

		case AFTER_BIND:

			// after a bind there must be a name

			if(car==ANY_NAME)
			{
				res_creator.add_any_name();
				state = WAIT_BIND;
			}
			else if(isname(car))
			{
				input[0] = car;
				input_len = 1;
				state = IN_NAME;
			}
			else
			{
				error.add_error(line,column,"invalid character, line skipped");
				res_creator.invalidate();
				state = COMMENT;
			}
			break;

		case WAIT_BIND:

			// after the name ANY there must be a bind

			if(car==WILD_BIND)
			{
				res_creator.add_wild_bind();
				state = AFTER_BIND;
			}
			else if(car==STRICT_BIND)
			{
				state = AFTER_BIND;
			}
			else
			{
				error.add_error(line,column,"invalid character, line skipped");
				res_creator.invalidate();
				state = COMMENT;
			}
			break;

		case IN_NAME:

			// valid end of name are a bind or end of name filter

			if(isname(car))
			{
				if(input_len+1==INPUT_LEN)
				{
					error.add_error(line,column,"name too long, line skipped");
					res_creator.invalidate();
					state = COMMENT;
				}
				else
				{
					input[input_len++] = car;
				}
			}
			else if(car==WILD_BIND || car==STRICT_BIND
				|| car==END_OF_NAMES || isspace(car))
			{
				input[input_len] = 0;
				res_creator.add_name(input);
				if(car==WILD_BIND)
				{
					res_creator.add_wild_bind();
					state = AFTER_BIND;
				}
				else if(car==STRICT_BIND)
				{
					state = AFTER_BIND;
				}
				else if(isspace(car))
				{
					res_creator.end_naming();
					state = WAIT_END_NAMES;
				}
				else if(car==END_OF_NAMES)
				{
					res_creator.end_naming();
					state = WAIT_VALUE;
				}
			}
			else
			{
				error.add_error(line,column,"invalid character, line skipped");
				res_creator.invalidate();
				state = COMMENT;
			}
			break;

		case WAIT_END_NAMES:

			// when names are ended with a space we wait end marker

			if(car==END_OF_NAMES)
			{
				state = WAIT_VALUE;
			}
			else if(!isspace(car))
			{
				error.add_error(line,column,"invalid character, line skipped");
				res_creator.invalidate();
				state = COMMENT;
			}
			break;

		case WAIT_VALUE:

			// after end name we expect a value

			if(isspace(car)) break;
			state = READ_VALUE;
			input_len = 0;
				// CAUTION: no break here
			
		case READ_VALUE:

			// read the value and treat the escape codes

			if(car==END_VALUE || input_len+1==INPUT_LEN)
			{
				input[input_len] = 0;
				res_creator.set_value(input);
				res_creator.validate();
				if(car!=END_VALUE)
				{
					error.add_error(line,column,"value too long, value troncated");
					state = COMMENT;
				}
				else
				{
					state = WAIT;
				}
			}
			else if(car==ESCAPE)
			{
				state = ESCAPED_VALUE;
			}
			else
			{
				input[input_len++] = car;
			}
			break;

		case ESCAPED_VALUE:

			// wait valid escape continuation

			if(car==END_OF_LINE)
			{
				state = READ_VALUE;
			}
			else if(isspace(car) || car==ESCAPE)
			{
				input[input_len++] = ESCAPE;
				state = READ_VALUE;
			}
			else if(car=='n')
			{
				input[input_len++] = '\n';
				state = READ_VALUE;
			}
			else if(car=='r')
			{
				input[input_len++] = '\r';
				state = READ_VALUE;
			}
			else if(isoctal(car))
			{
				state = OCTAL_VALUE1;
				input[input_len] = car - '0';
			}
			else
			{
				error.add_error(line,column,"invalid character in escape, ignored");
				state = READ_VALUE;
			}
			break;

		case OCTAL_VALUE1:

			// octal char value escape continuation

			if(isoctal(car))
			{
				input[input_len] *= 8;
				input[input_len] += car - '0';
				state = OCTAL_VALUE2;
			}
			else
			{
				error.add_error(line,column,"invalid octal in escape, ignored");
				state = READ_VALUE;
			}
			break;

		case OCTAL_VALUE2:

			// octal char value escape continuation bis

			if(isoctal(car))
			{
				input[input_len] *= 8;
				input[input_len++] += car - '0';
				state = READ_VALUE;
			}
			else
			{
				error.add_error(line,column,"invalid octal in escape, ignored");
				state = READ_VALUE;
			}
			break;

		case COMMENT:

			// comment or error degradation run until end

			if(car == END_COMMENT) state = WAIT;
			break;
		}

		// update position for error reporting

		if(car==END_OF_LINE)
			column=1, line++;
		else
			column++;
	}

	// return TRUE when no error was found, FALSE otherwise

	return error.get_error_count() == 0;
}
//-------------------------------------------------------------------

