//-------------------------------------------------------------------
// WINDOWS_MANAGER.cxx
// -------------------
// that class is a static class designed to wrap the window system
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#include "WINDOWS_MANAGER.hxx"
#include "WINDOW.hxx"
#include "RESOURCE_DATABASE.hxx"
#include "RESOURCE_SCANNER.hxx"
#include "SIMPLE_ERROR.hxx"
#include "READ_RESOURCE_X.hxx"
#include "WINDOWS_SUPER_CLASS.hxx"
//-------------------------------------------------------------------
const int growing_size_of_items_array = 16;
const int items_number_offset = 101;
//-------------------------------------------------------------------
HINSTANCE               WINDOWS_MANAGER::application_instance = NULL;
RESOURCE_DATABASE       WINDOWS_MANAGER::resources_database;
int						WINDOWS_MANAGER::free_items_count = 0;
int						WINDOWS_MANAGER::allocated_items_count = 0;
ITEM **					WINDOWS_MANAGER::items_array = NULL;
//-------------------------------------------------------------------
int						WINDOWS_MANAGER::freeze_counter = 0;
int						WINDOWS_MANAGER::request_record_count = 0;
int						WINDOWS_MANAGER::request_record_free = 0;
int						WINDOWS_MANAGER::request_record_allocated = 0;
WINDOWS_MANAGER::REQUEST_RECORD * WINDOWS_MANAGER::request_records = NULL;
//-------------------------------------------------------------------
//  MESSAGES MANAGEMENT PART
//-------------------------------------------------------------------
int WINDOWS_MANAGER::main_loop()
	// performs the main message loop processing
{
	MSG msg;
	while(GetMessage(&msg,NULL,0,0))
	{
		if(!is_WINDOW_message(&msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	return msg.wParam;
}
//-------------------------------------------------------------------
bool WINDOWS_MANAGER::is_WINDOW_message(MSG *pmsg)
{
	static HWND previous_hwnd = NULL; 
			// used to prevent destroy problems (WM_DESTROY is not sent)
	static WINDOW * previous_window = NULL; 

	HWND hwnd = pmsg->hwnd;
	WINDOW * window = NULL;

	// is hwnd is a primary WINDOW object
	if(GetWindowLong(hwnd,GWL_WNDPROC)!=(LONG)main_callback)
	{
		// no so get the parent
		hwnd = GetParent(hwnd);

		// if parent is not a WINDOW object return false
		if(GetWindowLong(hwnd,GWL_WNDPROC)!=(LONG)main_callback)
			return false; // NOT A WINDOW OBJECT
	}

	// retrieve the WINDOW objet
	window = WINDOW::instance_of(hwnd);

	// guard for cases of problem or on creation
	if(window!=NULL)
	{
		if(pmsg->message==WM_MOUSEMOVE && previous_window != window)
		{
			if(previous_window!=NULL && IsWindow(previous_hwnd)!=0)
			{
				assert (WINDOW::instance_of(previous_hwnd)==previous_window,("internal problem"));
				previous_window->callback(WMM_LEAVE,0,0);
			}

			if(window!=NULL)
				window->callback(WMM_ENTER,pmsg->wParam,pmsg->lParam);

			previous_window = window;
			previous_hwnd = hwnd;
		}

		if(window->treat_the_message(pmsg))
			return true;
	}

	// default processing
	TranslateMessage(pmsg);
	DispatchMessage(pmsg);
	return true;
}
//-------------------------------------------------------------------
WINDOW * WINDOWS_MANAGER::hwnd_to_window(HWND hwnd)
	// retrieve the window object associated to hwnd
{
	// simple control

	if(hwnd==NULL) return NULL;

	// check if primary WINDOW object

	if(GetWindowLong(hwnd,GWL_WNDPROC)==(LONG)main_callback)
		return (WINDOW*)WINDOW::instance_of(hwnd);

	// check if parent is the WINDOW object (a kind of WIN_PRIMITIVE)

	hwnd = GetParent(hwnd);
	if(GetWindowLong(hwnd,GWL_WNDPROC)==(LONG)main_callback)
		return (WINDOW*)WINDOW::instance_of(hwnd);

	// unrecognized WINDOW object

	return NULL;
}
//-------------------------------------------------------------------
LRESULT WINAPI WINDOWS_MANAGER::main_callback(HWND hWnd,UINT uMsg,WPARAM wParam,LPARAM lParam)
	// all windows have the same callback: this function
{
	LRESULT resu;
	WINDOW * window;
	if(uMsg==WM_COMMAND)
	{
		ITEM * item = item_of_id(LOWORD(wParam));
		if(item!=NULL)
			return item->callback(uMsg,wParam,lParam);
		window = (WINDOW*)WINDOW::instance_of(hWnd);
		assert(window!=NULL,("Unmanaged WM_COMMAND window error!"));
	}
	else
	if(uMsg==WM_NCCREATE)
	{
		window = (WINDOW *)(((LPCREATESTRUCT)lParam)->lpCreateParams);
		assert(window!=NULL,("unbelievable CREATE error! big problem! check all!"));
		window->attach(hWnd);
	}
	else
	{
		window = (WINDOW*)WINDOW::instance_of(hWnd);
		if(window==NULL)
		{
			assert(uMsg==WM_GETMINMAXINFO,("Unmanaged window error! msg=%d.",uMsg));
//			trace(("Unmanaged window error! msg=%d.",uMsg));
			return DefWindowProc(hWnd,uMsg,wParam,lParam);
		}
	}
	resu = window->callback(uMsg,wParam,lParam);
	if(uMsg==WM_NCDESTROY)
		window->detach();
	return resu;
}
//-------------------------------------------------------------------
// WINDOWS MANAGEMENT PART
//-------------------------------------------------------------------
ATOM WINDOWS_MANAGER::register_window_class(WNDCLASSEX * wce)
{
	assert(wce!=NULL,("null pointer"));
	assert(wce->lpszClassName!=NULL,("null name pointer"));

	wce->cbSize = sizeof(WNDCLASSEX);
	wce->hInstance = get_application_instance();
	wce->lpfnWndProc = (WNDPROC)main_callback;

	ATOM atom = RegisterClassEx(wce);
	assert(atom!=NULL,("RegisterClassEx fail for %s, code %ld",
								wce->lpszClassName,GetLastError()));
	return atom;
}
//-------------------------------------------------------------------
ATOM WINDOWS_MANAGER::register_window_class
(
	const char * class_name,
	UINT		 style,
	HCURSOR		 cursor,
	HICON		 icon,
	HBRUSH		 background
)
{
	WNDCLASSEX wce;
	memset(&wce,0,sizeof wce);
	wce.lpszClassName = class_name;
	wce.style = style;
	wce.hCursor = cursor;
	wce.hIcon = icon;
	return register_window_class(&wce);
}
//-------------------------------------------------------------------
void WINDOWS_MANAGER::unregister_window_class(const char * name)
{
	assert(name!=NULL,("null pointer"));
	UnregisterClass(name,get_application_instance());
}
//-------------------------------------------------------------------
void WINDOWS_MANAGER::get_window_class_info(const char * name,WNDCLASSEX *wce)
{
	wce->cbSize = sizeof(WNDCLASSEX);
	GetClassInfoEx(get_application_instance(),name,wce);
}
//-------------------------------------------------------------------
void WINDOWS_MANAGER::set_application_instance(HINSTANCE hInstance)
{
	application_instance = hInstance;
}
//-------------------------------------------------------------------
bool WINDOWS_MANAGER::create_window
		// creation of a window for the system
(
	ATOM		 window_class_atom,
	WINDOW     * the_window,
	WINDOW     * the_parent,
	LONG         style,
	DWORD        dwExStyle,
	LPCTSTR      lpszWindowText
)
{
	assert(window_class_atom!=NULL,("null window class"));
	HWND parent_hwnd = the_parent==NULL ? NULL : the_parent->hwnd();

	HWND hwnd = CreateWindowEx(
		// the hwnd is attached to the_window in the main_callback
		dwExStyle,
		(const char*)window_class_atom,
		lpszWindowText==NULL ? "" : lpszWindowText,
		style,
		0, 0, 0, 0,
		parent_hwnd,
		0,
		application_instance,
		the_window
		);
 
	return hwnd!=NULL;
}
//-------------------------------------------------------------------
int WINDOWS_MANAGER::read_X_resource_file(const char *filename,SIMPLE_ERROR &errors)
{
	READ_RESOURCE_X reader;
	return (int)reader.read_resource(filename,resources_database,errors);
}
//-------------------------------------------------------------------
WINDOWS_SUPER_CLASS WINDOWS_MANAGER::super_class_window(HWND hwnd)
	// this changes the window procedure of the control and returns
	// the corresponding WINDOWS_SUPER_CLASS 
{
	WINDOWS_SUPER_CLASS super_class;
	super_class.super_class_window(hwnd,(WNDPROC)main_callback);
	return super_class;
}
//-------------------------------------------------------------------
//                    ITEMS PART
//-------------------------------------------------------------------
int WINDOWS_MANAGER::add_item(ITEM *item)
	// set item into the array of managed items
	// returns the item ID
	// returns 0 when item can not be recorded
{
	// set index at bottom
	int index = allocated_items_count;

	// checks free count
	if(free_items_count==0)
	{
		// realloc array
		int new_count = allocated_items_count + growing_size_of_items_array;
		ITEM ** new_array = (ITEM **)realloc(items_array,new_count * sizeof(ITEM *));
		if(new_array==NULL)
				return 0;

		// reinit variables and array bottom
		items_array = new_array;
		free_items_count = growing_size_of_items_array;
		allocated_items_count = new_count;
		while(index < allocated_items_count)
			items_array[index++] = NULL;
	}

	// loop on searching a free location
	while(index>0)
	{
		if(items_array[--index]==NULL)
		{
			// free location found: set it and return item ID
			free_items_count--;
			items_array[index] = item;
			return items_number_offset+index;
		}
	}
	return 0;
}
//-------------------------------------------------------------------
void WINDOWS_MANAGER::remove_item(const ITEM *item)
	// remove item from the array of managed items
{
	// loop on searching the item
	int index = allocated_items_count;
	while(index>0)
	{
		if(items_array[--index]==item)
		{
			// item location found: clear it
			free_items_count++;
			items_array[index] = NULL;
			return;
		}
	}
}
//-------------------------------------------------------------------
void WINDOWS_MANAGER::remove_item_by_id(int item_id)
	// remove item of id item_id from the array of managed items
{
	// computes the index
	int index = item_id - items_number_offset;
	if(index<0 || index>=allocated_items_count)
		return;
	if(items_array[index]!=NULL)
	{
		// item location not empty: clear it
		free_items_count++;
		items_array[index] = NULL;
	}
}
//-------------------------------------------------------------------
int WINDOWS_MANAGER::id_of_item(const ITEM *item)
	// returns the id of item if item is recorded
	// returns 0 when item is not recorded
{
	// loop on searching the item
	int index = allocated_items_count;
	while(index>0)
	{
		if(items_array[--index]==item)
			return items_number_offset+index;
	}
	return 0;
}
//-------------------------------------------------------------------
ITEM * WINDOWS_MANAGER::item_of_id(int item_id)
	// returns the item that have the id item_id
	// returns NULL when not found
{
	// computes the index
	int index = item_id - items_number_offset;
	if(index<0 || index>=allocated_items_count)
		return NULL;
	return items_array[index];
}

//-------------------------------------------------------------------
//                RESIZE, MOVE AND REDRAW PART
//-------------------------------------------------------------------
void WINDOWS_MANAGER::freeze_changes()
	// add one frozen status on move and resize
	// after calling freeze_changes, the move, resize and redraw actions
	// are deferred until a call to unfreeze_changes
{
	freeze_counter++;
}
//-------------------------------------------------------------------
void WINDOWS_MANAGER::unfreeze_changes()
	// sub one frozen status on move and resize
	// when count falls to zero and when actions was recorded
	// performs those recorded actions in a such way that
	// the system take it correctly into account
	// (WINDOWS is a kind of stupid system :(
{
	// dont decrement further more if zero is the actual value!
	if(freeze_counter>0)
	{
		// if counter falls to zero...
		if(--freeze_counter==0)
		{
			// perform recorded actions
			int idx_min, idx_max, idx, i_min, i_max;
			HWND h;

//			int order;

			// first do the redraw action and remove redrawn windows
			for(idx_max=idx=0;idx<request_record_count;idx++)
			{
				if(request_records[idx].operation==REQUEST_RECORD::resize)
				{
					if(idx!=idx_max)
						request_records[idx_max] = request_records[idx];
					idx_max++;
				}
				else
				{
					InvalidateRect(request_records[idx].hwnd,NULL,TRUE);
				}
			}

			// new count: the count of resize actions
			request_record_count = idx_max;

			// in that loop all the parents that are not linked
			// together are grouped at head to be resized together
			// using DeferWindowPos facility
			// then the loop continue on the staying child windows

			idx_min=0;
			while(idx_min<request_record_count)
			{
				i_min = idx_min;
				idx_max = request_record_count;

				// loop while windows are candidate to be grouped
				while(i_min<idx_max)
				{
					// search one hwnd that is not a child in idx_min
					h = request_records[i_min].hwnd;
					idx = i_min+1;
					while(idx<request_record_count)
					{
						if(IsChild(request_records[idx].hwnd,h)!=0)
						{
							REQUEST_RECORD rr = request_records[idx];
							request_records[idx] = request_records[i_min];
							request_records[i_min] = rr;
							h = rr.hwnd;
							idx = i_min+1;
						}
						else
							idx++;
					}

					// search up to idx_max-1 that are not child of hwnd(idx_min)
					i_max = i_min+1;
					h = request_records[i_min].hwnd;
					idx = i_min+1;
					while(idx<idx_max)
					{
						if(IsChild(h,request_records[idx].hwnd)==0)
						{
							if(idx!=i_max)
							{
								REQUEST_RECORD rr = request_records[idx];
								request_records[idx] = request_records[i_max];
								request_records[i_max] = rr;
							}
							i_max++;
						}
						idx++;
					}

					idx_max = i_max;
					i_min++;
				}

				// resize all the windows of the founded group
				HDWP hdwp = BeginDeferWindowPos(idx_max-idx_min);
				assert(hdwp!=NULL,(""));
				for(idx=idx_min ; idx<idx_max ; idx++)
				{
					hdwp = DeferWindowPos(
						hdwp,
						request_records[idx].hwnd,
						NULL,
						request_records[idx].x,
						request_records[idx].y,
						request_records[idx].width,
						request_records[idx].height,
						SWP_NOCOPYBITS|SWP_NOZORDER|SWP_NOACTIVATE);
					assert (hdwp!=NULL,("Problem in DeferWindowPos"));
				}
				EndDeferWindowPos(hdwp);

				// point the staying windows
				idx_min = idx_max;
			}

			// reset the counters		
			request_record_count = 0;
			request_record_free = request_record_allocated;
		}
	}		
}
//-------------------------------------------------------------------
void WINDOWS_MANAGER::redraw_window(WINDOW * win)
	// redraw the window win
	// it takes effect immediatly if freeze_counter is zero
	// otherwise a redraw action is recorded for later
{
	redraw_window(win->hwnd());
}
//-------------------------------------------------------------------
void WINDOWS_MANAGER::redraw_window(HWND hwnd)
	// redraw the window hwnd
	// it takes effect immediatly if freeze_counter is zero
	// otherwise a redraw action is recorded for later
{
	// immediate redraw
	if(freeze_counter==0)
		InvalidateRect(hwnd,NULL,TRUE);

	// deffered redraw
	else
	{
		// dont record the action if hwnd is already recorded
		// for redraw or resize
		int i = request_record_count;
		while(i)
			if(request_records[--i].hwnd==hwnd)
				return;

		// eventually grows the array of records
		if(request_record_free==0)
		{
			int new_free = 60;
			int new_alloc = request_record_allocated + new_free;
			void * p = realloc(request_records,new_alloc*sizeof(REQUEST_RECORD));
			assert(p!=NULL,("memory depletion"));
			request_records = (REQUEST_RECORD*)p;
			request_record_allocated = new_alloc;
			request_record_free = new_free;
		}

		// record the redraw action
		request_records[request_record_count].operation = REQUEST_RECORD::redraw;
		request_records[request_record_count].hwnd = hwnd;
		request_record_count++;
		request_record_free--;
	}
}
//-------------------------------------------------------------------
void WINDOWS_MANAGER::move_window(WINDOW * win,int x,int y,int width,int height)
	// resize and/or move the window win
	// it takes effect immediatly if freeze_counter is zero
	// otherwise a resize action is recorded for later
{
	move_window(win->hwnd(),x,y,width,height);
}
//-------------------------------------------------------------------
void WINDOWS_MANAGER::move_window(HWND hwnd,int x,int y,int width,int height)
	// resize and/or move the window hwnd
	// it takes effect immediatly if freeze_counter is zero
	// otherwise a resize action is recorded for later
{
	// immediate resize
	if(freeze_counter==0)
		MoveWindow(hwnd,x,y,width,height,TRUE);

	// deffered resize
	else
	{
		// search hwnd in the records
		int i = 0;
		while(i<request_record_count && request_records[i].hwnd!=hwnd) i++;

		// new record if hwnd was not found
		if (i == request_record_count)
		{
			// eventually grows the array of records
			if(request_record_free==0)
			{
				int new_free = 60;
				int new_alloc = request_record_allocated + new_free;
				void * p = realloc(request_records,new_alloc*sizeof(REQUEST_RECORD));
				assert(p!=NULL,("memory depletion"));
				request_records = (REQUEST_RECORD*)p;
				request_record_allocated = new_alloc;
				request_record_free = new_free;
			}

			// allocate the record for hwnd
			request_record_free--;
			request_record_count++;
		}

		// record the resize action
		request_records[i].operation = REQUEST_RECORD::resize;
		request_records[i].hwnd = hwnd;
		request_records[i].x = x;
		request_records[i].y = y;
		request_records[i].width = width;
		request_records[i].height = height;
	}
}
//-------------------------------------------------------------------
