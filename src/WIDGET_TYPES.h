//---------------------------------------------------------
// WIDGET_TYPES.h
// --------------
// the standards types for widgets emulation
// J.E.Bollo, M3G (c) 1998
//---------------------------------------------------------
#ifndef __widget_types_h
#define __widget_types_h

//---------------------------------------------------------
// the windows types
//---------------------------------------------------------

#define  OEMRESOURCE
	// access the OEM ressources (see LoadBitmap function help...)
#define  STRICT
	// strict typing is requested
#include <windows.h>

//---------------------------------------------------------
// the basic bool definition
//---------------------------------------------------------

#if (!defined(_MSC_VER) || _MSC_VER<=1000 || !defined(__cplusplus))
#  define bool		char
#  define true		1
#  define false		0
#endif

//---------------------------------------------------------
//  Xt types for geometry management
//---------------------------------------------------------

typedef enum
	{
		XtGeometryYes,		// request accepted
		XtGeometryNo,		// denied
		XtGeometryAlmost,	// request denied but willing to take reply
		XtGeometryDone		// request accepted and performed
	}
	XtGeometryResult;

typedef struct
	{
		int request_mode;	// valid fields, in a request for example
							// is a OR combination of CWX, CWY, CWWidth, 
							// and CWHeight (see below)
		int x, y;			// position
		int width, height;	// dimension
	}
	XtWidgetGeometry;

#define CWX			1		// valid the x field
#define CWY			2		// valid the y field
#define CWWidth		4		// valid the width field
#define CWHeight	8		// valid the height field

//---------------------------------------------------------
// compatibility with Xt of the set_values
//---------------------------------------------------------

#ifdef __cplusplus
	union RES_VAL
		// what is pointed
		{
			COLORREF	  colorref;
			char		* psztext;
			char		**texttable;
			HFONT		  hfont;
			HBRUSH		  hbrush;
			int			  integer;
			bool		  boolean;
			void		* address;
			RES_VAL		* pvalue;

			RES_VAL(char *txt)				: psztext(txt) {}
			RES_VAL(char **txttbl)			: texttable(txttbl) {}
			RES_VAL(const char *txt)		: psztext((char*)txt) {}
			RES_VAL(void *any_ptr)			: address(any_ptr) {}
			RES_VAL(const void *any_ptr)	: address((void*)any_ptr) {}
			RES_VAL(int i)					: integer(i) {}
			RES_VAL(COLORREF c)				: colorref(c) {}
			RES_VAL(bool b)					: boolean(b) {}
			RES_VAL()							{}
		};
#else
	typedef long RES_VAL;
#endif

typedef struct 
	{
		const char * name;
		RES_VAL      value;
	}
	ARG, *ARGLIST;

#define set_arg(arg,n,v)	((void)( \
								(arg).name  = (n), \
								(arg).value = (RES_VAL)(v) \
							))

//---------------------------------------------------------

#endif

