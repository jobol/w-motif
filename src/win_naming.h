//---------------------------------------------------------
// win_naming.h
// ---------------
// defines the X-Motif name for each class name
// J.E.Bollo & G. Musset, M3G (c) 1998
//---------------------------------------------------------
#ifndef _win_naming_h
#define _win_naming_h
//---------------------------------------------------------
#ifdef __cplusplus
	extern "C" {
#endif
//---------------------------------------------------------

#if !defined(_win_naming_cxx_)
#	define DECLARE_NAME(name)		extern char name[];
#	define NEW_WIN_NAME(name,text)	DECLARE_NAME(name)
#	define NEW_ATR_NAME(name)		DECLARE_NAME(XmN##name)
#	define NEW_CALLBACK(name)		DECLARE_NAME(XmN##name)
#	define NEW_EVENT(name)			DECLARE_NAME(name)
#	define NEW_ENUM(name)
#else
#	define DECLARE_NAME(name,text)	char name[] = text;
#	define NEW_WIN_NAME(name,text)	DECLARE_NAME(name,text)
#	define NEW_ATR_NAME(name)		DECLARE_NAME(XmN##name,#name)
#	define NEW_CALLBACK(name)		DECLARE_NAME(XmN##name,#name)
#	define NEW_EVENT(name)			DECLARE_NAME(name,#name)
#	define NEW_ENUM(name)			{ #name, name },
#endif

//---------------------------------------------------------
// class names definitions
//---------------------------------------------------------

NEW_WIN_NAME(NAME_OF_WIN_CORE_CLASS,			"Core")
NEW_WIN_NAME(NAME_OF_WIN_COMPOSITE_CLASS,		"Composite")
NEW_WIN_NAME(NAME_OF_WIN_SHELL_CLASS,			"Shell")
NEW_WIN_NAME(NAME_OF_WIN_TOP_LEVEL_SHELL_CLASS,	"TopLevelShell")

NEW_WIN_NAME(NAME_OF_WIN_BULLETIN_BOARD_CLASS,	"XmBulletinBoard")
NEW_WIN_NAME(NAME_OF_WIN_CASCADEBUTTON_CLASS,	"XmCascadeButton")
NEW_WIN_NAME(NAME_OF_WIN_DRAWINGAREA_CLASS,		"XmDrawingArea")
NEW_WIN_NAME(NAME_OF_WIN_FILESELECTIONBOX_CLASS,"XmFileSelectionBox")
NEW_WIN_NAME(NAME_OF_WIN_FORM_CLASS,			"XmForm")
NEW_WIN_NAME(NAME_OF_WIN_FRAME_CLASS,			"XmFrame")
NEW_WIN_NAME(NAME_OF_WIN_LABEL_CLASS,			"XmLabel")
NEW_WIN_NAME(NAME_OF_WIN_LIST_CLASS,			"XmList")
NEW_WIN_NAME(NAME_OF_WIN_MESSAGE_BOX_CLASS,		"XmMessageBox")
NEW_WIN_NAME(NAME_OF_WIN_PANEDWINDOW_CLASS,		"XmPanedWindow")
NEW_WIN_NAME(NAME_OF_WIN_PRIMITIVE_CLASS,		"XmPrimitive")
NEW_WIN_NAME(NAME_OF_WIN_PUSHBUTTON_CLASS,		"XmPushButton")
NEW_WIN_NAME(NAME_OF_WIN_ROWCOLUMN_CLASS,		"XmRowColumn")
NEW_WIN_NAME(NAME_OF_WIN_SCALE_CLASS,			"XmScale")
NEW_WIN_NAME(NAME_OF_WIN_SCROLLBAR_CLASS,		"XmScrollBar")
NEW_WIN_NAME(NAME_OF_WIN_SCROLLEDWINDOW_CLASS,	"XmScrolledWindow")
NEW_WIN_NAME(NAME_OF_WIN_SELECTION_BOX_CLASS,	"XmSelectionBox")
NEW_WIN_NAME(NAME_OF_WIN_SEPARATOR_CLASS,		"XmSeparator")
NEW_WIN_NAME(NAME_OF_WIN_TEXT_CLASS,			"XmText")
NEW_WIN_NAME(NAME_OF_WIN_TOGGLEBUTTON_CLASS,	"XmToggleButton")

NEW_WIN_NAME(NAME_OF_FORM_CONSTRAINT_CLASS,		"XmFormConstraint")

NEW_WIN_NAME(NAME_OF_WIN_OPTIONMENU_CLASS,		"WmOptionMenu")
NEW_WIN_NAME(NAME_OF_WIN_OPTIONITEM_CLASS,		"WmOptionItem")
NEW_WIN_NAME(NAME_OF_WIN_MENU_CLASS,			"WmMenu")
NEW_WIN_NAME(NAME_OF_WIN_MENUBAR_CLASS,			"WmMenuBar")
NEW_WIN_NAME(NAME_OF_WIN_MENUPOPUP_CLASS,		"WmMenuPopup")
NEW_WIN_NAME(NAME_OF_WIN_SUBMENU_CLASS,			"WmSubMenu")
NEW_WIN_NAME(NAME_OF_WIN_MENUITEM_CLASS,		"WmMenuItem")

//---------------------------------------------------------
// resource names definition
//---------------------------------------------------------

NEW_ATR_NAME(accelerator)
NEW_ATR_NAME(adjustLast)
NEW_ATR_NAME(adjustMargin)
NEW_ATR_NAME(alignment)
NEW_ATR_NAME(allowShellResize)
NEW_ATR_NAME(applyLabelString)
NEW_ATR_NAME(autoShowCursorPosition)
NEW_ATR_NAME(background)
NEW_ATR_NAME(buttonFontList)
NEW_ATR_NAME(cancelLabelString)
NEW_ATR_NAME(children)
NEW_ATR_NAME(columns)
NEW_ATR_NAME(cursorPositionVisible)
NEW_ATR_NAME(decimalPoints)
NEW_ATR_NAME(deleteResponse)
NEW_ATR_NAME(directory)
NEW_ATR_NAME(dirListLabelString)
NEW_ATR_NAME(editable)
NEW_ATR_NAME(editMode)
NEW_ATR_NAME(entryAlignment)
NEW_ATR_NAME(fileListLabelString)
NEW_ATR_NAME(filterLabelString)
NEW_ATR_NAME(fontList)
NEW_ATR_NAME(foreground)
NEW_ATR_NAME(fractionBase)
NEW_ATR_NAME(geometry)
NEW_ATR_NAME(height)
NEW_ATR_NAME(helpLabelString)
NEW_ATR_NAME(horizontalSpacing)
NEW_ATR_NAME(iconic)
NEW_ATR_NAME(iconName)
NEW_ATR_NAME(iconPixmap)
NEW_ATR_NAME(indicatorOn)
NEW_ATR_NAME(indicatorType)
NEW_ATR_NAME(initialState)
NEW_ATR_NAME(isAligned)
NEW_ATR_NAME(itemCount)
NEW_ATR_NAME(items)
NEW_ATR_NAME(label)
NEW_ATR_NAME(labelFontList)
NEW_ATR_NAME(labelInsensitivePixmap)
NEW_ATR_NAME(labelPixmap)
NEW_ATR_NAME(labelString)
NEW_ATR_NAME(labelType)
NEW_ATR_NAME(listSizePolicy)
NEW_ATR_NAME(margin)
NEW_ATR_NAME(marginWidth)
NEW_ATR_NAME(marginHeight)
NEW_ATR_NAME(marginLeft)
NEW_ATR_NAME(marginTop)
NEW_ATR_NAME(marginRight)
NEW_ATR_NAME(marginBottom)
NEW_ATR_NAME(maximum)
NEW_ATR_NAME(maxWidth)
NEW_ATR_NAME(maxHeight)
NEW_ATR_NAME(menuHistory)
NEW_ATR_NAME(messageString)
NEW_ATR_NAME(minimum)
NEW_ATR_NAME(minWidth)
NEW_ATR_NAME(minHeight)
NEW_ATR_NAME(mnemonic)
NEW_ATR_NAME(mwmDecorations)
NEW_ATR_NAME(mwmFunctions)
NEW_ATR_NAME(numChildren)
NEW_ATR_NAME(numColumns)
NEW_ATR_NAME(okLabelString)
NEW_ATR_NAME(optionLabel)
NEW_ATR_NAME(orientation)
NEW_ATR_NAME(packing)
NEW_ATR_NAME(paneMaximum)
NEW_ATR_NAME(paneMinimum)
NEW_ATR_NAME(pattern)
NEW_ATR_NAME(radioAlwaysOne)
NEW_ATR_NAME(radioBehavior)
NEW_ATR_NAME(rows)
NEW_ATR_NAME(resizable)
NEW_ATR_NAME(resizePolicy)
NEW_ATR_NAME(scrollHorizontal)
NEW_ATR_NAME(scrollVertical)
NEW_ATR_NAME(scrollLeftSide)
NEW_ATR_NAME(scrollTopSide)
NEW_ATR_NAME(selectedItemCount)
NEW_ATR_NAME(selectedItems)
NEW_ATR_NAME(selectInsensitivePixmap)
NEW_ATR_NAME(selectionPolicy)
NEW_ATR_NAME(selectionLabelString)
NEW_ATR_NAME(selectPixmap)
NEW_ATR_NAME(sensitive)
NEW_ATR_NAME(separatorType)
NEW_ATR_NAME(separatorOn)
NEW_ATR_NAME(set)
NEW_ATR_NAME(shadowType)
NEW_ATR_NAME(showValue)
NEW_ATR_NAME(skipAdjust)
NEW_ATR_NAME(spacing)
NEW_ATR_NAME(textFontList)
NEW_ATR_NAME(textString)
NEW_ATR_NAME(title)
NEW_ATR_NAME(titleString)
NEW_ATR_NAME(topCharacter)
NEW_ATR_NAME(topItemPosition)
NEW_ATR_NAME(transientFor)
NEW_ATR_NAME(transient)
NEW_ATR_NAME(traversalOn)
NEW_ATR_NAME(traversalType)
NEW_ATR_NAME(value)
NEW_ATR_NAME(verticalSpacing)
NEW_ATR_NAME(visibleItemCount)
NEW_ATR_NAME(width)
NEW_ATR_NAME(x)
NEW_ATR_NAME(y)

NEW_ATR_NAME(bottomAttachment)
NEW_ATR_NAME(bottomOffset)
NEW_ATR_NAME(bottomPosition)
NEW_ATR_NAME(bottomWidget)
NEW_ATR_NAME(leftAttachment)
NEW_ATR_NAME(leftOffset)
NEW_ATR_NAME(leftPosition)
NEW_ATR_NAME(leftWidget)
NEW_ATR_NAME(rightAttachment)
NEW_ATR_NAME(rightOffset)
NEW_ATR_NAME(rightPosition)
NEW_ATR_NAME(rightWidget)
NEW_ATR_NAME(topAttachment)
NEW_ATR_NAME(topOffset)
NEW_ATR_NAME(topPosition)
NEW_ATR_NAME(topWidget)

//---------------------------------------------------------
// callbacks names definition
//---------------------------------------------------------

NEW_CALLBACK(activateCallback)
NEW_CALLBACK(applyCallback)
NEW_CALLBACK(armCallback)
NEW_CALLBACK(browseSelectionCallback)
NEW_CALLBACK(cancelCallback)
NEW_CALLBACK(defaultActionCallback)
NEW_CALLBACK(destroyCallback)
NEW_CALLBACK(disarmCallback)
NEW_CALLBACK(dragCallback)
NEW_CALLBACK(extentedSelectionCallback)
NEW_CALLBACK(losingFocusCallback)
NEW_CALLBACK(multipleSelectionCallback)
NEW_CALLBACK(okCallback)
NEW_CALLBACK(singleSelectionCallback)
NEW_CALLBACK(valueChangedCallback)

//---------------------------------------------------------
// event names definition
//---------------------------------------------------------

NEW_EVENT(EnterWindowMask) 
NEW_EVENT(LeaveWindowMask) 
NEW_EVENT(StructureNotifyMask)

//---------------------------------------------------------
// enumerations
//---------------------------------------------------------
#if !defined(_win_naming_cxx_)
	extern struct _ENUMERATIONS_VALUES_
			{ char *name; int value; } array_of_enum_values[];
#else
	struct _ENUMERATIONS_VALUES_ 
			{ char *name; int value; } array_of_enum_values[] = {
#endif
//---------------------------------------------------------

#define XmSINGLE_SELECT				0
#define XmBROWSE_SELECT				1
#define XmMULTIPLE_SELECT			2
#define XmEXTENDED_SELECT			3

NEW_ENUM(XmBROWSE_SELECT)
NEW_ENUM(XmEXTENDED_SELECT)
NEW_ENUM(XmMULTIPLE_SELECT)
NEW_ENUM(XmSINGLE_SELECT)

#define XmVARIABLE					0
#define XmCONSTANT					1
#define XmRESIZE_IF_POSSIBLE		2

NEW_ENUM(XmVARIABLE)
NEW_ENUM(XmCONSTANT)
NEW_ENUM(XmRESIZE_IF_POSSIBLE)

#define XmN_OF_MANY					0
#define XmONE_OF_MANY				1

NEW_ENUM(XmN_OF_MANY)
NEW_ENUM(XmONE_OF_MANY)

#define XmSINGLE_LINE_EDIT			0
#define XmMULTI_LINE_EDIT			1

NEW_ENUM(XmMULTI_LINE_EDIT)
NEW_ENUM(XmSINGLE_LINE_EDIT)

#define XmSTRING					0
#define XmPIXMAP					1

NEW_ENUM(XmPIXMAP)
NEW_ENUM(XmSTRING)

#define XmVERTICAL					0
#define XmHORIZONTAL				1

NEW_ENUM(XmHORIZONTAL)
NEW_ENUM(XmVERTICAL)

#define XmPACK_NONE					0
#define XmPACK_TIGHT				1
#define XmPACK_COLUMN				2

NEW_ENUM(XmPACK_TIGHT)
NEW_ENUM(XmPACK_COLUMN)
NEW_ENUM(XmPACK_NONE)

#define XmALIGNMENT_BEGINNING		0
#define XmALIGNMENT_CENTER			1
#define XmALIGNMENT_END				2

NEW_ENUM(XmALIGNMENT_BEGINNING)
NEW_ENUM(XmALIGNMENT_END)
NEW_ENUM(XmALIGNMENT_CENTER)

#define XmATTACH_NONE				0
#define XmATTACH_FORM				1
#define XmATTACH_OPPOSITE_FORM		2
#define XmATTACH_WIDGET				3
#define XmATTACH_OPPOSITE_WIDGET	4
#define XmATTACH_POSITION			5
#define XmATTACH_SELF				6

NEW_ENUM(XmATTACH_NONE)
NEW_ENUM(XmATTACH_FORM)
NEW_ENUM(XmATTACH_OPPOSITE_FORM)
NEW_ENUM(XmATTACH_WIDGET)
NEW_ENUM(XmATTACH_OPPOSITE_WIDGET)
NEW_ENUM(XmATTACH_POSITION)
NEW_ENUM(XmATTACH_SELF)

#define XmSHADOW_IN					0
#define XmSHADOW_OUT				1
#define XmSHADOW_ETCHED_IN			2
#define XmSHADOW_ETCHED_OUT			3

NEW_ENUM(XmSHADOW_IN)
NEW_ENUM(XmSHADOW_OUT)
NEW_ENUM(XmSHADOW_ETCHED_IN)
NEW_ENUM(XmSHADOW_ETCHED_OUT)

#define XmSINGLE_LINE				0
#define XmDOUBLE_LINE				1
#define XmSINGLE_DASHED_LINE		4
#define XmDOUBLE_DASHED_LINE		5
#define XmNO_LINE					6

NEW_ENUM(XmSINGLE_LINE)
NEW_ENUM(XmDOUBLE_LINE)
NEW_ENUM(XmSINGLE_DASHED_LINE)
NEW_ENUM(XmDOUBLE_DASHED_LINE)
NEW_ENUM(XmNO_LINE)

#define XmNONE						0
#define XmTAB_GROUP					1
#define XmSTICKY_TAB_GROUP			2
#define XmEXCLUSIVE_TAB_GROUP		3

NEW_ENUM(XmNONE)
NEW_ENUM(XmTAB_GROUP)
NEW_ENUM(XmSTICKY_TAB_GROUP)
NEW_ENUM(XmEXCLUSIVE_TAB_GROUP)

#define XmTRAVERSE_CURRENT			0
#define XmTRAVERSE_PREV_TAB_GROUP	1
#define XmTRAVERSE_NEXT_TAB_GROUP	2
#define XmTRAVERSE_LEFT				3
#define XmTRAVERSE_UP				4
#define XmTRAVERSE_RIGHT			5
#define XmTRAVERSE_DOWN				6

NEW_ENUM(XmTRAVERSE_CURRENT)
NEW_ENUM(XmTRAVERSE_PREV_TAB_GROUP)
NEW_ENUM(XmTRAVERSE_NEXT_TAB_GROUP)
NEW_ENUM(XmTRAVERSE_LEFT)
NEW_ENUM(XmTRAVERSE_UP)
NEW_ENUM(XmTRAVERSE_RIGHT)
NEW_ENUM(XmTRAVERSE_DOWN)

#define XmRESIZE_NONE				0
#define XmRESIZE_GROW				1
#define XmRESIZE_ANY				2

NEW_ENUM(XmRESIZE_NONE)
NEW_ENUM(XmRESIZE_GROW)
NEW_ENUM(XmRESIZE_ANY)

#define XmDESTROY					0
#define XmUNMAP						1
#define XmDO_NOTHING				2

NEW_ENUM(XmDESTROY)
NEW_ENUM(XmUNMAP)
NEW_ENUM(XmDO_NOTHING)

//---------------------------------------------------------
#if defined(_win_naming_cxx_)
	{ NULL, 0 } };
#endif
//---------------------------------------------------------

#undef DECLARE_NAME
#undef NEW_WIN_NAME
#undef NEW_ATR_NAME
#undef NEW_CALLBACK
#undef NEW_EVENT
#undef NEW_ENUM

//---------------------------------------------------------
// some exceptions
//---------------------------------------------------------

#ifndef XtNlabel
#	define XtNlabel					XmNlabel
#	define XtNdestroyCallback		XmNdestroyCallback
#endif

//---------------------------------------------------------
#ifdef __cplusplus
	}
#endif
//---------------------------------------------------------
#endif
