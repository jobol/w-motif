//-------------------------------------------------------------------
// WIN_MENUPOPUP.cxx
// -----------------
// implements a class that defines the menu popup
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#include "WIN_MENUPOPUP.hxx"
#include "win_naming.h"
//-------------------------------------------------------------------
BEGIN_RES_DESC(WIN_MENUPOPUP,WIN_MENU,NAME_OF_WIN_MENUPOPUP_CLASS)
END_RES_DESC
//-------------------------------------------------------------------
WIN_MENUPOPUP::WIN_MENUPOPUP()
 : WIN_MENU()
{
}
//-------------------------------------------------------------------
WIN_MENUPOPUP::~WIN_MENUPOPUP()
{
}
//-------------------------------------------------------------------
void WIN_MENUPOPUP::set_name_and_parent(const char *name,WIN_COMPOSITE *parent)
	// store a widget name and its parent
{
	set_name(name);
	if(parent!=NULL)
		parent->set_popup_menu(this);
}
//-------------------------------------------------------------------
void WIN_MENUPOPUP::query_popup(const WIN_CORE * from,int x,int y) const
	// query from from widget to popup the menu
{
	if(menu_handle!=NULL)
		TrackPopupMenu(menu_handle,TPM_LEFTALIGN|TPM_TOPALIGN|TPM_NONOTIFY,
					x,y,0,from->hwnd(),NULL);
}
//-------------------------------------------------------------------
HMENU WIN_MENUPOPUP::create_menu() const
	// return a new menu handle for this
{
	return CreatePopupMenu();
}
//-------------------------------------------------------------------

