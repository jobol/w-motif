//-------------------------------------------------------------------
// WIN_FRAME.cxx
// ---------------
// implements a class which defines the Motif class "XmFrame"
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#include "WIN_FRAME.hxx"
#include "win_naming.h"
#include "WINDOWS_MANAGER.hxx"
#include "WINDOWS_COMMONS.h"
//-------------------------------------------------------------------
const int border_width  = 2;
const int border_height = 2;
//-------------------------------------------------------------------
BEGIN_RES_DESC(WIN_FRAME,WIN_COMPOSITE,NAME_OF_WIN_FRAME_CLASS)
	RES_DESC_ATR(XmNmarginWidth,	INTEGER,	WIN_FRAME, margin_width),
	RES_DESC_ATR(XmNmarginHeight,	INTEGER,	WIN_FRAME, margin_height),
	RES_DESC_ATR(XmNshadowType,		ENUM,		WIN_FRAME, shadow_type),
END_RES_DESC
//-------------------------------------------------------------------
IMPLEMENT_WINDOW(WIN_FRAME)
//-------------------------------------------------------------------
ATOM WIN_FRAME::register_window_class() const
{
	return WINDOWS_MANAGER::register_window_class(
					(const char*)get_class_name(),CS_VREDRAW|CS_HREDRAW,
					LoadCursor(NULL,IDC_ARROW));
}
//-------------------------------------------------------------------
WIN_FRAME::WIN_FRAME()
	: WIN_COMPOSITE()
	, margin_width(0)
	, margin_height(0)
	, shadow_type(XmSHADOW_ETCHED_IN)
{
}
//-------------------------------------------------------------------
WIN_FRAME::~WIN_FRAME()
{
}

//-------------------------------------------------------------------

LONG WIN_FRAME::get_windows_style() const
	// retrieve the windows style used to create the widget 
{
	return WS_CHILD|WS_CLIPSIBLINGS|WS_VISIBLE|WS_CLIPCHILDREN;
}

//-------------------------------------------------------------------

bool WIN_FRAME::create_window()
{
	return WIN_CORE::create_window(get_windows_style());
}

//-------------------------------------------------------------------

void WIN_FRAME::change_managed()
	// The method change_manager called once during the realize phase and then
	// every time that one of its children becomes managed or unmanaged
{
	if (num_children == 0) return;

	// a frame has at most one unique child
	WIN_CORE *child = (WIN_CORE *)children[0];
	assert(child!=NULL,("internal children problem"));

	int new_width = width;
	int new_height = height;
	XtGeometryResult resu;
	XtWidgetGeometry request, prefered;

	request.request_mode = 0;
	if(width == 0)
	{
		request.request_mode = CWWidth;
		new_width = request.width = 100;
	}
	if(height == 0)
	{
		request.request_mode |= CWHeight;
		new_height = request.height = 100;
	}
	if(request.request_mode != 0)
	{
		request.width -= 2*(border_width + margin_width); 
		request.height -= 2*(border_height + margin_height);
		resu = child->query_geometry(&request,&prefered);
		if(resu!=XtGeometryNo)
		{
			if(width==0 && (prefered.request_mode & CWWidth)!=0)
				new_width = prefered.width + 2*(border_width + margin_width);
			if(height==0 && (prefered.request_mode & CWHeight)!=0)
				new_height = prefered.height + 2*(border_height + margin_height);
		}
	}

	set_geometry(x,y,new_width,new_height);
}

//-------------------------------------------------------------------

XtGeometryResult WIN_FRAME::geometry_manager(
	WIN_CORE			*child,
	XtWidgetGeometry	*request,
	XtWidgetGeometry	*geometry_return
) 
		// The method geometry_manager called every time  
		// one of its children requests a new size
{
	XtWidgetGeometry my_request;
	*geometry_return = *request;

	// seeks for border clipping constraints
	bool wrong_x_y = false;
	if((request->request_mode & CWX)!=0 && request->x<margin_width)
	{
		wrong_x_y = true;
		geometry_return->x = margin_width;
	}
	if((request->request_mode & CWY)!=0 && request->y<margin_height)
	{
		wrong_x_y = true;
		geometry_return->y = margin_height;
	}

	// compute new size
	my_request.request_mode = 0;
	if((request->request_mode & CWWidth)!=0)
	{
		my_request.width = request->width + 2*(border_width + margin_width); 
		if(my_request.width!=width)
			my_request.request_mode |= CWWidth;
	}
	if((request->request_mode & CWHeight)!=0)
	{
		my_request.height = request->height + 2*(border_height + margin_height); 
		if(my_request.height!=height)
			my_request.request_mode |= CWHeight;
	}

	// ask parent
	if(my_request.request_mode==0 && geometry_request(&my_request)==XtGeometryNo)
	{
		// no if growing
		if(((my_request.request_mode & CWWidth)!=0 && my_request.width>width)
		|| ((my_request.request_mode & CWHeight)!=0 && my_request.height>height))
			return XtGeometryNo;
	}

	// return yes or almost when margin problem encountered
	return wrong_x_y ? XtGeometryAlmost : XtGeometryYes;
}

//-------------------------------------------------------------------

void WIN_FRAME::resize()
	// The method resize is called every time a widget changes size
{
	if (num_children > 0)
	{
		WIN_CORE *child = (WIN_CORE *)children[0];
		assert(child!=NULL,("internal children problem"));
		int child_width = width - 2*(border_width + margin_width);
		int child_height = height - 2*(border_height + margin_height);
		if(child_width < 0)
			child_width = 0;
		if(child_height < 0)
			child_height = 0;
		child->set_geometry(margin_width+border_width,margin_height+border_height,
			child_width,child_height);
	}
}

//-------------------------------------------------------------------

XtGeometryResult WIN_FRAME::query_geometry (XtWidgetGeometry *request, 
											 XtWidgetGeometry *prefered)
	// The method query_geometry called when the parent  
	// wants to know the widget's prefered size
{
	if (num_children == 0)
		return XtGeometryYes;

	WIN_CORE *child = (WIN_CORE *)children[0];
	assert(child!=NULL,("internal children problem"));

	XtWidgetGeometry pref, child_request, child_prefered;

	pref.request_mode = CWWidth|CWHeight;
	pref.width = width; 
	pref.height = height;

	child_request.request_mode = CWWidth|CWHeight;
	child_request.width = width - 2*(border_width + margin_width);
	child_request.height = height  - 2*(border_height + margin_height);

	if(child->query_geometry(&child_request,&child_prefered)==XtGeometryAlmost)
	{
		if((child_prefered.request_mode & CWWidth)!=0)
			pref.width = child_prefered.width + 2*(border_width + margin_width);
		if((child_prefered.request_mode & CWHeight)!=0)
			pref.height = child_prefered.height + 2*(border_height + margin_height);
	}
	return complete_query_geometry(request,prefered,&pref);
}
//-------------------------------------------------------------------

void WIN_FRAME::do_paint(HDC hdc,const RECT *rect) const
	// paint in response to a WM_PAINT
{
	// erase background
	WIN_CORE::do_paint(hdc,rect);

	// computes the pens

	LOGBRUSH logbr;
	GetObject(background,sizeof(logbr),&logbr);

	HPEN lighted = CreatePen(PS_SOLID,0,enlight_color(logbr.lbColor,224));
	HPEN darked = CreatePen(PS_SOLID,0,enlight_color(logbr.lbColor,96));

	bool f1 = shadow_type==XmSHADOW_OUT || shadow_type==XmSHADOW_ETCHED_OUT;
	bool f2 = shadow_type==XmSHADOW_OUT || shadow_type==XmSHADOW_ETCHED_IN;

	POINT oldpos;
	HGDIOBJ oldpen = SelectObject(hdc,f1 ? lighted : darked);
	MoveToEx(hdc,0,height,&oldpos);
	LineTo(hdc,0,0);
	LineTo(hdc,width,0);

	SelectObject(hdc,f1 ? darked : lighted);
	MoveToEx(hdc,width-1,1,NULL);
	LineTo(hdc,width-1,height-1);
	LineTo(hdc,0,height-1);

	SelectObject(hdc,f2 ? lighted : darked);
	MoveToEx(hdc,1,height-2,NULL);
	LineTo(hdc,1,1);
	LineTo(hdc,width-1,1);

	SelectObject(hdc,f2 ? darked : lighted);
	MoveToEx(hdc,width-2,2,NULL);
	LineTo(hdc,width-2,height-2);
	LineTo(hdc,1,height-2);

	MoveToEx(hdc,oldpos.x,oldpos.y,NULL);
	SelectObject(hdc,oldpen);
	DeleteObject(lighted);
	DeleteObject(darked);
}

//-------------------------------------------------------------------

int WIN_FRAME::internal_get_values
	// function used internally to get attribut values 
	// should not be called directly (call get_values)
	// arglist is the array of the values to get
	// count is the count of values to be put into arglist
	// got is an array that flag if an attribut have been put
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs put
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) const
{
	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.

		if(equality(attribut,XmNmarginWidth))
			arglist[index].value.pvalue->integer = margin_width;
		else
		if(equality(attribut,XmNmarginHeight))
			arglist[index].value.pvalue->integer = margin_height;
		else
		if(equality(attribut,XmNshadowType))
			arglist[index].value.pvalue->integer = shadow_type;
		else
			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
	}

	// return total count of found attributs (including ancestors)
	return found_count + WIN_COMPOSITE::internal_get_values(arglist,count,got,equality);
}

//-------------------------------------------------------------------

int WIN_FRAME::internal_set_values
	// function used internally to set attribut values 
	// should not be called directly (call set_values)
	// arglist is the array of the values to be set,
	// count is the count of values in arglist
	// got is an array that flag if an attribut have been set
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs set
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
)
{
	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.
		if(equality(attribut,XmNmarginWidth))
			margin_width = arglist[index].value.integer;
		else
		if(equality(attribut,XmNmarginHeight))
			margin_height = arglist[index].value.integer;
		else
		if(equality(attribut,XmNshadowType))
			shadow_type = arglist[index].value.integer;
		else
			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
	}

	if(found_count!=0)
		redraw();

	// return total count of found attributs (including ancestors)
	return found_count  + WIN_COMPOSITE::internal_set_values(arglist,count,got,equality);
}

//-------------------------------------------------------------------

