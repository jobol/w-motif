//---------------------------------------------------------
// WINDOW.hxx
// ------------------
// the window class for Windows
// J.E.Bollo, M3G (c) 1998
//---------------------------------------------------------
#ifndef __window_hxx
#define __window_hxx
//---------------------------------------------------------
#include "macros.h"
//---------------------------------------------------------
class WINDOW
	// this class wrap the Windows windows
{
private:

	HWND	the_hwnd;
		// the window HANDLE

public:

	WINDOW() : the_hwnd(NULL) {}
		// construction

	~WINDOW() { if(is_attached()) DestroyWindow(hwnd()); }
		// destruction

	HWND hwnd() const { return the_hwnd; }
		// the attached window HANDLE or NULL if unattached

	bool is_attached() const { return the_hwnd!=NULL; }
		// is this attached to a HWND?

	void attach(HWND hwnd);
		// attach "this" to hwnd

	void detach();
		// detach "this" from previous attachment

	static WINDOW *instance_of(HWND hwnd);
		// get the attached WINDOW, NULL if unattached window

	virtual LRESULT callback(UINT uMsg,WPARAM wParam,LPARAM lParam);
		// the "this" WINDOW callback

	virtual LRESULT default_callback(UINT uMsg,WPARAM wParam,LPARAM lParam);
		// the "this" WINDOW callback

	virtual bool treat_the_message(MSG *msg);
		// returns true if window pre-treats the message and false otherwise
		// when it returns true the caller should not call neither
		// the TranslateMeassage nor the DispatchMessage calls

	virtual bool create_window() = 0;
		// create the window, pure virtual

	virtual void destroy_window();
		// destruction of the window

	virtual void set_text(const char* txt);
		// replaces all the text by "txt"

	virtual char* get_text() const;
		// gets all the text

	virtual void redraw() const;
		// redraw the window

	virtual void set_focus() const;
		// set the focus to the window
};
//---------------------------------------------------------
#define DECLARE_WINDOW(class) \
	private:\
		static ATOM atom_of_window_class;\
	public:\
		virtual bool is_window_class_registered() const\
			{ return atom_of_window_class!=NULL; }\
		virtual ATOM register_window_class() const;\
		virtual ATOM get_window_class_atom() const\
			{ if(!is_window_class_registered()) \
				atom_of_window_class=register_window_class();\
			  return atom_of_window_class; }
//---------------------------------------------------------
#define IMPLEMENT_WINDOW(class) ATOM class::atom_of_window_class=NULL;
//---------------------------------------------------------
#endif
