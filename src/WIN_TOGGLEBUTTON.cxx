//-------------------------------------------------------------------
// WIN_TOGGLEBUTTON.cxx
// ---------------
// implements the Motif class "XmToggleButton"
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#include "WIN_TOGGLEBUTTON.hxx"
#include "WINDOWS_MANAGER.hxx"
#include "WIN_COMPOSITE.hxx"
#include "win_naming.h"
#include "WINDOWS_COMMONS.h"
//-------------------------------------------------------------------
BEGIN_RES_DESC(WIN_TOGGLEBUTTON,WIN_LABEL,NAME_OF_WIN_TOGGLEBUTTON_CLASS)
	RES_DESC_ATR(XmNindicatorOn,			BOOLEA,		WIN_TOGGLEBUTTON, indicator_on),
	RES_DESC_ATR(XmNset,					BOOLEA,		WIN_TOGGLEBUTTON, set),
	RES_DESC_ATR(XmNindicatorType,			ENUM,		WIN_TOGGLEBUTTON, indicator_type),
//	RES_DESC_ATR(XmNselectInsensitivePixmap,TEXT,		WIN_TOGGLEBUTTON, select_insensitive_pixmap),
//	RES_DESC_ATR(XmNselectPixmap,			TEXT,		WIN_TOGGLEBUTTON, select_pixmap),
END_RES_DESC
//-------------------------------------------------------------------
IMPLEMENT_WINDOW(WIN_TOGGLEBUTTON)
//-------------------------------------------------------------------
ATOM WIN_TOGGLEBUTTON::register_window_class() const
{
	WNDCLASSEX wce;
	WINDOWS_MANAGER::get_window_class_info("BUTTON",&wce);
	return WINDOWS_MANAGER::register_window_class(
					(const char*)get_class_name(),wce.style);
}
//-------------------------------------------------------------------
WIN_TOGGLEBUTTON::WIN_TOGGLEBUTTON()
  : WIN_LABEL()
  , indicator_on(true)
  , set(false)
  , indicator_type(XmN_OF_MANY)
//	char * select_insensitive_pixmap;
//	char * select_pixmap;
{
	traversal_on = true;
}
//-------------------------------------------------------------------
WIN_TOGGLEBUTTON::~WIN_TOGGLEBUTTON()
{
}
//-------------------------------------------------------------------
LONG WIN_TOGGLEBUTTON::get_windows_style() const
	// retrieve the windows style used to create the widget 
{
	DWORD style = WS_CHILD | WS_VISIBLE | BS_TEXT | BS_MULTILINE;

	if (indicator_type == XmONE_OF_MANY)
//		style |= BS_AUTORADIOBUTTON;
		style |= BS_RADIOBUTTON;
	else
//		style |= BS_AUTOCHECKBOX;
		style |= BS_CHECKBOX;

	if (!indicator_on)	style |= BS_PUSHLIKE;

	return style;
}
//-------------------------------------------------------------------
bool WIN_TOGGLEBUTTON::create_control()
{
	hwndCtl = CreateWindow(
				"BUTTON",
				label_string,
				get_windows_style(),
				0, 0,width,height,
				this->hwnd(),
				(HMENU)1,
				WINDOWS_MANAGER::get_application_instance(),
				NULL);
	if (hwndCtl==NULL) return false;
	if (set == TRUE) SendMessage(hwndCtl,BM_SETCHECK, (WPARAM)BST_CHECKED, 0);
	return true;
}
//-------------------------------------------------------------------
void WIN_TOGGLEBUTTON::arm() 
	// virtual function called when button is pressed down
{
	activate_callbacks(XmNarmCallback);
}
//-------------------------------------------------------------------
void WIN_TOGGLEBUTTON::disarm() 
	// virtual function called when button is no longer pressed down
{
	activate_callbacks(XmNdisarmCallback);
}
//-------------------------------------------------------------------
void WIN_TOGGLEBUTTON::value_changed() 
	// virtual function called when value change
{
//	SendMessage(parent_win->hwnd(), WM_TOGGLE_STATE_CHANGE, (WPARAM)this, 0);
	activate_callbacks(XmNvalueChangedCallback);
}
//-------------------------------------------------------------------
LRESULT WIN_TOGGLEBUTTON::callback(UINT uMsg,WPARAM wParam,LPARAM lParam)
{
	switch(uMsg)
	{

	// not for toggle button
	case WM_CTLCOLORBTN:
		{
			HDC hdc = (HDC)wParam;
			SetBkMode(hdc, TRANSPARENT);
			SetTextColor(hdc, foreground);
			return (LRESULT)background;
		}


	case WM_COMMAND:
		{
			int notif_code = HIWORD(wParam);
			switch (notif_code)
			{
			case BN_CLICKED:
				{
					disarm();
					change_state();
//					value_changed();
					break;
				}
			}
			return 0;
		}
	case WM_PARENTNOTIFY:
		{
			int notif_code = LOWORD(wParam);
			switch (notif_code)
			{
			case WM_LBUTTONDOWN:
				{
					arm();
					return 0;
				}
			}
		}
	}

	return WIN_LABEL::callback(uMsg,wParam,lParam);
}
//-------------------------------------------------------------------
void WIN_TOGGLEBUTTON::set_indicator_on(bool state)
	// set state of the attribute indicator_on
{
	indicator_on = state;
}

//-------------------------------------------------------------------
void WIN_TOGGLEBUTTON::set_state(bool state)
{
	if(get_state()!=state)
		change_state();
}
//-------------------------------------------------------------------
void WIN_TOGGLEBUTTON::change_state()
	// change state of the Toggle (checked or unchecked)
{
	if(parent_win==NULL)
		set = !set;
	else
	{
		if(parent_win->callback(WM_TOGGLE_STATE_CHANGE,(WPARAM)this,0)==0)
		{
			if(hwndCtl!=NULL)
				SendMessage(hwndCtl,BM_SETCHECK,get_state()?BST_UNCHECKED:BST_CHECKED,0);
			else
				set = !set;
			value_changed();
		}
	}
}
//-------------------------------------------------------------------
bool WIN_TOGGLEBUTTON::get_state() const
{
	return hwnd()==NULL
		? set
		: (SendMessage(hwndCtl, BM_GETCHECK, 0, 0)==BST_CHECKED);
}

//-------------------------------------------------------------------

XtGeometryResult WIN_TOGGLEBUTTON::query_geometry (XtWidgetGeometry *request, 
										 XtWidgetGeometry *prefered)
	// The method query_geometry called when the parent  
	// wants to know the widget's prefered size
{
	SIZE size;
	XtWidgetGeometry pref;

	window_multiline_text_extend(hwndCtl,label_string,font_list,&size);
	if(indicator_on)
	{
/*
		obj = (HGDIOBJ)LoadBitmap(NULL,MAKEINTRESOURCE(OBM_CHECKBOXES));
		assert(obj!=NULL,("no more system resource! can't continue."));
		BITMAP bitmap;
		if(GetObject(obj,sizeof bitmap,&bitmap)==sizeof bitmap)
		{
			pref.width = size.cx + bitmap.bmWidth + 4;
			pref.height = max(size.cy,bitmap.bmHeight);
		}
		DeleteObject(obj);
*/
		pref.width = size.cx + 25;
		pref.height = max(size.cy,15);
	}
	else
	{
		pref.width = size.cx + 2*GetSystemMetrics(SM_CXEDGE);
		pref.height = size.cy + 2*GetSystemMetrics(SM_CYEDGE);
	}
	pref.request_mode = CWWidth|CWHeight;

	return complete_query_geometry(request,prefered,&pref);
}

//-------------------------------------------------------------------


int WIN_TOGGLEBUTTON::internal_get_values
	// function used internally to get attribut values 
	// should not be called directly (call get_values)
	// arglist is the array of the values to get
	// count is the count of values to be put into arglist
	// got is an array that flag if an attribut have been put
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs put
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) const
{
	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.

		if(equality(attribut,XmNindicatorOn))
			arglist[index].value.pvalue->boolean = indicator_on;
		else
		if(equality(attribut,XmNset))
			arglist[index].value.pvalue->boolean = get_state();
		else
		if(equality(attribut,XmNindicatorType))
			arglist[index].value.pvalue->integer = indicator_type;
		else
			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
	}

	// return total count of found attributs (including ancestors)
	return found_count  + WIN_LABEL::internal_get_values(arglist,count,got,equality);
}



//-------------------------------------------------------------------

int WIN_TOGGLEBUTTON::internal_set_values
	// function used internally to set attribut values 
	// should not be called directly (call set_values)
	// arglist is the array of the values to be set,
	// count is the count of values in arglist
	// got is an array that flag if an attribut have been set
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs set
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) 
{
	// empty geometry request
	bool redraw_needed = false;
	XtWidgetGeometry request;
	request.request_mode = 0;

	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.

		if(equality(attribut,XmNindicatorOn))
			set_indicator_on(arglist[index].value.boolean);
		else
		if(equality(attribut,XmNset))
			set_state(arglist[index].value.boolean);
		else
		if(equality(attribut,XmNindicatorType))
			indicator_type = arglist[index].value.integer;
		else
			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
	}

/*
	// make the geometry request if necessary
	if(request.request_mode!=0)
		geometry_request(&request);
	if(redraw_needed)
		redraw();
*/
	// return total count of found attributs (including ancestors)
	return found_count  + WIN_LABEL::internal_set_values(arglist,count,got,equality);
}

