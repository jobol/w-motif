//-------------------------------------------------------------------
// WINDOWS_MANAGER.hxx
// -------------------
// that class is a static class designed to wrap the window system
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __windows_manager_hxx
#define __windows_manager_hxx
//-------------------------------------------------------------------
#include "macros.h"
#include "NAMES_SPACE.hxx"
#include "RESOURCE_NAMES_SPACE.hxx"
#include "RESOURCE_DATABASE.hxx"
//-------------------------------------------------------------------
class WINDOW;
class SIMPLE_ERROR;
class WINDOWS_SUPER_CLASS;
//-------------------------------------------------------------------
#define ITEM WINDOW
//-------------------------------------------------------------------
#define WMM_LEAVE				(WM_USER + 1)
#define WMM_ENTER				(WM_USER + 2)
#define WM_TOGGLE_STATE_CHANGE	(WM_USER + 3)
//-------------------------------------------------------------------
class WINDOWS_MANAGER
{
private:

	static HINSTANCE application_instance;
		// handle of the application

	static RESOURCE_DATABASE resources_database;
		// the resource database

	static int free_items_count;
		// count of free item entries in the items_array

	static int allocated_items_count;
		// count of allocated item entries in the items_array

	static ITEM ** items_array;
		// array of the items

private:

	struct REQUEST_RECORD
		// structure used to record move, resize and redraw actions
		// when the freeze_counter is not null
		{
			enum { nothing, redraw, resize } operation;
			int order;
			HWND hwnd;
			int x, y, width, height;
		};

	static int freeze_counter;
		// freeze_counter is used to deffer move, 
		// resize and redraw requests

	static REQUEST_RECORD * request_records;
		// array of request records for deferred actions

	static int request_record_free;
		// count of free records in request_records

	static int request_record_allocated;
		// count of records allocated for request_records

	static int request_record_count;
		// count of used records in request_records

private:

	static LRESULT WINAPI main_callback(HWND hWnd,UINT uMsg,WPARAM wParam,LPARAM lParam);
		// main call back function

public:

	static ATOM register_window_class(WNDCLASSEX * wce);
		// register a new window class

	static ATOM register_window_class
		// register a new window class
			(
				const char * class_name,
				UINT		 style,
				HCURSOR		 cursor = NULL,
				HICON		 icon = NULL,
				HBRUSH		 background = NULL
			);

	static bool is_WINDOW_message(MSG *pmsg);
		// return true if the message is a WINDOW message
		// when it returns true the caller should not call neither
		// the TranslateMeassage nor the DispatchMessage calls

	static int main_loop();
		// performs the main message loop processing
		// returs the exit code

	static void unregister_window_class(const char * name);
		// unregister a window class

	static void get_window_class_info(const char * name,WNDCLASSEX *wce);
		// get in wce the WNDCLASSEX of the window class of name name

	static HINSTANCE get_application_instance() { return application_instance; }
		// return the application instance

	static void set_application_instance(HINSTANCE hInstance);
		// record the application instance

	static int read_X_resource_file(const char *filename,SIMPLE_ERROR &errors);
		// read a X resource file in the resource database

	static bool create_window
		// creation of a window for the system
			(
				ATOM		 window_class_atom,
				WINDOW     * the_window,
				WINDOW     * the_parent,
				LONG         style,
				DWORD        dwExStyle = 0,
				LPCTSTR      lpszWindowText = NULL
			);

	static WINDOW * hwnd_to_window(HWND hwnd);
		// retrieve the window object associated to hwnd

	static const RESOURCE_DATABASE & get_resources_database()
		// return the resource database
		{ return resources_database; }

	static int add_item(ITEM *item);
		// set item into the array of managed items
		// returns the item ID
		// returns 0 when item can not be recorded

	static void remove_item(const ITEM *item);
		// remove item from the array of managed items

	static void remove_item_by_id(int item_id);
		// remove item of id item_id from the array of managed items

	static int id_of_item(const ITEM *item);
		// returns the id of item if item is recorded
		// returns 0 when item is not recorded

	static ITEM * item_of_id(int item_id);
		// returns the item that have the id item_id
		// returns NULL when not found

	static void freeze_changes();
		// add one frozen status on move and resize
		// after calling freeze_changes, the move, resize and redraw actions
		// are deferred until a call to unfreeze_changes

	static void unfreeze_changes();
		// sub one frozen status on move and resize
		// when count falls to zero and when actions was recorded
		// performs those recorded actions in a such way that
		// the system take it correctly into account
		// (WINDOWS is a kind of stupid system :(

	static void redraw_window(WINDOW * win);
		// redraw the window win
		// it takes effect immediatly if freeze_counter is zero
		// otherwise a redraw action is recorded for later

	static void redraw_window(HWND hwnd);
		// redraw the window hwnd
		// it takes effect immediatly if freeze_counter is zero
		// otherwise a redraw action is recorded for later

	static void move_window(WINDOW * win,int x,int y,int width,int height);
		// resize and/or move the window win
		// it takes effect immediatly if freeze_counter is zero
		// otherwise a resize action is recorded for later

	static void move_window(HWND hwnd,int x,int y,int width,int height);
		// resize and/or move the window hwnd
		// it takes effect immediatly if freeze_counter is zero
		// otherwise a resize action is recorded for later

	static WINDOWS_SUPER_CLASS super_class_window(HWND hwnd);
		// this changes the window procedure of the control and returns
		// the corresponding WINDOWS_SUPER_CLASS 
};
//-------------------------------------------------------------------
#endif
