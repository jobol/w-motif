//-------------------------------------------------------------------
// WINDOWS_SUPER_CLASS.hxx
// -----------------------
// that class is a common wrapper for super classing
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __windows_super_class_hxx
#define __windows_super_class_hxx
//-------------------------------------------------------------------
#include "macros.h"
//-------------------------------------------------------------------
class WINDOWS_SUPER_CLASS
{
private:

	WNDPROC original_procedure;
		// the superclassed original procedure

public:

	WINDOWS_SUPER_CLASS()
		// construction
		: original_procedure(0) {}

	WINDOWS_SUPER_CLASS(const WINDOWS_SUPER_CLASS &other)
		// construction
		: original_procedure(other.original_procedure) {}

	WINDOWS_SUPER_CLASS & operator =(const WINDOWS_SUPER_CLASS &other)
		// copy
		{ original_procedure = other.original_procedure; return *this; }

	void super_class_window(HWND hwnd,WNDPROC new_procedure)
		// set the procedure
		{
			original_procedure = (WNDPROC)GetWindowLong(hwnd,GWL_WNDPROC);
			SetWindowLong(hwnd,GWL_WNDPROC,(LONG)new_procedure);
		}

	LRESULT callback(HWND hwnd,UINT msg,WPARAM wparam,LPARAM lparam)
		// call the original window proc
		{
			assert(original_procedure!=0,("null procedure"));
			return CallWindowProc(original_procedure,hwnd,msg,wparam,lparam); 
		}
};
//-------------------------------------------------------------------
#endif