//---------------------------------------------------------
// WIN_CORE.cxx
// ------------------
// the WIN_CORE class
// J.E.Bollo, M3G (c) 1998
//---------------------------------------------------------
#include "WINDOWS_MANAGER.hxx"
#include "RESOURCE_NAMES_SPACE.hxx"
#include "WIN_CORE.hxx"
#include "WIN_COMPOSITE.hxx"
#include "WIN_SHELL.hxx"
#include "WIN_MENUPOPUP.hxx"
#include "win_naming.h"
#include "WINDOWS_COMMONS.h"
//---------------------------------------------------------
BEGIN_ROOT_RES_DESC(WIN_CORE,NAME_OF_WIN_CORE_CLASS)
	RES_DESC_ATR(XmNx,         INTEGER, WIN_CORE, x),
	RES_DESC_ATR(XmNy,         INTEGER, WIN_CORE, y),
	RES_DESC_ATR(XmNwidth,     INTEGER, WIN_CORE, width),
	RES_DESC_ATR(XmNheight,    INTEGER, WIN_CORE, height),
	RES_DESC_ATR(XmNbackground,BRUSH,   WIN_CORE, background),
	RES_DESC_ATR(XmNsensitive, BOOLEA,  WIN_CORE, sensitive),
	RES_DESC_ATR(XmNforeground,COLOR,   WIN_CORE, foreground),
	RES_DESC_ATR(XmNtraversalOn,BOOLEA, WIN_CORE, traversal_on),
	RES_DESC_ATR(XmNtraversalType,ENUM, WIN_CORE, traversal_type),
END_RES_DESC
//---------------------------------------------------------
WIN_CORE::WIN_CORE()
  : WINDOW()
  , x(0), y(0), width(0), height(0)
  , sensitive(true)
  , background(CreateSolidBrush(RGB(200,200,200)))
  , foreground(RGB(0,0,0))
  , constraints(NULL)
  , managed(true)
  , parent_win(NULL)
  , popup_menu(NULL)
  , traversal_on(true)
  , traversal_type(XmNONE)
  , destroy_pending(false)
{}
//---------------------------------------------------------
WIN_CORE::~WIN_CORE()
{
	if(constraints!=NULL)
		delete constraints;
	if(background!=NULL)
		DeleteObject(background);
}
//---------------------------------------------------------
void WIN_CORE::destroy()
{
	if (!destroy_pending)
	{
		destroy_pending = true;
		activate_callbacks(XmNdestroyCallback);
		if (get_parent_win() != NULL)
			get_parent_win()->delete_child(this);
		destroy_window();
		if (is_in_activate_callbacks <= 0)
			delete this;
		else
			deferred_delete = true;
	}
}
//---------------------------------------------------------
void WIN_CORE::do_paint(HDC hdc,const RECT *rect) const
{
	FillRect(hdc,rect,background);
}
//-------------------------------------------------------------------
void WIN_CORE::paint() const
{
	if(GetUpdateRect(hwnd(),NULL,FALSE)!=FALSE)
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hwnd(),&ps);
		do_paint(hdc,&ps.rcPaint);
		ps.fErase = 0;
		EndPaint(hwnd(),&ps);
	}
}
//-------------------------------------------------------------------
LRESULT WIN_CORE::callback(UINT uMsg,WPARAM wParam,LPARAM lParam)
{
	switch(uMsg)
	{
	case WM_CREATE:
		{
			LPCREATESTRUCT lpcs = (LPCREATESTRUCT)lParam;
			lpcs->x = x;
			lpcs->y = y;
			lpcs->cx = width;
			lpcs->cy = height;
			WINDOWS_MANAGER::move_window(this,x,y,width,height);
		}
		return 0;

	case WM_ERASEBKGND:
		return 0;

	case WM_PAINT:
		paint();
		return 0;

	case WM_SIZE:
		if(!IsIconic(hwnd()))
		{
			resize();
		}
		return 0;

	case WM_CONTEXTMENU:
		if(popup_menu!=NULL)
			popup_menu->query_popup(this,LOWORD(lParam),HIWORD(lParam));
		return 0;

	case WMM_ENTER:
		activate_callbacks(EnterWindowMask);
		return 0;
	
	case WMM_LEAVE:
		activate_callbacks(LeaveWindowMask);
		return 0;

	case WM_DESTROY:
		destroy();
		return 0;
	}

	return WINDOW::callback(uMsg,wParam,lParam);
}

//-------------------------------------------------------------------
void WIN_CORE::set_sensitive(bool value)
	// set attribute sensitive
{
	if(sensitive != value)
	{
		sensitive = value;
		if(is_realized())
			EnableWindow(hwnd(),value);
	}
}

//-------------------------------------------------------------------
void WIN_CORE::set_background (COLORREF color)
	// set the background with color
{
	if (background != NULL)
		DeleteObject(background);
	background = CreateSolidBrush(color);
	redraw();
}

//-------------------------------------------------------------------
void WIN_CORE::set_background (const char* color)
	// set the background with color
{
	set_background (text_to_color(color));
}
//-------------------------------------------------------------------

void WIN_CORE::set_foreground (COLORREF color)
	// set the foreground with color
{ 
	foreground = color;
	redraw();
}

//-------------------------------------------------------------------

void WIN_CORE::set_foreground (const char* color)
	// set the foreground with color
{
	set_foreground (text_to_color(color));
}

//-------------------------------------------------------------------

void WIN_CORE::set_resource_values(const RESOURCE_SCANNER & scanner)
	// put values of scanner, the currently scanned resources
	// use default and check for constraints
{
	RESOURCE_CLIENT::set_resource_values(scanner);
	if(constraints!=NULL)
		constraints->set_resource_values(scanner);
}

//-------------------------------------------------------------------

void WIN_CORE::scan_resources()
	// scan the resource database
{
	RESOURCE_NAMED_CLIENT::scan_resource_values(
						WINDOWS_MANAGER::get_resources_database());
}

//-------------------------------------------------------------------

void WIN_CORE::set_geometry(int x,int y,int width,int height)
	// apply unconditionnaly the requested geometry
{
	bool changed = false;
	if(x!=this->x)
	{
		this->x = x;
		changed = true;
	}
	if(y!=this->y) 
	{
		this->y = y;
		changed = true;
	}
	if(width!=this->width) 
	{
		this->width = width;
		changed = true;
	}
	if(height!=this->height) 
	{
		this->height = height;
		changed = true;
	}

	if(changed)
	{
		if(hwnd()!=NULL)
			WINDOWS_MANAGER::move_window(this,x,y,width,height);
		else
			resize();
	}
}

//---------------------------------------------------------

void WIN_CORE::set_name_and_parent(const char *name, WIN_COMPOSITE *parent)
	// memorise le nom d'une widget
{
	set_name(name);
	parent_win = parent;
	if(parent!=NULL)
		parent->insert_child(this);
}

//-------------------------------------------------------------------

RESOURCE_NAMED_CLIENT * WIN_CORE::get_parent_resource() const
	// get the parent
{
	return parent_win;
}

//-------------------------------------------------------------------

WIN_SHELL * WIN_CORE::get_parent_shell() const
	// return the parent widget
{
	const WIN_CORE * iter = this;
	while(iter->parent_win != NULL)
		iter = iter->parent_win;
	if(iter->is_kind_of(WIN_SHELL::the_resource_class()))
		return (WIN_SHELL*)iter;
	error(("it could not append"));
	return NULL;
}

//-------------------------------------------------------------------

bool WIN_CORE::create_window
	// creation of a window for the system
(
	LONG         style,
	DWORD        dwExStyle,
	LPCTSTR      lpszWindowText
)
{
	return WINDOWS_MANAGER::create_window(
			get_window_class_atom(),
			(WINDOW *)this,
			(WINDOW *)get_parent_win(),
			style,
			dwExStyle | WS_EX_CONTROLPARENT,
			lpszWindowText
		);
}

//-------------------------------------------------------------------

void WIN_CORE::set_geometry(XtWidgetGeometry *request)
	// apply unconditionnaly the requested geometry
{
	set_geometry(
		(request->request_mode & CWX)!=0 ? request->x : x,
		(request->request_mode & CWY)!=0 ? request->y : y,
		(request->request_mode & CWWidth)!=0 ? request->width : width,
		(request->request_mode & CWHeight)!=0 ? request->height : height);
}

//-------------------------------------------------------------------

XtGeometryResult WIN_CORE::geometry_request (XtWidgetGeometry *request)
	// request to change the geometry of this according to request
	// returns only XtGeometryYes (request accepted) or XtGeometryNo (denied)
{
	if(hwnd()==NULL)
		set_geometry(request);
	else
	{
		WIN_COMPOSITE * parent = get_parent_win();
		if(parent==NULL) return XtGeometryNo;
		XtWidgetGeometry allowed;
		XtGeometryResult result = parent->geometry_manager(this,request,&allowed);
		if(result==XtGeometryNo) return XtGeometryNo;
		if(result==XtGeometryAlmost) 
		{
			XtWidgetGeometry no_care;
			result = parent->geometry_manager(this,&allowed,&no_care);
			assert(result==XtGeometryYes||result==XtGeometryDone,
											("problem in geometry management"));
			request = &allowed;
		}
		if(result==XtGeometryYes)
			set_geometry(request);
	}
	return XtGeometryYes;
}

//-------------------------------------------------------------------

XtGeometryResult WIN_CORE::complete_query_geometry
(
	XtWidgetGeometry *original_request, 
	XtWidgetGeometry *original_prefered, 
	XtWidgetGeometry *prefered
)
	// fills original_prefered accordling to the original_request
	// and the this prefered geometry and then return what should
	// be returned by the calling query_geometry method
{
	*original_prefered = *prefered;
	return XtGeometryAlmost;
/*
	int  common = original_request->request_mode & prefered->request_mode;
	bool changed = false;
	*original_prefered = *original_request;
	if((common & CWX)!=0 && original_prefered->x!=prefered->x)
	{
		original_prefered->x = prefered->x; 
		changed = true;
	}
	if((common & CWY)!=0 && original_prefered->y!=prefered->y)
	{
		original_prefered->y = prefered->y; 
		changed = true;
	}
	if((common & CWWidth)!=0 && original_prefered->width!=prefered->width)
	{
		original_prefered->width = prefered->width; 
		changed = true;
	}
	if((common & CWHeight)!=0 && original_prefered->height!=prefered->height)
	{
		original_prefered->height = prefered->height; 
		changed = true;
	}
	return changed ? XtGeometryAlmost : XtGeometryYes;
*/
}

//-------------------------------------------------------------------

void WIN_CORE::update_geometry_request(int mask)
	// request new geometry determined upon the CWX, CWY, CWHeight, CWWidth
	// values of mask and the returned value and geometry of query_geometry
{
	XtWidgetGeometry request, prefered;
	XtGeometryResult result;

	request.request_mode = mask;
	if((mask & CWX)!=0) request.x = x;
	if((mask & CWY)!=0) request.y = y;
	if((mask & CWWidth)!=0) request.width = width;
	if((mask & CWHeight)!=0) request.height = height;

	result = query_geometry(&request,&prefered);
	if(result==XtGeometryAlmost)
	{
		if(hwnd()!=NULL)
			geometry_request(&prefered);
		else
		{
			mask &= prefered.request_mode;
			if((mask & CWX)!=0) x = prefered.x;
			if((mask & CWY)!=0) y = prefered.y;
			if((mask & CWWidth)!=0) width = prefered.width;
			if((mask & CWHeight)!=0) height = prefered.height;
		}
	}
}

//-------------------------------------------------------------------

static bool pointer_equality(const char *string1,const char *string2)
{
	return string1==string2;
}

//-------------------------------------------------------------------

static bool string_equality(const char *string1,const char *string2)
{
	return strcmp(string1,string2)==0;
}

//-------------------------------------------------------------------

void WIN_CORE::get_values(ARGLIST arglist,int count) const
	// get count values from this to arglist
{
	// prepare boolean done array
	const int allocated_in_stack = 32;
	if(count<=0)
		return;
	bool do_alloc = count>allocated_in_stack;
	bool default_gotten[allocated_in_stack];
	bool *gotten = do_alloc ? ((bool*)malloc(count*sizeof(bool))) : default_gotten;
	int index = count;
	while(index>0)
		gotten[--index] = false;

	// first pass with pointer comparison
	int got_count = internal_get_values(arglist,count,gotten,pointer_equality);
	if(got_count!=count && constraints!=NULL)
	{
		assert(parent_win!=NULL,("internal error"));
		got_count += parent_win->constraint_get_values(
							this,arglist,count,gotten,pointer_equality);
	}

	// second pass with pointer comparison
	if(got_count!=count)
		got_count += internal_get_values(arglist,count,gotten,string_equality);
	if(got_count!=count && constraints!=NULL)
	{
		assert(parent_win!=NULL,("internal error"));
		got_count += parent_win->constraint_get_values(
							this,arglist,count,gotten,string_equality);
	}

	// release allocated memory
	if(do_alloc)
		free(gotten);
}

//-------------------------------------------------------------------

void WIN_CORE::set_values(ARGLIST arglist,int count)
	// set count values from arglist to this
{
	// prepare boolean done array
	const int allocated_in_stack = 32;
	if(count<=0)
		return;
	bool do_alloc = count>allocated_in_stack;
	bool default_gotten[allocated_in_stack];
	bool *gotten = do_alloc ? ((bool*)malloc(count*sizeof(bool))) : default_gotten;
	int index = count;
	while(index>0)
		gotten[--index] = false;

	// freeze output
	WINDOWS_MANAGER::freeze_changes();

	// first pass with pointer comparison
	int got_count = internal_set_values(arglist,count,gotten,pointer_equality);
	if(got_count!=count && constraints!=NULL)
	{
		assert(parent_win!=NULL,("internal error"));
		got_count += parent_win->constraint_set_values(
							this,arglist,count,gotten,pointer_equality);
	}

	// second pass with pointer comparison
	if(got_count!=count)
		got_count += internal_set_values(arglist,count,gotten,string_equality);
	if(got_count!=count && constraints!=NULL)
	{
		assert(parent_win!=NULL,("internal error"));
		got_count += parent_win->constraint_set_values(
							this,arglist,count,gotten,string_equality);
	}

	// unfreeze output
	WINDOWS_MANAGER::unfreeze_changes();

	// release allocated memory
	if(do_alloc)
		free(gotten);
}

//-------------------------------------------------------------------

int WIN_CORE::internal_get_values
	// function used internally to get attribut values 
	// should not be called directly (call get_values)
	// arglist is the array of the values to get
	// count is the count of values to be put into arglist
	// got is an array that flag if an attribut have been put
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs put
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) const
{
	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.
		if(equality(attribut,XmNx))
			arglist[index].value.pvalue->integer = x;
		else
		if(equality(attribut,XmNy))
			arglist[index].value.pvalue->integer = y;
		else
		if(equality(attribut,XmNwidth))
			arglist[index].value.pvalue->integer = width;
		else
		if(equality(attribut,XmNheight))
			arglist[index].value.pvalue->integer = height;
		else
		if(equality(attribut,XmNsensitive))
			arglist[index].value.pvalue->boolean = sensitive;
		else
		if(equality(attribut,XmNforeground))
			arglist[index].value.pvalue->colorref = foreground;

#if VERSION_OF_WINDOW >= 2

		else
		if(equality(attribut,XmNbackground))
			arglist[index].value.pvalue->hbrush = background;

#else

		else
		if(equality(attribut,XmNbackground))
		{
			if(background==NULL)
				arglist[index].value.pvalue->hbrush = NULL;
			else
			{
				LOGBRUSH logbrush;
				GetObject(background,sizeof logbrush,&logbrush);
				arglist[index].value.pvalue->hbrush = 
							CreateBrushIndirect(&logbrush);
			}
		}

#endif

		else
			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
	}

	// return total count of found attributs (including ancestors)
	return found_count;
}

//-------------------------------------------------------------------

int WIN_CORE::internal_set_values
	// function used internally to set attribut values 
	// should not be called directly (call set_values)
	// arglist is the array of the values to be set,
	// count is the count of values in arglist
	// got is an array that flag if an attribut have been set
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs set
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) 
{
	// empty geometry request
	bool redraw_needed = false;
	XtWidgetGeometry request;
	request.request_mode = 0;

	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.
		if(equality(attribut,XmNx))
		{
			request.request_mode |= CWX;
			request.x = arglist[index].value.integer;
		}
		else
		if(equality(attribut,XmNy))
		{
			request.request_mode |= CWY;
			request.y = arglist[index].value.integer;
		}
		else
		if(equality(attribut,XmNwidth))
		{
			request.request_mode |= CWWidth;
			request.width = arglist[index].value.integer;
		}
		else
		if(equality(attribut,XmNheight))
		{
			request.request_mode |= CWHeight;
			request.height = arglist[index].value.integer;
		}
		else
		if(equality(attribut,XmNsensitive))
		{
			sensitive = arglist->value.boolean;
			redraw_needed = true;
		}
		else
		if(equality(attribut,XmNforeground))
		{
			foreground = arglist[index].value.colorref;
			redraw_needed = true;
		}
		else
		if(equality(attribut,XmNbackground))
		{
			if(background!=NULL)
			{
				DeleteObject(background);
				background = NULL;
			}
			if(arglist[index].value.hbrush!=NULL)
			{
				LOGBRUSH logbrush;
				if(GetObject(arglist[index].value.hbrush,sizeof logbrush,&logbrush))
					background = CreateBrushIndirect(&logbrush);
			}
			redraw_needed = true;
		}
		else
			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
	}

	// make the geometry request if necessary
	if(request.request_mode!=0)
		geometry_request(&request);
	if(redraw_needed)
		redraw();

	// return total count of found attributs (including ancestors)
	return found_count;
}

//-------------------------------------------------------------------

void WIN_CORE::manage()
	// set this managed
{
	if(!is_managed())
	{
		managed = true;
		if(parent_win!=NULL)
			parent_win->change_managed();
	}
}

//-------------------------------------------------------------------

void WIN_CORE::unmanage()
	// set this not managed
{
	if(is_managed())
	{
		managed = false;
		if(parent_win!=NULL)
			parent_win->change_managed();
	}
}

//-------------------------------------------------------------------

void WIN_CORE::map()
	// set this mapped
{
	if(is_realized() && IsWindowVisible(hwnd())==0)
		ShowWindow(hwnd(),SW_SHOW);
}

//-------------------------------------------------------------------

void WIN_CORE::unmap()
	// set this mapped
{
	if(is_realized() && IsWindowVisible(hwnd())!=0)
		ShowWindow(hwnd(),SW_HIDE);
}

//-------------------------------------------------------------------

void WIN_CORE::realize()
	// realise this
{
	if(is_realized())
		return;

	create_window();

	if(popup_menu!=NULL)
		popup_menu->realize();
}

//-------------------------------------------------------------------

bool WIN_CORE::process_traversal(int traversal_action)
{
	return get_parent_shell()->shell_process_traversal(this,traversal_action);
}
