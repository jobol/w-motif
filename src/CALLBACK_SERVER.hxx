//---------------------------------------------------------
// CALLBACK_SERVER.hxx
// -------------------
// class for callback Xt like serving
// J.E.Bollo & G. Musset, M3G (c) 1998
//---------------------------------------------------------
#ifndef __callback_server_hxx
#define __callback_server_hxx
//---------------------------------------------------------
#include "macros.h"
//---------------------------------------------------------
class CALLBACK_SERVER;
//---------------------------------------------------------
typedef       void * CB_EMITER;
typedef const char * CB_REASON;
typedef       void * CB_DATA;
typedef       void (*CB_FUNCTION)(CB_EMITER		emiter,
								  CB_DATA		data,
								  void *		call_data);
//---------------------------------------------------------
class CALLBACK_SERVER
{
private:
	// internal callback management is private

	struct CB_RECORD_STRUCT
		// unitary array cell structure for callback records
	{
		CB_REASON	reason;
		CB_FUNCTION	function;
		CB_DATA		data;
	};

	int callbacks_count;
		// count of callbacks

	CB_RECORD_STRUCT * callbacks_array;
		// array of the registered callbacks

protected:

	int is_in_activate_callbacks;
		// not null if method activate_callbacks is running

	bool deferred_delete;
		// indicates that object deletion must me deferred

public:

	CALLBACK_SERVER() : 
		  callbacks_count(0)
		, callbacks_array(NULL)
		, is_in_activate_callbacks(0)
		, deferred_delete(false)
		  {}
		// construction

	~CALLBACK_SERVER() { if(callbacks_array!=NULL) free(callbacks_array); }
		// destruction

	void add_callback(CB_REASON reason,CB_FUNCTION callback,CB_DATA data);
		// register a new callback function to call with data on reason

	void remove_callback(CB_REASON reason,CB_FUNCTION callback,CB_DATA data);
		// remove a registered callback

	void activate_callbacks(CB_EMITER emiter,CB_REASON reason);
		// perform registered callbacks in response to reason

	virtual void delete_this(CB_EMITER emiter) { delete (CALLBACK_SERVER *)emiter; }
		// delete this
};
//---------------------------------------------------------
#endif

