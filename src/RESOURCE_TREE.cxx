//---------------------------------------------------------
// RESOURCE_TREE.cxx
// ------------------
// object to manage the resource (in the X11 way) tree
// J.E.Bollo and G. Musset, M3G (c) 1998
//---------------------------------------------------------

#include "NAMES_SPACE.hxx"
#include "RESOURCE_TREE.hxx"

//--------------------------------
// delete this and its daughter(s)
//--------------------------------

RESOURCE_TREE::~RESOURCE_TREE()
{
	// delete all children
	RESOURCE_TREE *child = first_daughter;
	while (child != NULL) {
		RESOURCE_TREE *next_child = child->next_sister;
		delete child;
		child = next_child;
	};

	// free allocated memory for value if necessary
	if (my_value != NULL) free (my_value);

	// update the links of the neigthbour sisters and the parent to this
	if (previous_sister != NULL) 
		previous_sister->next_sister = next_sister;
	else
		parent->first_daughter = next_sister;
	if (next_sister != NULL) next_sister->previous_sister = previous_sister;
}

//--------------------------------
// create a daughter of the_parent
//--------------------------------

RESOURCE_TREE::RESOURCE_TREE(RESOURCE_TREE *the_parent, TYPE_OF_NODE the_type, NAME the_name)
  : type(the_type)
  , my_name(the_name)
  , parent(the_parent)
  , previous_sister(NULL)
  , first_daughter(NULL)
  , my_value(NULL)
  , count_of_refs(1)
{
	next_sister = parent->first_daughter;
	parent->first_daughter = this;
	// link it to the next child if it exists
	if (next_sister != NULL) next_sister->previous_sister = this;
}

// create a root
//--------------

RESOURCE_TREE::RESOURCE_TREE()
  : type(ROOT)
  , my_name()
  , previous_sister(NULL)
  , next_sister(NULL)
  , first_daughter(NULL)
  , my_value(NULL)
  , count_of_refs(1)
{
	parent = this;	// a root resource is its own parent
}

//----------------------------------------
// add a daughter named name and return it
//----------------------------------------

RESOURCE_TREE * RESOURCE_TREE::add_named_daughter(NAME name)
{
	// Search if a resource with the same name already exists
	//-------------------------------------------------------

	RESOURCE_TREE * daughter = get_named_daughter(name);
	if (daughter != NULL) {

		// The resource already exists : increment reference counter
		//----------------------------------------------------------

		daughter->count_of_refs++; 

	} else {

		// The resource does not exists:  create a new one
		//------------------------------------------------

		daughter = new RESOURCE_TREE (this, NAMED, name);
		assert(daughter!=NULL,("memory depletion"));
	}
	return daughter;
}

//--------------------------------------
// add a any name daughter and return it
//--------------------------------------

RESOURCE_TREE * RESOURCE_TREE::add_any_name_daughter()
{
	// Search if a any name resource already exists
	//---------------------------------------------

	RESOURCE_TREE * daughter = get_any_name_daughter();
	if (daughter != NULL) {

		// The resource already exists : increment reference counter
		//----------------------------------------------------------

		daughter->count_of_refs++; 

	} else {

		// The resource does not exists:  create a new one
		//------------------------------------------------

		daughter = new RESOURCE_TREE (this, ANY_NAME, NAME());
		assert(daughter!=NULL,("memory depletion"));
	}
	return daughter;
}

//---------------------------------------
// add a wild bind daughter and return it
//---------------------------------------

RESOURCE_TREE * RESOURCE_TREE::add_wild_bind_daughter()
{
	// Search if a wild bind resource already exists
	//---------------------------------------------

	RESOURCE_TREE * daughter = get_wild_bind_daughter();
	if (daughter != NULL) {

		// The resource already exists : increment reference counter
		//----------------------------------------------------------

		daughter->count_of_refs++; 

	} else {

		// The resource does not exists:  create a new one
		//------------------------------------------------

		daughter = new RESOURCE_TREE (this, WILD_BIND, NAME());
		assert(daughter!=NULL,("memory depletion"));
	}
	return daughter;
}

//----------------------------------
// set this value to a copy of value
//----------------------------------
void RESOURCE_TREE::set_value(const char * value)
{
	assert(value!=NULL, ("invalid value"));
	int length = 1 + strlen(value);
	my_value = (char *)realloc(my_value,length);
	assert(my_value!=NULL,("memory depletion"));
	memcpy(my_value, value, length);
}

//-------------
// get the root
//-------------

RESOURCE_TREE * RESOURCE_TREE::get_root() const
{
	RESOURCE_TREE *resource = this->parent;
	while (resource != resource->parent) resource = resource->parent;
	return resource;
}

//----------------------------------------------------------
// return pointer to the daughter named name or NULL if none
//----------------------------------------------------------

RESOURCE_TREE * RESOURCE_TREE::get_named_daughter(NAME name) const
{

	RESOURCE_TREE *resource = first_daughter;
	while (resource != NULL) {
		if ((resource->type == NAMED) && (resource->my_name == name)) break;
		resource = resource->next_sister;
	};
	return resource;
}

//--------------------------------------------------------
// return pointer to the any name daughter or NULL if none
//--------------------------------------------------------

RESOURCE_TREE * RESOURCE_TREE::get_any_name_daughter() const
{
	RESOURCE_TREE *resource = first_daughter;
	while (resource != NULL) {
		if (resource->type == ANY_NAME) break;
		resource = resource->next_sister;
	};
	return resource;
}

//---------------------------------------------------------
// return pointer to the wild bind daughter or NULL if none
//---------------------------------------------------------

RESOURCE_TREE * RESOURCE_TREE::get_wild_bind_daughter() const
{
	RESOURCE_TREE *resource = first_daughter;
	while (resource != NULL) {
		if (resource->type == WILD_BIND) break;
		resource = resource->next_sister;
	};
	return resource;
}

//--------------------------------------------------
//decrements count_of_refs and delete this if needed
//--------------------------------------------------

void RESOURCE_TREE::remove()
{
	assert(!is_root(),("please dont remove root"));
	if (--count_of_refs <= 0) delete this;
}
