//-------------------------------------------------------------------
// WIN_MENUPOPUP.hxx
// -----------------
// define a class that defines the menu popup
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __win_menupopup_hxx
#define __win_menupopup_hxx
//-------------------------------------------------------------------
#include "WIN_MENU.hxx"
//-------------------------------------------------------------------
class WIN_MENUPOPUP : public WIN_MENU
{
	DECLARE_RESOURCE_CLIENT(WIN_MENUPOPUP)
		// use the resource mechanism

protected:

	~WIN_MENUPOPUP();
		// destruction

public:

	WIN_MENUPOPUP();
		// construction

	void query_popup(const WIN_CORE * from,int x,int y) const;
		// query from from widget to popup the menu

	virtual void set_name_and_parent(const char *name,WIN_COMPOSITE *parent);
		// store a widget name and its parent

	virtual HMENU create_menu() const;
		// return a new menu handle for this
};
//-------------------------------------------------------------------
#endif
