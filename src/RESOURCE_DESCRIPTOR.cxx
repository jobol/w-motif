//---------------------------------------------------------
// RESOURCE_DESCRIPTOR.cxx
// -----------------------
// polymorphic resource value class
// J.E.Bollo, M3G (c) 1998
//---------------------------------------------------------
#include "RESOURCE_DESCRIPTOR.hxx"
#include "win_naming.h"
#include "WINDOWS_COMMONS.h"
//---------------------------------------------------------

int RESOURCE_DESCRIPTOR::text_to_enum(const char *text) 
	// this function convert a string that should be
	// a declared enum value to its ENUM_VALUE value
	// may return NULL on error
{
	int offset = (text[0]=='X' && text[1]=='m') ? 0 : 2;
	struct _ENUMERATIONS_VALUES_ * current = array_of_enum_values;
	while(current->name!=NULL && strcmp(current->name+offset,text)) current++;
	return current==NULL ? 0 : current->value;
}

//---------------------------------------------------------

void RESOURCE_DESCRIPTOR::set_value(RESOURCE_CLIENT * client,const char *value) const
{
	switch(type)
	{
	case COLOR:
		set_color(client, text_to_color(value));
		break;

	case TEXT:
		set_text(client,value);
		break;

	case FONT:
		set_font(client,text_to_font(value));
		break;

	case BRUSH:
		set_hbrush(client,CreateSolidBrush(text_to_color(value)));
		break;

	case INTEGER:
		set_integer(client,atoi(value));
		break;

	case BOOLEA:
		set_boolean(client,text_to_boolean(value));
		break;

	case ENUM:
		set_enum(client,text_to_enum(value));
		break;

#if VERSION_OF_WINDOW >= 2

	case TEXT_TABLE:
		set_text_table(client,text_to_text_table(value));
		break;

#endif
	}
}
//---------------------------------------------------------

void RESOURCE_DESCRIPTOR::set_color(RESOURCE_CLIENT *client,COLORREF the_color) const
	// set to a color
{
	if(type!=COLOR) return;
	(client->*address).colorref = the_color;
}

//---------------------------------------------------------

void RESOURCE_DESCRIPTOR::set_text(RESOURCE_CLIENT *client,const char *the_text) const
	// set to a text
{
	if(type!=TEXT) return;
	assign_copy_of_text (&((client->*address).psztext), the_text);
}

//---------------------------------------------------------

void RESOURCE_DESCRIPTOR::set_font(RESOURCE_CLIENT *client,HFONT the_hfont) const
	// set to a font resource
{
	if(type!=FONT) return;
	assign_copy_of_font (&((client->*address).hfont), the_hfont);
}

//---------------------------------------------------------

void RESOURCE_DESCRIPTOR::set_hbrush(RESOURCE_CLIENT *client,HBRUSH the_hbrush) const
	// set to a brush ressource
{
	if(type!=BRUSH) return;
	assign_copy_of_brush (&((client->*address).hbrush), the_hbrush);
}

//---------------------------------------------------------

void RESOURCE_DESCRIPTOR::set_integer(RESOURCE_CLIENT *client,int the_integer) const
	// set to an integer value
{
	if(type!=INTEGER) return;
	(client->*address).integer = the_integer;
}

//---------------------------------------------------------

void RESOURCE_DESCRIPTOR::set_boolean(RESOURCE_CLIENT *client,bool the_boolean) const
	// set to a boolean value
{
	if(type!=BOOLEA) return;
	(client->*address).boolean = the_boolean;
}

//---------------------------------------------------------

void RESOURCE_DESCRIPTOR::set_enum(RESOURCE_CLIENT *client,int the_enum) const
	// set to a boolean value
{
	if(type!=ENUM) return;
	(client->*address).integer = the_enum;
}

//---------------------------------------------------------

#if VERSION_OF_WINDOW >= 2


void RESOURCE_DESCRIPTOR::set_text_table(RESOURCE_CLIENT *client,char **the_text_table) const
{
	if(type!=TEXT_TABLE) return;
	assign_text_table (&((client->*address).texttable), the_text_table);
}

#endif

//---------------------------------------------------------

