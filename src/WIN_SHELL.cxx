//-------------------------------------------------------------------
// WIN_SHELL.cxx
// ---------------
// implements a class which groups the Xt classes "Shell", "WMShell",
// and "VendorShell" 
// J.E.Bollo & G. Musset, M3G (c) 1998
//-------------------------------------------------------------------
#include "WIN_SHELL.hxx"
#include "WIN_MENUBAR.hxx"
#include "WINDOWS_MANAGER.hxx"
#include "win_naming.h"
#include "WIDGET_TREE.hxx"
#include "STRING_SCANNER.hxx"
#include "windows_commons.h"
//-------------------------------------------------------------------
BEGIN_RES_DESC(WIN_SHELL,WIN_COMPOSITE,NAME_OF_WIN_SHELL_CLASS)
	RES_DESC_ATR(XmNallowShellResize,BOOLEA,	WIN_SHELL, allow_shell_resize),
	RES_DESC_ATR(XmNgeometry,		TEXT,		WIN_SHELL, geometry),
	RES_DESC_ATR(XmNmaxWidth,		INTEGER,	WIN_SHELL, max_width),
	RES_DESC_ATR(XmNmaxHeight,		INTEGER,	WIN_SHELL, max_height),
	RES_DESC_ATR(XmNminWidth,		INTEGER,	WIN_SHELL, min_width),
	RES_DESC_ATR(XmNminHeight,		INTEGER,	WIN_SHELL, min_height),
	RES_DESC_ATR(XmNtitle,			TEXT,		WIN_SHELL, title),
	RES_DESC_ATR(XmNtransient,		BOOLEA,		WIN_SHELL, transient),
	RES_DESC_ATR(XmNbuttonFontList,	FONT,		WIN_SHELL, button_font_list),
	RES_DESC_ATR(XmNtextFontList,	FONT,		WIN_SHELL, text_font_list),
	RES_DESC_ATR(XmNlabelFontList,	FONT,		WIN_SHELL, label_font_list),
	RES_DESC_ATR(XmNdeleteResponse,	ENUM,		WIN_SHELL, delete_response),
	RES_DESC_ATR(XmNmwmDecorations,	INTEGER,	WIN_SHELL, mwm_decorations),
	RES_DESC_ATR(XmNmwmFunctions,	INTEGER,	WIN_SHELL, mwm_functions),
END_RES_DESC
//-------------------------------------------------------------------
IMPLEMENT_WINDOW(WIN_SHELL)
//-------------------------------------------------------------------
ATOM WIN_SHELL::register_window_class() const
{
	return WINDOWS_MANAGER::register_window_class(
					(const char*)get_class_name(),0,
					LoadCursor(NULL,IDC_ARROW));
}
//-------------------------------------------------------------------

WIN_SHELL::WIN_SHELL()
  : WIN_COMPOSITE()
  , allow_shell_resize(false)
  ,	geometry(NULL)
  , max_width(0)
  , max_height(0)
  , min_width(0)
  , min_height(0)
  , transient(0)
  , button_font_list(NULL)
  , label_font_list(NULL)
  , text_font_list(NULL)
  , mwm_decorations(-1)
  , mwm_functions(-1)
  , menubar(NULL)
  , delete_response(XmDESTROY)
{
	title = strdup("Cw_Shell");
}

//-------------------------------------------------------------------

WIN_SHELL::~WIN_SHELL()
{
	if (title != NULL) free(title);
	if (geometry != NULL) free (geometry);
	if (button_font_list != NULL) DeleteObject (button_font_list);
	if (label_font_list != NULL) DeleteObject (label_font_list);
	if (text_font_list != NULL) DeleteObject (text_font_list);
}

//-------------------------------------------------------------------

LONG WIN_SHELL::get_windows_style() const
	// retrieve the windows style used to create the widget 
{
#define isset(from,bit) ((from)==-1 || (((from)&bit)!=0)==(((from)&1)==0))

	LONG style = WS_CLIPSIBLINGS|WS_CLIPCHILDREN;

	if(isset(mwm_decorations,4))
		style |= WS_THICKFRAME;

	if(isset(mwm_decorations,8))
		style |= WS_CAPTION;

	if(isset(mwm_decorations,16))
		style |= WS_SYSMENU;

	if(isset(mwm_decorations,32))
		style |= WS_MINIMIZEBOX;

	if(isset(mwm_decorations,64))
		style |= WS_MAXIMIZEBOX;

	return style;
//	return WS_OVERLAPPEDWINDOW|WS_CLIPSIBLINGS|WS_CLIPCHILDREN;

#undef isset
}

//-------------------------------------------------------------------

bool WIN_SHELL::create_window()
{
	return WIN_CORE::create_window(get_windows_style(),0,title);
}

//-------------------------------------------------------------------

LRESULT WIN_SHELL::callback(UINT uMsg,WPARAM wParam,LPARAM lParam)
{
	switch(uMsg)
	{
	case WM_CREATE:
		{
			LPCREATESTRUCT lpcs = (LPCREATESTRUCT)lParam;
			lpcs->x = x;
			lpcs->y = y;
			lpcs->cx = width;
			lpcs->cy = height;
			RECT rect;
			LONG style = GetWindowLong(hwnd(),GWL_STYLE);
			rect.left = x;
			rect.top = y;
			rect.right = x+width;
			rect.bottom = y+height;
			AdjustWindowRect(&rect, style, menubar!=NULL);
			WINDOWS_MANAGER::move_window(this,rect.left,rect.top,
				rect.right-rect.left,rect.bottom-rect.top);
		}
		return 0;

	case WM_SIZE:
		if(!IsIconic(hwnd()))
		{
			width = (int)LOWORD(lParam);
			height = (int) HIWORD(lParam);
			resize();
		}
		return 0;

	case WM_MOVE:
		if(!IsIconic(hwnd()))
		{
			x = (int)LOWORD(lParam);
			y = (int)HIWORD(lParam);
		}
		return 0;

	case WM_CLOSE:
		switch (delete_response)
		{
		default:
		case XmDESTROY:
			destroy();
			break;
		case XmUNMAP:
			unmap();
			break;
		case XmDO_NOTHING:
			break;
		}
		return 0;
	}
	return WIN_COMPOSITE::callback(uMsg,wParam,lParam);
}

//-------------------------------------------------------------------

void WIN_SHELL::do_paint(HDC hdc,const RECT *rect) const
	// paint in response to a WM_PAINT
{
}

//-------------------------------------------------------------------

void WIN_SHELL::set_text(const char* txt)
	// sets the title of the shell
{
	if (hwnd() != NULL)
		SendMessage(hwnd(),WM_SETTEXT,0,(LPARAM)txt);
	else
		title = strdup(txt);
}

//-------------------------------------------------------------------

void WIN_SHELL::realize()
	// realise a shell widget and sub-widgets

{
	// Get the list of all Composite widgets children of the 
	// shell widget (including the shell itself ordered by depth
	//----------------------------------------------------------
	WIDGET_TREE wdg_tree;
	WIN_COMPOSITE **wdg_array = wdg_tree.get_composite_list(this);
	int wdg_num = wdg_tree.get_composite_number();

	// call the change_managed method of all Composite widget
	// from bottom to top
	//-------------------------------------------------------
	for (int i = wdg_num-1; i >= 0; i--)
		wdg_array[i]->change_managed();

	// Show the window and its children
	//---------------------------------

	WIN_COMPOSITE::realize();

	if(menubar!=NULL)
	{
		menubar->realize();
		menubar->attach_menu(this);
	}

	ShowWindow(hwnd(), SW_SHOWNORMAL);
	redraw();
}

//-------------------------------------------------------------------

void WIN_SHELL::change_managed()
	// The method change_manager called once during the realize phase and then
	// every time that one of its children becomes managed or unmanaged
{
	if (num_children == 0) return;
	WIN_CORE *child = (WIN_CORE *)children[0];
	assert(child!=NULL,("internal children problem"));
		// a shell has at most one unique child

	int new_width = width;
	int new_height = height;
	XtGeometryResult resu;
	XtWidgetGeometry request, prefered;

	request.request_mode = 0;
	if(width == 0)
	{
		request.request_mode = CWWidth;
		new_width = request.width = 100;
	}
	if(height == 0)
	{
		request.request_mode |= CWHeight;
		new_height = request.height = 100;
	}
	if(request.request_mode != 0)
	{
		resu = child->query_geometry(&request,&prefered);
		if(resu!=XtGeometryNo)
		{
			if(width==0 && (prefered.request_mode & CWWidth)!=0)
				new_width = prefered.width;
			if(height==0 && (prefered.request_mode & CWHeight)!=0)
				new_height = prefered.height;
		}
	}

	set_geometry(x,y,new_width,new_height);
}

//-------------------------------------------------------------------

XtGeometryResult WIN_SHELL::geometry_manager(
	WIN_CORE			*child,
	XtWidgetGeometry	*request,
	XtWidgetGeometry	*geometry_return
) 
		// The method geometry_manager called every time  
		// one of its children requests a new size
{
	if(!allow_shell_resize) return XtGeometryNo;
	return geometry_request(request);
}

//-------------------------------------------------------------------

void WIN_SHELL::do_layout(int )//call_type)
	// If the shell widget size is fixed, the shell widget
	// applies this size to its unique child, otherwise
	// it changes its own size to the size of the child
{
}

//-------------------------------------------------------------------

void WIN_SHELL::set_menu_bar(WIN_MENUBAR * menu)
	// set the menu
{
	menubar = menu;
	if(menubar!=NULL && is_realized())
	{
		menubar->attach_menu(this);
		menubar->redraw();
	}
}

//-------------------------------------------------------------------

bool WIN_SHELL::shell_process_traversal(WIN_CORE * child,int traversal_action)
	// process children traversal from child to the one that
	// correspond to traversal_action. traversal_action can be:
	// XmTRAVERSE_CURRENT, XmTRAVERSE_PREV_TAB_GROUP, 
	// XmTRAVERSE_NEXT_TAB_GROUP, XmTRAVERSE_LEFT, XmTRAVERSE_UP,
	// XmTRAVERSE_RIGHT, XmTRAVERSE_DOWN
	// returns true if the focus has been set
	// ................
	// Current version doesn't implement all
{
	int crazy_guard = 1500;
	WIN_COMPOSITE * parent;
	int i;

	switch(traversal_action)
	{
	case XmTRAVERSE_CURRENT:
		break;

	case XmTRAVERSE_PREV_TAB_GROUP:
	case XmTRAVERSE_LEFT:
	case XmTRAVERSE_UP:
		do
		{
			do
			{
				parent = child->get_parent_win();
				if(parent==NULL)
					return false;
				i = parent->get_index_of_child(child);
				if(i>0)
				{
					child = parent->get_child_at(i-1);
					break;
				}
				child = parent;
			}
			while(child!=this);

			while(child->is_kind_of(WIN_COMPOSITE::the_resource_class()))
			{
				parent = (WIN_COMPOSITE*)child;
				child = parent->get_child_at(parent->get_num_children()-1);
			}
			crazy_guard--;
		}
		while(crazy_guard!=0 
			&& !(child->traversal_on)); // && child->traversal_type!=XmNONE));
		break;

	case XmTRAVERSE_NEXT_TAB_GROUP:
	case XmTRAVERSE_RIGHT:
	case XmTRAVERSE_DOWN:
		do
		{
			do
			{
				parent = child->get_parent_win();
				if(parent==NULL)
					return false;
				i = 1 + parent->get_index_of_child(child);
				if(i<parent->get_num_children())
				{
					child = parent->get_child_at(i);
					break;
				}
				child = parent;
			}
			while(child!=this);

			while(child->is_kind_of(WIN_COMPOSITE::the_resource_class()))
			{
				parent = (WIN_COMPOSITE*)child;
				child = parent->get_child_at(0);
			}
			crazy_guard--;
		}
		while(crazy_guard!=0 
			&& !(child->traversal_on)); // && child->traversal_type!=XmNONE));
		break;
	}

	if(crazy_guard==0)
		return false;
	child->set_focus();
	return true;
}

//-------------------------------------------------------------------

void WIN_SHELL::resize()
	// The method resize is called every time a widget changes size
{
	RECT rect;
	if(GetClientRect(hwnd(),&rect)!=0)
	{
		if (num_children > 0)
		{
			WIN_CORE *child = (WIN_CORE *)children[0];
			assert(child!=NULL,("internal children problem"));
			child->set_geometry(0,0,rect.right,rect.bottom);
		}
	}
}

//-------------------------------------------------------------------

XtGeometryResult WIN_SHELL::geometry_request (XtWidgetGeometry *request)
	// request to change the geometry of this according to request
	// returns only XtGeometryYes (request accepted) or XtGeometryNo (denied)
{
//	if(!allow_shell_resize) return XtGeometryNo;
	WIN_CORE::set_geometry(request);
	return XtGeometryYes;
}

//-------------------------------------------------------------------

void WIN_SHELL::set_geometry(int x,int y,int width,int height)
	// apply unconditionnaly the requested geometry
{
	bool changed = false;
	if(x!=this->x)
	{
		this->x = x;
		changed = true;
	}
	if(y!=this->y) 
	{
		this->y = y;
		changed = true;
	}
	if(width!=this->width) 
	{
		this->width = width;
		changed = true;
	}
	if(height!=this->height) 
	{
		this->height = height;
		changed = true;
	}

	if(changed)
	{
		if(hwnd()!=NULL)
		{
			RECT rect;
			LONG style = GetWindowLong(hwnd(),GWL_STYLE);
			rect.left = x;
			rect.top = y;
			rect.right = x+width;
			rect.bottom = y+height;
			AdjustWindowRect(&rect, style, menubar!=NULL);
			WINDOWS_MANAGER::move_window(this,rect.left,rect.top,
				rect.right-rect.left,rect.bottom-rect.top);
		}
	}
}

//-------------------------------------------------------------------

void WIN_SHELL::geometry_changed()
	// called when the geometry attribut has changed
{
	// checks validity
	if(geometry==NULL)
		return;

	// request to build
	XtWidgetGeometry request;
	request.request_mode = 0;

	// 
	STRING_SCANNER string(geometry);

	// scans the width
	if(string.start_with_digit())
	{
		request.width = string.read_integer();
		request.request_mode |= CWWidth;
	}

	// scans the height
	if(string.start_with_char('x'))
	{
		string.next();
		if(string.start_with_digit())
		{
			request.height = string.read_integer();
			request.request_mode |= CWHeight;
		}
	}

	// scans the x offset
	if(string.start_with_char('-'))
	{
		string.next();
		if(string.start_with_digit())
		{
			request.x = GetSystemMetrics(SM_CXFULLSCREEN)
				- string.read_integer();
			if((request.request_mode & CWWidth)!=0)
				request.x -= request.width;
			else
				request.x -= width;
			request.request_mode |= CWX;
		}
	}
	else
	if(string.start_with_char('+'))
	{
		string.next();
		if(string.start_with_digit())
		{
			request.x = string.read_integer();
			request.request_mode |= CWX;
		}
	}

	// scans the y offset
	if(string.start_with_char('-'))
	{
		string.next();
		if(string.start_with_digit())
		{
			request.y = GetSystemMetrics(SM_CYFULLSCREEN)
				- string.read_integer();
			if((request.request_mode & CWHeight)!=0)
				request.y -= request.height;
			else
				request.y -= width;
			request.request_mode |= CWY;
		}
	}
	else
	if(string.start_with_char('+'))
	{
		string.next();
		if(string.start_with_digit())
		{
			request.y = string.read_integer();
			request.request_mode |= CWY;
		}
	}


	// emit a geometry request
	if(request.request_mode!=0)
		geometry_request(&request);
}

//-------------------------------------------------------------------

void WIN_SHELL::set_resource_values(const RESOURCE_SCANNER & scanner)
	// put values of scanner, the currently scanned resources
	// use default and check if constraints should be checked
{
	char * previous_geometry = geometry;
	WIN_CORE::set_resource_values(scanner);
	if(previous_geometry != geometry)
		geometry_changed();
}

//-------------------------------------------------------------------

int WIN_SHELL::internal_get_values
	// function used internally to get attribut values 
	// should not be called directly (call get_values)
	// arglist is the array of the values to get
	// count is the count of values to be put into arglist
	// got is an array that flag if an attribut have been put
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs put
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) const
{
	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.

		if(equality(attribut,XmNallowShellResize))
			arglist[index].value.pvalue->boolean = allow_shell_resize;
		else
		if(equality(attribut,XmNmaxWidth))
			arglist[index].value.pvalue->integer = max_width;
		else
		if(equality(attribut,XmNmaxHeight))
			arglist[index].value.pvalue->integer = max_height;
		else
		if(equality(attribut,XmNminWidth))
			arglist[index].value.pvalue->integer = min_width;
		else
		if(equality(attribut,XmNminHeight))
			arglist[index].value.pvalue->integer = min_height;
		else
		if(equality(attribut,XmNtransient))
			arglist[index].value.pvalue->boolean = transient;
		else
		if(equality(attribut,XmNmwmDecorations))
			arglist[index].value.pvalue->integer = mwm_decorations;
		else
		if(equality(attribut,XmNmwmFunctions))
			arglist[index].value.pvalue->integer = mwm_functions;
		else
		if(equality(attribut,XmNdeleteResponse))
			arglist[index].value.pvalue->integer = delete_response;

#if VERSION_OF_WINDOW >= 2

		else
		if(equality(attribut,XmNgeometry))
			arglist[index].value.pvalue->psztext = geometry;
		else
		if(equality(attribut,XmNtitle))
			arglist[index].value.pvalue->psztext = title;
		else
		if(equality(attribut,XmNbuttonFontList))
			arglist[index].value.pvalue->hfont = button_font_list;
		else	
		if(equality(attribut,XmNlabelFontList))
			arglist[index].value.pvalue->hfont = label_font_list;
		else
		if(equality(attribut,XmNtextFontList))
			arglist[index].value.pvalue->hfont = text_font_list;

#else

		else
		if(equality(attribut,XmNgeometry))
			arglist[index].value.pvalue->psztext = strdup(geometry==NULL ? "" : geometry);
		else
		if(equality(attribut,XmNtitle))
			arglist[index].value.pvalue->psztext = strdup(title==NULL ? "" : title);
		else
		if(equality(attribut,XmNbuttonFontList))
		{	
			if(button_font_list==NULL)
				arglist[index].value.pvalue->hfont = NULL;
			else
			{
				LOGFONT logfont;
				GetObject(button_font_list,sizeof logfont,&logfont);
				arglist[index].value.pvalue->hfont = 
							CreateFontIndirect(&logfont);
			}
		}
		else	
		if(equality(attribut,XmNlabelFontList))
		{	
			if(label_font_list==NULL)
				arglist[index].value.pvalue->hfont = NULL;
			else
			{
				LOGFONT logfont;
				GetObject(label_font_list,sizeof logfont,&logfont);
				arglist[index].value.pvalue->hfont = 
							CreateFontIndirect(&logfont);
			}
		}
		else
		if(equality(attribut,XmNtextFontList))
		{	
			if(text_font_list==NULL)
				arglist[index].value.pvalue->hfont = NULL;
			else
			{
				LOGFONT logfont;
				GetObject(text_font_list,sizeof logfont,&logfont);
				arglist[index].value.pvalue->hfont = 
							CreateFontIndirect(&logfont);
			}
		}

#endif

		else
			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
	}

	// return total count of found attributs (including ancestors)
	return found_count + WIN_COMPOSITE::internal_get_values(arglist,count,got,equality);
}


//-------------------------------------------------------------------

int WIN_SHELL::internal_set_values
	// function used internally to set attribut values 
	// should not be called directly (call set_values)
	// arglist is the array of the values to be set,
	// count is the count of values in arglist
	// got is an array that flag if an attribut have been set
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs set
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) 
{
	// empty geometry request
	bool redraw_needed = false;
/*
	XtWidgetGeometry request;
	request.request_mode = 0;
*/

	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.
		if(equality(attribut,XmNallowShellResize))
			allow_shell_resize = arglist[index].value.boolean;
		else
		if(equality(attribut,XmNgeometry))
		{
			safe_assign_copy_of_text (&geometry, arglist[index].value.psztext);
			geometry_changed();
			redraw_needed = true;
		}
		else
		if(equality(attribut,XmNmaxWidth))
		{
			max_width = arglist[index].value.integer;
			redraw_needed = true;
		}
		else
		if(equality(attribut,XmNmaxHeight))
		{
			max_height = arglist[index].value.integer;
			redraw_needed = true;
		}
		else
		if(equality(attribut,XmNminWidth))
		{
			min_width = arglist[index].value.integer;
			redraw_needed = true;
		}
		else
		if(equality(attribut,XmNminHeight))
		{
			min_height = arglist[index].value.integer;
			redraw_needed = true;
		}
		else
		if(equality(attribut,XmNdeleteResponse))
			delete_response = arglist[index].value.integer;
		else
		if(equality(attribut,XmNtitle))
			set_text(arglist[index].value.psztext);
		else
		if(equality(attribut,XmNtransient))
			transient = arglist[index].value.boolean;
		else
		if(equality(attribut,XmNbuttonFontList))
		{	
			assign_copy_of_font (&button_font_list, arglist[index].value.hfont);
			redraw_needed = true;
		}
		else	
		if(equality(attribut,XmNlabelFontList))
		{	
			assign_copy_of_font (&label_font_list, arglist[index].value.hfont);
			redraw_needed = true;
		}
		else
		if(equality(attribut,XmNtextFontList))
		{	
			assign_copy_of_font (&text_font_list, arglist[index].value.hfont);
			redraw_needed = true;
		}
		else
		if(equality(attribut,XmNmwmDecorations))
			set_mwm_decorations(arglist[index].value.integer);
		else
		if(equality(attribut,XmNmwmFunctions))
		{
			mwm_functions = arglist[index].value.integer;
			redraw_needed = true;
		}
		else
			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
	}

/*
	// make the geometry request if necessary
	if(request.request_mode!=0)
		geometry_request(&request);
*/
	if(redraw_needed)
		redraw();

	// return total count of found attributs (including ancestors)
	return found_count  + WIN_COMPOSITE::internal_set_values(arglist,count,got,equality);
}

//-------------------------------------------------------------------

