//-------------------------------------------------------------------
// WIN_OPTIONITEM.cxx
// ---------------
// Class which implements the items of option menus
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#include "WIN_OPTIONITEM.hxx"
#include "WIN_OPTIONMENU.hxx"
#include "win_naming.h"
#include "WINDOWS_MANAGER.hxx"
//-------------------------------------------------------------------
BEGIN_RES_DESC(WIN_OPTIONITEM,WIN_PUSHBUTTON,NAME_OF_WIN_OPTIONITEM_CLASS)
END_RES_DESC
//-------------------------------------------------------------------
WIN_OPTIONITEM::WIN_OPTIONITEM()
  : WIN_PUSHBUTTON()
  , mapped(false)
{
}
//-------------------------------------------------------------------
WIN_OPTIONITEM::~WIN_OPTIONITEM()
{
}
//-------------------------------------------------------------------
void WIN_OPTIONITEM::set_text(const char* txt)
	// replaces all the text by "txt"
{
	WIN_LABEL::set_text(txt);
	WIN_OPTIONMENU * menu = get_option_menu();
	if(menu!=NULL)
		menu->update_string_item(this);
}
//-------------------------------------------------------------------
char* WIN_OPTIONITEM::get_text() const
	// gets all the text
{
	return WIN_LABEL::get_text();
}
//-------------------------------------------------------------------
void WIN_OPTIONITEM::redraw() const
	// redraw the window
{
	WIN_OPTIONMENU * menu = get_option_menu();
	if(menu!=NULL)
		menu->redraw();
}
//-------------------------------------------------------------------
void WIN_OPTIONITEM::set_name_and_parent(const char *name,WIN_COMPOSITE *parent)
	// store a widget name and its parent
{
	assert(parent!=NULL,("no parent"));
	assert(parent->is_kind_of(WIN_OPTIONMENU::the_resource_class()),
		("bad parent type"));
	WIN_CORE::set_name_and_parent(name,parent);
}
//-------------------------------------------------------------------
void WIN_OPTIONITEM::set_sensitive(bool value)
	// set attribute sensitive
{
	WIN_OPTIONMENU * menu = get_option_menu();
	if(menu!=NULL)
		menu->update_sensitive_item(this,value);
}


//-------------------------------------------------------------------
void WIN_OPTIONITEM::manage()
	// set this managed
{
	WIN_OPTIONMENU * menu = get_option_menu();
	if(menu!=NULL)
		menu->update_manage_item(this,true);
}
//-------------------------------------------------------------------
void WIN_OPTIONITEM::unmanage()
	// set this not managed
{
	WIN_OPTIONMENU * menu = get_option_menu();
	if(menu!=NULL)
		menu->update_manage_item(this,false);
}
//-------------------------------------------------------------------
void WIN_OPTIONITEM::map()
	// set this mapped
{
	WIN_OPTIONMENU * menu = get_option_menu();
	if(menu!=NULL)
		menu->update_map_item(this,true);
}
//-------------------------------------------------------------------
void WIN_OPTIONITEM::unmap()
	// set this mapped
{
	WIN_OPTIONMENU * menu = get_option_menu();
	if(menu!=NULL)
		menu->update_map_item(this,false);
}
//-------------------------------------------------------------------
void WIN_OPTIONITEM::realize()
	// realise this
{
	map();
}
//-------------------------------------------------------------------
bool WIN_OPTIONITEM::is_realized() const
	// is this realized?
{
	return get_option_menu()->is_realized();
}
