//---------------------------------------------------------
// RESOURCE_TREE.hxx
// ------------------
// object to manage the resource (in the X11 way) tree
// J.E.Bollo, M3G (c) 1998
//---------------------------------------------------------
#ifndef __resource_tree_hxx
#define __resource_tree_hxx
//---------------------------------------------------------
#include "macros.h"
#include "NAMES_SPACE.hxx"
//---------------------------------------------------------
class RESOURCE_TREE
{
private:
	enum TYPE_OF_NODE { ROOT, NAMED, ANY_NAME, WILD_BIND };
		// resource node is either a name, 
		// or any name, or a wild bind

	TYPE_OF_NODE type;
		// type of the node

	NAME my_name;
		// the name if type==NAMED

	RESOURCE_TREE * parent;
		// link to the parent of this
		// NULL or this if root

	RESOURCE_TREE * previous_sister;
		// link to the previous sister
		// NULL if first sister

	RESOURCE_TREE * next_sister;
		// link to the next sister
		// NULL if last sister

	RESOURCE_TREE * first_daughter;
		// link to the first daughter
		// NULL if no daughter

	char * my_value;
		// string to the value
		// NULL if no value

	int count_of_refs;
		// count the number of add/remove

private:

	RESOURCE_TREE(RESOURCE_TREE *the_parent, TYPE_OF_NODE the_type, NAME the_name);
		// create a daughter of the_parent

public:

	RESOURCE_TREE();
		// create a root

	~RESOURCE_TREE();
		// delete this and its daughter(s)

	RESOURCE_TREE * add_named_daughter(NAME name);
		// add a daughter named name and return it

	RESOURCE_TREE * add_any_name_daughter();
		// add a any name daughter and return it

	RESOURCE_TREE * add_wild_bind_daughter();
		// add a wild bind daughter and return it

	int is_named() const { return type==NAMED; }
		// is this named

	int is_any_name() const { return type==ANY_NAME; }
		// is this the any name

	int is_wild_bind() const { return type==WILD_BIND; }
		// is this a wild bind

	void set_value(const char * value);
		// set this value to a copy of value

	const char * get_value() const { return my_value; }
		// returns the value

	int has_value() const { return my_value!=NULL; }
		// true if this has a value set to something

	int is_root() const { return parent==this; }
		// true if this is root

	RESOURCE_TREE * get_parent() const { return parent; }
		// get parent of this

	RESOURCE_TREE * get_root() const;
		// get the root

	int has_daugther() const { return first_daughter!=NULL; }
		// true if this has at least one daughter

	RESOURCE_TREE * get_first_daughter() const { return first_daughter; }
		// return pointer to first daughter or NULL

	int has_sister() const { return next_sister!=NULL; }
		// true if this is not the last daughter of parent

	RESOURCE_TREE * get_next_sister() const { return next_sister; }
		// return pointer to next sister or NULL

	RESOURCE_TREE * get_named_daughter(NAME name) const;
		// return pointer to the daughter named name or NULL if none

	RESOURCE_TREE * get_any_name_daughter() const;
		// return pointer to the any name daughter or NULL if none

	RESOURCE_TREE * get_wild_bind_daughter() const;
		// return pointer to the wild bind daughter or NULL if none

	void remove();
		// decrements count_of_refs 
		// delete this if needed
};
//---------------------------------------------------------
#endif

