//-------------------------------------------------------------------
// RESOURCE_READER.hxx
// -------------------
// super class of all objects that read resources into a RESOURCE_DATABASE
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __resource_reader_hxx
#define __resource_reader_hxx
//-------------------------------------------------------------------
#include "macros.h"
#include "RESOURCE_DATABASE.hxx"
#include "RESOURCE_READER.hxx"
//-------------------------------------------------------------------
class RESOURCE_READER
{
private:
	DWORD last_error;
		// records the pending error code if any

protected:
	void set_last_error(DWORD error) { last_error = error; }
		// set the last error code (only childs)

public:
	RESOURCE_READER() : last_error(0) {}
		// public constructor

	DWORD get_last_error() { return last_error; }
		// retrieving the last error code

	virtual BOOL read_resource(const char *input_name,
				RESOURCE_DATABASE &resource_database,SIMPLE_ERROR &error) = 0;
		// read the resource and return TRUE if OK, FALSE otherwise
};
//-------------------------------------------------------------------
#endif




