//-------------------------------------------------------------------
// WIN_TOP_LEVEL_SHELL.cxx
// ---------------
// implements a class which defines the Xt class "ToplevelShell"
// J.E.Bollo & G. Musset, M3G (c) 1998
//-------------------------------------------------------------------
#include "WIN_TOP_LEVEL_SHELL.hxx"
#include "WINDOWS_MANAGER.hxx"
#include "win_naming.h"
#include "windows_commons.h"
//-------------------------------------------------------------------
BEGIN_RES_DESC(WIN_TOP_LEVEL_SHELL,WIN_SHELL,NAME_OF_WIN_TOP_LEVEL_SHELL_CLASS)
	RES_DESC_ATR(XmNiconic,   BOOLEA, WIN_TOP_LEVEL_SHELL, iconic),
	RES_DESC_ATR(XmNiconName, TEXT,    WIN_TOP_LEVEL_SHELL, icon_name),
END_RES_DESC
//-------------------------------------------------------------------
IMPLEMENT_WINDOW(WIN_TOP_LEVEL_SHELL)
//-------------------------------------------------------------------
ATOM WIN_TOP_LEVEL_SHELL::register_window_class() const
{
	return WINDOWS_MANAGER::register_window_class(
					(const char*)get_class_name(),0,
					LoadCursor(NULL,IDC_ARROW));
}
//-------------------------------------------------------------------
WIN_TOP_LEVEL_SHELL::WIN_TOP_LEVEL_SHELL()
  : WIN_SHELL()
  , iconic(false)
  , icon_name(NULL)
{
}
//-------------------------------------------------------------------
WIN_TOP_LEVEL_SHELL::~WIN_TOP_LEVEL_SHELL()
{
}
//-------------------------------------------------------------------
LRESULT WIN_TOP_LEVEL_SHELL::callback(UINT uMsg,WPARAM wParam,LPARAM lParam)
{
//	if (uMsg == WM_DESTROY)
//		printf ("%s\n", WIN_TOP_LEVEL_SHELL::name.name->the_string);

	return WIN_SHELL::callback(uMsg,wParam,lParam);
}
//-------------------------------------------------------------------
LONG WIN_TOP_LEVEL_SHELL::get_windows_style() const
	// retrieve the windows style used to create the widget 
{
//	DWORD style = WS_OVERLAPPEDWINDOW|WS_CLIPSIBLINGS|WS_CLIPCHILDREN;
	DWORD style = WIN_SHELL::get_windows_style();
	if (iconic) style |= WS_ICONIC;
	return style;
}
//-------------------------------------------------------------------
bool WIN_TOP_LEVEL_SHELL::create_window()
{
	return WIN_CORE::create_window(get_windows_style(),0,title);
}
//-------------------------------------------------------------------

void WIN_TOP_LEVEL_SHELL::set_iconic(bool value)
{
	iconic = value;
}

//-------------------------------------------------------------------


int WIN_TOP_LEVEL_SHELL::internal_get_values
	// function used internally to get attribut values 
	// should not be called directly (call get_values)
	// arglist is the array of the values to get
	// count is the count of values to be put into arglist
	// got is an array that flag if an attribut have been put
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs put
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) const
{
	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.

		if(equality(attribut,XmNiconic))
			arglist[index].value.pvalue->boolean = iconic;

#if VERSION_OF_WINDOW >= 2

		else
		if(equality(attribut,XmNiconName))
			arglist[index].value.pvalue->psztext = icon_name;

#else

		else
		if(equality(attribut,XmNiconName))
			arglist[index].value.pvalue->psztext = strdup(icon_name==NULL ? "" : icon_name);

#endif

		else
			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
	}

	// return total count of found attributs (including ancestors)
	return found_count + WIN_SHELL::internal_get_values(arglist,count,got,equality);
}


//-------------------------------------------------------------------

int WIN_TOP_LEVEL_SHELL::internal_set_values
	// function used internally to set attribut values 
	// should not be called directly (call set_values)
	// arglist is the array of the values to be set,
	// count is the count of values in arglist
	// got is an array that flag if an attribut have been set
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs set
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) 
{
	// empty geometry request
/*
	bool redraw_needed = false;
	XtWidgetGeometry request;
	request.request_mode = 0;
*/

	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.
		if(equality(attribut,XmNiconic))
			set_iconic(arglist[index].value.boolean);
		else
		if(equality(attribut,XmNiconName))
			safe_assign_copy_of_text (&icon_name, arglist[index].value.psztext);
		else
			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
	}

/*
	// make the geometry request if necessary
	if(request.request_mode!=0)
		geometry_request(&request);
	if(redraw_needed)
		redraw();
*/

	// return total count of found attributs (including ancestors)
	return found_count  + WIN_SHELL::internal_set_values(arglist,count,got,equality);
}

