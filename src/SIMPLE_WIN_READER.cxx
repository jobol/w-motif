//---------------------------------------------------------
// SIMPLE_WIN_READER.cxx
// ---------------------
// Just a simple reader
// J.E.Bollo, M3G (c) 1998
//---------------------------------------------------------
#include "SIMPLE_WIN_READER.hxx"
//---------------------------------------------------------
SIMPLE_WIN_READER::SIMPLE_WIN_READER()
	// construction
  : SIMPLE_READER()
  , hfile(NULL)
{}

SIMPLE_WIN_READER::~SIMPLE_WIN_READER()
	// destruction
{
	if(is_opened()) close();
}

BOOL SIMPLE_WIN_READER::open_file(LPCSTR nom_fichier)
	// open the file nom_fichier
	// return TRUE if ok, FALSE otherwise
{
	assert(hfile==NULL,("can't open file if is already opened"));
	HANDLE hf = CreateFile(
					nom_fichier,
					GENERIC_READ,
					FILE_SHARE_READ,
					NULL,
					OPEN_EXISTING,
					FILE_FLAG_SEQUENTIAL_SCAN|FILE_ATTRIBUTE_NORMAL,
					NULL);
	if(hf==INVALID_HANDLE_VALUE)
	{
		set_error(GetLastError());
		return FALSE;
	}
	hfile = hf;
	return TRUE;
}

int SIMPLE_WIN_READER::read(void *where,int len)
	// read len byte at memory place where
	// return number of bytes read, 0 at EOF, -1 in case of error
{
	assert(len>0,("can't read nul or negative length %d",len));
	assert(hfile!=NULL,("can't read if nothing is opened"));
	DWORD nrlu;
	if(!ReadFile(hfile,(LPVOID)where,1,&nrlu,NULL))
	{
		set_error(GetLastError());
		return -1;
	}
	return (int)nrlu;
}

void SIMPLE_WIN_READER::close()
	// close the opened file
{
	assert(hfile!=NULL,("can't close if nothing is opened"));
	CloseHandle(hfile);
	hfile = NULL;
}

BOOL SIMPLE_WIN_READER::is_opened()
	// checks if opened, returns TRUE when opened
{
	return hfile!=NULL;
}
//---------------------------------------------------------
