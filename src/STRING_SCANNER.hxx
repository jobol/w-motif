//-------------------------------------------------------------------
// STRING_SCANNER.hxx
// ------------------
// implements a simple string scanner
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __string_scanner_hxx
#define __string_scanner_hxx
//-------------------------------------------------------------------
class STRING_SCANNER
	// a simple string scanner
{
private:

	const char * string;
		// head of the scanned string

public:

	STRING_SCANNER(const char * str) 
		// constructor
		: string(str) {}

	bool start_with_char(char c) const
		// does the string begin with the char c?
		{ return string[0] == c; }

	bool start_with_digit() const
		// does the string begin with a digit?
		{ return string[0]>='0' && string[0]<='9'; }

	bool at_end() const
		// does the string begin with a null (so at eos)?
		{ return string[0] == 0; }

	void next()
		// go to the next char
		{ if(!at_end()) string++; }

	int read_integer()
		// read the next integer and advance head
		{
			int value = 0;
			while(start_with_digit())
			{
				value = 10 * value + string[0] - '0';
				next();
			}
			return value;
		}

/*
	bool start_with_upper() const
		{ return string[0]>='A' && string[0]<='Z'; }

	bool start_with_lower() const
		{ return string[0]>='a' && string[0]<='z'; }

	bool start_with_letter() const
		{ return start_with_lower() || start_with_upper(); }

	bool start_with_some_of(const char *chars) const
		{ 
			while(chars[0] && chars[0]!=string[0])
					chars++;
			return chars[0]==string[0];
		}

	void skip_any_of(const char *chars)
		{
			while(!at_end() && start_with_some_of(chars))
				next();
		}

	bool start_with(const char *chars) const
		{
			int i = 0;
			while(chars[i]!=0 && string[i]==chars[i])
				i++;
			return i!=0 && chars[i]==0;
		}

	void skip(int len)
		{
			while(len>0 && !at_end())
			{
				len--;
				next();
			}
		}
*/
};
//-------------------------------------------------------------------
#endif

