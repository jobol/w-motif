//-------------------------------------------------------------------
// WIN_COMPOSITE.cxx
// ---------------
// implements a class used to define the Xt class "WIN_COMPOSITE"
// J.E.Bollo & G. Musset, M3G (c) 1998
//-------------------------------------------------------------------
#include "WIN_COMPOSITE.hxx"
#include "win_naming.h"
//-------------------------------------------------------------------
BEGIN_RES_DESC(WIN_COMPOSITE,WIN_CORE,NAME_OF_WIN_COMPOSITE_CLASS)
	RES_DESC_ATR(XmNnumChildren,  INTEGER, WIN_COMPOSITE, num_children),
//	RES_DESC_ATR(XmNchildren,  ????, WIN_COMPOSITE, children),
END_RES_DESC
//-------------------------------------------------------------------
WIN_COMPOSITE::WIN_COMPOSITE()
  : WIN_CORE()
  , children(NULL)
  , num_children(0)
{
	traversal_type = XmTAB_GROUP;
}
//-------------------------------------------------------------------
WIN_COMPOSITE::~WIN_COMPOSITE()
{
	if (children != NULL)
		free (children);
}

//-------------------------------------------------------------------

void WIN_COMPOSITE::destroy()
	// public destruction
{
	if (!destroy_pending)
	{
		destroy_pending = true;
		activate_callbacks(XmNdestroyCallback);
		while (num_children > 0)
			children[num_children-1]->destroy();
		if (get_parent_win() != NULL)
			get_parent_win()->delete_child(this);
		destroy_window();
		if (is_in_activate_callbacks <= 0)
			delete this;
		else
			deferred_delete = true;
	}
}

//-------------------------------------------------------------------

void WIN_COMPOSITE::insert_child (WIN_CORE * child)
	// insert a child in the children array
{
	assert(child!=NULL,("invalid parameter"));

	// adjust array size
	children = (WIN_CORE**) realloc(children, (num_children+1)*sizeof(WIN_CORE *));
	assert (children!=NULL, ("memory depletion"));

	// insert new child at the last position of array
	children[num_children++] = child;
}

//-------------------------------------------------------------------

void WIN_COMPOSITE::delete_child (WIN_CORE * child)
	// delete a child in the children array
{
	int i;

	assert(child!=NULL,("invalid parameter"));
	assert (num_children>0 ,("no children in composite widget"));

	// search child in children array
	for (i=num_children; i>0 ; )
		if (children[--i] == child) break;
	assert(i>=0,("child not found"));

	// update children counter
	num_children--;

	// shift all children behind deleted child
	while(i<num_children)
	{
		children[i] = children[i+1];
		i++;
	}

	// remove child constraint if any
	if(child->constraints != NULL)
	{
		delete child->constraints;
		child->constraints = NULL;
	}
}

//-------------------------------------------------------------------

void WIN_COMPOSITE::realize()
	// realise this
{
	WIN_CORE::realize();

	int i;
	for (i=0; i<num_children; i++)
	{
		if(children[i]->is_managed())
			children[i]->realize();
	}
}

//-------------------------------------------------------------------

int WIN_COMPOSITE::constraint_get_values
(
	const WIN_CORE *	/* child */,
	ARGLIST		/* arglist */,
	int			/* count */,
	bool	 *	/* got */,
	bool    (*	/* equality */)(const char*,const char*)
) const
{
	return 0;
}

//-------------------------------------------------------------------

int WIN_COMPOSITE::constraint_set_values
(
	WIN_CORE *	/* child */,
	ARGLIST		/* arglist */,
	int			/* count */,
	bool	 *	/* got */,
	bool    (*	/* equality */)(const char*,const char*)
)
{
	return 0;
}

//-------------------------------------------------------------------

