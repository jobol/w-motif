//---------------------------------------------------------
// WIDGET_ITF_C.cxx
// ----------------
// widgets interface function with C
// J.E.Bollo, M3G (c) 1998
//---------------------------------------------------------

#include "macros.h"
#include "WIDGET_ITF_C.h"

#include "RESOURCE_CLASS.hxx"

#include "WIN_CORE.hxx"
#include "WIN_COMPOSITE.hxx"
#include "WIN_SHELL.hxx"
#include "WIN_TOP_LEVEL_SHELL.hxx"
#include "WIN_LABEL.hxx"
#include "WIN_PUSHBUTTON.hxx"
#include "WIN_TOGGLEBUTTON.hxx"
#include "WIN_TEXT.hxx"
#include "WIN_LIST.hxx"
#include "WIN_BULLETIN_BOARD.hxx"
#include "WIN_MESSAGE_BOX.hxx"
#include "WIN_FRAME.hxx"
#include "WIN_FORM.hxx"
#include "WIN_SEPARATOR.hxx"
#include "WIN_ROWCOLUMN.hxx"
#include "WIN_OPTIONMENU.hxx"
#include "WIN_OPTIONITEM.hxx"
#include "WIN_MENUBAR.hxx"
#include "WIN_MENUPOPUP.hxx"
#include "WIN_SUBMENU.hxx"
#include "WIN_MENUITEM.hxx"
#include "WIN_SCROLLEDWINDOW.hxx"
#include "WIN_DRAWINGAREA.hxx"

#include "SIMPLE_ERROR.hxx"
#include "WINDOWS_MANAGER.hxx"

//---------------------------------------------------------
// SOME PRIVATE MACROS
//---------------------------------------------------------

#define  core_class					(WIN_CORE::the_resource_class())
#define  composite_class			(WIN_COMPOSITE::the_resource_class())
#define  shell_class				(WIN_SHELL::the_resource_class())
#define  is_valid_widget(wdg)		(wdg!=NULL)
#define  core_of(wdg)				((WIN_CORE*)wdg)
#define  composite_of(wdg)			((WIN_COMPOSITE*)wdg)
#define  is_widget_core(wdg)		(core_of(wdg)->is_kind_of(core_class))
#define  is_widget_composite(wdg)	(core_of(wdg)->is_kind_of(composite_class))
#define  is_widget_shell(wdg)		(core_of(wdg)->is_kind_of(shell_class))
#define  is_valid_core(wdg)			(is_valid_widget(wdg) && is_widget_core(wdg))
#define  is_valid_shell(wdg)		(is_valid_widget(wdg) && is_widget_shell(wdg))

//---------------------------------------------------------
// C WIDGET CLASSES
//---------------------------------------------------------

extern "C" 
	CWIDGETCLASS CSHELL_CLASS = &WIN_SHELL::__the__class;
extern "C" 
	CWIDGETCLASS CTOP_LEVEL_SHELL_CLASS = &WIN_TOP_LEVEL_SHELL::__the__class;
extern "C" 
	CWIDGETCLASS CLABEL_CLASS = &WIN_LABEL::__the__class;
extern "C" 
	CWIDGETCLASS CPUSHBUTTON_CLASS = &WIN_PUSHBUTTON::__the__class;
extern "C" 
	CWIDGETCLASS CTOGGLEBUTTON_CLASS = &WIN_TOGGLEBUTTON::__the__class;
extern "C" 
	CWIDGETCLASS CTEXT_CLASS = &WIN_TEXT::__the__class;
extern "C" 
	CWIDGETCLASS CLIST_CLASS = &WIN_LIST::__the__class;
extern "C" 
	CWIDGETCLASS CBULLETIN_BOARD_CLASS = &WIN_BULLETIN_BOARD::__the__class;
extern "C" 
	CWIDGETCLASS CROWCOLUMN_CLASS = &WIN_ROWCOLUMN::__the__class;
extern "C"
	CWIDGETCLASS CMESSAGE_BOX_CLASS = &WIN_MESSAGE_BOX::__the__class;
extern "C"
	CWIDGETCLASS CSELECTION_BOX_CLASS = &WIN_MESSAGE_BOX::__the__class;
extern "C" 
	CWIDGETCLASS CFRAME_CLASS = &WIN_FRAME::__the__class;
extern "C" 
	CWIDGETCLASS CFORM_CLASS = &WIN_FORM::__the__class;
extern "C"
	CWIDGETCLASS CSEPARATOR_CLASS = &WIN_SEPARATOR::__the__class;
extern "C"
	CWIDGETCLASS COPTIONMENU_CLASS = &WIN_OPTIONMENU::__the__class;
extern "C"
	CWIDGETCLASS COPTIONITEM_CLASS = &WIN_OPTIONITEM::__the__class;
extern "C"
	CWIDGETCLASS CMENUITEM_CLASS = &WIN_MENUITEM::__the__class;
extern "C"
	CWIDGETCLASS CMENUBAR_CLASS = &WIN_MENUBAR::__the__class;
extern "C"
	CWIDGETCLASS CMENUPOPUP_CLASS = &WIN_MENUPOPUP::__the__class;
extern "C"
	CWIDGETCLASS CSUBMENU_CLASS = &WIN_SUBMENU::__the__class;
extern "C"
	CWIDGETCLASS CSCROLLEDWINDOW_CLASS = &WIN_SCROLLEDWINDOW::__the__class;
extern "C"
	CWIDGETCLASS CDRAWINGAREA_CLASS = &WIN_DRAWINGAREA::__the__class;

extern "C" 
	CWIDGETCLASS CCORE_CLASS = &WIN_CORE::__the__class;
extern "C" 
	CWIDGETCLASS CCOMPOSITE_CLASS = &WIN_COMPOSITE::__the__class;
extern "C" 
	CWIDGETCLASS CPRIMITIVE_CLASS = &WIN_PRIMITIVE::__the__class;

//---------------------------------------------------------
// C INTERFACE FUNCTIONS
//---------------------------------------------------------

extern "C"
void init_widget_system(const char * resource_filename,HINSTANCE hInstance)
	// initialise the widget system
	// reads the resource file of name resource_filename
	// set the application instance hInstance
{
	WINDOWS_MANAGER::set_application_instance(hInstance);
	resource_names_space.set_table_size(97);
	RESOURCE_CLASS::do_all_names();

	if(resource_filename==NULL)
		return;

	SIMPLE_ERROR errors;
	if(!WINDOWS_MANAGER::read_X_resource_file(resource_filename,errors))
	{
		MessageBox(
			NULL,
			errors.get_details(),
			"Error encountered while reading resource file",
			MB_OK|MB_ICONEXCLAMATION
			);
	}
}

//---------------------------------------------------------

extern "C"
CWIDGETCLASS widget_class_of_class_name(const char * class_name)
	// returns the class of widget for the given class_name
	// if invalid class_name returns NULL
{
	return (CWIDGETCLASS)(RESOURCE_CLASS::get_class(class_name));
}

//---------------------------------------------------------

extern "C"
CWIDGET create_widget_solo(CWIDGETCLASS widget_class)
	// create a widget of class class_name and return it
	// return NULL in case of problems
{
	const RESOURCE_CLASS * the_class = (const RESOURCE_CLASS *)widget_class;
	if (the_class==NULL)
		return NULL;
	if(!RESOURCE_CLASS::is_valid_resource_class(the_class))
		return NULL;
	return (CWIDGET)the_class->create_instance();
}

//---------------------------------------------------------

extern "C"
CWIDGET create_widget
(
	const char *	name,
	CWIDGETCLASS	widget_class,
	CWIDGET			parent,
	ARGLIST			arg_list,
	int				count_of_arg
)
{
	// checks validity of the widget class

	const RESOURCE_CLASS * the_class = (const RESOURCE_CLASS *)widget_class;
	if (the_class==NULL)
		return NULL;
	if(!RESOURCE_CLASS::is_valid_resource_class(the_class))
		return NULL;

	// create the widget and checks it is a widget

	CWIDGET widget = the_class->create_instance();
	if(widget==NULL)
		return NULL;
	if(!is_widget_core(widget))
	{
		delete widget;
		return NULL;
	}

	// set the name and the parent widget

	if(parent!=NULL)
		assert(is_widget_composite(parent),("invalid parent widget parameter"));
	core_of(widget)->set_name_and_parent(name,composite_of(parent));

	// set the defaults values

	if(count_of_arg>0 && arg_list!=NULL)
		core_of(widget)->set_values(arg_list,count_of_arg);

	// retrieves the resource values

	core_of(widget)->scan_resources();

	// end

	return widget;
}

//---------------------------------------------------------

extern "C"
void set_widget_name_and_parent(CWIDGET widget,const char * name,CWIDGET parent)
	// set the name and the parent of the widget
	// then scans the resource defined values
{
	assert(is_valid_core(widget),("invalid widget parameter"));
	if(parent!=NULL)
		assert(is_widget_composite(parent),("invalid parent widget parameter"));
	core_of(widget)->set_name_and_parent(name,composite_of(parent));
	core_of(widget)->scan_resources();
}

//---------------------------------------------------------

extern "C"
void set_widget_values(CWIDGET widget,ARGLIST arglist,int count)
	// set the values to the widget
	// a number of count values are set in arglist
{
	assert(is_valid_core(widget),("invalid widget parameter"));
	assert(arglist!=NULL,("null parameter"));
	core_of(widget)->set_values(arglist,count);
}

//---------------------------------------------------------

extern "C"
void get_widget_values(CWIDGET widget,ARGLIST arglist,int count)
	// get the values from the widget
	// a number of count values are read through arglist
{
	assert(is_valid_core(widget),("invalid widget parameter"));
	assert(arglist!=NULL,("null parameter"));
	core_of(widget)->get_values(arglist,count);
}


//---------------------------------------------------------

extern "C"
void realize_widget(CWIDGET widget)
	// realize widget
{
	assert(is_valid_shell(widget),("invalid widget parameter"));
	core_of(widget)->realize();
}

//---------------------------------------------------------

extern "C"
void destroy_widget(CWIDGET widget)
	// destroy widget
{
	assert(is_valid_core(widget),("invalid widget parameter"));
	core_of(widget)->destroy();
}

//---------------------------------------------------------

extern "C"
void add_widget_callback
	// add a callback function to widget for the reason
	// callback function will be called with data
(
	CWIDGET			widget,
	const char *	reason,
	WIDGET_CALLBACK	callback,
	void *			data
)
{
	assert(is_valid_core(widget),("invalid widget parameter"));
	core_of(widget)->add_callback(
		(CB_REASON)reason,(CB_FUNCTION)callback,(CB_DATA)data);
}

//---------------------------------------------------------

extern "C"
void remove_widget_callback
	// remove a callback function that have been recorded
	// for the widget, the reason and the data
(
	CWIDGET			widget,
	const char *	reason,
	WIDGET_CALLBACK	callback,
	void *			data
)
{
	assert(is_valid_core(widget),("invalid widget parameter"));
	core_of(widget)->remove_callback(
		(CB_REASON)reason,(CB_FUNCTION)callback,(CB_DATA)data);
}

//---------------------------------------------------------

extern "C"
int map_widget(CWIDGET widget)
	// map widget
{
	int	status;
	if (status = is_valid_core(widget))
		core_of(widget)->map();
	else	printf ("invalid widget parameter");

	return status;
}

//---------------------------------------------------------

extern "C"
void unmap_widget(CWIDGET widget)
	// unmap widget
{
	assert(is_valid_core(widget),("invalid widget parameter"));
	core_of(widget)->unmap();
}

//---------------------------------------------------------

extern "C"
void manage_widget(CWIDGET widget)
	// manage widget
{
	assert(is_valid_core(widget),("invalid widget parameter"));
	core_of(widget)->manage();
}

//---------------------------------------------------------

extern "C"
void unmanage_widget(CWIDGET widget)
	// unmanage widget
{
	assert(is_valid_core(widget),("invalid widget parameter"));
	core_of(widget)->unmanage();
}

//---------------------------------------------------------

extern "C"
LONG get_windows_style_of_widget(CWIDGET widget)
	// retrieve the windows style used to create the widget 
	// (intended especially for cervicad-mask-sub-system)
{
	assert(is_valid_core(widget),("invalid widget parameter"));
	return core_of(widget)->get_windows_style();
}

//---------------------------------------------------------

extern "C"
CWIDGETCLASS get_widget_class(CWIDGET widget)
	// retrieve the widget class of the widget
	// return NULL in case of problems
{
	assert(is_valid_core(widget),("invalid widget parameter"));
	return (CWIDGETCLASS)(core_of(widget)->get_resource_class());
}

//---------------------------------------------------------

extern "C"
CWIDGET get_parent_widget(CWIDGET widget)
	// retrieve the parent widget of the widget
	// return NULL in case of problems
{
	assert(is_valid_core(widget),("invalid widget parameter"));
	return (CWIDGET)(core_of(widget)->get_parent_win());
}

//---------------------------------------------------------

extern "C"
bool is_widget_message(MSG * pmsg)
	// return true if the message is a WINDOW message
	// when it returns true the caller should not call neither
	// the TranslateMeassage nor the DispatchMessage calls
{
	return WINDOWS_MANAGER::is_WINDOW_message(pmsg);
}

//---------------------------------------------------------

extern "C"
HWND get_widget_hwnd(CWIDGET widget)
	// returns the HWND of the widget 
{
	assert(is_valid_core(widget),("invalid widget parameter"));
	return core_of(widget)->hwnd();
}
	

//---------------------------------------------------------

