//-------------------------------------------------------------------
// WIN_MENU.cxx
// ------------
// implements a class which defines the menus
// J.E.Bollo & G. Musset, M3G (c) 1998
//-------------------------------------------------------------------
#include "WIN_MENU.hxx"
#include "WINDOWS_MANAGER.hxx"
#include "WIN_SHELL.hxx"
#include "WIN_SUBMENU.hxx"
#include "win_naming.h"
#include "WINDOWS_COMMONS.h"
//-------------------------------------------------------------------
BEGIN_RES_DESC(WIN_MENU,WIN_ROWCOLUMN,NAME_OF_WIN_MENU_CLASS)
END_RES_DESC
//-------------------------------------------------------------------
WIN_MENU::WIN_MENU()
 : WIN_ROWCOLUMN()
 , menu_handle(NULL)
 , mapped(false)
{
	traversal_type = XmNONE;
	traversal_on = false;
}

//-------------------------------------------------------------------
WIN_MENU::~WIN_MENU()
{
	if(menu_handle!=NULL)
		DestroyMenu(menu_handle);
}
//-------------------------------------------------------------------
LONG WIN_MENU::get_windows_style() const
	// retrieve the windows style used to create the widget 
{
	return 0;
}
//-------------------------------------------------------------------
bool WIN_MENU::create_window()
{
	if(menu_handle==NULL)
		menu_handle = create_menu();
	return menu_handle!=NULL;
}

//-------------------------------------------------------------------

void WIN_MENU::resize()
	// set the size 
{}

//---------------------------------------------------------

void WIN_MENU::redraw() const
{}

//-------------------------------------------------------------------

LRESULT WIN_MENU::callback(UINT uMsg,WPARAM wParam,LPARAM lParam)
{
	return 0;
}

//-------------------------------------------------------------------

void WIN_MENU::insert_child (WIN_CORE * child)
	// insert a child in the children array
{
	assert(child!=NULL,("invalid parameter"));
	if(!child->is_kind_of(WIN_MENUITEM::the_resource_class())
		&& !child->is_kind_of(WIN_SUBMENU::the_resource_class()))
		return;
	assert(child->is_kind_of(WIN_MENUITEM::the_resource_class())
		|| child->is_kind_of(WIN_SUBMENU::the_resource_class()), ("invalid child type"));

	WIN_COMPOSITE::insert_child (child);
//	child->map();
}

//-------------------------------------------------------------------

void WIN_MENU::delete_child (WIN_CORE * child)
	// delete a child in the children array
{
//	child->unmap();
	WIN_COMPOSITE::delete_child (child);
}

//-------------------------------------------------------------------

bool WIN_MENU::is_item_visible(const WIN_CORE * item) const
{
	return item->is_mapped() && item->is_managed();
}

//-------------------------------------------------------------------

int WIN_MENU::get_index_of_item(const WIN_CORE * item) const
	// get the combox index of the item
{
	assert(item->is_mapped(),("item should be mapped"));
	int i, index;
	for(index=i=0; i<num_children ; i++)
		if(children[i]==item)
			return index;
		else
		if(is_item_visible(children[i]))
			index++;
	error(("should not occur"));
	return 0;
}

//-------------------------------------------------------------------

WIN_CORE * WIN_MENU::get_item_of_index(int index) const
	// get the item for an index in the combox
{
	int i, idx;
	for(idx=i=0; i<num_children ; i++)
		if(is_item_visible(children[i]))
		{
			if(idx==index)
				return children[i];
			idx++;
		}
	error(("should not occur"));
	return NULL;
}

//-------------------------------------------------------------------

void WIN_MENU::show_menu_item(const WIN_CORE * item) 
{
	if(menu_handle==NULL)
		return;

	int i, index;
	for(index=i=0; i<num_children && children[i]!=item ; i++)
		if(is_item_visible(children[i]))
			index++;

	MENUITEMINFO mii;
	mii.cbSize = sizeof(mii);

	if(item->is_kind_of(WIN_MENUITEM::the_resource_class()))
		((WIN_MENUITEM*)item)->fill_menu_info_struct(mii);
	else
	if(item->is_kind_of(WIN_SUBMENU::the_resource_class()))
		((WIN_SUBMENU*)item)->fill_menu_info_struct(mii);
	else
		error(("it could not append!"));

	InsertMenuItem(menu_handle,index,MF_BYPOSITION,&mii);
}

//-------------------------------------------------------------------

void WIN_MENU::hide_menu_item(const WIN_CORE * item)
{
	if(menu_handle==NULL)
		return;

	int i, index;
	for(index=i=0; i<num_children && children[i]!=item ; i++)
		if(is_item_visible(children[i]))
			index++;

	RemoveMenu(menu_handle,index,MF_BYPOSITION);
}

//-------------------------------------------------------------------

void WIN_MENU::update_map_item(WIN_MENUITEM * item,bool map)
	// change the mapping for item of this
{
	if(item->mapped == map)
		return;
	item->mapped = map;
	if(map)
		show_menu_item(item);
	else
		hide_menu_item(item);
	redraw();
}

//-------------------------------------------------------------------

void WIN_MENU::update_item(const WIN_CORE * item)
	// change the string for item of this
{
	if(is_item_visible(item))
	{
		hide_menu_item(item);
		show_menu_item(item);
	}
}
//-------------------------------------------------------------------

void WIN_MENU::update_manage_item(WIN_MENUITEM * item,bool manage)
	// change the management for item of this
{
	if(item->managed == manage)
		return;
	item->managed = manage;
	if(manage)
		show_menu_item(item);
	else
		hide_menu_item(item);
	redraw();
}

//-------------------------------------------------------------------

void WIN_MENU::update_sensitive_item(WIN_MENUITEM * item,bool sensitive)
	// change the sensitivity for item of this
{
	if(item->sensitive == sensitive)
		return;
	item->sensitive = sensitive;
	if(menu_handle==NULL)
		return;
	EnableMenuItem(menu_handle,item->item_id,
		MF_BYCOMMAND|(sensitive ? MF_ENABLED : MF_GRAYED));
	redraw();
}

//-------------------------------------------------------------------

void WIN_MENU::update_map_submenu(WIN_SUBMENU * submenu,bool map)
	// change the mapping for submenu of this
{
	if(submenu->mapped == map)
		return;
	submenu->mapped = map;
	if(submenu)
		show_menu_item(submenu);
	else
		hide_menu_item(submenu);
	redraw();
}

//-------------------------------------------------------------------

void WIN_MENU::update_manage_submenu(WIN_SUBMENU * submenu,bool manage)
	// change the management for submenu of this
{
	if(submenu->managed == manage)
		return;
	submenu->managed = manage;
	if(manage)
		show_menu_item(submenu);
	else
		hide_menu_item(submenu);
	redraw();
}

//-------------------------------------------------------------------

void WIN_MENU::update_sensitive_submenu(WIN_SUBMENU * submenu,bool sensitive)
	// change the sensitivity for submenu of this
{
}

//-------------------------------------------------------------------

void WIN_MENU::map()
	// set this mapped
{
	mapped = true;
}

//-------------------------------------------------------------------

void WIN_MENU::unmap()
	// set this mapped
{
	mapped = false;
}

//-------------------------------------------------------------------


