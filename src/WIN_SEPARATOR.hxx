//-------------------------------------------------------------------
// WIN_SEPARATOR.hxx
// -----------------
// implements a class which defines the Motif class "XmSeparator"
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __win_separator_hxx
#define __win_separator_hxx
//-------------------------------------------------------------------
#include "macros.h"
#include "WIN_CORE.hxx"
//-------------------------------------------------------------------
class WIN_SEPARATOR : public WIN_CORE
{
	DECLARE_RESOURCE_CLIENT(WIN_SEPARATOR)
		// use the resource mechanism

	DECLARE_WINDOW(WIN_SEPARATOR)
		// use window class mechanism

public:

	int margin;
		// margin on the edges

	int orientation;
		// orientation can be either XmHORIZONTAL or XmVERTICAL

	int separator_type;
		// type of separator can be one of:
		// XmNO_LINE, XmSINGLE_LINE, XmDOUBLE_LINE,
		// XmSINGLE_DASHED_LINE, XmDOUBLE_DASHED_LINE,
		// XmSHADOW_ETCHED_IN, XmSHADOW_ETCHED_OUT

protected:

	~WIN_SEPARATOR();
		// destruction

public:

	WIN_SEPARATOR();
		// construction

	virtual void do_paint(HDC hdc,const RECT *rect) const;
		// paint in response to a WM_PAINT

	virtual bool create_window();
		// create the window associated to the object

	virtual LONG get_windows_style() const;
		// retrieve the windows style used to create the widget 

	virtual XtGeometryResult query_geometry (XtWidgetGeometry *request, 
											 XtWidgetGeometry *prefered);
		// The method query_geometry called when the parent  
		// wants to know the widget's prefered size

	// --------------- Attributs management methods -------------------

	virtual int internal_get_values
		// function used internally to get attribut values 
		// should not be called directly (call get_values)
		// arglist is the array of the values to get
		// count is the count of values to be put into arglist
		// got is an array that flag if an attribut have been put
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs put
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		) const;

	virtual int internal_set_values
		// function used internally to set attribut values 
		// should not be called directly (call set_values)
		// arglist is the array of the values to be set,
		// count is the count of values in arglist
		// got is an array that flag if an attribut have been set
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs set
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		);
};
//-------------------------------------------------------------------
#endif
