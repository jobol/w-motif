//-------------------------------------------------------------------
// WIN_TEXT.cxx
// ---------------
// implements the Motif class "XmLabel"
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#include "WIN_TEXT.hxx"
#include "WINDOWS_MANAGER.hxx"
#include "WINDOWS_COMMONS.h"
#include "win_naming.h"
//-------------------------------------------------------------------
BEGIN_RES_DESC(WIN_TEXT,WIN_PRIMITIVE,NAME_OF_WIN_TEXT_CLASS)
	RES_DESC_ATR(XmNvalue,					TEXT,		WIN_TEXT, value),
	RES_DESC_ATR(XmNautoShowCursorPosition,	BOOLEA,		WIN_TEXT, auto_show_cursor_position),
	RES_DESC_ATR(XmNeditable,				BOOLEA,		WIN_TEXT, editable),
	RES_DESC_ATR(XmNeditMode,				ENUM,		WIN_TEXT, edit_mode),
	RES_DESC_ATR(XmNtopCharacter,			INTEGER,	WIN_TEXT, top_character),
	RES_DESC_ATR(XmNcolumns,				INTEGER,	WIN_TEXT, columns),
	RES_DESC_ATR(XmNrows,					INTEGER,	WIN_TEXT, rows),
	RES_DESC_ATR(XmNscrollHorizontal,		BOOLEA,		WIN_TEXT, scroll_horizontal),
	RES_DESC_ATR(XmNscrollVertical,			BOOLEA,		WIN_TEXT, scroll_vertical),
	RES_DESC_ATR(XmNscrollLeftSide,			BOOLEA,		WIN_TEXT, scroll_left_side),
	RES_DESC_ATR(XmNscrollTopSide,			BOOLEA,		WIN_TEXT, scroll_top_side),
END_RES_DESC
//-------------------------------------------------------------------
IMPLEMENT_WINDOW(WIN_TEXT)
//-------------------------------------------------------------------
ATOM WIN_TEXT::register_window_class() const
{
	WNDCLASSEX wce;
	WINDOWS_MANAGER::get_window_class_info("EDIT",&wce);
	return WINDOWS_MANAGER::register_window_class(
					(const char*)get_class_name(),wce.style);
}
//-------------------------------------------------------------------
WIN_TEXT::WIN_TEXT()
  : WIN_PRIMITIVE()
  , auto_show_cursor_position(true)
  , edit_mode(XmSINGLE_LINE_EDIT)
  , editable(true)
  , top_character(0)
  , columns(0)
  , rows(0)
  , scroll_horizontal(false)
  , scroll_vertical(false)
  , scroll_left_side(false)
  , scroll_top_side(false)
{
	value = strdup("");
	traversal_type = XmTAB_GROUP;
}
//-------------------------------------------------------------------
WIN_TEXT::~WIN_TEXT()
{
	if(value!=NULL) free(value);
}
//-------------------------------------------------------------------
LONG WIN_TEXT::get_windows_style() const
	// retrieve the windows style used to create the widget 
{
	DWORD style = WS_CHILD | WS_VISIBLE | ES_LEFT | ES_AUTOHSCROLL;
	if (!editable) style |= ES_READONLY ;
	if (edit_mode == XmMULTI_LINE_EDIT)
		style |= ES_MULTILINE | ES_WANTRETURN | ES_AUTOVSCROLL ;
	if (scroll_horizontal) style |= WS_HSCROLL;
	if (scroll_vertical) style |= WS_VSCROLL;
	return style;
}
//-------------------------------------------------------------------
bool WIN_TEXT::create_control()
{
	char * text = add_carriage_return(value);

	hwndCtl = CreateWindowEx(
#ifdef WITH_BORDERS
				WS_EX_CLIENTEDGE,
#else
				0,
#endif
				"EDIT",
				text,
				get_windows_style(),
				0, 0,width,height,
				this->hwnd(),
				(HMENU)1,
				WINDOWS_MANAGER::get_application_instance(),
				NULL);

	free(text);

	return hwndCtl!=NULL;
}

//-------------------------------------------------------------------
LRESULT WIN_TEXT::callback(UINT uMsg,WPARAM wParam,LPARAM lParam)
{
	switch(uMsg)
	{
	case WM_CTLCOLOREDIT:
		{
			HDC hdc = (HDC)wParam;
			SetBkColor(hdc, color_of_brush(background));
			SetTextColor(hdc, foreground);
			return (LRESULT)background;
		}
	}
						
	return WIN_PRIMITIVE::callback(uMsg,wParam,lParam);
}
//-------------------------------------------------------------------

void WIN_TEXT::add_text(const char* txt)
	// adds txt at the position of the cursor or at the end of the selection
{
	int pos;
	if(is_realized())
	{
		int debut_sel, fin_sel;
		char * text_crlf = add_carriage_return(value);
		
		SendMessage(hwndCtl,EM_GETSEL,(WPARAM)&debut_sel, (LPARAM)&fin_sel);
		// if there is a selected text or just a cursor : adds at the end 
		// of the selection and deselects, or adds at the cursor position
		// -----------------------------------------------------------
		SendMessage(hwndCtl,EM_SETSEL,fin_sel,fin_sel);
		SendMessage(hwndCtl,EM_REPLACESEL,FALSE,(LPARAM)text_crlf);
		free(text_crlf);

		pos = translate_char_pos_from_crlf(value,fin_sel);
	}
	else
		pos = strlen(value);

	insert_text_at (&value, pos, txt);
}

//-------------------------------------------------------------------

void WIN_TEXT::append_text(const char* txt)
	// adds txt at the end of the text
{
	if(is_realized())
	{
		char * text_crlf = add_carriage_return(txt);
		int length = SendMessage(hwndCtl,WM_GETTEXTLENGTH,0,0);
		SendMessage(hwndCtl,EM_SETSEL,length,length);		
		SendMessage(hwndCtl,EM_REPLACESEL,FALSE,(LPARAM)text_crlf);
		free(text_crlf);
	}
	insert_text_at (&value, strlen(value), txt);
}

//-------------------------------------------------------------------

void WIN_TEXT::set_text(const char* txt)
	// replaces all the text by "txt"
{
	safe_assign_copy_of_text (&value, txt);
	if(is_realized())
	{
		char * text_crlf = add_carriage_return(value);
		SendMessage(hwndCtl,WM_SETTEXT,0,(LPARAM)text_crlf);
		free(text_crlf);

		update_geometry_request(CWWidth|CWHeight);
	}
}

//-------------------------------------------------------------------

char* WIN_TEXT::get_text() const
	// gets all the text
{
	char*	text;

	if(is_realized())
	{
		int length = 1+SendMessage(hwndCtl,WM_GETTEXTLENGTH,0,0);
		text = (char*)malloc(length);
		if (text != NULL)
		{
//			((volatile WIN_TEXT*)this)->value = text;
			SendMessage(hwndCtl,WM_GETTEXT,length,(LPARAM)text);
			remove_carriage_return_inside(text);
		}
	}
	else
		text = NULL;
	return text;
}

//-------------------------------------------------------------------

char* WIN_TEXT::get_selected_text() const
	// gets the selected text
{
	if(!is_realized())
		return safe_copy_of_text(value);

	int debut_sel, fin_sel;
	SendMessage(hwndCtl,EM_GETSEL,(WPARAM)&debut_sel, (LPARAM)&fin_sel);
	int length = SendMessage(hwndCtl,WM_GETTEXTLENGTH,0,0);
	char* text_crlf = (char*)malloc(length+1);
	assert(text_crlf!=NULL,("memory depletion"));
	SendMessage(hwndCtl,WM_GETTEXT,length+1,(LPARAM)text_crlf);
	text_crlf[fin_sel] = 0;
	char * text = remove_carriage_return(text_crlf+debut_sel);
	free(text_crlf);

	return text;
}

//-------------------------------------------------------------------

int WIN_TEXT::get_top_character() const
	// returns the position of the first visible character of text
	// as the number of characters from the beginning of the text
	// where the first position character is 0
{
	if (is_realized())
	{
		int first_line_or_char = SendMessage(hwndCtl,EM_GETFIRSTVISIBLELINE,0, 0);
		if (edit_mode == XmMULTI_LINE_EDIT)
		{	//in multiple-line edit mode, first_line_or_char is the index of
			//the first visible line; whereas in single-line edit mode, it is
			//the position of the first character
			first_line_or_char = SendMessage(hwndCtl,EM_LINEINDEX,first_line_or_char, 0);
		}
		((volatile WIN_TEXT*)this)->top_character = translate_char_pos_from_crlf(value, first_line_or_char);
	}
	return top_character;
}

//-------------------------------------------------------------------

void WIN_TEXT::set_top_character(int char_position)
{
	top_character = char_position;
	if (is_realized())
	{
		int pos = translate_char_pos_to_crlf(value,top_character);

		if (edit_mode == XmSINGLE_LINE_EDIT)
		{		// --- single-line 
			// --- NOTE : Pas r�ussi � le g�rer dans le cas single-line :
			//Pb : EM_LINESCROLL ne marche qu'en multi-lignes
			//int current_top_char = get_top_character();
			//SendMessage(hwndCtl,EM_LINESCROLL,char_position - current_top_char,0);

			//Pb : EM_SCROLLCARET decale forc�ment de 6 caract�res
			//SendMessage(hwndCtl,EM_SETSEL,char_position + 20,char_position + 20);
			//SendMessage(hwndCtl,EM_SCROLLCARET,0,0);
		}
		else
		{		// --- multi-line : decaler verticalement mais pas horizontalement
			int new_top_line = SendMessage(hwndCtl,EM_LINEFROMCHAR,pos,0);
			int current_top_line = SendMessage(hwndCtl,EM_GETFIRSTVISIBLELINE,0, 0);
			SendMessage(hwndCtl,EM_LINESCROLL,0,new_top_line - current_top_line );	
				// positive value : going up ; negative value : going down
		}
	}
}

//-------------------------------------------------------------------

void WIN_TEXT::set_editable(bool state)
		// sets the state of attribute editable
{
	editable = state;
	if (is_realized())
		SendMessage(hwndCtl, EM_SETREADONLY, !state, 0);
}

//-------------------------------------------------------------------

void WIN_TEXT::set_edit_mode(int val)
		// sets attribute edit_mode
{
	edit_mode = val==XmMULTI_LINE_EDIT ? XmMULTI_LINE_EDIT : XmSINGLE_LINE_EDIT;
}

//-------------------------------------------------------------------

int WIN_TEXT::get_insertion_position() const
	// returns the position of insertion cursor
	// NOTE : if there is a selection, the highest position is returned (en Motif : 
	// si on a selectionn� du texte de droite � gauche, le curseur est en position gauche;
	// ne voyons pas comment faire ici)
{
	int pos;
	if (is_realized())
	{
		int debut_sel, fin_sel = 0;
		SendMessage(hwndCtl,EM_GETSEL,(WPARAM)&debut_sel, (LPARAM)&fin_sel);
		pos = translate_char_pos_from_crlf(value, fin_sel);
	}
	else
		pos = strlen(value);
	return pos;
}

//-------------------------------------------------------------------

void WIN_TEXT::set_rows(int val)
	// sets the rows number of the text widget
{ 
	rows = val;
	update_geometry_request(CWHeight);
}
//-------------------------------------------------------------------

void WIN_TEXT::set_columns(int val)
	// sets the columns number of the text widget
{ 
	columns = val;
	update_geometry_request(CWWidth);
}

//-------------------------------------------------------------------

void WIN_TEXT::get_prefered_size(SIZE &result)
	// computes the prefered size of this (from rows & columns) in result
{
	window_multiline_text_extend (hwndCtl, "M", font_list, &result);

	if(rows!=0)
		result.cy *= rows;
#ifdef WITH_BORDERS
	result.cy += 2*GetSystemMetrics(SM_CYEDGE);
#endif
	if (scroll_horizontal == true) 
		result.cy += GetSystemMetrics(SM_CYHSCROLL);

	if(columns==0)
		result.cx *= 20;
	else
		result.cx *= columns;

#ifdef WITH_BORDERS
	result.cx += 2*GetSystemMetrics(SM_CXEDGE);
#endif
	if (scroll_vertical == true) 
		result.cx += GetSystemMetrics(SM_CXHSCROLL);
}

//-------------------------------------------------------------------

XtGeometryResult WIN_TEXT::query_geometry (XtWidgetGeometry *request, 
										 XtWidgetGeometry *prefered)
	// The method query_geometry called when the parent  
	// wants to know the widget's prefered size
{
	SIZE size;
	XtWidgetGeometry pref;
	pref.request_mode = 0;

	get_prefered_size(size);

	pref.request_mode = CWHeight|CWWidth;
	pref.height = size.cy;
	pref.width = size.cx;

	return complete_query_geometry(request,prefered,&pref);
}

//-------------------------------------------------------------------


int WIN_TEXT::internal_get_values
	// function used internally to get attribut values 
	// should not be called directly (call get_values)
	// arglist is the array of the values to get
	// count is the count of values to be put into arglist
	// got is an array that flag if an attribut have been put
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs put
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) const
{
	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.

		if(equality(attribut,XmNvalue))
			arglist[index].value.pvalue->psztext = get_text();
		else
		if(equality(attribut,XmNautoShowCursorPosition))
			arglist[index].value.pvalue->boolean = auto_show_cursor_position;
		else
		if(equality(attribut,XmNeditable))
			arglist[index].value.pvalue->boolean = editable;
		else
		if(equality(attribut,XmNeditMode))
			arglist[index].value.pvalue->integer = edit_mode;
		else
		if(equality(attribut,XmNtopCharacter))
			arglist[index].value.pvalue->integer = get_top_character();
		else
		if(equality(attribut,XmNcolumns))
			arglist[index].value.pvalue->integer = columns;
		else
		if(equality(attribut,XmNrows))
			arglist[index].value.pvalue->integer = rows;
		else
		if(equality(attribut,XmNscrollHorizontal))
			arglist[index].value.pvalue->boolean = scroll_horizontal;
		else
		if(equality(attribut,XmNscrollVertical))
			arglist[index].value.pvalue->boolean = scroll_vertical;
		else
		if(equality(attribut,XmNscrollLeftSide))
			arglist[index].value.pvalue->boolean = scroll_left_side;
		else
		if(equality(attribut,XmNscrollTopSide))
			arglist[index].value.pvalue->boolean = scroll_top_side;
		else
			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
	}

	// return total count of found attributs (including ancestors)
	return found_count  + WIN_PRIMITIVE::internal_get_values(arglist,count,got,equality);
}


//-------------------------------------------------------------------

int WIN_TEXT::internal_set_values
	// function used internally to set attribut values 
	// should not be called directly (call set_values)
	// arglist is the array of the values to be set,
	// count is the count of values in arglist
	// got is an array that flag if an attribut have been set
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs set
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) 
{
	// empty geometry request
	bool redraw_needed = false;
/*
	XtWidgetGeometry request;
	request.request_mode = 0;
*/

	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.

		if(equality(attribut,XmNvalue))
			set_text(arglist[index].value.psztext);
		else
		if(equality(attribut,XmNautoShowCursorPosition))
			auto_show_cursor_position = arglist[index].value.boolean;
		else
		if(equality(attribut,XmNeditable))
		{
			set_editable(arglist[index].value.boolean);
			redraw_needed = true;
		}
		else
		if(equality(attribut,XmNeditMode))
			set_edit_mode(arglist[index].value.integer);
		else
		if(equality(attribut,XmNtopCharacter))
			set_top_character(arglist[index].value.integer);
		else
		if(equality(attribut,XmNcolumns))
			set_columns(arglist[index].value.integer);
		else
		if(equality(attribut,XmNrows))
			set_rows(arglist[index].value.integer);
		else
		if(equality(attribut,XmNscrollHorizontal))
		{
			scroll_horizontal = arglist[index].value.boolean;
			redraw_needed = true;
		}
		else
		if(equality(attribut,XmNscrollVertical))
		{
			scroll_vertical = arglist[index].value.boolean;
			redraw_needed = true;
		}
		else
		if(equality(attribut,XmNscrollLeftSide))
		{
			scroll_left_side = arglist[index].value.boolean;
			redraw_needed = true;
		}
		else
		if(equality(attribut,XmNscrollTopSide))
		{
			scroll_top_side = arglist[index].value.boolean;
			redraw_needed = true;
		}
		else
			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
	}

/*
	// make the geometry request if necessary
	if(request.request_mode!=0)
		geometry_request(&request);
*/
	if(redraw_needed)
		redraw();

	// return total count of found attributs (including ancestors)
	return found_count  + WIN_PRIMITIVE::internal_set_values(arglist,count,got,equality);
}

//-------------------------------------------------------------------
#endif // VERSION_OF_WINDOW != 1
