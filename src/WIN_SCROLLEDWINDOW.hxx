//-------------------------------------------------------------------
// WIN_SCROLLEDWINDOW.hxx
// ---------------
// implements the "XmScrolledWindow" widget
// J.E.Bollo & G. Musset, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __win_scrolledwindow_hxx
#define __win_scrolledwindow_hxx
//-------------------------------------------------------------------
#include "macros.h"
#include "WIN_COMPOSITE.hxx"
//-------------------------------------------------------------------
class WIN_SCROLLEDWINDOW : public WIN_COMPOSITE
{
	DECLARE_RESOURCE_CLIENT(WIN_SCROLLEDWINDOW)
		// use the resource mechanism

	DECLARE_WINDOW(WIN_SCROLLEDWINDOW)
		// use window class mechanism

private:
		
	int hscroll_pos;
		// current horizontal scroll position

	int hscroll_range;
		// current horizontal scroll range

	int vscroll_pos;
		// current vertical scroll position

	int vscroll_range;
		// current vertical scroll range

	int visible_width;
		// the width less the VSCROLL width when visible

	int visible_height;
		// the height less the HSCROLL height when visible
	
protected:

	~WIN_SCROLLEDWINDOW();
		// destruction

public:

	WIN_SCROLLEDWINDOW();
		// construction

	LRESULT callback(UINT uMsg,WPARAM wParam,LPARAM lParam);
		// callback 

	virtual bool create_window();
		// create the window associated to the object

	virtual LONG get_windows_style() const;
		// retrieve the windows style used to create the widget 

	void set_horizontal_position(int pos);
		// set a new value pos to hscroll_pos

	void set_vertical_position(int pos);
		// set a new value pos to vscroll_pos

	virtual void change_managed();
		// The method change_manager called once during the realize phase and then
		// every time that one of its children becomes managed or unmanaged
		
	virtual XtGeometryResult geometry_manager(WIN_CORE *child,
								  XtWidgetGeometry *request,
								  XtWidgetGeometry *geometry_return); 
		// The method geometry_manager called every time  
		// one of its children requests a new size
		
		
	virtual XtGeometryResult query_geometry (XtWidgetGeometry *request, 
											 XtWidgetGeometry *prefered);
		// The method query_geometry called when the parent  
		// wants to know the widget's prefered size

	virtual void resize();
		// The method resize is called every time a widget changes size


	// --------------- Attributs management methods -------------------

	virtual int internal_get_values
		// function used internally to get attribut values 
		// should not be called directly (call get_values)
		// arglist is the array of the values to get
		// count is the count of values to be put into arglist
		// got is an array that flag if an attribut have been put
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs put
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		) const;

	virtual int internal_set_values
		// function used internally to set attribut values 
		// should not be called directly (call set_values)
		// arglist is the array of the values to be set,
		// count is the count of values in arglist
		// got is an array that flag if an attribut have been set
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs set
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		);
};
//-------------------------------------------------------------------
#endif
