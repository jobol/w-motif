//---------------------------------------------------------
// RESOURCE_NAMED_CLIENT.hxx
// -------------------
// polymorphic resource client class
// J.E.Bollo, M3G (c) 1998
//---------------------------------------------------------
#ifndef __resource_named_client_hxx
#define __resource_named_client_hxx
//---------------------------------------------------------
#include "macros.h"
#include "RESOURCE_NAME.hxx"
#include "RESOURCE_NAMES_SPACE.hxx"
#include "RESOURCE_CLIENT.hxx"
#include "RESOURCE_DATABASE.hxx"
//---------------------------------------------------------
class RESOURCE_NAMED_CLIENT : public RESOURCE_CLIENT
	// this is the base class for classes that implements
	// the resource mechanismes
{
private:

	NAME name;
		// my name

public:

	RESOURCE_NAMED_CLIENT() : name() {}
		// constructor

	~RESOURCE_NAMED_CLIENT()
		// destructor
		{
			if(name.is_valid())
				resource_names_space.remove(name);
		}

	NAME get_name() const
		// return the name
		{ return name; }

	void set_name(const char *the_name)
		// sets name to the_name
		{
			if(name.is_valid())
				resource_names_space.remove(name);
			name = resource_names_space.add(the_name);
		}

	virtual RESOURCE_NAMED_CLIENT * get_parent_resource() const 
		// get the parent
		{ return NULL; }

	void make_resource_name(RESOURCE_NAME &resource_name);
		// make the resource_name using name, class name and parent hierarchy

	virtual void scan_resource_values(const RESOURCE_DATABASE & database);
		// put values from the resources database to this
		// default builds a RESOURCE_NAME
		//   and then builds a RESOURCE_SCANNER and scans with the RESOURCE_NAME
		//   and then calls set_resource_values on the RESOURCE_SCANNER
};
//---------------------------------------------------------
#endif
