//---------------------------------------------------------
// macros.h
// --------
// some macros for debugging and configuration
// that file is used only for the WINDOW (FENETRE) project of Cervicad
// J.E.Bollo, M3G (c) 1998
//---------------------------------------------------------
#ifndef __macros_h
#define __macros_h
//---------------------------------------------------------
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
//---------------------------------------------------------
#ifndef _WIN32
#  error
#endif
//---------------------------------------------------------
#include "WIDGET_TYPES.h"
//---------------------------------------------------------
#ifdef __cplusplus
	extern "C" int quit(int code);
	extern "C" int mon_printf(const char *,...);
#else
	extern int quit(int code);
	extern int mon_printf(const char *,...);
#endif
#ifdef printf
#   undef printf
#endif
#define printf mon_printf
//---------------------------------------------------------
#define message(x)  (printf x)
#define whereami	(message(("%s,%d: ",__FILE__,__LINE__)))
#define trace(x)    (whereami,message(x),message(("\n")))
#define error(x)    (trace(x),quit(1))
#ifdef NDEBUG
#	define assert(x,y)
#	define verbose(x)  
#else
#	define assert(x,y) ((x) || (error(y)))
#	define verbose(x)  (message(x))
#endif
//---------------------------------------------------------
// project informations and configuration
//---------------------------------------------------------
#define	VERSION_OF_WINDOW		2	// see file VERSION_OF_WINDOW.txt for explanations
#define	WITH_BORDERS				// for border in some widgets
#define CR_AND_LF					// should translation occurs
//---------------------------------------------------------
#endif

