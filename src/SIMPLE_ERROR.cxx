//---------------------------------------------------------
// SIMPLE_ERROR.cxx
// -----------------
// Just a simple error reporting object
// J.E.Bollo, M3G (c) 1998
//---------------------------------------------------------
#include "SIMPLE_ERROR.hxx"
//---------------------------------------------------------
SIMPLE_ERROR::SIMPLE_ERROR()
	// construction
  : nr_of_errors(0)
  , nr_of_warning(0)
  , length_of_text(0)
  , errors_text(NULL)
  , current_context(NULL)
{}
//---------------------------------------------------------
SIMPLE_ERROR::~SIMPLE_ERROR()
	// destruction
{
	reset();
}
//---------------------------------------------------------
void SIMPLE_ERROR::reset()
	// reset to nothing: clear buffers and raz counts
{
	if(errors_text!=NULL) free(errors_text); 
	if(current_context!=NULL) free(current_context); 
	errors_text = NULL;
	current_context = NULL;
	nr_of_errors = 0;
	nr_of_warning = 0;
	length_of_text = 0;
}
//---------------------------------------------------------
int SIMPLE_ERROR::get_error_count() const
	// return the count of errors
{
	return nr_of_errors;
}
//---------------------------------------------------------
int SIMPLE_ERROR::get_warning_count() const
	// return the count of warning
{
	return nr_of_warning;
}
//---------------------------------------------------------
const char * SIMPLE_ERROR::get_details() const
	// returns the text of the warnings and errors
{
	return errors_text;
}
//---------------------------------------------------------
void SIMPLE_ERROR::set_context(const char * context)
	// set the context of the next errors
{
	assert(context!=NULL,("null pointer"));
	int length = 1+strlen(context);
	current_context = (char *)realloc(current_context,length);
	assert(current_context!=NULL,("memory depletion"));
	memcpy(current_context,context,length);
}
//---------------------------------------------------------
const char * SIMPLE_ERROR::get_context() const
	// returns the current context
{
	return current_context;
}
//---------------------------------------------------------
void SIMPLE_ERROR::vprint(const char *text,va_list valis)
	// main function to write in the internal errors buffer 
{
	// first print in a static bloc

	char bloc[2048];
	int length = vsprintf(bloc,text,valis) + 1;
	assert(length<=2048,("memory crash! emergency exit requested"));

	// expand the internal error buffer

	char * buffer = (char*)realloc(errors_text,length_of_text+length);
	if(buffer!=NULL)
	{
		// update if enought memory is available

		errors_text = buffer;
		memcpy(errors_text+length_of_text,bloc,length);
		length_of_text += length-1;
	}
}
//---------------------------------------------------------
void SIMPLE_ERROR::print(const char *text,...)
	// simple print without error count
{
	va_list valiz;
	va_start(valiz,text);
	vprint(text,valiz);
}
//---------------------------------------------------------
void SIMPLE_ERROR::set_file_name(const char * file_name)
	// this function set the context to the given file_name
{
	assert(file_name!=NULL,("null pointer"));
	int length = strlen(file_name);
	char * buffer = new char[length+20];
	assert(buffer!=NULL,("memory depletion"));
	sprintf(buffer,"file '%s'",file_name);
	set_context(buffer);
	delete [] buffer;
}
//---------------------------------------------------------
void SIMPLE_ERROR::add_error(int line,int column,const char *label,...)
	// add a new error detected at line and column 
	// explanation of error is given by label and ...
{
	if(current_context!=NULL) print("%s, ",current_context);
	print("line %d, column %d: ERROR ",line,column);
	va_list valiz;
	va_start(valiz,label);
	vprint(label,valiz);
	print("\n");
	nr_of_errors++;
}
//---------------------------------------------------------
void SIMPLE_ERROR::add_warning(int line,int column,const char *label,...)
	// add a new warning detected at line and column 
	// explanation of warning is given by label and ...
{
	if(current_context!=NULL) print("%s, ",current_context);
	print("line %d, column %d: warning ",line,column);
	va_list valiz;
	va_start(valiz,label);
	vprint(label,valiz);
	print("\n");
	nr_of_warning++;
}
//---------------------------------------------------------
void SIMPLE_ERROR::add_error(const char *label,...)
	// add a new error with explanation given by label and ...
{
	if(current_context!=NULL) print("%s: ",current_context);
	va_list valiz;
	va_start(valiz,label);
	vprint(label,valiz);
	print("\n");
	nr_of_errors++;
}
//---------------------------------------------------------
void SIMPLE_ERROR::add_warning(const char *label,...)
	// add a new warning with explanation given by label and ...
{
	if(current_context!=NULL) print("%s, ",current_context);
	va_list valiz;
	va_start(valiz,label);
	vprint(label,valiz);
	print("\n");
	nr_of_warning++;
}
//---------------------------------------------------------
