//-------------------------------------------------------------------
// WIN_SCROLLEDWINDOW.cxx
// ---------------
// implements the "XmScrolledWindow" class
// J.E.Bollo & G. Musset, M3G (c) 1998
//-------------------------------------------------------------------
#include "WIN_SCROLLEDWINDOW.hxx"
#include "WINDOWS_MANAGER.hxx"
#include "win_naming.h"
//-------------------------------------------------------------------
const int unitary_increment = 5;
//-------------------------------------------------------------------
BEGIN_RES_DESC(WIN_SCROLLEDWINDOW,WIN_COMPOSITE,NAME_OF_WIN_SCROLLEDWINDOW_CLASS)
END_RES_DESC
//-------------------------------------------------------------------
IMPLEMENT_WINDOW(WIN_SCROLLEDWINDOW)
//-------------------------------------------------------------------
ATOM WIN_SCROLLEDWINDOW::register_window_class() const
{
	return WINDOWS_MANAGER::register_window_class(
					(const char*)get_class_name(),0,
					LoadCursor(NULL,IDC_ARROW));
}
//-------------------------------------------------------------------
WIN_SCROLLEDWINDOW::WIN_SCROLLEDWINDOW()
	: WIN_COMPOSITE()
	, hscroll_pos(0)
	, vscroll_pos(0)
	, hscroll_range(0)
	, vscroll_range(0)
	, visible_width(0)
	, visible_height(0)
{
}
//-------------------------------------------------------------------
WIN_SCROLLEDWINDOW::~WIN_SCROLLEDWINDOW()
{
}
//-------------------------------------------------------------------
LONG WIN_SCROLLEDWINDOW::get_windows_style() const
	// retrieve the windows style used to create the widget 
{
	return WS_CHILD|WS_CLIPSIBLINGS|WS_VISIBLE|WS_CLIPCHILDREN
			|WS_HSCROLL|WS_VSCROLL;
}
//-------------------------------------------------------------------
bool WIN_SCROLLEDWINDOW::create_window()
{
	return WIN_CORE::create_window(get_windows_style());
}

//-------------------------------------------------------------------

LRESULT WIN_SCROLLEDWINDOW::callback(UINT uMsg,WPARAM wParam,LPARAM lParam)
{
	switch (uMsg) 
	{ 
	case WM_HSCROLL:     
		if (num_children != 0) 
		{
			assert (children[0] != NULL, ("child window not valid"));
			int new_pos;    // new position         
			
			switch (LOWORD(wParam))         
			{ 
				// User clicked the shaft left of the scroll box.  
				case SB_PAGEUP:                 
					new_pos = hscroll_pos - visible_width;
					break;
					
				// User clicked the shaft right of the scroll box.  
				case SB_PAGEDOWN:                 
					new_pos = hscroll_pos + visible_width;
					break;
					
				// User clicked the left arrow.  
				case SB_LINEUP:                 
					new_pos = hscroll_pos - unitary_increment; 
					break;
					
				// User clicked the right arrow.  
				case SB_LINEDOWN:                 
					new_pos = hscroll_pos + unitary_increment; 
					break;              
				// User dragged the scroll box.  
				case SB_THUMBTRACK:
					new_pos = HIWORD(wParam); 
					break;
					
				default: 
					new_pos = hscroll_pos;
			}
			set_horizontal_position(new_pos);
		}         
		return 0;
		
	case WM_VSCROLL:     
		if (num_children != 0) 
		{         
			assert (children[0] != NULL, ("child window not valid"));
			int new_pos;    // new position          

			switch (LOWORD(wParam)) 
			{             
				// User clicked the shaft above the scroll box.  
				case SB_PAGEUP:                 
					new_pos = vscroll_pos - visible_height;
					break;
					
				// User clicked the shaft below the scroll box.  
				case SB_PAGEDOWN:                 
					new_pos = vscroll_pos + visible_height;
					break;
					
				// User clicked the top arrow.  
				case SB_LINEUP:                 
					new_pos = vscroll_pos - unitary_increment; 
					break;
					
				// User clicked the bottom arrow.  
				case SB_LINEDOWN:                 
					new_pos = vscroll_pos + unitary_increment; 
					break;
					
				// User dragged the scroll box.  
				case SB_THUMBTRACK:                 
					new_pos = HIWORD(wParam); 
					break;
					
				default: 
					new_pos = vscroll_pos;         
			}  
			set_vertical_position(new_pos);
		}         
		return 0;

#ifdef WM_MOUSEWHEEL
	case WM_MOUSEWHEEL:
		if (num_children != 0) 
		{         
			assert (children[0] != NULL, ("child window not valid"));
			set_vertical_position(vscroll_pos+((short)HIWORD(wParam)));
		}         
		return 0;
#endif
	}
	return WIN_COMPOSITE::callback(uMsg, wParam, lParam);
}

//-------------------------------------------------------------------
void WIN_SCROLLEDWINDOW::change_managed()
	// The method change_manager called once during the realize phase and then
	// every time that one of its children becomes managed or unmanaged
{
	if (num_children == 0) return;
	assert (children[0] != NULL, ("child window not valid"));

	if (children[0]->is_managed())
	{
		children[0]->update_geometry_request(CWWidth | CWHeight);
		resize();
	}
}
//-------------------------------------------------------------------
XtGeometryResult WIN_SCROLLEDWINDOW::geometry_manager
(
	WIN_CORE			*child,
	XtWidgetGeometry	*request,
	XtWidgetGeometry	*geometry_return
) 
	// The method geometry_manager called every time  
	// one of its children requests a new size or position
{
	if (child != NULL && request != NULL) 
	{
		request->request_mode &= (CWWidth | CWHeight);
			// do not accept position change but only size change
		child->set_geometry(request);
		if (geometry_return != NULL )
			*geometry_return = *request;
		// The method resize is called to adjust the
		// parameters of the scroll bar according to the
		// new child size
		resize();
		return XtGeometryDone;
	}
	return XtGeometryYes;
}

//-------------------------------------------------------------------

XtGeometryResult WIN_SCROLLEDWINDOW::query_geometry
(
	XtWidgetGeometry *request, 
	XtWidgetGeometry *prefered
)
		// The method query_geometry called when the parent  
		// wants to know the widget's prefered size
{
	XtWidgetGeometry pref;
	pref.width = width;
	pref.height = height;
	pref.request_mode = CWWidth | CWHeight;

	if (num_children!=0 && children[0]->is_managed())
	{
		XtWidgetGeometry req = pref;
		children[0]->query_geometry(&req,&pref);
	}

	return complete_query_geometry(request,prefered,&pref);
}

//-------------------------------------------------------------------


int WIN_SCROLLEDWINDOW::internal_get_values
	// function used internally to get attribut values 
	// should not be called directly (call get_values)
	// arglist is the array of the values to get
	// count is the count of values to be put into arglist
	// got is an array that flag if an attribut have been put
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs put
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) const
{
	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.

			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
	}

	// return total count of found attributs (including ancestors)
	return found_count + WIN_COMPOSITE::internal_get_values(arglist,count,got,equality);
}

//-------------------------------------------------------------------

int WIN_SCROLLEDWINDOW::internal_set_values
	// function used internally to set attribut values 
	// should not be called directly (call set_values)
	// arglist is the array of the values to be set,
	// count is the count of values in arglist
	// got is an array that flag if an attribut have been set
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs set
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) 
{
	// empty geometry request
	bool redraw_needed = false;
/*
	XtWidgetGeometry request;
	request.request_mode = 0;
*/

	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.
			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
	}

/*
	// make the geometry request if necessary
	if(request.request_mode!=0)
		geometry_request(&request);
*/
	if(redraw_needed)
		redraw();

	// return total count of found attributs (including ancestors)
	return found_count  + WIN_COMPOSITE::internal_set_values(arglist,count,got,equality);
}

//-------------------------------------------------------------------

void WIN_SCROLLEDWINDOW::set_horizontal_position(int pos)
	// set a new value pos to hscroll_pos
{
	// New position must be from 0 to hscroll_range
	if(pos<0)
		pos = 0;
	else
	if(pos>hscroll_range)
		pos = hscroll_range;
	
	// If the current position does not change, do not scroll. 
	if (pos == hscroll_pos)
		return;
	
	// Reset the current scroll position.  
	hscroll_pos = pos;
	
	// Set the scroll bar.          
	SCROLLINFO si;
	si.cbSize = sizeof(si); 
	si.fMask  = SIF_POS;         
	si.nPos   = hscroll_pos; 
	SetScrollInfo(hwnd(), SB_HORZ, &si, TRUE);      
	
	// Scroll the window. 
	if (num_children == 0) 
		return;
	children[0]->set_geometry(-pos,children[0]->y,
					children[0]->width,children[0]->height); 
}

//-------------------------------------------------------------------

void WIN_SCROLLEDWINDOW::set_vertical_position(int pos)
	// set a new value pos to vscroll_pos
{
	// New position must be from 0 to vscroll_range
	if(pos<0)
		pos = 0;
	else
	if(pos>vscroll_range)
		pos = vscroll_range;
	
	// If the current position does not change, do not scroll. 
	if (pos == vscroll_pos)
		return;
	
	// Reset the current scroll position.  
	vscroll_pos = pos;
	
	// Set the scroll bar.          
	SCROLLINFO si;
	si.cbSize = sizeof(si); 
	si.fMask  = SIF_POS;         
	si.nPos   = vscroll_pos; 
	SetScrollInfo(hwnd(), SB_VERT, &si, TRUE);      
	
	// Scroll the window. 
	if (num_children == 0) 
		return;
	children[0]->set_geometry(children[0]->x,-pos,
					children[0]->width,children[0]->height); 
}

//-------------------------------------------------------------------

void WIN_SCROLLEDWINDOW::resize()
	// The method resize is called every time a widget changes size
{         
	if (num_children == 0) 
		return;

	assert (children[0] != NULL, ("child window not valid"));

	// compute the visible width and height

	int child_width = children[0]->width;
	int child_height = children[0]->height;

	visible_width = width;
	visible_height = height;

	bool hscroll = visible_width<child_width;
	bool vscroll = visible_height<child_height;

	if(hscroll)
		visible_height = height - GetSystemMetrics(SM_CYHSCROLL);
	if(vscroll)
		visible_width = width - GetSystemMetrics(SM_CXVSCROLL);
		
	hscroll = visible_width<child_width;
	vscroll = visible_height<child_height;

	if(hscroll)
		visible_height = height - GetSystemMetrics(SM_CYHSCROLL);
	if(vscroll)
		visible_width = width - GetSystemMetrics(SM_CXVSCROLL);
/*
	-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
	two times should be good enought. no?
	-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
	hscroll = visible_width<child_width;
	vscroll = visible_height<child_height;

	if(hscroll)
		visible_height = height - GetSystemMetrics(SM_CYHSCROLL);
	if(vscroll)
		visible_width = width - GetSystemMetrics(SM_CXVSCROLL);
	-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
*/		
	// init the scroll bars info structure

	SCROLLINFO si;
	si.cbSize = sizeof(si);
	si.fMask  = SIF_RANGE | SIF_PAGE | SIF_POS;
	si.nMin   = 0;

	// The horizontal scrolling range is defined by 
	// (child_width) - (client_width). The current horizontal 
	// scroll value remains within the horizontal scrolling range.

	if(hscroll)
	{
		hscroll_range = child_width - visible_width;
		if(hscroll_pos>hscroll_range)
			hscroll_pos = hscroll_range;

		si.nMax   = child_width-1;
		si.nPage  = visible_width;
		si.nPos   = hscroll_pos;
	}
	else
	{
		hscroll_range = hscroll_pos = 0;
		si.nMax = si.nPage = si.nPos = 0;
	}
	SetScrollInfo(hwnd(), SB_HORZ, &si, TRUE);

	// The vertical scrolling range is defined by 
	// (child_height) - (client_height). The current vertical 
	// scroll value remains within the vertical scrolling range.

	if(vscroll)
	{
		vscroll_range = child_height - visible_height;
		if(vscroll_pos>vscroll_range)
			vscroll_pos = vscroll_range;

		si.nMax   = child_height-1;
		si.nPage  = visible_height;
		si.nPos   = vscroll_pos;
	}
	else
	{
		vscroll_range = vscroll_pos = 0;
		si.nMax = si.nPage = si.nPos = 0;
	}
	SetScrollInfo(hwnd(), SB_VERT, &si, TRUE);      

	// reposition the whildren
	children[0]->set_geometry(-hscroll_pos,-vscroll_pos,child_width,child_height);
}

