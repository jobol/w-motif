//-------------------------------------------------------------------
// WIN_SUBMENU.hxx
// ---------------
// define a class which defines the menus
// J.E.Bollo & G. Musset, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __win_submenu_hxx
#define __win_submenu_hxx
//-------------------------------------------------------------------
#include "WIN_MENUPOPUP.hxx"
//-------------------------------------------------------------------
class WIN_SUBMENU : public WIN_MENUPOPUP
{
	DECLARE_RESOURCE_CLIENT(WIN_SUBMENU)
		// use the resource mechanism

public:

	WIN_MENUITEM * menu_item;
		// a sub menu is also a menu item

protected:

	~WIN_SUBMENU();
		// destruction

public:

	WIN_SUBMENU();
		// construction

	void fill_menu_info_struct(MENUITEMINFO &menu_info) const;
		// fill the menu_info accordling to this

	virtual void set_resource_values(const RESOURCE_SCANNER & scanner);
		// put values of scanner, the currently scanned resources
		// use default and check if constraints should be checked

	virtual void set_text(const char* txt);
		// replaces all the text by "txt"

	virtual char* get_text() const;
		// gets all the text

	virtual void realize();
		// realise this and sub-widgets

	virtual void manage();
		// set this managed

	virtual void unmanage();
		// set this not managed

	virtual void map();
		// set this mapped

	virtual void unmap();
		// set this mapped

	virtual void set_name_and_parent(const char *name,WIN_COMPOSITE *parent);
		// store a widget name and its parent

	WIN_MENU * get_parent_menu() const
		// returns the parent option menu
		{ return (WIN_MENU*)get_parent_win(); }

	virtual void set_sensitive(bool value);
		// set attribute sensitive

	// --------------- Attributs management methods -------------------

	virtual int internal_get_values
		// function used internally to get attribut values 
		// should not be called directly (call get_values)
		// arglist is the array of the values to get
		// count is the count of values to be put into arglist
		// got is an array that flag if an attribut have been put
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs put
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		) const;

	virtual int internal_set_values
		// function used internally to set attribut values 
		// should not be called directly (call set_values)
		// arglist is the array of the values to be set,
		// count is the count of values in arglist
		// got is an array that flag if an attribut have been set
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs set
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		);

};
//-------------------------------------------------------------------
#endif
