//-------------------------------------------------------------------
// WIN_MENUBAR.hxx
// ---------------
// define a class that defines the menu bars
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __win_menubar_hxx
#define __win_menubar_hxx
//-------------------------------------------------------------------
#include "WIN_MENU.hxx"
//-------------------------------------------------------------------
class WIN_MENUBAR : public WIN_MENU
{
	DECLARE_RESOURCE_CLIENT(WIN_MENUBAR)
		// use the resource mechanism

protected:

	~WIN_MENUBAR();
		// destruction

public:

	WIN_MENUBAR();
		// construction

	virtual void redraw() const;
		// redraw the window

	void attach_menu(const WIN_CORE * to) const;
		// attach this menu to the window

	virtual void set_name_and_parent(const char *name,WIN_COMPOSITE *parent);
		// store a widget name and its parent

	virtual HMENU create_menu() const;
		// return a new menu handle for this

	virtual void map();
		// set this mapped

	virtual void unmap();
		// set this mapped
};
//-------------------------------------------------------------------
#endif
