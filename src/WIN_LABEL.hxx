//-------------------------------------------------------------------
// WIN_LABEL.hxx
// ---------------
// a class used to test the WINDOW system
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __win_label_hxx
#define __win_label_hxx
//-------------------------------------------------------------------
#include "macros.h"
#include "WIN_PRIMITIVE.hxx"
//-------------------------------------------------------------------
class WIN_LABEL : public WIN_PRIMITIVE
{
	DECLARE_RESOURCE_CLIENT(WIN_LABEL)
		// use the resource mechanism

	DECLARE_WINDOW(WIN_LABEL)
		// use window class mechanism

public:

	char * label_string;
		// the text to display

	char * accelerator;
		// the accelerator for action attached to a button

	char * mnemonic;
		// the mnemonic for action attached to a button

	char * label_pixmap;
		// name of the pixmap displayed in the label

	char * label_insensitive_pixmap;
		// name of the pixmap displayed in the label when inactive

	int label_type;
		// label type (XmSTRING or XmPIXMAP)

	int margin_width;
		// spacing between one side of the label text
		// and the nearest margin

	int margin_height;
		// spacing between one side of the label text
		// and the nearest margin

	int margin_top;
		// spacing between one side of the label text
		// and the nearest margin

	int margin_bottom;
		// spacing between one side of the label text
		// and the nearest margin

	int margin_left;
		// spacing between one side of the label text
		// and the nearest margin

	int margin_right;
		// spacing between one side of the label text
		// and the nearest margin

protected:

	~WIN_LABEL();
		// destruction

public:

	WIN_LABEL();
		// construction

	virtual LRESULT callback(UINT uMsg,WPARAM wParam,LPARAM lParam);
		// the "this" WINDOW callback

	virtual LONG get_windows_style() const;
		// retrieve the windows style used to create the widget 

	virtual bool create_control();
		// function called to create the embedded control
		// return true if creation process should continue, false otherwise

	virtual void set_text(const char* txt);
		// replaces all the text of the label by "txt"

	virtual char* get_text() const;
		// gets all the text

	virtual XtGeometryResult query_geometry (XtWidgetGeometry *request, 
											 XtWidgetGeometry *prefered);
		// The method query_geometry called when the parent  
		// wants to know the widget's prefered size


	// --------------- Attributs management methods -------------------

	virtual int internal_get_values
		// function used internally to get attribut values 
		// should not be called directly (call get_values)
		// arglist is the array of the values to get
		// count is the count of values to be put into arglist
		// got is an array that flag if an attribut have been put
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs put
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		) const;

	virtual int internal_set_values
		// function used internally to set attribut values 
		// should not be called directly (call set_values)
		// arglist is the array of the values to be set,
		// count is the count of values in arglist
		// got is an array that flag if an attribut have been set
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs set
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		);
};
//-------------------------------------------------------------------
#endif