//-------------------------------------------------------------------
// WIN_DRAWINGAREA.hxx
// -------------------
// implements a class which defines the Motif class "XmDrawingArea"
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __win_drawingarea_hxx
#define __win_drawingarea_hxx
//-------------------------------------------------------------------
#include "macros.h"
#include "WIN_BULLETIN_BOARD.hxx"
//-------------------------------------------------------------------
class WIN_DRAWINGAREA : public WIN_BULLETIN_BOARD
{
	DECLARE_RESOURCE_CLIENT(WIN_DRAWINGAREA)
		// use the resource mechanism

	DECLARE_WINDOW(WIN_DRAWINGAREA)
		// use window class mechanism

protected:

	~WIN_DRAWINGAREA();
		// destruction

public:

	WIN_DRAWINGAREA();
		// construction

};
//-------------------------------------------------------------------
#endif
