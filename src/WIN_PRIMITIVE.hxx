//-------------------------------------------------------------------
// WIN_PRIMITIVE.hxx
// ---------------
// implements a class which defines the Motif class "XmPrimitive"
// J.E.Bollo & G. Musset, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __win_primitive_hxx
#define __win_primitive_hxx
//-------------------------------------------------------------------
#include "macros.h"
#include "WIN_CORE.hxx"
//-------------------------------------------------------------------
class WIN_PRIMITIVE : public WIN_CORE
{
	DECLARE_VIRTUAL_RESOURCE_CLIENT(WIN_PRIMITIVE)
		// use the resource mechanism

public:

	HFONT font_list;
		// the font of the text to display
	
	HWND hwndCtl;
		// handle HWND of the child STATIC control

protected:

	~WIN_PRIMITIVE();
		// destruction

public:

	WIN_PRIMITIVE();
		// construction

	void set_font_list (HFONT fontlist);

	virtual void set_font_list (const char *font_list);
		// set the font_list attribute

	void set_sensitive(bool value);
		// set attribute sensitive

	virtual void redraw() const;
		// redraw the window

	virtual void set_focus() const;
		// set the focus to the window

	virtual LRESULT callback(UINT uMsg,WPARAM wParam,LPARAM lParam);
		// the "this" WINDOW callback

	virtual bool treat_the_message(MSG *msg);
		// returns true if window pre-treats the message and false otherwise
		// when it returns true the caller should not call neither
		// the TranslateMeassage nor the DispatchMessage calls
		
	virtual bool create_control() = 0;
		// function called to create the embedded control
		// return true if creation process should continue, false otherwise

	virtual bool create_window();
		// create a window

	virtual void resize();
		// The method resize called every time a widget changes size

	// --------------- Attributs management methods -------------------

	virtual int internal_get_values
		// function used internally to get attribut values 
		// should not be called directly (call get_values)
		// arglist is the array of the values to get
		// count is the count of values to be put into arglist
		// got is an array that flag if an attribut have been put
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs put
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		) const;

	virtual int internal_set_values
		// function used internally to set attribut values 
		// should not be called directly (call set_values)
		// arglist is the array of the values to be set,
		// count is the count of values in arglist
		// got is an array that flag if an attribut have been set
		// equality compare the attribut names with a predefined policy
		// it returns the count of attributs set
		(
			ARGLIST arglist,
			int     count,
			bool    *got,
			bool    (*equality)(const char*,const char*)
		);



};
//-------------------------------------------------------------------
#endif