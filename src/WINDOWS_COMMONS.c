//---------------------------------------------------------
// WINDOWS_COMMONS.c
// -----------------
// implements some commons functions
// J.E.Bollo, M3G (c) 1998
//---------------------------------------------------------
#include "macros.h"
#include "WINDOWS_COMMONS.h"
//---------------------------------------------------------
#ifdef __cplusplus
#	error "should be compiled in standard C"
#endif
//---------------------------------------------------------

int hex2bin(char digit)
	// convert a hexa digit to its value
{
	if(digit>='0' && digit<='9') return digit-'0';
	if(digit>='a' && digit<='f') return digit-'a'+10;
	if(digit>='A' && digit<='F') return digit-'A'+10;
	return 0;
}

//---------------------------------------------------------

int hex2bin2(const char *digits)
	// convert a 2 digits hexa string to a byte value
{
	return hex2bin(digits[0])*16 + hex2bin(digits[1]);
}

//---------------------------------------------------------

COLORREF text_to_color(const char *text)
	// this function convert a string that should represent
	// a color to its COLORREF value
	// unrecognized strings are converted to the 
	// medium gray RGB(128,128,128)
{
	int rouge = 128, vert = 128, bleu = 128;

	// only 2 cases are known

	if(text[0]=='#')
	{
		// strings starting with # are hexa values of kind...

		switch(strlen(text+1))
		{
		case 3: // ...#RGB

			rouge = hex2bin(text[1]) * 16;
			vert = hex2bin(text[2]) * 16;
			bleu = hex2bin(text[3]) * 16;
			break;

		case 6: // ...#RRGGBB

			rouge = hex2bin2(text+1);
			vert = hex2bin2(text+3);
			bleu = hex2bin2(text+5);
			break;

		case 12: // ...#RRRRGGGGBBBB

			rouge = hex2bin2(text+1);
			vert = hex2bin2(text+5);
			bleu = hex2bin2(text+9);
			break;
		}
	}
	else	{
		// the other strings are color given by name
		// the name is retrieved in a file
		// look at the X11 doc for the structure of this file

		char *env = getenv("CERVICAD");
		if (env != NULL)
			{
			FILE * pf;
			char * fic_rgb = (char *)malloc(1 + strlen(env) + strlen("\\Ressources\\Widgets\\rgb.txt"));
			sprintf(fic_rgb, "%s\\Ressources\\Widgets\\rgb.txt", env);
			pf = fopen(fic_rgb,"rt");
			free (fic_rgb);
			if(pf!=NULL)
				{
				int r, g, b;
				char buffer[1000];
				char nom[1000];
				while(fgets(buffer,1000,pf)!=NULL)
					{
					int	l = strlen (buffer);
					if (buffer[l-1] == '\n')
						buffer[l-1] = 0;
					if(sscanf(buffer," %d %d %d %s",&r,&g,&b,nom)==4)
						{
						char*	str = strstr (buffer, nom);
						if (!_stricmp(str,text))
							{
							rouge = r;
							vert = g;
							bleu = b;
							break;
							}
						}
					}
				fclose(pf);
				}
			}
		}

	// convert to COLORREF and return

	return RGB((BYTE)rouge,(BYTE)vert,(BYTE)bleu);
}

//---------------------------------------------------------

HFONT text_to_font(const char *text)
	// this function convert a string that should represent
	// a font to its HFONT value
	// unrecognized strings are converted to the 
	// default font
{
	LOGFONT log_font;
	int size ;

	// copy the text into a buffer because strtok modifies the text
	//-------------------------------------------------------------
	char * buf = strdup(text);

	// Retrieve the font name in the text and
	// store it the LOGFONT structure
	//----------------------------------------
    char * token = strtok(buf, "-");
	strncpy (log_font.lfFaceName, token!=NULL ? token : "", LF_FACESIZE);

	// Retrieve the font weight in the text and
	// store it the LOGFONT structure
	//-----------------------------------------
	token = strtok(NULL, "-");
	switch (token!=NULL ? token[0] : 'N')
	{
		case 'r' :
		case 'R' :	log_font.lfWeight = FW_REGULAR; break;
		case 'm' :
		case 'M' :	log_font.lfWeight = FW_MEDIUM; break;
		case 'b' :
		case 'B' :	log_font.lfWeight = FW_BOLD; break;
		case 'n' :
		case 'N' :	
		case '\0' :
		default :	log_font.lfWeight = FW_NORMAL; break;
	}

	// Retrieve the italic indicator in the text and
	// store it the LOGFONT structure
	//----------------------------------------------
	token = strtok(NULL, "-");
	switch (token!=NULL ? token[0] : 'N')
	{
		case 'i' :
		case 'I' :	
		case 'o' :
		case 'O' :	log_font.lfItalic = TRUE; break;
		default :	log_font.lfItalic = FALSE; break;
	}

	// Retrieve the font size in the text and
	// store it the LOGFONT structure
	//----------------------------------------
	token = strtok(NULL, "-");
	if (token==NULL || token[0]==0) 
		size = 12;
	else
		size = atoi(token);
	log_font.lfHeight = -size;

	free(buf);

	// set other fields of the LOGFONT structure to default values
	//------------------------------------------------------------
	log_font.lfWidth = 0;
	log_font.lfEscapement = 0;
	log_font.lfOrientation = 0;
	log_font.lfUnderline = FALSE;
	log_font.lfStrikeOut = FALSE;
	log_font.lfCharSet = ANSI_CHARSET;
	log_font.lfOutPrecision = OUT_DEFAULT_PRECIS;
	log_font.lfClipPrecision = CLIP_DEFAULT_PRECIS;
	log_font.lfQuality = PROOF_QUALITY;
	log_font.lfPitchAndFamily = DEFAULT_PITCH;

	return CreateFontIndirect(&log_font);
}

//---------------------------------------------------------

bool text_to_boolean(const char *text)
	// this function convert a string that should represent
	// a boolean to its boolean value
	// unrecognized strings are converted to boolean "false"
{
	if (stricmp ("true", text) == 0) return true;
	return false;
}

//---------------------------------------------------------

COLORREF enlight_color(COLORREF color,BYTE lux)
	// change the light of the color but retails the color value
	// returns the color multiplied by max(red,green,blue)*lux/255
{
	BYTE red   = GetRValue(color);
	BYTE green = GetGValue(color);
	BYTE blue  = GetBValue(color);

	BYTE maxi  = red>green ? red : green;
	if(maxi<blue)
		maxi = blue;

	if(maxi==0)
		return RGB(lux,lux,lux);

	if(lux>=128)
		lux = (BYTE)(maxi + ((int)(255 - maxi) * (int)(lux - 128)) / 128);
	else
		lux = (BYTE)(((int)(maxi) * (int)(lux)) / 128);

#	define __dolight(x) ((BYTE)((((int)lux)*((int)x))/((int)maxi)))
	red   = __dolight(red);
	green = __dolight(green);
	blue  = __dolight(blue);
#	undef __dolight

	return RGB(red,green,blue);	
}

//---------------------------------------------------------

char * add_carriage_return(const char *texte)
	// return a new string where the \n not preceded by a \r
	// become a \r\n
{
	char *q, *r;
	int len;
	int ncr;
	int etat;
	const char * p;
	register char c;

	// control
	if (texte==NULL)
		return NULL;

	// count the number of \n that are not preceeded by a \r
	// ncr is the number of \r to add
	len = ncr = etat = 0;
	p = texte;
	c = *p;
	while(c!=0)
	{
		if(c=='\n' && etat==0)
			ncr++;
		etat = c=='\r';
		len++;
		c = *++p;
	}

	// alloc the string q
	q = (char*)malloc(len+ncr+1);
	if(q!=NULL)
	{
		etat = 0;
		r = q;
		p = texte;
		c = *p;
		while (c!=0)
		{
			if(c=='\n' && etat==0)
				*r++='\r';
			*r++ = c;
			etat = c=='\r';
			c = *++p;
		}
		*r = 0;
	}
	return q;
}

//---------------------------------------------------------

void remove_carriage_return_inside(char *texte)
	// remove all the \r in string texte
{
	if (texte != NULL)
	{
		register char c = *texte;
		while (c!=0 && c!='\r')
			c = *texte++;
		if (c == '\r')
		{
			char * w = texte++;
			while ((*w = c = *texte++)!=0)
				if(c!='\r')
					w++;
		}
	}
}

//---------------------------------------------------------

char * remove_carriage_return(const char *texte)
	// return a new string where all the \r are removed
{
	int len; // at least the ending null
	const char * p;
	char *q, *r;
	register char c;

	// control
	if (texte == NULL)
		return NULL;

	// compute the length without \r
	len = 1; // at least the ending null
	p = texte;
	c = *p;
	while(c!=0)
	{
		if(c!='\r')
			len++;
		c = *++p;
	}

	// alloc the new string
	q = (char*)malloc(len);
	if(q!=NULL)
	{
		// copy without the \r
		r = q;
		p = texte;
		while((*r = c = *p++)!=0)
			if(c!='\r')
				r++;
	}
	return q;
}

//---------------------------------------------------------

bool text_to_accelerator(char *text,ACCEL *accel)
	// convert a text that represent a key to a ACCEL struct
	// return false if problem
	// ------------------------------------
	// keys are expressed using the following grammar
	//  key = (modif '+')* char char+
	//  modif = control | shift | alt
	//  control = ('c' | 'C') char*
	//  shift = ('s' | 'S') char*
	//  alt = ('a' | 'A') char*
	// ------------------------------------
	// sample:
	//   "Ctrl+Shift+ " is control shift space
	//   "c+s+ " is the same
	//   "++" is "+"
	// ------------------------------------
{
	int pos = 0;
	bool putkey = true;

	accel->fVirt = FNOINVERT;
	accel->cmd = 0;
	accel->key = 0;

	while(text[pos]!=0)
	{
		if(putkey)
		{
			accel->key = text[pos];
			putkey = false;
		}
		else if(text[pos]=='+')
		{
			putkey = true;
			switch(accel->key)
			{
			case 'a': case 'A': accel->fVirt |= FALT; break;
			case 's': case 'S': accel->fVirt |= FSHIFT; break;
			case 'c': case 'C': accel->fVirt |= FCONTROL; break;
			}
		}
		pos++;
	}
	return accel->key!=0 && !putkey;
}

//---------------------------------------------------------

int	multiline_text_extend(HDC hdc,const char *text,SIZE *size)
	// compute the size of a multiline text using 
	// GetTextExtentPoint32 that does not provide multiline support
	// that functions use the given hdc and provide the same
	//   result on \n or \r\n end-of-line markers.
{
	int	nl = 0;
	char char_M = 'M';
	SIZE sz;
	size->cx = size->cy = 0;
	if(text==NULL)
		return 0;
	nl = 1;
	while(*text!=0)
	{
		const char *ptr = text;
		int len;

		for(;;)
		{
			if(*ptr==0)
			{
				len = ptr - text;
				break;
			}

			if(ptr[0]=='\r' && ptr[1]=='\n')
			{
				len = ptr - text;
				ptr += 2;
				nl++;
				break;
			}

			if(*ptr=='\n')
			{
				len = ptr - text;
				ptr++;
				nl++;
				break;
			}

			ptr++;
		}

		if(len==0)
			GetTextExtentPoint32(hdc,&char_M,1,&sz);
		else
			GetTextExtentPoint32(hdc,text,len,&sz);

		size->cy += sz.cy;
		if(sz.cx > size->cx)
			size->cx = sz.cx;

		text = ptr;
	}
	return nl;
}

//---------------------------------------------------------

void window_multiline_text_extend(HWND window,const char *text,HFONT font,SIZE *size)
	// that function computes the size of a multiline text
	// it make a computing DC and then calls the function multiline_text_extend.
	// it provides the same result on \n or \r\n end-of-line markers.
{
	HDC hdc;
	HGDIOBJ obj;
	if (window == NULL)
		window = HWND_DESKTOP;
	hdc = GetDC (window);
	obj = SelectObject(hdc,font);
	multiline_text_extend(hdc,text,size);
	SelectObject(hdc,obj);
	ReleaseDC(window,hdc);
}

//---------------------------------------------------------

HBRUSH copy_of_brush (HBRUSH brush)
	// returns the handle of a copy of brush
{
	LOGBRUSH logbrush;

	if (brush==NULL || GetObject(brush, sizeof(logbrush), &logbrush)==0)
		return (HBRUSH) GetStockObject(NULL_BRUSH);
	
	return CreateBrushIndirect (&logbrush);
}

//---------------------------------------------------------

HFONT copy_of_font (HFONT font)
	// returns the handle of a copy of font
{
	LOGFONT logfont;

	if (font==NULL || GetObject(font, sizeof(logfont), &logfont)==0)
		return (HFONT)GetStockObject(SYSTEM_FONT);
	
	return CreateFontIndirect (&logfont);
}

//---------------------------------------------------------

void assign_gdi_obj (HGDIOBJ *to, HGDIOBJ from)
	// assign a gdi object with an other (of same kind perhaps)
	//  delete previous *to if not NULL
{
	assert(to!=NULL,("null pointer"));
	if (*to != NULL)
		DeleteObject (*to);
	*to = from;
}

//---------------------------------------------------------

void assign_copy_of_brush (HBRUSH *dest, HBRUSH brush)
	// copy brush to dest and delete the previous 
	//  *dest if not NULL
{
	assign_brush (dest, copy_of_brush (brush));
}

//---------------------------------------------------------

void assign_copy_of_font (HFONT *dest, HFONT font)
	// copy font to dest and delete the previous 
	//  *dest if not NULL
{
	assign_font (dest, copy_of_font (font));
}

//---------------------------------------------------------

COLORREF color_of_brush(HBRUSH brush)
	// returns the color of the brush
	// when the brush is not solid or brush if not valid the
	//   returned color is RGB(128,128,128)
{
	LOGBRUSH logbr;
	return (GetObject(brush,sizeof(logbr),&logbr)==0 || logbr.lbStyle!=BS_SOLID)
		? RGB(128,128,128)
		: logbr.lbColor;
}

//---------------------------------------------------------

static const char * ttta_next(const char *start, int *len)
	// that function is used by text_to_text_array (see below).
	// it scans the string start and return in *len its length up to '\n' or '\r\n' or '\0'.
	// it returns the pointer to the beginning of the next item.
{
	int l = 0;
	const char * stop = start;
	char c = *stop;
	while(c != 0 && c!='\n')
	{
		if (c!='\r' || stop[1]!='\n')
			l++;
		c = *++stop;
	}
	if(c=='\n')
		stop++;
	*len = l;
	return stop;
}

//---------------------------------------------------------

char ** text_to_text_table(const char *text)
	// expands a text compound of \n separated fields to
	//   an array of text.
	// the fields are copied and the array ends with a null.
{
	int nr_of_items = 0;
	char ** array;
	const char * p;
	const char * n;
	char * t;
	int len;
	int i;

	// compute the number of items
	if (text != NULL)
	{
		p = text;
		while (*p != 0)
		{
			nr_of_items++;
			p = ttta_next(p, &len);
		}
	}

	array = (char**)malloc((1+nr_of_items)*sizeof(char*));
	if (array != NULL)
	{
		if (text != NULL)
		{
			nr_of_items = 0;
			p = text;
			while (*p != 0)
			{
				n = ttta_next(p, &len);
				t = (char *)malloc(len+1);
				if (t!=NULL)
				{
					for(i=0;i<len;i++)
						t[i] = p[i];
					t[i] = 0;
				}
				array[nr_of_items] = t;
				nr_of_items++;
				p = n;
			}
		}
		array[nr_of_items] = NULL;
	}
	return array;
}

//---------------------------------------------------------

int text_table_count (char const * const * textbl)
	// returns the number of items in the text table textbl
{
	int len = 0;
	if (textbl != NULL)
	{
		while(textbl[len] != NULL)
			len++;
	}
	return len;
}

//---------------------------------------------------------

void free_text_table (char **textbl)
	// free the text table textbl 
{
	if (textbl != NULL)
	{
		int i = 0;
		while (textbl[i] != NULL)
			free (textbl[i++]);
		free (textbl);
	}
}

//---------------------------------------------------------

char ** copy_of_text_table (char const * const * textbl)
	// returns a copy of the text table
{
	int len = text_table_count (textbl);
	char ** p = (char**) malloc ((1+len)*sizeof(char*));
	if (p != NULL)
	{
		int i = 0;
		while (i < len)
		{
			p[i] = copy_of_text(textbl[i]);
			i++;
		}
		p[len] = NULL;
	}

	return p;
}

//---------------------------------------------------------

void assign_text_table (char *** to, char ** from)
	// assign to whith from. to is first deleted if not null.
{
	char ** p;
	assert(to!=NULL,("null pointer"));
	p = *to;
	if (p != NULL)
	{
		while(*p != NULL)
			free (*p++);
		free (*to);
	}
	*to = from;
}

//---------------------------------------------------------

void assign_copy_of_text_table (char *** to, char const * const * from)
	// assign to whith a copy of from. to is first deleted if not null.
{
	assign_text_table (to, copy_of_text_table(from));
}

//---------------------------------------------------------

int translate_char_pos_to_crlf (const char *txt, int pos)
	// translate the position of a character in txt to
	//   its position in add_carriage_return(txt)
{
	int p = 0;
	int etat = 0;
	while(pos>0 && *txt!=0)
	{
		if(*txt=='\n' && etat==0)
			p++;
		etat = *txt=='\r';
		p++;
		pos--;
	}
	return p;
}

//---------------------------------------------------------

int translate_char_pos_from_crlf (const char *txt, int pos)
	// translate the position of a character in add_carriage_return(txt)
	//   to its position in txt 
{
	int p = 0;
	int etat = 0;
	while(pos>0 && *txt!=0)
	{
		if(*txt=='\n' && etat==0)
			pos--;
		etat = *txt=='\r';
		if (pos>0)
			p++;
		pos--;
	}
	return p;
}

//---------------------------------------------------------

void insert_text_at (char **to, int pos, const char *txt)
	// insert into 'to' a copy of 'txt' at pos. pointer to may change.
{
	int len_to = strlen(*to);
	int len_txt = strlen(txt);
	int len = len_to + len_txt;
	char * p = (char*) realloc (*to, len + 1);
	if (p != NULL)
	{
		*to = p;
		if (pos<0)
			pos = 0;
		else
		if (pos>len_to)
			pos = len_to;

		p[len] = 0;
		while (pos < len_to)
			p[--len] = p[--len_to];

		while (len_txt > 0)
			p[--len] = txt[--len_txt];
	}
}

//---------------------------------------------------------

char * safe_copy_of_text(const char *text)
{
	char * sf_txt;
	sf_txt = safe_text(text);
	if (sf_txt[0] != '\0')
		return strdup(sf_txt);

	/* pour pallier le probleme de duplication des chaines vides sous Windows NT */
	sf_txt = malloc(1);
	if (sf_txt != NULL)
		sf_txt[0] = '\0';
	return sf_txt;
}

