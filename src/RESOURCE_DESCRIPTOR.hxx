//---------------------------------------------------------
// RESOURCE_DESCRIPTOR.hxx
// -----------------------
// polymorphic resource descriptor class
// J.E.Bollo, M3G (c) 1998
//---------------------------------------------------------
#ifndef __resource_descriptor_hxx
#define __resource_descriptor_hxx
//---------------------------------------------------------
#include "macros.h"
#include "RESOURCE_NAMES_SPACE.hxx"
#include "RESOURCE_CLIENT.hxx"
//---------------------------------------------------------
enum RESOURCE_TYPE
	// define the integer constants that represent the type
{
	NOTHING = 0,
		// not a resource

	COLOR,
		// resource of COLORREF kind

	TEXT,
		// resource of 'char *' kind

	FONT,
		// resource of HFONT kind

	BRUSH,
		// ressource of HBRUSH kind
	
	INTEGER,
		// ressource of int type
	
	BOOLEA,
		// ressource of bool type

	ENUM,
		// ressource of ENUM type

#if VERSION_OF_WINDOW >= 2

	TEXT_TABLE,
		// resource of 'char **' kind (array of strings ending with NULL)

#endif
};
//---------------------------------------------------------
typedef  RES_VAL   RESOURCE_CLIENT::*RES_VAL_ADR;
//---------------------------------------------------------
class RESOURCE_DESCRIPTOR
	// this class implements the description of the attribute resources
{
private:

	char * duplicate_string(const char *string) const
		// duplicate a string
	{
		return string==NULL ? NULL : strdup(string);
	}

	void remove_string(char *string) const
		// remove a string created (eventually) by duplicate string
	{
		if(string!=NULL) free(string);
	}

private:

	const char * string_name;
		// name of the resource

	NAME name;
		// unic identifier to name through names space

	RESOURCE_TYPE type;
		// the type of the resource

	RES_VAL_ADR address;
		// relative adress to a RESOURCE_CLIENT instance of the variant value

public:

	RESOURCE_DESCRIPTOR()
		// empty construction
	  : string_name(NULL)
	  , name()
	  , type(NOTHING)
	  , address(NULL)
	{}

	RESOURCE_DESCRIPTOR(const char *the_name,RESOURCE_TYPE the_type,
									RES_VAL_ADR	the_address)
		// complete construction
	  : string_name(the_name)
	  , name()
	  , type(the_type)
	  , address(the_address)
	{}

	void do_name()
		// do the name from the string_name
		{
			name = resource_names_space.add(string_name);
		}

	bool is_empty() const { return string_name==NULL; }
		// is the resource descriptor empty?

	NAME get_name() const { return name; }
		// return unic name

	static int text_to_enum(const char *text);
		// this function convert a string that should be
		// to an integer according to the enum values of win_namings

	void set_color(RESOURCE_CLIENT *client,COLORREF the_color) const;
		// set to a color

	void set_text(RESOURCE_CLIENT *client,const char *the_text) const;
		// set to a text

	void set_font(RESOURCE_CLIENT *client,HFONT the_hfont) const;
		// set to a font resource

	void set_hbrush(RESOURCE_CLIENT *client,HBRUSH the_hbrush) const;
		// set to a brush ressource

	void set_integer(RESOURCE_CLIENT *client,int the_integer) const;
		// set to an integer value

	void set_boolean(RESOURCE_CLIENT *client,bool the_boolean) const;
		// set to a boolean value

	void set_enum(RESOURCE_CLIENT *client,int the_enum) const;
		// set to a boolean value

#if VERSION_OF_WINDOW >= 2

	void set_text_table(RESOURCE_CLIENT *client,char **the_text_table) const;
		// set to a text table value

#endif

	void set_value(RESOURCE_CLIENT *client,const char *value) const;
		// abstract set from a string value with a type
};
//-------------------------------------------------------------------

#define RES_DESC_ATR(name,type,classe,member) \
			RESOURCE_DESCRIPTOR(name, type, \
					(RES_VAL_ADR)(RES_VAL classe::*)&classe::member)

#define RES_DESC_ATR_END \
			RESOURCE_DESCRIPTOR()

//-------------------------------------------------------------------
#endif
