//-------------------------------------------------------------------
// RESOURCE_CREATOR.cxx
// --------------------
// object used to create new entries in a RESOURCE_DATABASE
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#include "RESOURCE_NAMES_SPACE.hxx"
#include "RESOURCE_TREE.hxx"
#include "RESOURCE_DATABASE.hxx"
#include "RESOURCE_CREATOR.hxx"
//-------------------------------------------------------------------
RESOURCE_CREATOR::RESOURCE_CREATOR(RESOURCE_DATABASE &resources)
	// construction of the resource creator
  : state(CLEAR)
  , the_database(&resources)
  , current_resource(NULL)
{}
//-------------------------------------------------------------------
RESOURCE_CREATOR::~RESOURCE_CREATOR()
	// destruction: if the state is not clear previous allocated
	// resources are cleared
{
	if(state!=CLEAR) invalidate();
}
//-------------------------------------------------------------------
void RESOURCE_CREATOR::start_naming()
	// indicated the start transition
	// reset to the root of the database
	// state must be CLEAR, resulting state: NAMING
{
	assert(state==CLEAR,("can not start naming, check states"));
	current_resource = the_database->get_root();
	assert(current_resource!=NULL,("null root for resource database, abnormal"));
	state = NAMING;
}
//-------------------------------------------------------------------
void RESOURCE_CREATOR::add_wild_bind()
	// add a wild bind in the resource database
	// state must be NAMING
{
	assert(state==NAMING,("can not name, check states"));
	assert(current_resource!=NULL,("internal problem"));
	current_resource = current_resource->add_wild_bind_daughter();
	assert(current_resource!=NULL,("memory depletion"));
}
//-------------------------------------------------------------------
void RESOURCE_CREATOR::add_any_name()
	// add a the name any name in the resource database
	// state must be NAMING
{
	assert(state==NAMING,("can not name, check states"));
	assert(current_resource!=NULL,("internal problem"));
	current_resource = current_resource->add_any_name_daughter();
	assert(current_resource!=NULL,("memory depletion"));
}
//-------------------------------------------------------------------
void RESOURCE_CREATOR::add_name(const char * name)
	// add a name in the resource database
{
	assert(state==NAMING,("can not name, check states"));
	assert(current_resource!=NULL,("internal problem"));
	NAME the_name = resource_names_space.add(name);
	assert(the_name.is_valid(),("problem in naming"));
	current_resource = current_resource->add_named_daughter(the_name);
	assert(current_resource!=NULL,("memory depletion"));
}
//-------------------------------------------------------------------
void RESOURCE_CREATOR::end_naming()
	// indicate the end of naming and validate the set of a value
	// state must be NAMING, resulting state is VALUING
{
	assert(state==NAMING,("can not end naming, check states"));
	state = VALUING;
}
//-------------------------------------------------------------------
void RESOURCE_CREATOR::set_value(const char * value)
	// give a value to the current created database resource
	// state must be VALUING
{
	assert(state==VALUING,("can not start valuing, check states"));
	assert(current_resource!=NULL,("internal problem"));
	current_resource->set_value(value);
}
//-------------------------------------------------------------------
void RESOURCE_CREATOR::invalidate()
	// cancel the current resource creation if any
	// any state is allowed on entering, resulting state is CLEAR
{
	if(state!=CLEAR && current_resource!=NULL)
	{
		while(!current_resource->is_root())
		{
			RESOURCE_TREE * parent = current_resource->get_parent();
			// CAUTION here is a problem: the name is not removed from
			// the NAMES_SPACE
			current_resource->remove();
			current_resource = parent;
		}
	}
	state = CLEAR;
	current_resource = NULL;
}
//-------------------------------------------------------------------
void RESOURCE_CREATOR::validate()
	// indicate that the resource has been well entered and is valid
	// state must be VALUING, resulting state is CLEAR
{
	assert(state==VALUING,("can not validate, check states"));
	assert(current_resource!=NULL,("internal problem"));
	state = CLEAR;
	current_resource = NULL;
}
//-------------------------------------------------------------------
