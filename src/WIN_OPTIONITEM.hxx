//-------------------------------------------------------------------
// WIN_OPTIONITEM.hxx
// ---------------
// Class which implements the items of option menus
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#ifndef __win_optionitem_hxx
#define __win_optionitem_hxx
//-------------------------------------------------------------------
#include "macros.h"
#include "WIN_PUSHBUTTON.hxx"
//-------------------------------------------------------------------
class WIN_OPTIONMENU;
//-------------------------------------------------------------------
class WIN_OPTIONITEM : public WIN_PUSHBUTTON
{
	DECLARE_RESOURCE_CLIENT(WIN_OPTIONITEM)
		// use the resource mechanism

//	DECLARE_WINDOW(WIN_OPTIONITEM)
		// use window class mechanism

public:

	bool mapped;
		// is this mapped?

protected:

	~WIN_OPTIONITEM();
		// destruction

public:

	WIN_OPTIONITEM();
		// construction

	virtual bool create_window()
		// function called to create window
		// don't create window for menu items
		{ return true; }

	virtual void set_text(const char* txt);
		// replaces all the text by "txt"

	virtual char* get_text() const;
		// gets all the text

	virtual void redraw() const;
		// redraw the window

	virtual void set_name_and_parent(const char *name,WIN_COMPOSITE *parent);
		// store a widget name and its parent

	virtual void set_geometry(int x,int y,int width,int heigth) {}
		// apply unconditionnaly the requested geometry

	WIN_OPTIONMENU * get_option_menu() const
		// returns the parent option menu
		{ return (WIN_OPTIONMENU *)get_parent_win(); }

	virtual void set_sensitive(bool value);
		// set attribute sensitive

	virtual void manage();
		// set this managed

	virtual void unmanage();
		// set this not managed

	virtual void map();
		// set this mapped

	virtual void unmap();
		// set this mapped

	virtual void realize();
		// realise this

	virtual bool is_realized() const;
		// is this realized?

	virtual bool is_mapped() const
		// is this managed?
		{ return mapped; }

};
//-------------------------------------------------------------------
#endif
