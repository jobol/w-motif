//---------------------------------------------------------
// WINDOWS_COMMONS.h
// -------------------
// declare some commons functions
// J.E.Bollo, M3G (c) 1998
//---------------------------------------------------------
#ifndef __windows_commons_h
#define __windows_commons_h
//---------------------------------------------------------
#include "WIDGET_TYPES.h"
//---------------------------------------------------------
#ifdef __cplusplus
	extern "C" {
#endif
//---------------------------------------------------------

int hex2bin(char digit);
	// convert a hexa digit to its value

int hex2bin2(const char *digits);
	// convert a 2 digits hexa string to a byte value

COLORREF text_to_color(const char *text);
	// this function convert a string that should represent
	// a color to its COLORREF value
	// unrecognized strings are converted to the 
	// medium gray RGB(128,128,128)

HFONT text_to_font(const char *text);
	// this function convert a string that should represent
	// a font to its HFONT value
	// unrecognized strings are converted to the 
	// default font

bool text_to_boolean(const char *text);
	// this function convert a string that should represent
	// a boolean to its boolean value
	// unrecognized strings are converted to boolean "false"

COLORREF enlight_color(COLORREF color,BYTE lux);
	// change the light of the color but retails the color value
	// returns the color multiplied by max(red,green,blue)*lux/255

char * add_carriage_return(const char *texte);
	// return a new string where the \n not preceded by a \r
	// become a \r\n

void remove_carriage_return_inside(char *texte);
	// remove all the \r in string texte

char * remove_carriage_return(const char *texte);
	// return a new string where all the \r are removed

bool text_to_accelerator(char *text,ACCEL *accel);
	// convert a text that represent a key to a ACCEL struct
	// return false if the text can not be translated
	// ------------------------------------
	// keys are expressed using the following grammar
	//  key = (modif '+')* char char+
	//  modif = control | shift | alt
	//  control = ('c' | 'C') char*
	//  shift = ('s' | 'S') char*
	//  alt = ('a' | 'A') char*
	// ------------------------------------
	// sample:
	//   "Ctrl+Shift+ " is control shift space
	//   "c+s+ " is the same
	//   "++" is "+"
	// ------------------------------------

int multiline_text_extend(HDC hdc,const char *text,SIZE *size);
	// compute the size of a multiline text using 
	// GetTextExtentPoint32 that does not provide multiline support
	// that functions use the given hdc and provide the same
	//   result on \n or \r\n end-of-line markers.

void window_multiline_text_extend(HWND window,const char *text,HFONT font,SIZE *size);
	// that function computes the size of a multiline text
	// it make a computing DC and then calls the function multiline_text_extend.
	// it provides the same result on \n or \r\n end-of-line markers.

HBRUSH copy_of_brush (HBRUSH brush);
	// returns the handle of a copy of brush

void assign_copy_of_brush (HBRUSH *dest, HBRUSH brush);
	// copy brush to dest and delete the previous 
	//  *dest if not NULL

HFONT copy_of_font (HFONT font);
	// returns the handle of a copy of font

void assign_copy_of_font (HFONT *dest, HFONT font);
	// copy font to dest and delete the previous 
	//  *dest if not NULL

COLORREF color_of_brush(HBRUSH brush);
	// returns the color of the brush
	// when the brush is not solid or brush if not valid the
	//   returned color is RGB(128,128,128)

void assign_gdi_obj (HGDIOBJ *to, HGDIOBJ from);
	// assign a gdi object with an other (of same kind perhaps)
	//  delete previous *to if not NULL

int text_table_count (char const * const * textbl);
	// returns the number of items in the text table textbl

char ** text_to_text_table (const char *text);
	// expands a text compound of \n separated fields to
	//   an array of text.
	// the fields are copied and the array ends with a null.

void assign_copy_of_text_table (char *** to, char const * const * from);
	// assign to whith a copy of from. to is first deleted if not null.

char ** copy_of_text_table (char const * const * textbl);
	// returns a copy of the text table textbl

void assign_text_table (char *** to, char ** from);
	// assign to whith from. to is first deleted if not null.

void free_text_table (char * * textbl);
	// free the text table textbl 

int translate_char_pos_to_crlf (const char *txt, int pos);
	// translate the position of a character in txt to
	//   its position in add_carriage_return(txt)

int translate_char_pos_from_crlf (const char *txt, int pos);
	// translate the position of a character in add_carriage_return(txt)
	//   to its position in txt 

void insert_text_at (char **to, int pos, const char *txt);
	// insert into 'to' a copy of 'txt' at pos. pointer to may change.

char * safe_copy_of_text(const char *text);
//---------------------------------------------------------
// a commenter rapido

#define assign_brush(to,from)					assign_gdi_obj(to,from)
#define assign_font(to,from)					assign_gdi_obj(to,from)

#define copy_of_text(text)						((text)==NULL ? NULL : strdup(text))
#define assign_copy_of_text(dest,text)			do{if(*(dest)!=NULL)free(*(dest));*(dest)=copy_of_text(text);}while(0)
#define safe_text(text)							((text)==NULL ? "" : (text))
/*#define safe_copy_of_text(text)					(strdup(safe_text(text))) */
#define safe_assign_copy_of_text(dest,text)		do{if(*(dest)!=NULL)free(*(dest));*(dest)=safe_copy_of_text(text);}while(0)

//---------------------------------------------------------
#ifdef __cplusplus
	}
#endif
//---------------------------------------------------------
#endif