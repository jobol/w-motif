//-------------------------------------------------------------------
// WIN_OPTIONMENU.cxx
// ---------------
// implements a class which defines the Motif widget "XmOptionMenu"
// J.E.Bollo & G. Musset, M3G (c) 1998
//-------------------------------------------------------------------
#include "WIN_OPTIONMENU.hxx"
#include "WINDOWS_MANAGER.hxx"
#include "win_naming.h"
#include "WINDOWS_COMMONS.h"
//-------------------------------------------------------------------
BEGIN_RES_DESC(WIN_OPTIONMENU,WIN_COMPOSITE,NAME_OF_WIN_OPTIONMENU_CLASS)
	RES_DESC_ATR(XmNfontList,	FONT,	WIN_OPTIONMENU, font_list),
END_RES_DESC
//-------------------------------------------------------------------
IMPLEMENT_WINDOW(WIN_OPTIONMENU)
//-------------------------------------------------------------------
ATOM WIN_OPTIONMENU::register_window_class() const
{
	WNDCLASSEX wce;
	WINDOWS_MANAGER::get_window_class_info("COMBOBOX",&wce);
	return WINDOWS_MANAGER::register_window_class(
					(const char*)get_class_name(),wce.style);
}
//-------------------------------------------------------------------

WIN_OPTIONMENU::WIN_OPTIONMENU()
 : WIN_COMPOSITE()
 , font_list(NULL)
 , hwndCtl(NULL)
{
}

//-------------------------------------------------------------------
WIN_OPTIONMENU::~WIN_OPTIONMENU()
{
	if(font_list!=NULL)
		DeleteObject(font_list);
}

//-------------------------------------------------------------------

LONG WIN_OPTIONMENU::get_windows_style() const
	// retrieve the windows style used to create the widget 
{
	DWORD style = WS_CHILD | WS_VISIBLE | CBS_AUTOHSCROLL 
				| CBS_DROPDOWNLIST | CBS_NOINTEGRALHEIGHT | CBS_DISABLENOSCROLL ; 

	return style;
}

//---------------------------------------------------------

bool WIN_OPTIONMENU::create_window()
{
	if(!WIN_CORE::create_window(WS_CHILD|WS_CLIPCHILDREN|WS_VISIBLE))
		return false;
	if(!create_control())
		return false;
	if(hwndCtl!=NULL)
		SendMessage(hwndCtl,WM_SETFONT, (WPARAM)font_list, TRUE);
	return true;
}
//-------------------------------------------------------------------

bool WIN_OPTIONMENU::create_control()
{
				
	// create the COMBOBOX window
	//--------------------------
	hwndCtl = CreateWindowEx(
#ifdef WITH_BORDERS
					WS_EX_CLIENTEDGE,
#else
					0,
#endif
					"COMBOBOX",
					"",
					get_windows_style(),
					0, 0,width,2000,
					this->hwnd(),
					(HMENU)1,
					WINDOWS_MANAGER::get_application_instance(),
					NULL);
	if (hwndCtl==NULL) return false;
	return true;
}
//---------------------------------------------------------

LRESULT WIN_OPTIONMENU::callback(UINT uMsg,WPARAM wParam,LPARAM lParam)
{
	switch(uMsg)
	{
	case WM_SETFONT:
		if(hwndCtl!=NULL)
			SendMessage(hwndCtl,uMsg,wParam,lParam);
		return 0;

	case WM_COMMAND:
		{
			if(HIWORD(wParam)==CBN_SELCHANGE)
			{
				LRESULT sel = SendMessage(hwndCtl,CB_GETCURSEL,0,0);
				if(sel!=CB_ERR)
					get_item_of_index(sel)->activate();
			}
		}
		return 0;

	case WM_CTLCOLORLISTBOX:
	case WM_CTLCOLOREDIT:
		{
			HDC hdc = (HDC)wParam;
			SetBkColor(hdc, color_of_brush(background));
			SetTextColor(hdc, foreground);
			return (LRESULT)background;
		}
	}

	return WIN_COMPOSITE::callback(uMsg,wParam,lParam);
}

//-------------------------------------------------------------------

void WIN_OPTIONMENU::set_font_list (HFONT fontlist)
	// set the font_list attribute
{
	assign_copy_of_font (&font_list, fontlist);
	if (hwnd() != NULL)
		SendMessage(hwnd(),WM_SETFONT, (WPARAM)font_list, TRUE);
	update_geometry_request(CWWidth|CWHeight);
}

//-------------------------------------------------------------------

void WIN_OPTIONMENU::set_font_list (const char *fontlist)
	// set the font_list attribute
{
	HFONT font = text_to_font(fontlist);
	set_font_list (font);
	if (font != NULL)
		DeleteObject(font);
}

//---------------------------------------------------------

void WIN_OPTIONMENU::redraw() const
{
	if (hwndCtl != NULL)
		WINDOWS_MANAGER::redraw_window(hwndCtl);
}

//-------------------------------------------------------------------

void WIN_OPTIONMENU::set_sensitive(bool value)
	// set attribute sensitive
{
	if(sensitive != value)
	{
//		WIN_COMPOSITE::set_sensitive(value);
		if (hwndCtl != NULL)
			EnableWindow(hwndCtl, sensitive);
	}
}

//-------------------------------------------------------------------

void WIN_OPTIONMENU::resize()
	//set the size to the embedded control hwndCtl
{
	if(hwndCtl != NULL)
		WINDOWS_MANAGER::move_window(hwndCtl, 0, 0, width, height);
}

//-------------------------------------------------------------------

int WIN_OPTIONMENU::internal_get_values
	// function used internally to get attribut values 
	// should not be called directly (call get_values)
	// arglist is the array of the values to get
	// count is the count of values to be put into arglist
	// got is an array that flag if an attribut have been put
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs put
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) const
{
	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.
#if VERSION_OF_WINDOW >= 2

		if(equality(attribut,XmNfontList))
			arglist[index].value.pvalue->hfont = font_list;

#else

		if(equality(attribut,XmNfontList))
		{
			if(font_list==NULL)
				arglist[index].value.pvalue->hfont = NULL;
			else
			{
				LOGFONT logfont;
				GetObject(font_list,sizeof logfont,&logfont);
				arglist[index].value.pvalue->hfont = 
							CreateFontIndirect(&logfont);
			}
		}

#endif
/*
		else
		if(equality(attribut,XmNmenuHistory))
		{
		}
*/
		else
			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
	}

	// return total count of found attributs (including ancestors)
	return found_count + WIN_COMPOSITE::internal_get_values(arglist,count,got,equality);
}

//-------------------------------------------------------------------


int WIN_OPTIONMENU::internal_set_values
	// function used internally to set attribut values 
	// should not be called directly (call set_values)
	// arglist is the array of the values to be set,
	// count is the count of values in arglist
	// got is an array that flag if an attribut have been set
	// equality compare the attribut names with a predefined policy
	// it returns the count of attributs set
(
	ARGLIST arglist,
	int     count,
	bool    *got,
	bool    (*equality)(const char*,const char*)
) 
{
	// loop on arglist
	int found_count = 0;
	int index = count;
	while(index>0)
	{
		if(got[--index])
			// skip when already done
			continue;

		bool found = true;
		const char * attribut = arglist[index].name;

		// is it one of our attibuts? yes. so treat it.
		if(equality(attribut,XmNfontList))
			set_font_list(arglist[index].value.hfont);
		else
			found = false;

		// update found_count and done flags
		if(found)
		{
			found_count++;
			got[index] = true;
		}
	}

	// return total count of found attributs (including ancestors)
	return found_count  + WIN_COMPOSITE::internal_set_values(arglist,count,got,equality);
}

//-------------------------------------------------------------------

void WIN_OPTIONMENU::insert_child (WIN_CORE * child)
	// insert a child in the children array
{
	assert(child!=NULL,("invalid parameter"));
	if(!child->is_kind_of(WIN_OPTIONITEM::the_resource_class()))
		return;
	assert(child->is_kind_of(WIN_OPTIONITEM::the_resource_class()),
		("invalid child type"));

	WIN_COMPOSITE::insert_child (child);

	if(hwndCtl!=NULL)
		child->map();
}

//-------------------------------------------------------------------

void WIN_OPTIONMENU::delete_child (WIN_CORE * child)
	// delete a child in the children array
{
	if(hwndCtl!=NULL)
		child->unmap();
	WIN_COMPOSITE::delete_child (child);
}

//-------------------------------------------------------------------

int WIN_OPTIONMENU::get_index_of_item(const WIN_OPTIONITEM * item) const
	// get the combox index of the item
{
	assert(item->mapped,("item should be mapped"));
	int i, j;
	for(j=i=0; i<num_children ; i++)
		if(children[i]==(WIN_CORE *)item)
			return j;
		else
		if(children[i]->is_mapped())
			j++;
	error(("should not occur"));
	return 0;
}

//-------------------------------------------------------------------

WIN_OPTIONITEM * WIN_OPTIONMENU::get_item_of_index(int index) const
	// get the item for an index in the combox
{
	int i, j;
	for(j=i=0; i<num_children ; i++)
		if(children[i]->is_mapped())
		{
			if(j==index)
				return (WIN_OPTIONITEM *)children[i];
			j++;
		}
	error(("should not occur"));
	return NULL;
}

//-------------------------------------------------------------------

void WIN_OPTIONMENU::update_map_item(WIN_OPTIONITEM * item,bool map)
	// change the mapping for item of this
{
	if(item->mapped == map)
		return;
	if(hwndCtl==NULL)
	{
		item->mapped = map;
		return;
	}
	if(map)
	{
		item->mapped = map;
		int index = get_index_of_item(item);
		LRESULT count = SendMessage(hwndCtl,CB_GETCOUNT,0,0);
		if(count==CB_ERR || count<=index)
			index = -1;
		SendMessage(hwndCtl,CB_INSERTSTRING,index,(LPARAM)item->label_string);
		if(count==0)
		{
			SendMessage(hwndCtl,CB_SETCURSEL,0,0);
			item->activate();
		}
	}
	else
	{
		int index = get_index_of_item(item);
		item->mapped = map;
		SendMessage(hwndCtl,CB_DELETESTRING,index,0);
	}
}

//-------------------------------------------------------------------

void WIN_OPTIONMENU::update_string_item(const WIN_OPTIONITEM * item)
	// change the string for item of this
{
	if(hwndCtl!=NULL && item->mapped)
	{
		LRESULT sel = SendMessage(hwndCtl,CB_GETCURSEL,0,0);
		int index = get_index_of_item(item);
		SendMessage(hwndCtl,CB_DELETESTRING,index,0);
		LRESULT count = SendMessage(hwndCtl,CB_GETCOUNT,0,0);
		if(count==CB_ERR || count<=index)
			index = -1;
		SendMessage(hwndCtl,CB_INSERTSTRING,index,(LPARAM)item->label_string);
		if(sel!=CB_ERR)
			SendMessage(hwndCtl,CB_SETCURSEL,sel,0);
	}
}
//-------------------------------------------------------------------

void WIN_OPTIONMENU::update_manage_item(WIN_OPTIONITEM * item,bool manage)
	// change the management for item of this
{
}

//-------------------------------------------------------------------

void WIN_OPTIONMENU::update_sensitive_item(WIN_OPTIONITEM * item,bool sensitive)
	// change the sensitivity for item of this
{
}

//-------------------------------------------------------------------

XtGeometryResult WIN_OPTIONMENU::query_geometry (XtWidgetGeometry *request, 
										 XtWidgetGeometry *prefered)
	// The method query_geometry called when the parent  
	// wants to know the widget's prefered size
{
	SIZE size;
	HWND hwndRef = hwndCtl==NULL ? HWND_DESKTOP : hwndCtl;
	HDC hdc = GetDC(hwndRef);
	HGDIOBJ obj = SelectObject(hdc,font_list);

	int i;
	for(size.cx=size.cy=i=0; i<num_children ; i++)
	{
//		if(children[i]->is_mapped())
//		{
			WIN_OPTIONITEM * child = (WIN_OPTIONITEM *)children[i];
			char * text = child->label_string;
			if(text!=NULL)
			{
				SIZE sz;
				int len = strlen(text);
				GetTextExtentPoint32(hdc,text,len,&sz);
				if(sz.cx>size.cx)
					size.cx = sz.cx;
				if(sz.cy>size.cy)
					size.cy = sz.cy;
			}
//		}
	}
	SelectObject(hdc,obj);
	ReleaseDC(hwndRef,hdc);

	if(hwndCtl!=NULL)
	{
		int field_height = SendMessage(hwndCtl,CB_GETITEMHEIGHT,-1,0);
		if(size.cy < field_height)
			size.cy = field_height;
	}
	else
		size.cy += 2;

#ifdef WITH_BORDERS
	size.cy += 3*GetSystemMetrics(SM_CYEDGE);
#endif

#ifdef WITH_BORDERS
	size.cx += 2*GetSystemMetrics(SM_CXEDGE);
#endif
	size.cx += GetSystemMetrics(SM_CXVSCROLL);

	XtWidgetGeometry pref;
	pref.request_mode = CWHeight|CWWidth;
	pref.height = size.cy;
	pref.width = size.cx;

	return complete_query_geometry(request,prefered,&pref);
}

//-------------------------------------------------------------------

