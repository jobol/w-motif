//-------------------------------------------------------------------
// WIN_MENUITEM.cxx
// ---------------
// Class which implements the items of menus
// J.E.Bollo, M3G (c) 1998
//-------------------------------------------------------------------
#include "WIN_MENUITEM.hxx"
#include "WIN_MENU.hxx"
#include "win_naming.h"
#include "WINDOWS_MANAGER.hxx"
//-------------------------------------------------------------------
BEGIN_RES_DESC(WIN_MENUITEM,WIN_TOGGLEBUTTON,NAME_OF_WIN_MENUITEM_CLASS)
END_RES_DESC
//-------------------------------------------------------------------
WIN_MENUITEM::WIN_MENUITEM()
  : WIN_TOGGLEBUTTON()
  , mapped(false)
{
	item_id = WINDOWS_MANAGER::add_item(this);
	assert(item_id!=0,("unrecorded item"));
}
//-------------------------------------------------------------------
WIN_MENUITEM::~WIN_MENUITEM()
{
	WINDOWS_MANAGER::remove_item_by_id(item_id);
}
//-------------------------------------------------------------------
LRESULT WIN_MENUITEM::callback(UINT uMsg,WPARAM wParam,LPARAM lParam)
{
	if(uMsg==WM_COMMAND)
		activate();
	return 0;
}
//-------------------------------------------------------------------
void WIN_MENUITEM::activate()
	// primitive activate callback
{
	activate_callbacks(XmNactivateCallback);
}
//-------------------------------------------------------------------
void WIN_MENUITEM::set_text(const char* txt)
	// replaces all the text by "txt"
{
	WIN_LABEL::set_text(txt);
	if(get_parent_menu()!=NULL)
		get_parent_menu()->update_item(this);
}
//-------------------------------------------------------------------
char* WIN_MENUITEM::get_text() const
	// gets all the text
{
	return WIN_LABEL::get_text();
}
//-------------------------------------------------------------------
void WIN_MENUITEM::redraw() const
	// redraw the window
{
	if(get_parent_menu()!=NULL)
		get_parent_menu()->redraw();
}
//-------------------------------------------------------------------
void WIN_MENUITEM::fill_menu_info_struct(MENUITEMINFO &menu_info) const
	// fill the menu_info accordling to this
{
	menu_info.fMask = MIIM_ID|MIIM_STATE|MIIM_TYPE;
	menu_info.fState = sensitive ? MFS_ENABLED : MFS_GRAYED;
	menu_info.fState |= set ? MFS_CHECKED : MFS_UNCHECKED;
	menu_info.wID = item_id;
	menu_info.fType = MFT_SEPARATOR;
	if(label_string!=NULL)
	{
		menu_info.dwTypeData = label_string;
		menu_info.cch = strlen(label_string);
		if(menu_info.cch != 0)
			menu_info.fType = MFT_STRING;
	}
	if(indicator_type==XmONE_OF_MANY)
		menu_info.fType |= MFT_RADIOCHECK;
}
//-------------------------------------------------------------------
void WIN_MENUITEM::set_name_and_parent(const char *name,WIN_COMPOSITE *parent)
	// store a widget name and its parent
{
	assert(parent!=NULL,("no parent"));
	assert(parent->is_kind_of(WIN_MENU::the_resource_class()),("bad parent type"));
	WIN_CORE::set_name_and_parent(name,parent);
}
//-------------------------------------------------------------------
void WIN_MENUITEM::set_sensitive(bool value)
	// set attribute sensitive
{
	if(get_parent_menu()!=NULL)
		get_parent_menu()->update_sensitive_item(this,value);
}
//-------------------------------------------------------------------
void WIN_MENUITEM::manage()
	// set this managed
{
	if(get_parent_menu()!=NULL)
		get_parent_menu()->update_manage_item(this,true);
}
//-------------------------------------------------------------------
void WIN_MENUITEM::unmanage()
	// set this not managed
{
	if(get_parent_menu()!=NULL)
		get_parent_menu()->update_manage_item(this,false);
}
//-------------------------------------------------------------------
void WIN_MENUITEM::map()
	// set this mapped
{
	if(get_parent_menu()!=NULL)
		get_parent_menu()->update_map_item(this,true);
}
//-------------------------------------------------------------------
void WIN_MENUITEM::unmap()
	// set this mapped
{
	if(get_parent_menu()!=NULL)
		get_parent_menu()->update_map_item(this,false);
}
//-------------------------------------------------------------------
void WIN_MENUITEM::realize()
	// realise this
{
	map();
}
//-------------------------------------------------------------------
bool WIN_MENUITEM::is_realized() const
	// is this realized?
{
	return get_parent_menu()->is_realized();
}
//-------------------------------------------------------------------
void WIN_MENUITEM::set_state(bool state)
	// set state of the Toggle (checked or unchecked)
{
	if(get_state()!=state)
		change_state();
}
//-------------------------------------------------------------------
void WIN_MENUITEM::change_state()
	// change state of the Toggle (checked or unchecked)
{
	set = !set;
	if(get_parent_menu()!=NULL)
		get_parent_menu()->update_item(this);
}
//-------------------------------------------------------------------

